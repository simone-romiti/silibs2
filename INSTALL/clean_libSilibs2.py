# clean_libSilibs2.py
# thisw script removes the old links to the Silib2 shared library

file="/etc/ld.so.conf.d/custom.conf"

F0 = open(file, "r") # open the file for reading
L0 = F0.readlines() # saving all the lines of file
F0.close() # closing the file

L1=[] # list of lines of the cleaned file
for l0 in L0: # loop over all lines
    if not ("Silibs2" in l0): # ignoring old Silibs2 occurrencies
        L1.append(l0) # appending lines of other libraries

F1 = open(file, "w") # opening from writing
for l1 in L1: # loop over new lines
    F1.write(l1) # writing the lines
    