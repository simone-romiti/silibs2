#!/bin/bash

echo "Installing the libSilibs2.so shared library"

out=/etc/ld.so.conf.d/custom.conf

printf "Cleaning onld Silibs2 installation residuals\n"
sudo python ./clean_libSilibs2.py

touch $out # creating the file if not existing
printf "Appending the following text to the file:\n" 
printf $out "\n"


printf "\n----------\n"
	printf "# Silibs2 shared library\n" | sudo tee -a $out
	printf "/home/simone/Documents/Silibs2/\n" | sudo tee -a $out
printf "\n----------\n\n"

sudo ldconfig # create file links and update the cache

printf "Verify The library has been added. Check the output below.\n"
ldconfig -p | grep "libSilibs2.so" # verify it's been added 

echo "Done."

