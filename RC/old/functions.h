// functions.h
/*! Utilities to interact with the table class objects */

#include "../Functions/functions.hpp"
#include "./table.h"

namespace Silibs2 {
namespace RC {
#ifndef Silibs2_RC_functions_h
#define Silibs2_RC_functions_h
using namespace Silibs2::Functions;

#endif
} // namespace RC
} // namespace Silibs2
