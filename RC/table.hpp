// table.hpp
/*! Definitions for table.h */

#include "../Tables/table.hpp"
#include "./table.h"

namespace Silibs2 {
namespace RC {
#ifndef Silibs2_RC_table_hpp
#define Silibs2_RC_table_hpp
using namespace Functions;

template <class U> table<U>::table() {}
template <class U> table<U>::~table() {}

template <class U> void table<U>::check_sizes() const {
  try {
    if (CONTENT.rows() != (int)VEC_R_NAMES.size()) {
      std::string er = "Error : (rows of content) != (number of rows names).\n";
      throw er;
    }
    if (CONTENT.cols() != (int)VEC_C_NAMES.size()) {
      std::string ec = "(cols of content) != (number of cols names).\n";
      throw ec;
    }
  } catch (const std::string &e) {
    std::cerr << e;
    std::cerr << "Recall that names must be distinct.\n";
    std::cerr << "Aborting.\n";
    abort();
  }
} // check_sizes()

template <class U>
table<U>::table(const Tables::table<U> &t, const std::vector<std::string> &r,
                const std::vector<std::string> &c) {
  (*this).CONTENT = t;     //!< same elements
  (*this).VEC_R_NAMES = r; //!< same row names
  (*this).VEC_C_NAMES = c; //!< same col names

  std::map<std::string, int> r_names;
  std::map<std::string, int> c_names;
  const int nr = r.size();
  for (int i = 0; i < nr; ++i) {
    r_names.insert(std::make_pair(r[i], i));
  }
  const int nc = c.size();
  for (int j = 0; j < nc; ++j) {
    c_names.insert(std::make_pair(c[j], j));
  }
  (*this).R_NAMES = r_names; //!< same rows names
  (*this).C_NAMES = c_names; //!< same columns names
  this->check_sizes();
} // table()

template <class U> table<U>::table(const Tables::table<U> &t) {
  (*this).CONTENT = t; //!< same elements

  int nr = t.rows();

  for (int i = 0; i < nr; ++i) {
    const std::string row_name = "row " + std::to_string(i);
    (*this).VEC_R_NAMES.push_back(row_name);
    (*this).R_NAMES.insert(std::make_pair(row_name, i));
  }
  int nc = t.cols();
  for (int j = 0; j < nc; ++j) {
    const std::string col_name = "col " + std::to_string(j);
    (*this).VEC_C_NAMES.push_back(col_name);
    (*this).C_NAMES.insert(std::make_pair(col_name, j));
  }
} // table()

template <class U> table<U>::table(const RC::table<U> &m) { (*this) = m; }

template <class U>
table<U>::table(const int &ro, const int &co,
                const std::vector<std::string> &R_names,
                const std::vector<std::string> &C_names) {
  Tables::table<U> t(ro, co);
  (*this) = t;
  this->set_r_names(R_names);
  this->set_c_names(C_names);
} // table()

template <class U> table<U>::table(const int &ro, const int &co) {
  Tables::table<U> t(ro, co);
  (*this) = t;
}

template <class U> table<U>::table(const int &ro, const int &co, const U &val) {
  Tables::table<U> t(ro, co, val); // all elements equal to 'val'
  (*this) = t;
}

template <class U>
table<U>::table(const std::string &path, const std::string &head) {
  (*this) = RC::read_table<U>(path, head);
} // table()

template <class U>
void table<U>::set_r_names(const std::vector<std::string> &R) {
  const int n1 = (int)R.size();
  const int n2 = CONTENT.rows();
  if (n1 == n2) {
    (*this).VEC_R_NAMES = R; //!< same row names
    std::map<std::string, int> r_names;
    const int nr = R.size();
    for (int i = 0; i < nr; ++i) {
      r_names.insert(std::make_pair(R[i], i));
    }
    (*this).R_NAMES = r_names; //!< same rows names
    this->check_sizes();
  } else {
    std::cerr
        << "Error. You tried to set the rows names of an RC::table, but:\n";
    std::cerr << "Number of names : " << n1 << "\n";
    std::cerr << "Number of rows of the content : " << n2 << "\n";
    abort();
  }
}

template <class U> std::vector<std::string> table<U>::r_names() const {
  return (*this).VEC_R_NAMES;
}

template <class U>
void table<U>::set_c_names(const std::vector<std::string> &C) {
  const int n1 = (int)C.size();
  const int n2 = CONTENT.cols();
  if (n1 == n2) {
    (*this).VEC_C_NAMES = C; //!< same col names
    std::map<std::string, int> c_names;
    const int nc = C.size();
    for (int j = 0; j < nc; ++j) {
      c_names.insert(std::make_pair(C[j], j));
    }
    (*this).C_NAMES = c_names; //!< same columns names
    this->check_sizes();
  } else {
    std::cerr
        << "Error. You tried to set the column names of an RC::table, but:\n";
    std::cerr << "Number of names : " << n1 << "\n";
    std::cerr << "Number of columns of the content : " << n2 << "\n";
    abort();
  }
}

template <class U> std::vector<std::string> table<U>::c_names() const {
  return (*this).VEC_C_NAMES;
} // c_names()

template <class U> std::vector<U> table<U>::row(const std::string &name) const {
  return CONTENT.row(R_NAMES.at(name));
} // row()

template <class U> std::vector<U> table<U>::col(const std::string &name) const {
  return CONTENT.col(C_NAMES.at(name));
} // col()

template <class U> Tables::table<U> table<U>::content() const {
  return CONTENT;
} // content()

template <class U>
std::pair<int, int> table<U>::content_ij(const std::string &r,
                                         const std::string &c) const {
  auto err_msg = [](const std::string &name, const std::string &type) -> void {
    std::cerr << "Error: \'" << name << "\' isn't a " << type << " name.\n";
    std::cerr << "Aborting.\n";
    abort();
  };

  if (R_NAMES.count(r) == 0) {
    err_msg(r, "row");
  }
  if (C_NAMES.count(c) == 0) {
    err_msg(c, "column");
  }

  this->check_sizes();
  const int i = R_NAMES.at(r);
  const int j = C_NAMES.at(c);
  return std::make_pair(i, j);
} // content()

template <class U>
const U &table<U>::operator()(const std::string &r,
                              const std::string &c) const {
  const std::pair<int, int> ij = this->content_ij(r, c);
  const int i = ij.first;
  const int j = ij.second;
  return (*this).CONTENT(i, j);
} // operator()

template <class U>
U &table<U>::operator()(const std::string &r, const std::string &c) {
  const std::pair<int, int> ij = this->content_ij(r, c);
  const int i = ij.first;
  const int j = ij.second;
  return (*this).CONTENT(i, j);
} // operator()

template <class U>
std::string table<U>::to_string(const std::string &sep, const int &prec,
                                const std::string &header,
                                const std::string &upper_left) const {
  this->check_sizes();
  const int R = CONTENT.rows(), C = CONTENT.cols();

  std::stringstream ss;
  ss.precision(prec);
  ss.setf(std::ios_base::scientific);
  if (header != "") {
    ss << header << "\n";
  }
  ss << upper_left << sep;
  std::vector<std::string> rn = this->r_names();
  std::vector<std::string> cn = this->c_names();
  for (int j = 0; j < C; ++j) {
    ss << cn[j] << sep;
  }
  ss << "\n";
  for (int i = 0; i < R; ++i) {
    ss << rn[i] << sep;
    for (int j = 0; j < C; ++j) {
      ss << CONTENT(i, j) << sep;
    }
    ss << "\n";
  }
  return ss.str();
} // operator()

template <class U>
void table<U>::print(const std::string &file, const std::string &sep,
                     const int &prec, const std::string &header,
                     const std::string &upper_left) const {
  const std::string output_str = this->to_string(sep, prec, header, upper_left);
  // print the RC::table to the file
  Functions::string_to_file(output_str, file);
} // operator()

/* --------- */
/* functions */
/* --------- */

template <class U>
RC::table<U> read_table(const std::string &path, const std::string &head) {

  const Tables::table<std::string> t_str =
      Tables::read_table<std::string>(path, head, 0);

  const int nr = t_str.rows() - 1;
  const int nc = t_str.cols() - 1;
  const Tables::table<U> CONTENT =
      Tables::cast_table<U>(t_str.block(1, 1, nr, nc));

  /*! Rows names , which are on the 0th column of the read table */
  const std::vector<std::string> r_all = t_str.col(0);

  /*! r_all[0] isn't the name of a row */
  std::vector<std::string> VEC_R_NAMES(r_all.begin() + 1, r_all.end());

  /*! Cols names , which are on the 0th row of the read table */
  const std::vector<std::string> c_all = t_str.row(0); /*!< cols names  */
  /*! c_all[0] isn't the name of a column */
  std::vector<std::string> VEC_C_NAMES(c_all.begin() + 1, c_all.end());

  const RC::table<U> rc_tab(CONTENT, VEC_R_NAMES, VEC_C_NAMES);
  return rc_tab;
} // read_table()

#endif
} // namespace RC
} // namespace Silibs2
