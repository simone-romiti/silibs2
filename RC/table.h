// table.h
/*!
This header declares the class RC::table, a table with named rows and columns.
This is intended to be a class useful for retrieving informations about data in
an input file.
 */

#include <map>

#include "../Tables/table.h"

namespace Silibs2 {
namespace RC {
#ifndef Silibs2_RC_table_h
#define Silibs2_RC_table_h

template <class U = double> class table {
protected:
  std::vector<std::string> VEC_R_NAMES; /*! names of rows */
  std::vector<std::string> VEC_C_NAMES; /*! names of cols */
  std::map<std::string, int> R_NAMES;   /*! names of rows */
  std::map<std::string, int> C_NAMES;   /*! names of cols */
  Tables::table<U> CONTENT;             /*! elements in the table */

public:
  table();
  ~table();

  /*!
  check if the number of rows and columns names are the same as the number of
  rows and cols of the content
  */
  void check_sizes() const;

  /*! constructor */
  // table<U>(const Tables::table<U> &t, const std::map<std::string, int> &r,
  //          const std::map<std::string, int> &c);

  table<U>(const Tables::table<U> &t, const std::vector<std::string> &r,
           const std::vector<std::string> &c);

  table<U>(const Tables::table<U> &t); /*! Copy contructor */
  table<U>(const table<U> &table);     /*! Copy contructor */

  /* Create a RC::table of size ro*co */
  table<U>(const int &ro, const int &co);

  /* Create a RC::table of size ro*co specifying the names of rows and columns*/
  table<U>(const int &ro, const int &co,
           const std::vector<std::string> &R_names,
           const std::vector<std::string> &C_names);

  /* Create a RC::table of size ro*co with all elements equal to val */
  table<U>(const int &ro, const int &co, const U &val);

  /* Create a RC::table reading its elements from the file in 'path' */
  table<U>(const std::string &path, const std::string &head = "");

  void set_r_names(const std::vector<std::string> &R); /*! set rows names */
  std::vector<std::string> r_names() const;            /*! rows names */

  void set_c_names(const std::vector<std::string> &C); /*! set cols names */
  std::vector<std::string> c_names() const;            /*! cols names */

  std::vector<U> row(const std::string &name) const; /*!< row named "name" */
  std::vector<U> col(const std::string &name) const; /*!< col named "name" */

  /*! values inside (i.e. without the names of rows and columns) */
  Tables::table<U> content() const;

  /*! CONTENT indices given the name of row and column */
  std::pair<int, int> content_ij(const std::string &r,
                                 const std::string &c) const;

  /*! access elements by identifiers */
  const U &operator()(const std::string &r, const std::string &c) const;
  U &operator()(const std::string &r, const std::string &c);

  /*!
  converts the RC::table to a std::string for future printing.
  upper_left is an optional string that is printed on to of the rows names.
  If empty, 'sep' is printed in place of it.
  */
  std::string to_string(const std::string &sep = "\t", const int &prec = 16,
                        const std::string &header = "",
                        const std::string &upper_left = "") const;

  /*! prints on file with column separator 'sep' and precision 'prec'*/
  void print(const std::string &file, const std::string &sep = "\t",
             const int &prec = 16, const std::string &header = "",
             const std::string &upper_left = "") const;
};

/*!
Read a table, ignoring the 00 element, and storing the results in a RC::table.
---
Example:
#head
Names   one    two   three
A       1.2    5.7   0.2
B       1e-2   5.3   8.81
---
This functions returns an RC::table with content given by the 4 numbers above,
with {"A":0, "B":1} as rows names
and {"one":0, "two":1, "three":2} as cols names.
*/
template <class U>
RC::table<U> read_table(const std::string &path, const std::string &head = "");

#endif
} // namespace RC
} // namespace Silibs2
