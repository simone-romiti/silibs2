// fit.h
/*! All fitting libraries */

#include "./base.h" /*!< base class */

// #include "./1D/fit_xexy.hpp"   /*!< error on the 'x' only */
#include "./1D/fit_xexyey.hpp" /*!< error on 'x' and 'y' */
#include "./1D/fit_xyey.hpp"   /*!< error on the 'y' only */

#include "./nD/fit_xexyey.hpp" /*!< error on 'x' and 'y' */
