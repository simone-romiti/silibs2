# Fit with Minuit2

In this folder are implemented the basic routines to perform non-linear fit using the least squares minimization.

## How to include the routines

All headers are included inside the global one _fit.hpp_ , simply give:
``` c++
#include /path/to/here/fit.hpp
```

## File structure

In this folder it's implemented a class which serves as a frontend for the FCNBase class of Minuit2. It's scope is only to provide something that can be initialized from  an std::function .

There are also 2 folders:

* **1D/** , containing the routines for 1-dimensional fits, i.e. fits of a function of 1 variable.

* **nD/** , containing the routines for 1-dimensional fits, i.e. fits of a function of 1 variable.