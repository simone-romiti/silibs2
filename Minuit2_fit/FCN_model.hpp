// FCN_model.hpp

#include "FCN_model.h"

namespace Silibs2 {
namespace Minuit2_fit {
#ifndef Silibs2_Minuit2_FCN_model_hpp
#define Silibs2_Minuit2_FCN_model_hpp

/* ---------------------------------------- */
/* Methods of the FCN_model class           */
/* ---------------------------------------- */

template <class U> FCN_model<U>::FCN_model() {}
template <class U> FCN_model<U>::~FCN_model() {}

template <class U>
FCN_model<U>::FCN_model(const std::function<U(const std::vector<U> &)> &fcn) {
  FCN_function = fcn;
}

template <class U>
U FCN_model<U>::operator()(const std::vector<U> &pars) const {
  return FCN_function(pars);
}

template <class U> U FCN_model<U>::Up() const { return 1.0; }

/* ------------------------------ */
/* Routines for FCN_model objects */
/* ------------------------------ */

/*! Returns an FCN_model object with residue function given by 'fcn' */
template <class U>
FCN_model<U> to_FCN_model(const std::function<U(const std::vector<U> &)> &fcn) {
  FCN_model<U> the_FCN(fcn); /*!< creating the FCN_model object */
  return the_FCN;
} // to_FCN_model()

int get_dof(const int &n_pts, const int &n_pars, const bool &correct) {
  int n_dof = n_pts - n_pars;
  if (n_dof < 0) {
    std::cerr << "Error: The degrees of freedom for the fit cannot be "
                 "negative. Aborting."
              << '\n';
    abort();
  } else if (n_dof == 0 && correct) {
    // std::clog << "Warning: 0 degrees of freedom for the chi squared. Setting
    // "
    //              "it equal to 1."
    //           << '\n';
    n_dof = 1;
  }
  return n_dof;
} // get_dof()

#endif
} // namespace Minuit2_fit
} // namespace Silibs2
