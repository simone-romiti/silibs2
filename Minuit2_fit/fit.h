// fit.h
/*! All fitting libraries */

#include "./base.h" /*!< base class */

// #include "./1D/fit_xexy.h"   /*!< error on the 'x' only */

#include "./1D/fit_xexyey.h" /*!< error on 'x' and 'y' */
#include "./1D/fit_xyey.h"   /*!< error on the 'y' only */

#include "./nD/fit_xexyey.h" /*!< error on 'x' and 'y' */
