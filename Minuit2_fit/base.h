// base.h
/*
This file declares a base class for chi-squared minimization fits
using Minuit2.
*/
#ifndef Silibs2_Minuit2_fit_base_h
#define Silibs2_Minuit2_fit_base_h

#include <functional>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

/* Minuit2 libraries */
#include "Minuit2/FCNBase.h"
#include "Minuit2/MnScan.h"

namespace Silibs2 {
namespace Minuit2_fit {

template <class U = double> class base {

protected:
  /*
  control variables :
  true when the corresponding initialization has been done
  */
  bool pars_init = false; /* parameters: guesses, names, bounds */

  std::vector<std::string> NAMES; /*!< Names of the parameters */
  std::vector<U> GUESS;           /*!< Guess values of the parameters */
  std::vector<U> BOUND_L;         /*!< Left bounds for the parameters. */
  std::vector<U> BOUND_R;         /*!< Right bounds for the parameters. */

  int n_dof = 0.0; //!< number of degrees of freedom
  bool n_dof_maually_set =
      false; // true when the d.o.f. have been specified manually

  bool fit_converged = false; //!< true if fit converged
  bool verbose = false;       //!< when true prints info on std::cout

public:
  base();

  /*
  set the names and guesses of the parameters of the function.
  call_by_overload = true --> the function has been called by its overloading,
  so that bounds will be imposed in the end.
  */
  void set_pars(
      const std::vector<std::string> &names, /*!< Names of the parameters */
      const std::vector<U> &guesses, /*!< Guess values of the parameters */
      const bool &call_by_overload = false);

  /* set the parameters of the function */
  void set_pars(
      const std::vector<std::string> &names, /*!< Names of the parameters */
      const std::vector<U> &guesses,  /*!< Guess values of the parameters */
      const std::vector<U> &bounds_l, /*!< Left bounds for the parameters. */
      const std::vector<U> &bounds_r  /*!< Right bounds for the parameters. */
  );

  void set_verbose(const bool &v); // (*this).verbose = v

  /*! checks if pars_init is true */
  void check_pars_init() const;

  //! set the number of degrees of freedom
  void set_n_dof_manually(const int &n);
  int get_n_dof() const;

  int get_n_pars() const; /*!< number of parameters */

  bool fit_did_converge() const;
};

} // namespace Minuit2_fit
} // namespace Silibs2
#endif
