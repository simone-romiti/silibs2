// fit_Minuit_v2.hpp
/*
This file defines what is declared in fit_Minuit2.h
*/

#include "./base_fit.hpp"
#include "./fit_xexyey.h"

namespace Silibs2 {
namespace Minuit2_fit {
#ifndef Silibs2_Minuit2_fit_1D_fit_xexyey_hpp
#define Silibs2_Minuit2_fit_1D_fit_xexyey_hpp

template <class U> fit_xexyey<U>::fit_xexyey() {}  // end default contructor
template <class U> fit_xexyey<U>::~fit_xexyey() {} // end default contructor

template <class U> void fit_xexyey<U>::set_n_sigma_Bayes(const double &nsB) {
  (*this).n_sigma_Bayes = nsB;
}

template <class U>
void fit_xexyey<U>::set_data(const std::vector<U> &x, const std::vector<U> &ex,
                             const std::vector<U> &y,
                             const std::vector<U> &ey) {
  if (x.size() != ex.size() || x.size() != y.size() || y.size() != ey.size()) {
    std::cerr << "Invalid data. The number of points and errors doesn't match."
              << "\n";
    std::cerr << "x.size()  : " << x.size() << "\n";
    std::cerr << "ex.size() : " << ex.size() << "\n";
    std::cerr << "y.size()  : " << y.size() << "\n";
    std::cerr << "ey.size() : " << ey.size() << "\n";
    std::cerr << "Aborting."
              << "\n";
    abort();
  }
  vec_x = x;
  vec_y = y;
  vec_ex = ex;
  vec_ey = ey;
  (*this).data_init = true;
} // set_data()

template <class U> void fit_xexyey<U>::set_n_dof_auto() {
  this->check_data_init(); /*!< data initialization */
  this->check_pars_init(); /*!< parameters initialization */

  if (!(*this).n_dof_maually_set) { // if n_dof_maually_set = false ...
    const int n_pts = (int)vec_x.size() + (int)vec_y.size();
    const int n_pars = this->get_n_pars();
    (*this).n_dof = get_dof(n_pts, n_pars);
  }
}

template <class U>
void fit_xexyey<U>::set_ansatz(
    const std::function<U(const U &, const std::vector<U> &)> &ansatz) {
  this->set_n_dof_auto(); // check also data and parameters

  (*this).ANSATZ = ansatz;

  /*! automatic deduction of the CH2_RED_FUN function */
  (*this).CH2_RED_FUN = ch2_red_ansatz_xexyey((*this).ANSATZ, vec_x, vec_ex,
                                              vec_y, vec_ey, (*this).n_dof);
  (*this).residue_init = true;
} // set_ansatz()

template <class U> void fit_xexyey<U>::fit() {
  this->check_fit_init();
  std::vector<U> guess = (*this).GUESS;
  std::vector<U> bound_l = (*this).BOUND_L;
  std::vector<U> bound_r = (*this).BOUND_R;

  /*! Set the Minuit2 parameters */
  if ((*this).verbose) {
    std::cout.precision(8);
    std::cout.setf(std::ios_base::scientific);
    std::cout << "\nMinuit2 minimization:\n"
              << "\n";
    std::cout << "par\t||\tName\t||\tvalue\t\t||\tlimits"
              << "\n";
  }

  ROOT::Minuit2::MnUserParameters upar;
  const int n_pars = guess.size();
  for (int i = 0; i < n_pars; i++) {
    U err_i = fabs(guess[i]) / 100.0; // O(1%) relative error
    if (bound_l[i] == bound_r[i]) {   /* unbounded parameter */
      upar.Add((*this).NAMES[i], guess[i], err_i);
    } else { /* bounded parameter */
      upar.Add((*this).NAMES[i], guess[i], err_i, bound_l[i], bound_r[i]);
    }
    if ((*this).verbose) {
      std::cout << i << "\t||\t" << (*this).NAMES[i] << "\t||\t" << guess[i]
                << "\t||\t" << bound_l[i] << "\t---\t" << bound_r[i] << '\n';
    }
  }

  /*! xB_i parameters varying inside an interval of 2*sigma_x */
  int n_pts = vec_x.size();
  const int nsB = (*this).n_sigma_Bayes;
  for (int b = 0; b < n_pts; b++) {
    std::string xB_name = "xB_" + std::to_string(b);
    U xB_guess = vec_x[b];
    U err_b = fabs(vec_ex[b]);
    U xB_low = xB_guess - nsB * vec_ex[b], xB_up = xB_guess + nsB * vec_ex[b];
    upar.Add(xB_name, xB_guess, err_b, xB_low, xB_up);
    if ((*this).verbose) {
      std::cout << b << "\t||\t" << xB_name << "\t||\t" << xB_guess << "\t||\t"
                << xB_low << "\t---\t" << xB_up << '\n';
    }
  }

  /*! do the fit */
  FCN_model<U> rsd = to_FCN_model<U>((*this).CH2_RED_FUN);
  // ROOT::Minuit2::MnScan scan(rsd, upar);
  ROOT::Minuit2::MnMigrad migrad(rsd, upar);

  ROOT::Minuit2::FunctionMinimum min = migrad();
  (*this).fit_converged = min.IsValid();

  std::stringstream Minuit2_log; // log of the minimization
  Minuit2_log << min;            // filling the log
  const std::string Minuit2_log_str =
      Minuit2_log.str(); // converting to std::string

  const std::string eU = "Parameter is at Upper limit"; // error "Upper"
  const std::string eL = "Parameter is at Lower limit"; // error "Lower"
  const bool reached_limit = (Minuit2_log_str.find(eU) != std::string::npos) ||
                             (Minuit2_log_str.find(eL) != std::string::npos);
  /* outputs Minuit2 log when:
      - the minimum is invalid, or
      - at least one parameter has reached its upper or lower limit
  */
  if (reached_limit || !(*this).fit_converged) {
    std::cout << Minuit2_log_str;
  }

  /*! set PARS and CH2_RED */
  ROOT::Minuit2::MnUserParameterState state =
      min.UserState(); /*!< state AFTER the minimization */
  std::vector<U> best_fit_params =
      state.Params(); /*!< all parameters AFTER the minimization */

  /* Parameters of the anzatz (i.e. not including the "Bayesian" parameters
   * xB_i)*/
  (*this).PARS.resize(n_pars);
  for (int i = 0; i < n_pars; ++i) {
    (*this).PARS[i] = best_fit_params[i];
  }

  /*!< xB_i parameters obtained with the Bayesian procedure */
  x_Bayes.resize(n_pts);
  for (int b = 0; b < n_pts; ++b) {
    x_Bayes[b] = best_fit_params[n_pars + b];
  }

  (*this).CH2_RED = min.Fval();
  (*this).fit_done = true;

  /*! Theoretical values for the fitted points */
  vec_y_th.resize(n_pts);
  for (int i = 0; i < n_pts; ++i) {
    vec_y_th[i] = (*this).ANSATZ(vec_x[i], (*this).PARS);
  }

} // fit()

template <class U> std::vector<U> fit_xexyey<U>::get_vec_y_th() {
  return (*this).vec_y_th;
} // get_vec_y_th()

#endif
} // namespace Minuit2_fit
} // namespace Silibs2
