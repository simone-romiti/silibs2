// fit_Minuit_v2.hpp
/*! This file defines what is declared in fit_Minuit2.h */

#include "./base_fit.hpp"
#include "./fit_xyey.h"

namespace Silibs2 {
namespace Minuit2_fit {
#ifndef Silibs2_Minuit2_fit_1D_fit_xyey_hpp
#define Silibs2_Minuit2_fit_1D_fit_xyey_hpp

template <class U> fit_xyey<U>::fit_xyey() {}  // end default contructor
template <class U> fit_xyey<U>::~fit_xyey() {} // end default contructor

template <class U>
void fit_xyey<U>::set_data(const std::vector<U> &x, const std::vector<U> &y,
                           const std::vector<U> &ey) {
  (*this).vec_x = x;
  (*this).vec_y = y;
  (*this).vec_ey = ey;
  (*this).data_init = true;
} // set_data()

template <class U> void fit_xyey<U>::set_n_dof_auto() {
  this->check_data_init(); /*!< data initialization */
  this->check_pars_init(); /*!< parameters initialization */

  if (!(*this).n_dof_maually_set) { // if n_dof_maually_set = false ...
    const int n_pts = (int)vec_y.size();
    const int n_pars = this->get_n_pars();
    (*this).n_dof = get_dof(n_pts, n_pars);
  }
} // set_n_dof_auto()

template <class U>
void fit_xyey<U>::set_ansatz(
    const std::function<U(const U &, const std::vector<U> &)> &ansatz) {
  this->set_n_dof_auto();

  (*this).ANSATZ = ansatz; /*!< setting the ansatz */

  /*! automatic deduction of the CH2_RED_FUN function */
  (*this).CH2_RED_FUN =
      ch2_red_ansatz_xyey((*this).ANSATZ, vec_x, vec_y, vec_ey, (*this).n_dof);

  (*this).residue_init = true;
} // end set_ansatz()

template <class U> void fit_xyey<U>::fit() {

  this->check_fit_init();

  std::vector<U> guess = (*this).GUESS;
  std::vector<U> bound_l = (*this).BOUND_L;
  std::vector<U> bound_r = (*this).BOUND_R;

  /* set the Minuit2 parameters */
  ROOT::Minuit2::MnUserParameters upar;
  int n_pars = (int)guess.size();
  for (int i = 0; i < n_pars; i++) {
    U err_i = fabs(guess[i]) / 1e+3; // O(1e-3) relative error
    if (bound_l[i] == bound_r[i]) {  /* unbounded parameter */
      upar.Add((*this).NAMES[i], guess[i], err_i);
    } else { /* bounded parameter */
      upar.Add((*this).NAMES[i], guess[i], err_i, bound_l[i], bound_r[i]);
    }
  }

  /* do the fit */
  if ((*this).verbose) {
    std::cout << "\nMinuit2 minimization:\n"
              << "\n";
    std::cout << "par\t||\tName\t||\tvalue\t||\tlimits"
              << "\n";
    for (int i = 0; i < n_pars; i++) {
      std::cout << i << "\t||\t" << (*this).NAMES[i] << "\t||\t" << guess[i]
                << "\t||\t" << bound_l[i] << "\t---\t" << bound_r[i] << '\n';
    }
  }

  FCN_model<U> rsd = to_FCN_model<U>((*this).CH2_RED_FUN);
  ROOT::Minuit2::MnMigrad migrad(rsd, upar);

  ROOT::Minuit2::FunctionMinimum min = migrad();
  (*this).fit_converged = min.IsValid();

  std::stringstream Minuit2_log; // log of the minimization
  Minuit2_log << min;            // filling the log
  const std::string Minuit2_log_str =
      Minuit2_log.str(); // converting to std::string

  const std::string eU = "Parameter is at Upper limit"; // error "Upper"
  const std::string eL = "Parameter is at Lower limit"; // error "Lower"
  const bool reached_limit = (Minuit2_log_str.find(eU) != std::string::npos) ||
                             (Minuit2_log_str.find(eL) != std::string::npos);
  /* outputs Minuit2 log when:
      - the minimum is invalid, or
      - at least one parameter has reached its upper or lower limit
  */
  if (reached_limit || !(*this).fit_converged) {
    std::cerr << Minuit2_log_str;
  }

  ROOT::Minuit2::MnUserParameterState state =
      min.UserState();           /*!< state AFTER the minimization */
  (*this).PARS = state.Params(); /*!< parameters AFTER the minimization */

  /* set PARS and CH2_RED */
  (*this).CH2_RED = min.Fval();
  (*this).fit_done = true;

  /*! Theoretical values for the fitted points */
  int n_pts = ((*this).vec_x).size();
  vec_y_th.resize(n_pts);
  for (int i = 0; i < n_pts; ++i) {
    vec_y_th[i] = (*this).ANSATZ(vec_x[i], (*this).PARS);
  }
}

template <class U> std::vector<U> fit_xyey<U>::get_vec_y_th() {
  return (*this).vec_y_th;
} // get_vec_y_th()

#endif
} // namespace Minuit2_fit
} // namespace Silibs2
