// fit_Minuit_v2.hpp
/*
This file defines what is declared in fit_Minuit2.h
*/

/*! base class for 1D ansatz fitting */
#include "../FCN_model.hpp"
#include "../base.hpp"

#include "./base_fit.h"

namespace Silibs2 {
namespace Minuit2_fit {
#ifndef Silibs2_Minuit2_fit_1D_base_fit_hpp
#define Silibs2_Minuit2_fit_1D_base_fit_hpp

template <class U> base_fit<U>::base_fit() {}  // end default contructor
template <class U> base_fit<U>::~base_fit() {} // end default contructor

template <class U> void base_fit<U>::check_data_init() {
  if (!(*this).data_init) {
    std::cerr << "Error: you haven't passed the data yet.\n"
              << "(Recall for instance that the ansatz must be passed after "
                 "the data, so "
                 "that the redidue function can be deduced)\n"
              << "aborting." << '\n';
    abort();
  }
}

template <class U>
void base_fit<U>::set_residue(
    const std::function<U(const std::vector<U> &pars)> &res) {
  CH2_RED_FUN = res;
  residue_init = true;
  data_init = true;
}

template <class U> void base_fit<U>::check_residue_init() {
  if (!residue_init) {
    std::cerr << "Error: residue function wasn't specified. Check if you "
                 "passed it correctly or, if you gave the ansatz, if you "
                 "provided also the data. Aborting."
              << '\n';
    abort();
  }
} // check_residue_init()

template <class U> void base_fit<U>::check_fit_init() {
  this->check_residue_init();
  this->check_pars_init();
}

template <class U> void base_fit<U>::check_fit_done() {
  if (!fit_done) {
    std::cerr << "Error: you haven't done the fit yet. Aborting." << '\n';
    abort();
  }
}

template <class U> std::vector<U> base_fit<U>::get_PARS() {
  this->check_fit_done();
  return PARS;
}

template <class U> U base_fit<U>::get_CH2_RED() {
  this->check_fit_done();
  return CH2_RED;
}

/* ----------------------------------- */
/* Automatic deduction of ch2 function */
/* ----------------------------------- */

/*! ---- */
/*! xyey */
/*! ---- */

template <class U>
void check_xyey(const std::vector<U> &x, const std::vector<U> &y,
                const std::vector<U> &ey) {
  if (!(x.size() == y.size() && y.size() == ey.size())) {
    std::cerr << "Error: x, y, ey vectors must have the same sizes." << '\n';
    std::cerr << "x size: " << x.size() << '\n';
    std::cerr << "y size: " << y.size() << '\n';
    std::cerr << "ey size: " << ey.size() << '\n';
    std::cerr << "Aborting." << '\n';
    abort();
  }
}

template <class U>
U ch2_red_ansatz_xyey_value(
    const std::function<U(const U &, const std::vector<U> &)> &ansatz,
    const std::vector<U> &x, const std::vector<U> &y, const std::vector<U> &ey,
    const std::vector<U> &pars, const int &n_dof) {

  U ch2_value = 0.0;
  int n_pts = x.size();
  // int n_dof = get_dof(n_pts, (int)pars.size());

  for (int i = 0; i < n_pts; i++) {
    ch2_value += pow((y[i] - ansatz(x[i], pars)) / ey[i], 2);
  }
  return ch2_value / (U)n_dof;
} // ch2_red_ansatz_xyey_value()

template <class U>
std::function<U(const std::vector<U> &pars)> ch2_red_ansatz_xyey(
    const std::function<U(const U &, const std::vector<U> &)> &ansatz,
    const std::vector<U> &x, const std::vector<U> &y, const std::vector<U> &ey,
    const int &n_dof) {

  check_xyey<U>(x, y, ey);
  std::function<U(const std::vector<U> &pars)> ch2_fun =
      [&](const std::vector<U> &pars) {
        return ch2_red_ansatz_xyey_value(ansatz, x, y, ey, pars, n_dof);
      };

  return ch2_fun;
} // ch2_red_ansatz_xyey()

/*! ------ */
/*! xexyey */
/*! ------ */

template <class U>
void check_xexyey(const std::vector<U> &x, const std::vector<U> &ex,
                  const std::vector<U> &y, const std::vector<U> &ey) {
  if (!(x.size() == ex.size() && x.size() == y.size() &&
        y.size() == ey.size())) {
    std::cerr << "Error: x, ex, y, ey vectors must have the same sizes."
              << '\n';
    std::cerr << "x size: " << x.size() << '\n';
    std::cerr << "ex size: " << ex.size() << '\n';
    std::cerr << "y size: " << y.size() << '\n';
    std::cerr << "ey size: " << ey.size() << '\n';
    std::cerr << "Aborting." << '\n';
    abort();
  }
} // check_xexyey()

template <class U>
U ch2_red_ansatz_xexyey_value(
    const std::function<U(const U &, const std::vector<U> &)> &ansatz,
    const std::vector<U> &x, const std::vector<U> &ex, const std::vector<U> &y,
    const std::vector<U> &ey, const std::vector<U> &all_pars,
    const int &n_dof) {

  U ch2_value = 0.0;
  int n_pts = x.size(); /*!< number of points */
  int n_all_pars =
      all_pars.size(); /*!< all the parameters: ansatz ones + Bayesian xB_i*/
  int n_pars_ansatz = n_all_pars - n_pts; /*!< parameters of the ansatz only
                                           */
  std::vector<U> pars_ansatz(
      all_pars.begin(),
      all_pars.begin() +
          n_pars_ansatz); /*!< parameters for the ansatz function only */
  std::vector<U> xB(all_pars.begin() + n_pars_ansatz,
                    all_pars.end()); /*!< Bayesian xB_i */
  // int n_dof = get_dof(n_pts, n_pars_ansatz); /*!< degrees of freedom */

  for (int i = 0; i < n_pts; i++) {
    ch2_value += pow((y[i] - ansatz(xB[i], pars_ansatz)) / ey[i], 2);
    ch2_value += pow((x[i] - xB[i]) / ex[i], 2);
  }
  return ch2_value / (U)n_dof;
} // ch2_red_ansatz_xexyey_value()

template <class U>
std::function<U(const std::vector<U> &pars)> ch2_red_ansatz_xexyey(
    const std::function<U(const U &, const std::vector<U> &)> &ansatz,
    const std::vector<U> &x, const std::vector<U> &ex, const std::vector<U> &y,
    const std::vector<U> &ey, const int &n_dof) {

  check_xexyey<U>(x, ex, y, ey);
  std::function<U(const std::vector<U> &)> ch2_fun =
      [&](const std::vector<U> &all_pars) -> U {
    return ch2_red_ansatz_xexyey_value(ansatz, x, ex, y, ey, all_pars, n_dof);
  };

  return ch2_fun;
} // ch2_red_ansatz_xexyey()

/////////////////////////////////////
//////// deprecated stuff ///////////
/////////////////////////////////////

// /*! ------ */
// /*! xexy   */
// /*! ------ */
//
// template <class U>
// void check_xexy(const std::vector<U> &x, const std::vector<U> &ex,
//                 const std::vector<U> &y) {
//   if (!(x.size() == ex.size() && x.size() == y.size())) {
//     std::cerr << "Error: x, ex, y vectors must have the same sizes." << '\n';
//     std::cerr << "x size: " << x.size() << '\n';
//     std::cerr << "ex size: " << ex.size() << '\n';
//     std::cerr << "y size: " << y.size() << '\n';
//     std::cerr << "Aborting." << '\n';
//     abort();
//   }
// } // check_xexy()
//
// template <class U>
// U ch2_red_ansatz_xexy_value(
//     const std::function<U(const U &, const std::vector<U> &)> &ansatz,
//     const std::vector<U> &x, const std::vector<U> &ex, const std::vector<U>
//     &y, const std::vector<U> &all_pars) {
//
//   U ch2_value = 0.0;
//   int n_pts = x.size(); /*!< number of points */
//   int n_all_pars =
//       all_pars.size(); /*!< all the parameters: ansatz ones + Bayesian xB_i*/
//   int n_pars_ansatz = n_all_pars - n_pts; /*!< parameters of the ansatz only
//                                            */
//   std::vector<U> pars_ansatz(
//       all_pars.begin(),
//       all_pars.begin() +
//           n_pars_ansatz); /*!< parameters for the ansatz function only */
//   std::vector<U> xB(all_pars.begin() + n_pars_ansatz,
//                     all_pars.end());         /*!< Bayesian xB_i */
//   int n_dof = get_dof(n_pts, n_pars_ansatz); /*!< degrees of freedom */
//
//   for (int i = 0; i < n_pts; i++) {
//     ch2_value += pow((y[i] - ansatz(xB[i], pars_ansatz)), 2);
//     ch2_value += pow((x[i] - xB[i]) / ex[i], 2);
//   }
//   return ch2_value / (U)n_dof;
// } // ch2_red_ansatz_xexy_value()
//
// template <class U>
// std::function<U(const std::vector<U> &pars)> ch2_red_ansatz_xexy(
//     const std::function<U(const U &, const std::vector<U> &)> &ansatz,
//     const std::vector<U> &x, const std::vector<U> &ex,
//     const std::vector<U> &y) {
//
//   check_xexy<U>(x, ex, y);
//   std::function<U(const std::vector<U> &)> ch2_fun =
//       [&](const std::vector<U> &all_pars) -> U {
//     return ch2_red_ansatz_xexy_value(ansatz, x, ex, y, all_pars);
//   };
//
//   return ch2_fun;
// } // ch2_red_ansatz_xexy()

#endif
} // namespace Minuit2_fit
} // namespace Silibs2
