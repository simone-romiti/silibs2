// fit_xexy.hpp
/*
This file defines what is declared in fit_xexy.h
*/

#include "./fit_xexy.h"

namespace Silibs2 {
namespace Minuit2_fit {
#ifndef Silibs2_Minuit2_fit_1D_fit_xexy_hpp
#define Silibs2_Minuit2_fit_1D_fit_xexy_hpp

template <class U> fit_xexy<U>::fit_xexy() {}  // end default contructor
template <class U> fit_xexy<U>::~fit_xexy() {} // end default contructor

template <class U>
void fit_xexy<U>::set_data(const std::vector<U> &x, const std::vector<U> &ex,
                           const std::vector<U> &y) {
  if (x.size() != ex.size() || x.size() != y.size()) {
    std::cerr << "Invalid data. The number of points and errors doesn't match."
              << "\n";
    std::cerr << "x.size()  : " << x.size() << "\n";
    std::cerr << "ex.size() : " << ex.size() << "\n";
    std::cerr << "y.size()  : " << y.size() << "\n";
    std::cerr << "Aborting."
              << "\n";
    abort();
  }
  vec_x = x;
  vec_ex = ex;
  vec_y = y;
  (*this).data_init = true;
} // set_data()

template <class U>
void fit_xexy<U>::set_ansatz(
    const std::function<U(const U &, const std::vector<U> &)> &ansatz) {
  this->check_data_init(); /*!< data initialization */

  (*this).ANSATZ = ansatz;

  /*! automatic deduction of the CH2_RED_FUN function */
  (*this).CH2_RED_FUN =
      ch2_red_ansatz_xexy((*this).ANSATZ, vec_x, vec_ex, vec_y);
  (*this).residue_init = true;
} // set_ansatz()

template <class U> void fit_xexy<U>::fit() {
  this->check_fit_init();

  std::vector<U> guess = (*this).GUESS;
  std::vector<U> bound_l = (*this).BOUND_L;
  std::vector<U> bound_r = (*this).BOUND_R;

  /*! Set the Minuit2 parameters */
  std::cout.precision(8);
  std::cout.setf(std::ios_base::scientific);
  std::cout << "\nMinuit2 minimization:\n"
            << "\n";
  std::cout << "par\t||\tName\t||\tvalue\t\t||\tlimits"
            << "\n";
  ROOT::Minuit2::MnUserParameters upar;
  int n_pars = guess.size();
  for (int i = 0; i < n_pars; i++) {
    U err_i = fabs(guess[i]) / 100.0; // O(1%) relative error
    if (bound_l[i] == bound_r[i]) {   /* unbounded parameter */
      upar.Add((*this).NAMES[i], guess[i], err_i);
    } else { /* bounded parameter */
      upar.Add((*this).NAMES[i], guess[i], err_i, bound_l[i], bound_r[i]);
    }
    std::cout << i << "\t||\t" << (*this).NAMES[i] << "\t||\t" << guess[i]
              << "\t||\t" << bound_l[i] << "\t---\t" << bound_r[i] << '\n';
  }

  /*! xB_i parameters varying inside an interval of 2*sigma_x */
  int n_pts = vec_x.size();
  for (int b = 0; b < n_pts; b++) {
    std::string xB_name = "xB_" + std::to_string(b);
    U xB_guess = vec_x[b];
    U err_b = fabs(vec_ex[b]) / 100.0; /*!< O(1%) relative error */
    U xB_low = xB_guess - 2 * vec_ex[b], xB_up = xB_guess + 2 * vec_ex[b];
    upar.Add(xB_name, xB_guess, err_b, xB_low, xB_up);
    std::cout << b << "\t||\t" << xB_name << "\t||\t" << xB_guess << "\t||\t"
              << xB_low << "\t---\t" << xB_up << '\n';
  }

  /*! do the fit */
  FCN_model<U> rsd = to_FCN_model<U>((*this).CH2_RED_FUN);
  ROOT::Minuit2::MnMigrad migrad(rsd, upar);

  ROOT::Minuit2::FunctionMinimum min = migrad();
  std::cout << min;
  (*this).fit_converged = min.IsValid();

  /*! set PARS and CH2_RED */
  ROOT::Minuit2::MnUserParameterState state =
      min.UserState(); /*!< state AFTER the minimization */
  std::vector<U> best_fit_params =
      state.Params(); /*!< all parameters AFTER the minimization */

  /* Parameters of the anzatz (i.e. not including the "Bayesian" parameters
   * xB_i)*/
  (*this).PARS.resize(n_pars);
  for (int i = 0; i < n_pars; ++i) {
    (*this).PARS[i] = best_fit_params[i];
  }

  /*!< xB_i parameters obtained with the Bayesian procedure */
  x_Bayes.resize(n_pts);
  for (int b = 0; b < n_pts; ++b) {
    x_Bayes[b] = best_fit_params[n_pars + b];
  }

  (*this).CH2_RED = min.Fval();
  (*this).fit_done = true;

  /*! Theoretical values for the fitted points */
  vec_y_th.resize(n_pts);
  for (int i = 0; i < n_pts; ++i) {
    vec_y_th[i] = (*this).ANSATZ(vec_x[i], (*this).PARS);
  }

} // fit()

template <class U> std::vector<U> fit_xexy<U>::get_vec_y_th() {
  return (*this).vec_y_th;
} // get_vec_y_th()

#endif
} // namespace Minuit2_fit
} // namespace Silibs2
