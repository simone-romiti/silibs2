// fit_xexy.h
/*
This file declares routines allowing to fit a generic std::function
using the Minuit library, with data in the format xyey (i.e. no error on the x)
*/

#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <numeric>
#include <string>

/* base class for 1D ansatz fitting */
#include "../FCN_model.hpp"
#include "./base_fit.hpp"

/*! Minuit2 libraries */
#include "Minuit2/FCNBase.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnHesse.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/MnUserParameterState.h"
#include "Minuit2/MnUserParameters.h"

namespace Silibs2 {
namespace Minuit2_fit {
#ifndef Silibs2_Minuit2_fit_1D_fit_xexy_h
#define Silibs2_Minuit2_fit_1D_fit_xexy_h

//! 1-dimensional fit
template <class U = double> class fit_xexy : public base_fit<U> {
private:
  /*! x,y,ey */
  std::vector<U> vec_x, vec_ex, vec_y;
  std::vector<U> x_Bayes;

  /*!
  Theoretical values obtained from the ansatz
  with the best fit values for the parameters
  */
  std::vector<U> vec_y_th;

public:
  fit_xexy();
  ~fit_xexy();

  /*! setting the data: x, error_x, y, error_y */
  void set_data(const std::vector<U> &x, const std::vector<U> &ex,
                const std::vector<U> &y);

  /*!
  Sets the ansatz to fit and automatically deduce the
  reduced chi squared function
  */
  void set_ansatz(
      const std::function<U(const U &, const std::vector<U> &pars)> &ansatz);

  /*! do the fit */
  void fit();

  std::vector<U> get_vec_y_th(); /*!< returns vec_y_th */
};

#endif
} // namespace Minuit2_fit
} // namespace Silibs2
