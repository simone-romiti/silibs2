// fit_xexyey.h
/*
This file declares routines allowing to fit a generic std::function
using the Minuit library, with data in the format xyey (i.e. no error on the x)
*/

#ifndef Silibs2_Minuit2_fit_1D_fit_xexyey_h
#define Silibs2_Minuit2_fit_1D_fit_xexyey_h

#include <functional>
#include <iostream>

/* base class for 1D ansatz fitting */
#include "./base_fit.h"

namespace Silibs2 {
namespace Minuit2_fit {

//! 1-dimensional fit
template <class U = double> class fit_xexyey : public base_fit<U> {
private:
  /*! x,y,ey */
  std::vector<U> vec_x, vec_ex, vec_y, vec_ey;
  std::vector<U> x_Bayes;

  /*!
  Theoretical values obtained from the ansatz
  with the best fit values for the parameters
  */
  std::vector<U> vec_y_th;
  double n_sigma_Bayes = 100;

public:
  fit_xexyey();
  ~fit_xexyey();

  void set_n_sigma_Bayes(const double &nsB); // n_sigma_Bayes = nsB

  /*! setting the data: x, error_x, y, error_y */
  void set_data(const std::vector<U> &x, const std::vector<U> &ex,
                const std::vector<U> &y, const std::vector<U> &ey);

  // set n_dof = n_pts-n_pars unless n_dof has been set manually
  void set_n_dof_auto();

  /*!
  Sets the ansatz to fit and automatically deduce the
  reduced chi squared function
  */
  void set_ansatz(
      const std::function<U(const U &, const std::vector<U> &pars)> &ansatz);

  /*! do the fit */
  void fit();

  std::vector<U> get_vec_y_th(); /*!< returns vec_y_th */
};

} // namespace Minuit2_fit
} // namespace Silibs2
#endif
