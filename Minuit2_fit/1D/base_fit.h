// base_fit.h
/*
This file declares routines allowing to fit a generic std::function
using the Minuit library, with data in the format xyey (i.e. no error on the x)
*/
#ifndef Silibs2_Minuit2_fit_1D_base_fit_h
#define Silibs2_Minuit2_fit_1D_base_fit_h

#include <functional>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

/*! base class for 1D ansatz fitting */
#include "../base.h"

namespace Silibs2 {
namespace Minuit2_fit {

//! 1-dimensional fit
template <class U = double> class base_fit : public base<U> {
protected:
  /*! control variables */
  bool data_init = false;    /*!< true if data have been set */
  bool residue_init = false; /*!< true if the residue function has been set */
  bool fit_done = false;     /*!< true when the fit has been done */

  /*! Ansatz to be fitted (if unspecified it is the null function)*/
  std::function<U(const U &, const std::vector<U> &)> ANSATZ =
      [](const U &, const std::vector<U> &) { return 0; };

  /*! residue function */
  std::function<U(const std::vector<U> &)> CH2_RED_FUN =
      [](const std::vector<U> &) { return 0; };

  std::vector<U> PARS; /*!< Best-fit values for the parameters */
  U CH2_RED = -1.0; /*!< residue (reduced chi squared) value resulting from the
                       minimization (must be positive during and after the
                       minimization)*/

public:
  base_fit();
  ~base_fit();

  /*! Checks if data were passed */
  void check_data_init();

  /*! Setting the residue function*/
  void set_residue(const std::function<U(const std::vector<U> &pars)> &res);
  void check_residue_init(); /*!< Checks if residue_init is true */

  /*!
  Checks if all the conditions necessary
  to do the fit have been fullfilled
  */
  void check_fit_init();
  void check_fit_done(); /*!< checks if the fit was done */

  std::vector<U> get_PARS();

  U get_CH2_RED(); //!< returns CH2_RED
};

/* ----------------------------------- */
/* Automatic deduction of ch2 function */
/* ----------------------------------- */

/*!
Check if the vectors x,y,ey have the same sizes. If not, it aborts
*/
template <class U>
void check_xyey(const std::vector<U> &x, const std::vector<U> &y,
                const std::vector<U> &ey);

/*! ---- */
/*! xyey */
/*! ---- */

/*!
Return the value of the Chi squared from ansatz and data
\Chi^2 = \sum_i (\frac{y_i - y(x_i)}{ey_i})^2 /  dof
(dof = degrees of freedomof the fit)
*/
template <class U>
U ch2_red_ansatz_xyey_value(
    const std::function<U(const U &, const std::vector<U> &)> &ansatz,
    const std::vector<U> &x, const std::vector<U> &y, const std::vector<U> &ey,
    const std::vector<U> &pars, const int &n_dof);

/*! Returns the std::function giving the ch2_red_ansatz_xyey_value */
template <class U>
std::function<U(const std::vector<U> &pars)> ch2_red_ansatz_xyey(
    const std::function<U(const U &, const std::vector<U> &)> &ansatz,
    const std::vector<U> &x, const std::vector<U> &y, const std::vector<U> &ey,
    const int &n_dof);

/*! ------ */
/*! xexyey */
/*! ------ */

/*!
Check if the vectors x,ex,y,ey have the same sizes. If not, it aborts
*/
template <class U>
void check_xexyey(const std::vector<U> &x, const std::vector<U> &ex,
                  const std::vector<U> &y, const std::vector<U> &ey);
/*
"Bayesian" Chi squared from ansatz and data
\Chi^2 =
( \sum_i (\frac{y_i - y(xb_i)}{ey_i})^2 + (\frac{x_i-xb_i}{ex_i})^2)/d.o.f
(d.o.f = degrees of freedomof the fit)
*/
template <class U>
U ch2_red_ansatz_xexyey_value(
    const std::function<U(const U &, const std::vector<U> &)> &ansatz,
    const std::vector<U> &x, const std::vector<U> &ex, const std::vector<U> &y,
    const std::vector<U> &ey, const std::vector<U> &all_pars, const int &n_dof);

/*! Returns the std::function that gives ch2_red_ansatz_xexyey_value */
template <class U>
std::function<U(const std::vector<U> &pars)> ch2_red_ansatz_xexyey(
    const std::function<U(const U &, const std::vector<U> &)> &ansatz,
    const std::vector<U> &x, const std::vector<U> &ex, const std::vector<U> &y,
    const std::vector<U> &ey, const int &n_dof);

/////////////////////////////////////
//////// deprecated stuff ///////////
/////////////////////////////////////

// /*! ------ */
// /*! xexy   */
// /*! ------ */
//
// /*!
// Check if the vectors x,ex,y have the same sizes. If not, it aborts
// */
// template <class U>
// void check_xexy(const std::vector<U> &x, const std::vector<U> &ex,
//                 const std::vector<U> &y);
// /*
// "Bayesian" Chi squared from ansatz and data
// \Chi^2 =
// ( \sum_i (\frac{y_i - y(xb_i)}{ey_i})^2 + (\frac{x_i-xb_i}{ex_i})^2)/d.o.f
// (d.o.f = degrees of freedomof the fit)
// */
// template <class U>
// U ch2_red_ansatz_xexy_value(
//     const std::function<U(const U &, const std::vector<U> &)> &ansatz,
//     const std::vector<U> &x, const std::vector<U> &ex, const std::vector<U>
//     &y, const std::vector<U> &all_pars);
//
// /*! Returns the std::function that gives ch2_red_ansatz_xexy_value */
// template <class U>
// std::function<U(const std::vector<U> &pars)> ch2_red_ansatz_xexy(
//     const std::function<U(const U &, const std::vector<U> &)> &ansatz,
//     const std::vector<U> &x, const std::vector<U> &ex, const std::vector<U>
//     &y);

} // namespace Minuit2_fit
} // namespace Silibs2
#endif
