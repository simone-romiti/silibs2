// base.hpp
/* This file defines what is declared in base.h */
#include "./base.h"

namespace Silibs2 {

namespace Minuit2_fit {

#ifndef Silibs2_Minuit2_fit_base_hpp
#define Silibs2_Minuit2_fit_base_hpp

template <class U> base<U>::base() {}

template <class U>
void base<U>::set_pars(const std::vector<std::string> &names,
                       const std::vector<U> &guesses,
                       const bool &call_by_overload) {
  int n_guesses = guesses.size();
  int n_names = names.size();

  if (n_guesses != n_names) {
    std::cerr
        << "The number of parameters does not coincide with the number of "
           "names:"
        << "\n";
    std::cerr << "n_guesses : " << n_guesses << "\n";
    std::cerr << "n_names : " << n_names << "\n";

    std::cerr << "Aborting."
              << "\n";
    abort();
  }

  GUESS = guesses;
  NAMES = names; // fixed_pars = fixed;
  pars_init = true;

  if (!call_by_overload) {

    /* Left and right bounds weren't specified */
    BOUND_L.resize(n_guesses);
    fill(BOUND_L.begin(), BOUND_L.end(), 0);
    BOUND_R.resize(n_guesses);
    fill(BOUND_R.begin(), BOUND_R.end(), 0);

    if ((*this).verbose) {
      std::clog << "Warning : You didn't specify left and right bounds for the "
                   "parameters.\n"
                << "They have no limits inside which they can vary. "
                   "Continuing."
                << "\n";
    }
    pars_init = true;
  }

} // end set_pars()

template <class U>
void base<U>::set_pars(const std::vector<std::string> &names,
                       const std::vector<U> &guesses,
                       const std::vector<U> &bound_l,
                       const std::vector<U> &bound_r) {
  set_pars(names, guesses, true);
  if (bound_l.size() != bound_r.size()) {
    std::cerr << "Error. You gave a different number of left bounds than right "
                 "ones for the parameters. Aborting."
              << "\n";
    abort();
  }
  if (guesses.size() != bound_l.size()) {
    std::cerr << "Error. You gave a number of guesses that doesn't match the "
                 "number of bounds. Aborting."
              << "\n";
    abort();
  }
  BOUND_L = bound_l;
  BOUND_R = bound_r;
  pars_init = true;
} // end set_pars()

template <class U> void base<U>::set_verbose(const bool &v) {
  (*this).verbose = v;
}

template <class U> void base<U>::check_pars_init() const {
  if (!pars_init) {
    std::cerr << "Error: you didn't initialize the parameters of the fit."
              << "Aborting.\n";
    abort();
  }
} // check_pars_init()

template <class U> int base<U>::get_n_pars() const {
  return ((*this).NAMES).size();
} // get_n_pars()

template <class U> void base<U>::set_n_dof_manually(const int &n) {
  (*this).n_dof = n;
  (*this).n_dof_maually_set = true;
} // set_n_dof()

template <class U> int base<U>::get_n_dof() const {
  return (*this).n_dof;
} // get_n_dof()

template <class U> bool base<U>::fit_did_converge() const {
  return (*this).fit_converged;
} // get_vec_y_th()

#endif
} // namespace Minuit2_fit
} // namespace Silibs2
