// nD_paraboloid.cpp
/*
fit of a paraboloid in 3 dimensions:
f(x,y) = a*x^2 + b*y^2
*/

#include "Silibs2.h"

#include <iostream>
#include <random>
using namespace Silibs2;

int main(int argc, char *argv[]) {

  std::random_device rd;  //  seed for the random number engine
  std::mt19937 gen(rd()); // Standard mersenne_twister_engine seeded with rd()
  std::uniform_real_distribution<> dis(0.0, 6.0);

  const int n_var = 2;
  const int n_pts = 10;
  Tables::table<double> x(2, n_pts);
  Tables::table<double> ex(2, n_pts);
  std::vector<double> y(n_pts);
  std::vector<double> ey(n_pts);
  for (int j = 0; j < n_pts; ++j) {
    for (int i = 0; i < n_var; ++i) {
      x(i, j) = dis(gen);
      ex(i, j) = 0.01 * dis(gen);
    }
    y[j] = std::pow(x(0, j), 2) + std::pow(x(1, j), 2);
    ey[j] = 0.01 * y[j];
  }

  Silibs2::Minuit2_fit::nD::fit_xexyey<double> minu;
  minu.set_pars({"a", "b"}, {0.5, 1.4});
  minu.set_data(x, ex, y, ey);
  std::function<double(const std::vector<double> &,
                       const std::vector<double> &)>
      ansatz = [](const std::vector<double> &x, const std::vector<double> &p) {
        return p[0] * std::pow(x[0], 2) + p[1] * std::pow(x[1], 2);
      };

  minu.set_ansatz(ansatz);
  minu.fit();

  return 0;
}
