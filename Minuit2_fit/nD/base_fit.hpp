// fit_Minuit_v2.hpp
/*
This file defines what is declared in fit_Minuit2.h
*/

#include "../../Tables/table.hpp"

#include "./base_fit.h"

namespace Silibs2 {
namespace Minuit2_fit {
namespace nD {

#ifndef Silibs2_Minuit2_fit_nD_base_fit_hpp
#define Silibs2_Minuit2_fit_nD_base_fit_hpp

template <class U> base_fit<U>::base_fit() {}  // end default contructor
template <class U> base_fit<U>::~base_fit() {} // end default contructor

template <class U> void base_fit<U>::check_data_init() {
  if (!(*this).data_init) {
    std::cerr << "Error: you haven't passed the data yet.\n"
              << "(Recall for instance that the ansatz must be passed after "
                 "the data, so "
                 "that the redidue function can be deduced)\n"
              << "aborting." << '\n';
    abort();
  }
}

template <class U>
void base_fit<U>::set_residue(
    const std::function<U(const std::vector<U> &pars)> &res) {
  CH2_RED_FUN = res;
  residue_init = true;
  data_init = true;
}

template <class U> void base_fit<U>::check_residue_init() {
  if (!residue_init) {
    std::cerr << "Error: residue function wasn't specified. Check if you "
                 "passed it correctly or, if you gave the ansatz, if you "
                 "provided also the data. Aborting."
              << '\n';
    abort();
  }
} // check_residue_init()

template <class U> void base_fit<U>::check_fit_init() {
  this->check_residue_init();
  this->check_pars_init();
}

template <class U> void base_fit<U>::check_fit_done() {
  if (!fit_done) {
    std::cerr << "Error: you haven't done the fit yet. Aborting." << '\n';
    abort();
  }
}

template <class U> std::vector<U> base_fit<U>::get_PARS() {
  check_fit_done();
  return PARS;
}

template <class U> U base_fit<U>::get_CH2_RED() {
  check_fit_done();
  return CH2_RED;
}

/* ----------------------------------- */
/* Automatic deduction of ch2 function */
/* ----------------------------------- */

/*! ------ */
/*! xexyey */
/*! ------ */

template <class U>
void check_xexyey(const Tables::table<U> &x, const Tables::table<U> &ex,
                  const std::vector<U> &y, const std::vector<U> &ey) {
  bool illegal_fit = false;
  if (x.shape() != ex.shape()) {
    std::cerr << "Error : x and ex don't have the same shape\n";
    std::cerr << "x :\n";
    x.print_cerr();
    std::cerr << "ex :\n";
    ex.print_cerr();
    illegal_fit = true;
  }
  if (x.cols() != (int)y.size()) {
    std::cerr << "Error : number of x points != number of y points\n";
    std::cerr << "nx = " << x.cols();
    std::cerr << "ny = " << y.size();
    illegal_fit = true;
  }
  if (y.size() != ey.size()) {
    std::cerr << "Error : y and ey don't have the same size\n";
    std::cerr << "ny = " << y.size();
    std::cerr << "ney = " << ey.size();
    illegal_fit = true;
  }

  if (illegal_fit) {
    std::cerr << "Aborting.\n";
    abort();
  }
} // check_xexyey()

template <class U>
U ch2_red_ansatz_xexyey_value(
    const std::function<U(const std::vector<U> &, const std::vector<U> &)>
        &ansatz,
    const Tables::table<U> &x, const Tables::table<U> &ex,
    const std::vector<U> &y, const std::vector<U> &ey,
    const std::vector<U> &all_pars, const int &n_dof) {

  U ch2_value = 0.0;
  int n_var = x.rows(); /*!< number of variables */
  int n_pts = x.cols(); /*!< number of points */

  /*!< all the parameters: ansatz ones + Bayesian xB_i*/
  int n_all_pars = all_pars.size();
  /*!< number of parameters of the ansatz only */
  int npa = n_all_pars - n_var * n_pts;

  auto ap_beg = all_pars.begin();
  /*!< parameters for the ansatz function only */
  std::vector<U> pars_ansatz(ap_beg, ap_beg + npa);
  Tables::table<U> xB(n_var, n_pts);
  for (int i = 0; i < n_var; ++i) {
    for (int j = 0; j < n_pts; ++j) {
      /* bayesian parameters are thought as a list of rows */
      xB(i, j) = all_pars[npa + i * n_pts + j];
    }
  }

  // int n_dof = get_dof(n_pts, npa); /*!< degrees of freedom */

  for (int j = 0; j < n_pts; j++) {
    ch2_value += pow((y[j] - ansatz(xB.col(j), pars_ansatz)) / ey[j], 2);
    for (int i = 0; i < n_var; ++i) {
      if (ex(i, j) == 0) { // trivial variable (no error)
        continue;
      }
      ch2_value += pow((x(i, j) - xB(i, j)) / ex(i, j), 2);
    }
  }
  return ch2_value / (U)n_dof;
} // ch2_red_ansatz_xexyey_value()

template <class U>
std::function<U(const std::vector<U> &pars)> ch2_red_ansatz_xexyey(
    const std::function<U(const std::vector<U> &, const std::vector<U> &)>
        &ansatz,
    const Tables::table<U> &x, const Tables::table<U> &ex,
    const std::vector<U> &y, const std::vector<U> &ey, const int &n_dof) {

  check_xexyey<U>(x, ex, y, ey);
  std::function<U(const std::vector<U> &)> ch2_fun =
      [&](const std::vector<U> &all_pars) -> U {
    return ch2_red_ansatz_xexyey_value(ansatz, x, ex, y, ey, all_pars, n_dof);
  };

  return ch2_fun;
} // ch2_red_ansatz_xexyey()

#endif
} // namespace nD
} // namespace Minuit2_fit
} // namespace Silibs2