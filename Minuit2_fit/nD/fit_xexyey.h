// fit_xexyey.h
/*
This file declares routines allowing to fit a generic std::function
using the Minuit library, with data in the format xyey (i.e. no error on the x)
*/
#ifndef Silibs2_Minuit2_fit_nD_fit_xexyey_h
#define Silibs2_Minuit2_fit_nD_fit_xexyey_h

#include <functional>
#include <iostream>

/* base class for nD ansatz fitting */
#include "./base_fit.h"

namespace Silibs2 {
namespace Minuit2_fit {
namespace nD {

//! 1-dimensional fit
template <class U = double> class fit_xexyey : public base_fit<U> {
private:
  Tables::table<U> tab_x, tab_ex; //!< x, ex
  std::vector<U> vec_y, vec_ey;   //!< y, ey
  Tables::table<U> x_Bayes;
  // default number of sigma of which the xB_i can vary
  int n_sigma_Bayes = 100;

  /*!
  Theoretical values obtained from the ansatz
  with the best fit values for the parameters
  */
  std::vector<U> vec_y_th;

public:
  fit_xexyey();
  ~fit_xexyey();

  /*! setting the data: x, error_x, y, error_y */
  void set_data(const Tables::table<U> &x, const Tables::table<U> &ex,
                const std::vector<U> &y, const std::vector<U> &ey);

  // set n_dof = n_pts-n_pars unless n_dof has been set manually
  void set_n_dof_auto();

  /*!
  Sets the ansatz to fit and automatically deduce the
  reduced chi squared function
  */
  void set_ansatz(const std::function<U(const std::vector<U> &xi,
                                        const std::vector<U> &pars)> &ansatz);

  void set_n_sigma_Bayes(const int &n); // set n_sigma_Bayes = n

  /*! do the fit */
  void fit();

  std::vector<U> get_vec_y_th(); /*!< returns vec_y_th */
};

} // namespace nD
} // namespace Minuit2_fit
} // namespace Silibs2
#endif
