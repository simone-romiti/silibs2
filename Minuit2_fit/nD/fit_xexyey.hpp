// fit_Minuit_v2.hpp
/*
This file defines what is declared in fit_Minuit2.h
*/

#include "../FCN_model.hpp"
#include "./base_fit.hpp" // base class for nD ansatz fitting

#include "./fit_xexyey.h"

namespace Silibs2 {
namespace Minuit2_fit {
namespace nD {

#ifndef Silibs2_Minuit2_fit_nD_fit_xexyey_hpp
#define Silibs2_Minuit2_fit_nD_fit_xexyey_hpp

template <class U> fit_xexyey<U>::fit_xexyey() {}  // end default contructor
template <class U> fit_xexyey<U>::~fit_xexyey() {} // end default contructor

template <class U>
void fit_xexyey<U>::set_data(const Tables::table<U> &x,
                             const Tables::table<U> &ex,
                             const std::vector<U> &y,
                             const std::vector<U> &ey) {
  check_xexyey(x, ex, y, ey);

  tab_x = x;
  tab_ex = ex;
  vec_y = y;
  vec_ey = ey;
  (*this).data_init = true;
} // set_data()

template <class U> void fit_xexyey<U>::set_n_dof_auto() {
  this->check_data_init(); /*!< data initialization */
  this->check_pars_init(); /*!< parameters initialization */

  if (!(*this).n_dof_maually_set) { // if n_dof_maually_set = false ...
    const int n_pts = (int)tab_x.size();
    const int n_pars = this->get_n_pars();
    (*this).n_dof = get_dof(n_pts, n_pars);
  }
}

template <class U>
void fit_xexyey<U>::set_ansatz(
    const std::function<U(const std::vector<U> &, const std::vector<U> &)>
        &ansatz) {
  this->set_n_dof_auto(); // check also data and parameters

  (*this).ANSATZ = ansatz;

  /*! automatic deduction of the CH2_RED_FUN function */
  (*this).CH2_RED_FUN = ch2_red_ansatz_xexyey((*this).ANSATZ, tab_x, tab_ex,
                                              vec_y, vec_ey, (*this).n_dof);
  (*this).residue_init = true;
} // set_ansatz()

template <class U> void fit_xexyey<U>::set_n_sigma_Bayes(const int &n) {
  n_sigma_Bayes = n;
} // set_n_sigma_Bayes()

template <class U> void fit_xexyey<U>::fit() {
  this->check_fit_init();

  std::vector<U> guess = (*this).GUESS;
  std::vector<U> bound_l = (*this).BOUND_L;
  std::vector<U> bound_r = (*this).BOUND_R;

  /*! Set the Minuit2 parameters */
  if ((*this).verbose) {
    std::cout.precision(8);
    std::cout.setf(std::ios_base::scientific);
    std::cout << "\nMinuit2 minimization:\n"
              << "\n";
    std::cout << "par\t||\tName\t||\tvalue\t\t||\tlimits"
              << "\n";
  }

  ROOT::Minuit2::MnUserParameters upar;
  int n_pars = guess.size();
  for (int i = 0; i < n_pars; i++) {
    U err_i = fabs(guess[i]) / 100.0; // O(1%) relative error
    if (bound_l[i] == bound_r[i]) {   /* unbounded parameter */
      upar.Add((*this).NAMES[i], guess[i], err_i);
    } else { /* bounded parameter */
      upar.Add((*this).NAMES[i], guess[i], err_i, bound_l[i], bound_r[i]);
    }

    if ((*this).verbose) {
      std::cout << i << "\t||\t" << (*this).NAMES[i] << "\t||\t" << guess[i]
                << "\t||\t" << bound_l[i] << "\t---\t" << bound_r[i] << '\n';
    }
  }

  /*! xB_i parameters varying inside an interval of 2*sigma_x */
  int n_var = tab_x.rows();
  int n_pts = tab_x.cols();

  // parameters fixed because correspond to error-free variables
  std::vector<std::string> fixed_pars(0);
  // const int nsB = (*this).n_sigma_Bayes;
  for (int i = 0; i < n_var; ++i) {
    for (int j = 0; j < n_pts; ++j) {
      std::string xB_name = "xB_" + std::to_string(i) + "_" + std::to_string(j);
      U xB_ij = tab_x(i, j);
      U err_ij = fabs(tab_ex(i, j));

      // U xB_low = xB_ij - nsB * err_ij;
      // U xB_up = xB_ij + nsB * err_ij;
      if (fabs(err_ij) < 1e-15) {
        // variable with no error(Minuit uses double precision)
        upar.Add(xB_name, xB_ij, err_ij);
        fixed_pars.push_back(xB_name);
      } else {
        // upar.Add(xB_name, xB_ij, err_ij, xB_low, xB_up);
        upar.Add(xB_name, xB_ij, err_ij); // unbounded
      }

      if ((*this).verbose) {
        std::cout << "(" << i << "," << j << ")"
                  << "\t||\t" << xB_name << "\t||\t" << xB_ij << "\t||\t";
        // std::cout << xB_low << "\t---\t" << xB_up;
        std::cout << "\n";
      }
    }
  }

  /*! do the fit */
  FCN_model<U> rsd = to_FCN_model<U>((*this).CH2_RED_FUN);
  ROOT::Minuit2::MnMigrad migrad(rsd, upar);
  for (std::string fxd : fixed_pars) {
    migrad.Fix(&fxd[0]); // fix the parameter to it's guess
  }

  ROOT::Minuit2::FunctionMinimum min = migrad();
  (*this).fit_converged = min.IsValid();

  std::stringstream Minuit2_log; // log of the minimization
  Minuit2_log << min;            // filling the log
  const std::string Minuit2_log_str =
      Minuit2_log.str(); // converting to std::string

  const std::string eU = "Parameter is at Upper limit"; // error "Upper"
  const std::string eL = "Parameter is at Lower limit"; // error "Lower"
  const bool reached_limit = (Minuit2_log_str.find(eU) != std::string::npos) ||
                             (Minuit2_log_str.find(eL) != std::string::npos);
  /* outputs Minuit2 log when:
      - the minimum is invalid, or
      - at least one parameter has reached its upper or lower limit
  */
  if (reached_limit || !(*this).fit_converged) {
    std::cerr << Minuit2_log_str;
  }

  /*! set PARS and CH2_RED */

  /*!< state AFTER the minimization */
  ROOT::Minuit2::MnUserParameterState state = min.UserState();

  /*!< all parameters AFTER the minimization */
  const std::vector<U> best_fit_params = state.Params();

  /*
  Parameters of the anzatz
  (i.e. not including the "Bayesian" parameters xB_i)
  */
  (*this).PARS.resize(n_pars);
  for (int i = 0; i < n_pars; ++i) {
    (*this).PARS[i] = best_fit_params[i];
  }

  /*!< xB_i parameters obtained with the Bayesian procedure */
  x_Bayes.resize(n_var, n_pts);
  for (int i = 0; i < n_var; ++i) {
    for (int j = 0; j < n_pts; ++j) {
      // offset + index of list of columns
      const int idx = n_pars + i + n_var * j;
      x_Bayes(i, j) = best_fit_params[idx];
    }
  }

  (*this).CH2_RED = min.Fval();
  (*this).fit_done = true;

  /*! Theoretical values for the fitted points */
  vec_y_th.resize(n_pts);
  for (int j = 0; j < n_pts; ++j) {
    vec_y_th[j] = (*this).ANSATZ(tab_x.col(j), (*this).PARS);
  }

} // fit()

template <class U> std::vector<U> fit_xexyey<U>::get_vec_y_th() {
  return (*this).vec_y_th;
} // get_vec_y_th()

#endif
} // namespace nD
} // namespace Minuit2_fit
} // namespace Silibs2