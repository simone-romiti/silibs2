// FCN_model.h
/*
Minuit2 can minimize a FCN function if it is defined through a class derived by
FCNBase. Here are declared the generic type of this functions and the methods
dealing with this kind of objects
*/
#ifndef Silibs2_Minuit2_FCN_model_h
#define Silibs2_Minuit2_FCN_model_h

#include <functional>
#include <iostream>

/* Minuit2 libraries */
#include "Minuit2/FCNBase.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnHesse.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnPrint.h"
#include "Minuit2/MnUserParameters.h"

namespace Silibs2 {
namespace Minuit2_fit {

/* Residue class type. */
template <class U = double> class FCN_model : public ROOT::Minuit2::FCNBase {
public:
  FCN_model();
  ~FCN_model();

  /*! Custom constructor */
  FCN_model(const std::function<U(const std::vector<U> &)> &fcn);

  U operator()(const std::vector<U> &pars) const;
  U Up() const;

private:
  std::function<U(const std::vector<U> &)>
      FCN_function; /*!< function to be minimized */
};                  // class FCN_model

/*! Returns an FCN_model object with residue function given by 'fcn' */
template <class U>
FCN_model<U> to_FCN_model(const std::function<U(const std::vector<U> &)> &fcn);

/*
Returns the number of degrees of freedom checking if it is meaningless.
correct = true --> if (n_pts - n_pars) == 0 returns 1.
 */
int get_dof(const int &n_pts, const int &n_pars, const bool &correct = true);

} // namespace Minuit2_fit
} // namespace Silibs2
#endif
