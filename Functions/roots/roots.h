// roots.h
// This file declares functions for root finding

#include <algorithm>
#include <functional>
#include <vector>

// #include "../derivatives/diff.hpp"

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_roots_h
#define Silibs2_Functions_roots_h

//!< Numerical derivative
/*!< Finds the approximation of the first derivative of a function */
template <class U>
U N_Derive(U (*fun)(const U &x0, U *par), U x, U *p, U epsilon);

// template <class U>
// U Root_Newton(U (*fun)(const U &x, U *par), U *p, const U &start,
//               const U &epsilon0, const U &epsilon, const int &N_max);
//
// template <class U>
// U Root_Newton(U (*fun)(U x, U *par), U *p, U start, U left, U right, U
// epsilon0,
//               U epsilon, int N_max);

template <class U>
void FindRootInRange(U (*fun)(const U &x, U *par), U *p, U a, U b, U epsilon_x,
                     U epsilon_y, U cut_delta_y, U &r, bool &found);

/*
finds root of function 'f' on the left of 'start' point,
moving by steps of 'eps_x'
*/
template <class U>
U root_on_the_left(std::function<U(const U &, U *)> f, U *p, U start, U eps_x,
                   U eps_y);

/*
finds root of function 'f' on the right of 'start' point,
moving by steps of 'eps_x'
*/
template <class U>
U root_on_the_right(std::function<U(const U &, U *)> f, U *p, U start, U eps_x,
                    U eps_y);

/* find roots of a function in a given interval [a, b]*/
template <class U>
std::vector<U> roots_interval(std::function<U(const U &)> f, const U &a,
                              const U &b, const U &dx, const U &dy);

/*!
Inverts the equation f(x) = y0,
i.e. finding all the x0 in the interval [a,b] such that the previous equation is
satisfied. The roots are found using the function
roots_interval(), with and initial step dx.
*/
template <class U>
std::vector<U> find_roots(std::function<U(const U &)> f, const U &y0,
                          const U &a, const U &b, const U &dx, const U &dy);

/*!
Gets the only element resulting from the find_roots() functions.
If more than one root was found, abort() is called
*/
template <class U>
U find_root(std::function<U(const U &)> f, const U &y0, const U &a, const U &b,
            const U &dx, const U &dy);

#endif
} // namespace Functions
} // namespace Silibs2
