// roots.hpp
// This file contains the definition of what is declared in roots.h

#include <cassert>
#include <cmath>
#include <iostream>

#include "roots.h"

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_roots_hpp
#define Silibs2_Functions_roots_hpp

template <class U>
U N_Derive(U (*fun)(const U &x0, U *par), U x, U *p, U epsilon) {
  U num = fun(x + epsilon, p) - fun(x - epsilon, p);
  U den = 2.0 * epsilon;
  return num / den;
} // end N_Derive()

// template <class U>
// U Root_Newton(U (*fun)(const U &x, U *par), U *p, const U &start,
//               const U &epsilon0, const U &epsilon, const int &N_max) {
//   U x0 = start;
//   int n = 0;
//   while (fabs(fun(x0, p)) > epsilon0) {
//     U f = fun(x0, p);
//     U f_prime = N_Derive<U>(fun, x0, p, epsilon); // approximation of
//     derivative
//
//     x0 = x0 - f / f_prime; // Newton's method to approach the root
//
//     if (n > N_max) { // prevents the iteration of the method for more than
//     N_max
//                      // times
//       U x1 = x0;
//       U x2 = x1 - f / f_prime;
//       U y1 = f;
//       U y2 = fun(x2, p);
//       if (y1 * y2 < 0) {
//         return (x1 + x2) / 2.0;
//       } else {
//         std::cerr << "Cannot find root in " << N_max << " steps."
//                   << "\n";
//         abort();
//       }
//     }
//
//     n++; // counter of iterations
//   }
//
//   return x0;
// } // end Root_Newton()

template <class U>
void FindRootInRange(U (*fun)(const U &x, U *par), U *p, U a, U b, U epsilon_x,
                     U epsilon_y, U cut_delta_y, U &r, bool &found) {

  U y1 = fun(a, p), y2 = fun(b, p);

  bool y1_small = (fabs(y1) < epsilon_y);         // true if |y1| is too small
  bool y2_small = (fabs(y2) < epsilon_y);         // true if |y2| is too small
  bool delta_x_small = (fabs(b - a) < epsilon_x); // true if |b-a| is too small
  bool delta_y_ok =
      (fabs(y2 - y1) < cut_delta_y); // true if |y2-y1| is not too big

  if (y1_small) {

    r = a;
    found = true;

  } else if (y2_small) {

    r = b;
    found = true;

  } else if (delta_x_small && delta_y_ok) {
    r = a;
    found = true;
  } else {
    U s1 = copysign(1.0, y1);

    U c = (a + b) / 2.0;
    bool left = (a != c);
    bool right = (c != b);
    bool div = left && right; // true only if a<c<b (without the equal sign)
    //!< avoids numerical problems when bisection (a+b)/2 has no effect
    if (div) {

      U y_middle = fun(c, p);

      U s3 = copysign(1.0, y_middle);
      if (s3 * s1 > 0) {
        FindRootInRange(fun, p, c, b, epsilon_x, epsilon_y, cut_delta_y, r,
                        found);
      } else {
        FindRootInRange(fun, p, a, c, epsilon_x, epsilon_y, cut_delta_y, r,
                        found);
      }
    }
  }
} // end FindRootInRange()

template <class U>
U root_on_the_left(std::function<U(const U &, U *)> f, U *p, U start, U eps_x,
                   U eps_y) {

  U x = start;
  U value0 = f(x, p);
  U value = value0;

  while (fabs(value) > eps_y) {
    x -= eps_x;
    value = f(x, p);

    if (value0 * value < 0) { /* sign has changed */
      x = root_on_the_right(f, p, x, eps_x / 2.0, eps_y);
      value = f(x, p);
    }
  }

  return x;
} // end root_on_the_left()

template <class U>
U root_on_the_right(std::function<U(const U &, U *)> f, U *p, U start, U eps_x,
                    U eps_y) {
  U x = start;
  U value0 = f(x, p);
  U value = value0;

  while (fabs(value) >= eps_y) {
    x += eps_x;
    value = f(x, p);

    if (value0 * value < 0) { /* sign has changed */
      x = root_on_the_left(f, p, x, eps_x / 2.0, eps_y);
      value = f(x, p);
    }
  }

  return x;
} // end root_on_the_right()

template <class U>
std::vector<U> roots_interval(std::function<U(const U &)> f, const U &a,
                              const U &b, const U &dx, const U &dy) {
  std::function<U(const U &, U *)> f_alias = [&](const U &x, U *p) {
    return f(x); /* alias for the function */
  };

  U delta = 0.0;           /* increment over the 'x' */
  std::vector<U> roots(0); /* vector of roots found */

  U fL = f(a); /* first value */
  if (fabs(fL) <= dy) {
    /* maybe 'a' itself is a root */
    roots.push_back(a);
    /* continue because the function may be constantly 0 */
    while (f(a + delta) <= dy && delta <= (b - a)) {
      delta += dx;
    }
  }

  while (delta <= (b - a)) {
    U yi = f(a + delta);

    if (yi * fL > 0) { /* sign didn't change */
      if (fabs(yi) <= dy) {
        roots.push_back(a + delta);
        while (f(a + delta) <= dy && delta <= (b - a)) {
          delta += dx;
        }
        fL = f(a + delta);
      } else {
        delta += dx;
      }
    } else {

      const U x0 = a + delta;

      U p_dummy[1] = {0.0};
      U r0 = root_on_the_left(f_alias, p_dummy, x0, dx, dy);
      roots.push_back(r0);

      /*
      The root may be x0 itself, not on the left of x0.
      In this case fL must be updated so that the latter isn't counted twice
      */
      while (fabs(f(a + delta)) <= dy && delta <= (b - a)) {
        delta += dx;
      }

      /* updating fL to the first non-zero value after the found root */
      fL = f(a + delta);
    }
  }

  assert((int)roots.size() > 0 && "No roots were found.");
  return roots;
}

template <class U>
std::vector<U> find_roots(std::function<U(const U &)> f, const U &y0,
                          const U &a, const U &b, const U &dx, const U &dy) {
  /* g(x) = f(x) - y0 */
  const std::function<U(const U &)> f_sub = [&](const U &x) {
    return (f(x) - y0);
  };
  return roots_interval(f_sub, a, b, dx, dy);
}

template <class U>
U find_root(std::function<U(const U &)> f, const U &y0, const U &a, const U &b,
            const U &dx, const U &dy) {
  /*! std::vector of all roots */
  const std::vector<U> all_roots = find_roots(f, y0, a, b, dx, dy);
  const int nr = (int)all_roots.size(); /*!< number of all roots found */

  /*! check if only one root was found */
  try {
    if (nr == 0) {
      const std::string e0 =
          "No solution was found in the specified inteval.\n";
      throw e0;
    }
    if (nr > 1) {
      std::cerr << "Roots:\n";
      for (U ri : all_roots) {
        std::cout << ri << "\n";
      }

      std::string e1 = "The solution to the equation isn't unique.\n";
      e1 += "More than one root was found.\n";

      throw e1;
    }
    return all_roots[0];
  } catch (const std::string &e) {
    std::cerr << e << "\n";
    std::cerr << "Aborting."
              << "\n";
    abort();
  }
} // find_root()

#endif
} // namespace Functions
} // namespace Silibs2
