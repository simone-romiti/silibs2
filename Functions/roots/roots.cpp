// roots.cpp
// This file contains the definition of what is declared in roots.h

#include "roots.hpp"

namespace Silibs2 {
namespace Functions {
namespace cpp_partial_template_definition {
#ifndef Silibs2_Functions_roots_cpp
#define Silibs2_Functions_roots_cpp

auto N_Derive_double = N_Derive<double>;
auto FindRootInRange_double = FindRootInRange<double>;
auto root_on_the_left_double = root_on_the_left<double>;
auto root_on_the_right_double = root_on_the_right<double>;
auto roots_interval_double = roots_interval<double>;
auto find_roots_double = find_roots<double>;
auto find_root_double = find_root<double>;

#endif
} // namespace cpp_partial_template_definition
} // namespace Functions
} // namespace Silibs2
