// lambdify.h
/*
This file declares a class that lambdifies an std::string to an std::function.
The main scope of this class is to give the possibility
to pass std::string objects to the main program inside input files,
so that one doesn't need to re-compile every time a function is changed.
*/

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem/string_file.hpp>
#include <boost/lexical_cast.hpp>

namespace Silibs2 {
namespace lambdify {

class expr_parser {
  std::string CODE; // code as and std::string

public:
  expr_parser() {}
  ~expr_parser() {}

  expr_parser(const std::string &code) { CODE = this->strip(code); }

  // remove spaces, tabs and newlines from
  std::string strip(const std::string &code) {
    std::string expr = code;
    for (std::string c : {" ", "\t", "\n"}) {
      const char cc = boost::lexical_cast<char>(c);
      expr.erase(std::remove(expr.begin(), expr.end(), cc), expr.end());
    }
    return expr;
  }

  // variable to value
  double var2val(const double &x, const std::string &x_name,
                 const std::string &code) {
    const std::string expr = this->strip(code);
    if (expr == x_name) {
      return x;
    } else {
      return boost::lexical_cast<double>(expr);
    }
  } // expr2val()

  // process '+' and '-'
  double parse_pm(const double &x, const std::string &x_name,
                  const std::string &code) {
    const std::string expr = this->strip(code);
    double res = 0.0;
    std::vector<std::string> vp;
    boost::split(vp, expr, boost::is_any_of("+"));
    for (std::string vi : vp) {
      std::vector<std::string> vm;
      boost::split(vm, vi, boost::is_any_of("-"));
      const int Nm = (int)vm.size();
      double res2 = var2val(x, x_name, vm[0]);
      for (int j = 1; j < Nm; ++j) {
        res2 -= var2val(x, x_name, vm[j]);
      }
      res += res2;
    }
    return res;
  } // pars_plus()

  // variable to std::function
  std::function<double(double)> var2fun(const std::string &x_name) {
    const std::function<double(double)> f = [=](const double &x) {
      return this->parse_pm(x, x_name, CODE);
    };
    return f;
  } // expr2val()
};

} // namespace lambdify
} // namespace Silibs2
