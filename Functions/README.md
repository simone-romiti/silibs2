# Silibs2::Functions

In this folder are defined functions belonging to the namespace **Silibs2::Functions**. They are generic function, to be used as they are and called in the other Silibs2 classes. Wherever possible, they are implemented as template functions.

They are organized in subsections, each one corresponding to a sub-folder in which the header files are stored. They can be imported:

1. Individually from the *.hpp* files in each sub-folder, e.g.

    ``` cpp
    #include "path/to/Silibs2/Functions/files/files.hpp"
    ```
2. Globally, simply including the header *functions.hpp*:

    ``` cpp
    #include "path/to/Silibs2/Functions/functions.hpp"
    ```

The sub-folders are:

*   **containers**

    Dealing with containers (in practice with std::vector objects).

*   **files**

    Reading and writing utilities for files.

*   **integration**

    Numerical integration.

*   **lattice**

    Useful functions used in the analysis of euclidean lattice correlators.

*   **math**

    Mathematical functions.

*   **random**

    Random number generation using the c++ standard routines.

*   **roots**

    Root-finding algorithms for functions.

*   **statistics**

    Statistical functions, including some routines for jackknife manipulation.


## Pre-compiled headers

The creation and usage of a pre-compiled header generated from the global file _functions.hpp_ works.
In orger to generate it simply give (from the path **Silibs2/Functions/**):

``` bash
g++ functions.hpp -o functions.hpp.gch
```
