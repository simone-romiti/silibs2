// containers.h
/* Utilities which manipulate containers */

#include <functional>
#include <vector>

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_containers_h
#define Silibs2_Functions_containers_h

/*! prints on std::cout the elements of v with separation 'sep' */
template <class U = double>
void print_cout(const std::vector<U> &v, const std::string &sep = "\n");
template <class U = double>
void print_cerr(const std::vector<U> &v, const std::string &sep = "\n");

/*! returns {a, a+step, a+(2*step), ... , a+(n*step) < b} */
template <class U, class V = int>
std::vector<U> range(const U &a, const U &b, const V &step = 1);

/*! returns range(a, b, (b-a)/N) */
template <class U = double>
std::vector<U> linspace(const U &a, const U &b, const int &N);

/*! Product of the components in an object of 'n' terms (starting from in) */
template <class U = double> U prod(const U *p, int n, int in = 0);

//!< Multiplication of a vector by a number
template <class U = double> std::vector<U> mult(U lambda, std::vector<U> v0);

//!< dot product
template <class U = double>
U dot_product(const std::vector<U> &a, const std::vector<U> &b);

template <class U = double>
std::vector<U>
rm_comp(int c0, std::vector<U> v0); //!< Remove single component from object
/*!< Removes a component from an vector of size 'n'
   and returns the resulting one (of size 'n-1').
 */

template <class U = double>
std::vector<U> cut(std::vector<U> v0, int n, int l, int r); //!< Cut components
/*!< Returns the components of a vector
   from 'l' (included) to 'r' (excluded)
 */

//! (euclidean) norm squared of a vector
template <class U = double> U norm_squared(const std::vector<U> &x);
//! (euclidean) norm of a vector
template <class U = double> U norm(const std::vector<U> &x);

/*! concatenate vectors */
template <class U = double>
std::vector<U> concatenate(const std::vector<std::vector<U>> &list);

/*! ------------------------- */
/*! Functions for Z^3 vectors */
/*! ------------------------- */

//! Removes repetitions from a vector of objects
template <class U = double>
std::vector<U> rm_reps(std::vector<U> x, int in = 0);

//!< Gives the number of repetitions in a vector of numbers
template <class U = double> std::vector<int> reps(std::vector<U> x);

//! Multiplicity of a vector (of 'Itg' (integer) values, i.e. in Z^3 )
//! when we are looking only to its norm
template <class Itg = int> Itg multip(const std::vector<Itg> &nu);

/*!
This function returns a vector whose components are the multeplicities of
the relative index, (when we look only a the norm squared of the vector).
The maximum norm squared considered is the integer part of 'max'
( For Z^3 vectors [n1,n2,n3] )
*/
template <class U = double> std::vector<int> multips3(U max);

//! sum over integers smaller than a cutoff
template <class U = double>
U sum(U (*f)(int n0, U *p0), U *p, U Lambda, int in = 0);

//! sum over integers smaller than a cutoff
template <class U = double>
U sum(U (*f)(const int &n0, U *p0), U *p, U Lambda, int in = 0);

//! sum over integers smaller than a cutoff
template <class U = double>
U sum(const std::function<U(int, U *)> &f, U *p, U Lambda, int in = 0);

//! minimum value in the vector
template <class U = double> U min_value(const std::vector<U> &v);

//! maximum value in the vector
template <class U = double> U max_value(const std::vector<U> &v);

// /*! cast all the elements of a vector and stores them into another */
template <class T1, class T2>
std::vector<T1> change_type(const std::vector<T2> &v);

/*! alias for boost::lexical_cast */
template <class T1, class T2>
std::vector<T1> boost_cast(const std::vector<T2> &v);

// returns the number of occurrencies of 'x' in 'v'
template <class U> int occurrencies(const U &x, const std::vector<U> &v);

// returns true is 'x' is contained in 'v'
template <class U> bool is_contained(const U &x, const std::vector<U> &v);

#endif
} // namespace Functions
} // namespace Silibs2
