// containers.hpp
/* definitions of what declared in containers.h */

#include "../math/math.hpp"
#include "./containers.h"
#include <boost/lexical_cast.hpp>

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_containers_hpp
#define Silibs2_Functions_containers_hpp

template <class U>
void print_cout(const std::vector<U> &v, const std::string &sep) {
  for (U v_i : v) {
    std::cout << v_i << sep;
  }
  std::cout << "\n";
}

template <class U>
void print_cerr(const std::vector<U> &v, const std::string &sep) {
  for (U v_i : v) {
    std::cout << v_i << sep;
  }
  std::cout << "\n";
}

template <class U, class V>
std::vector<U> range(const U &a, const U &b, const V &step) {
  if (a > b) {
    std::cerr << "Error. Cannot create range of numbers.\n";
    std::cerr << "a : " << a << "\n";
    std::cerr << "b : " << b << "\n";
    std::cerr << "You should have a < b"
              << "\n";
    std::cerr << "Aborting"
              << "\n";
    abort();
  }

  std::vector<U> v(0);
  U n = 0;
  while (n * step < (b - a)) {
    v.push_back(a + n * step);
    ++n;
  }
  return v;
} // end range()

template <class U>
std::vector<U> linspace(const U &a, const U &b, const int &N) {
  std::vector<U> v(0);
  const U delta = (b - a) / (N + 0.0);
  for (int i = 0; i < N; ++i) {
    v.push_back(a + delta * i);
  }
  return v;
}

template <class U> U prod(const U *p, int n, int in) {
  if (n > in) {
    return p[in] * prod(p, n, in + 1); // recursive call
  } else {
    return 1; // The first index overtakes the last one.
              // (note that the last element is p[n-1] )
  }
} // end prod()

template <class U> std::vector<U> mult(U lambda, std::vector<U> v0) {

  int n = v0.size();
  std::vector<U> res;
  res.resize(n);

  for (int i = 0; i < n; i++) {

    res[i] = lambda * v0[i];
  }

  return res;

} // end mult()

template <class U>
U dot_product(const std::vector<U> &a, const std::vector<U> &b) {
  const int na = a.size();
  const int nb = b.size();
  if (!(na == nb)) {
    std::cerr
        << "Error. Cannot do the dot product of vectors with different sizes."
        << "\n";
    std::cerr << "a.size() : " << na << "\n";
    std::cerr << "b.size() : " << nb << "\n";
    std::cerr << "Aborting()"
              << "\n";
    abort();
  }

  U p = 0.0;
  for (int i = 0; i < nb; ++i) {
    p += a[i] * b[i];
  }
  return p;
} // dot_product()

template <class U> std::vector<U> rm_comp(int c0, std::vector<U> v0) {
  int n = v0.size();
  std::vector<U> res(n - 1, 0);
  if (c0 >= n) {
    std::cerr << "Invalid size of vector passed to function.\n";
    std::cerr << "A vector with all components equal to '0' has been returned."
              << std::endl;
    return res;
  } else {
    // components before c0
    for (int a = 0; a < c0; a++) {
      res[a] = v0[a];
    }
    // components after c0
    for (int b = c0; b < n - 1; b++) {
      res[b] = v0[b + 1];
    }
    return res;
  }
} // end rm_comp()
//----------------

template <class U> std::vector<U> cut(std::vector<U> v0, int n, int l, int r) {

  std::vector<U> res(n, 0);

  bool size_ok =
      (n >= r) && (r >= l) && (l >= 0); // true if method is applicable

  int n_v0 = v0.size();
  if (n_v0 != n) {
    std::cerr << "Invalid size of vector passed to function.\n";
    std::cerr << "A vector with all components equal to '0' has been returned."
              << std::endl;
    return res;
  } else {
    if (!size_ok) {
      std::cerr << "Invalid cut.\n";
      std::cerr
          << "Left and right bounds incompatible with the size of the vector."
          << std::endl;
    } else {
      int n_f = r - l; // final size of the vector
      res.resize(n_f); // resizing vector
      for (int i = 0; i < n_f; i++) {
        res[i] = v0[i + l];
      }
    }
    return res;
  }
} // end cut()

template <class U> U norm_squared(const std::vector<U> &x) {

  int n = x.size();
  U m = 0;
  for (int i = 0; i < n; i++) {
    m += std::pow(x[i], 2);
  }
  return m;
} // end norm_squared()

template <class U> U norm(const std::vector<U> &x) {
  return sqrt(norm_squared(x));
} // end norm()

template <class U>
std::vector<U> concatenate(const std::vector<std::vector<U>> &list) {
  std::vector<U> v(0);
  for (std::vector<U> x : list) {
    for (int i = 0; i < (int)x.size(); ++i) {
      v.push_back(x[i]);
    }
  }
  return v;
} // end concatenate()

template <class U> std::vector<U> rm_reps(std::vector<U> x, int in) {

  // size of vector passed (gets updated each time the function is called)
  int n = x.size();
  if (n > in) {
    // erase components from the end
    // no problems in adjusting consequently the value of the index 'i'
    for (int i = n - 1; i > in; i--) {
      if (x[i] == x[in]) {
        x.erase(x.begin() + i);
      }
    }
    return rm_reps(x, in + 1);
  } else {
    return x;
  }
} //	 end rm_reps()

template <class U> std::vector<int> reps(std::vector<U> x) {
  int n = x.size();

  std::vector<U> shrunk = rm_reps<U>(x);
  int m = shrunk.size();

  std::vector<int> res(
      m, 0); // res[i] = number of times shrunk[i] is appears in 'x'

  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      if (x[j] == shrunk[i]) {
        res[i]++;
      }
    }
  }
  return res;
} // end reps()

template <class Itg> Itg multip(const std::vector<Itg> &nu) {

  Itg n = nu.size();

  std::vector<Itg> x_R =
      rm_reps<Itg>(nu);               // elements of 'nu' without repetitions
  std::vector<Itg> R = reps<Itg>(nu); // repetitions of each element
  // now x_r and R form a 'tabular',
  // where each component of 'nu' is associated with
  // the number of times it repeats itself in 'nu'

  Itg m = R.size();

  Itg mult_0 =
      std::pow(2, n) * Silibs2::Functions::factorial(n); // default multiplicity
  //	(all components different and different from '0')

  Itg fact = 1;

  for (Itg i = 0; i < m; i++) {
    if (x_R[i] == 0) {
      fact *= std::pow(2, R[i]);
    }
    fact *= factorial(R[i]);
  }
  Itg M = mult_0 / fact;
  return M;
} // end multip()

template <class U> std::vector<int> multips3(U max) {

  int M = (int)max;
  std::vector<int> v(M + 1, 0);
  std::vector<int> w(M + 1, 0);

  std::vector<int> n(3);
  int c; // component of 'v' I'm selecting

  for (int i1 = 0; i1 <= M; i1++) {
    for (int i2 = 0; i2 <= i1; i2++) {
      for (int i3 = 0; i3 <= i2; i3++) {

        n = {i1, i2, i3};
        c = norm_squared(n);
        if (c <= M) {
          // sets component 'c' since it has been found a 3-ple whose norm
          // squared is 'c'
          v[c] += multip(n);
        }
      }
    }
  }
  return v;
} // end multips3()

template <class U> U sum(U (*f)(int n0, U *p0), U *p, U Lambda, int in) {
  U s = 0;
  for (int i = in; i <= Lambda; i++) {
    s += f(i, p);
  }
  return s;
} // end sum()

template <class U> U sum(U (*f)(const int &n0, U *p0), U *p, U Lambda, int in) {
  U s = 0;
  for (int i = in; i <= Lambda; i++) {
    s += f(i, p);
  }
  return s;
} // end sum()

template <class U>
U sum(const std::function<U(int, U *)> &f, U *p, U Lambda, int in) {
  // int N = (int) Lambda;
  U s = 0;
  for (int i = in; i <= Lambda; i++) {
    s += f(i, p);
  }
  return s;
} // end sum()

//! minimum value in the vector
template <class U> U min_value(const std::vector<U> &v) {
  return *std::min_element(v.begin(), v.end());
}

//! maximum value in the vector
template <class U> U max_value(const std::vector<U> &v) {
  return *std::max_element(v.begin(), v.end());
}

template <class T1, class T2>
std::vector<T1> change_type(const std::vector<T2> &v) {
  int n = v.size();
  std::vector<T1> V(n);
  for (int i = 0; i < n; ++i) {
    V[i] = static_cast<T1>(v[i]);
  }
  return V;
} // end change_type()

template <class T1, class T2>
std::vector<T1> boost_cast(const std::vector<T2> &v) {
  int n = v.size();
  std::vector<T1> V(n);
  for (int i = 0; i < n; ++i) {
    V[i] = boost::lexical_cast<T1>(v[i]);
  }
  return V;
} // end change_type()

template <class U> int occurrencies(const U &x, const std::vector<U> &v) {
  return std::count(v.begin(), v.end(), x);
}

template <class U> bool is_contained(const U &x, const std::vector<U> &v) {
  const int n = occurrencies(x, v);
  if (n > 0) {
    return true;
  } else {
    return false;
  }
}

#endif
} // namespace Functions
} // namespace Silibs2
