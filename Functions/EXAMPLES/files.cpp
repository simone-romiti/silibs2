// files.cpp
/* examples with the routines in ../files/files.hpp */

#include "../functions.hpp"

int main() {
  std::cout << "Examples with the routines of files.hpp"
            << "\n";
  using namespace Silibs2::Functions;

  { /*! rows and columns of files */
    const std::string file1 = "./input/01.txt";
    const int r = rows_file(file1);
    const int c = columns_file(file1, "\t");
    std::cout << "The file " << file1 << " contains  :\n";
    std::cout << r << " rows\n";
    std::cout << c << " columns\n";

    std::pair<std::string, std::string> pa = deduce_dir_file(file1);
    std::string fld_ded = pa.first;
    std::string file_ded = pa.second;

    std::cout << "You read the file " << file_ded << " located in the folder "
              << fld_ded << "\n";

    std::string file2 = fld_ded + "01_bis.txt";
    std::cout << "creating the file " << file2 << "\n";
    touch(file2, true);

    std::string word1 = "row";
    int occ = find_occurencies(word1, file1);
    std::cout << "In " << file1 << "the word " << word1 << " appears " << occ
              << " times.\n";

    std::cout << "Let's check it..."
              << "\n";
    check_occurencies(occ, word1, file1);
    std::cout << "Check done."
              << "\n";

    std::string par_name = "parameter";
    std::cout << "getting the parameter " << par_name << "\n";
    std::cout << par_name << " = " << get<double>(par_name, file1) << "\n";

    std::cout << "getting the vector \n";
    std::vector<double> vec = get_vector<double>("vector", file1);
    for (double v : vec) {
      std::cout << v << "\n";
    }

    std::cout << "Clearing the content of " << file2 << "\n";
    clear_content(file2);

    std::string end_file2 = "end of file2";
    std::cout << "Done. Now appending a string to " << file2 << "\n";
    append_string(end_file2, file2);
  }

  return 0;
}
