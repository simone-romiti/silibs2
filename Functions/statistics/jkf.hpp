// jkf_utils.hpp
/* This file defines what is declared in jkf_utils.h */

#include "./jkf.h"
#include "./stats.hpp"

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_jkf_hpp
#define Silibs2_Functions_jkf_hpp

template <typename U> std::vector<U> make_jkf(std::vector<U> v, int N_jkf) {
  int N = v.size();
  if (N % N_jkf != 0) {
    std::cerr << "Invalid number of jackknifes.\n";
    std::cerr << "It must be a divisor of the total number of data points.\n";
    abort();
  }
  int Dim_block = N / N_jkf;
  int N_tot = N - Dim_block;

  std::vector<U> v_jkf(N_jkf);
  for (int k = 0; k < N_jkf; k++) {

    U sum1 = std::accumulate(v.begin(), v.begin() + k * Dim_block, 0.0);
    U sum2 = std::accumulate(v.begin() + (k + 1) * Dim_block, v.end(), 0.0);

    v_jkf[k] = (sum1 + sum2) / (N_tot + 0.0);
  }

  return v_jkf;
} // end make_jkf()

template <class U> U error_jackknives_0(int N_jack, U *p) {

  U N = N_jack;
  U A = (N - 1.0) / N;

  U sum = sum_diff_squared(N_jack, p + 1, p[0]);

  U err = sqrt(A * sum);
  return err;

} // end error_jackknives_0()

template <class U> U error_jkf(const std::vector<U> &v) {
  U N = (U)v.size(); /* N_jkf */
  U A = (N - 1.0) / (N + 0.0);
  U av = average(v); /* Average of jackknifes */

  U sum_sqrs = 0.0;
  for (U value : v) {
    // NOTE: std::pow may not work with multi-precision libraries
    sum_sqrs += (value - av) * (value - av);
  }
  sum_sqrs *= A;

  return sqrt(sum_sqrs);
} // end error_jkf()

#endif
} // namespace Functions
} // namespace Silibs2
