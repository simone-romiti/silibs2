// jkf.h
/*!
This file contains the declaration of functions
useful to deal with the jackknife resampling of data
*/

#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "./stats.h"

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_jkf_h
#define Silibs2_Functions_jkf_h

// Returns the std::vector of jackknifes starting a std::vector of gauge confs
template <typename U> std::vector<U> make_jkf(std::vector<U> v, int N_jkf);

//!< Returns the jackknife error over a set of values
//!< whose first is the average of the others
template <class U>
U error_jackknives_0(int N_jack, /*Number of jackknives */
                     U *p        /*!< Pointer to value */
);

// Returns the jackknife error of the jackknifes in v
// (which must not contain the average)
template <class U> U error_jkf(const std::vector<U> &v);

#endif
} // namespace Functions
} // namespace Silibs2
