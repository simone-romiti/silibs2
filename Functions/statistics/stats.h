// stats.h
/*
This header declares functions
useful for statistical analysis of data
*/

#include <iostream>
#include <numeric>
#include <vector>

// #include <boost/algorithm/string.h>
// #include <boost/lexical_cast.h>

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_stats_h
#define Silibs2_Functions_stats_h

//!< Aritmetical average over a set of values:
/*!< those from val[start] (included) to val[end] (excluded) */
template <class U = double> U average(U *val, int start, int end);

template <class U = double> U average(const std::vector<U> &v);

template <class U = double>
U weighted_average(const std::vector<U> &x, const std::vector<U> &w);

/*
Average over the values of vector v,
from v[a] (included) to v[b] (excluded)
*/
template <class U = double>
U average_range(const std::vector<U> &v, const int &a, const int &b);

/*!
Aritmetical average over a set of values:
those from val[0] to val[n_i - 1]
and those from val[n_f] to val[n-1];
n_tot = total number of sets
*/
template <class U = double> U average_rm(U *val, int n_tot, int n_i, int n_f);

template <class U = double>
U sum_diff_squared(int n, /*!< Number of values */
                   U *p,  /*!< Pointer to values */
                   U m    /*!< Mean of values */
);

#endif
} // namespace Functions
} // namespace Silibs2
