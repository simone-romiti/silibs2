// jkf_utils.cpp
/* This file defines what is declared in jkf_utils.h */

#include "./jkf.hpp"

namespace Silibs2 {
namespace Functions {
namespace cpp_partial_template_definition {
#ifndef Silibs2_Functions_jkf_cpp
#define Silibs2_Functions_jkf_cpp

auto make_jkf_double = make_jkf<double>;
auto error_jackknives_0_double = error_jackknives_0<double>;
auto error_jkf_double = error_jkf<double>;

#endif
} // namespace cpp_partial_template_definition
} // namespace Functions
} // namespace Silibs2