// stats.hpp
/*
This file defines what is declared in stats.h
*/
#include "stats.h"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_stats_hpp
#define Silibs2_Functions_stats_hpp

template <class U> U average(U *val, int start, int end) {
  U n = (end - start) + 0.0; // total number of values
                             // is has a +0.0 to avoid problems with the
                             // division in case of a set of (int) [see below]

  // the average is meaningful only if 'n>0'; otherwise is '0'
  if (n > 0) {
    U A = 0;
    for (int i = start; i < end; i++) {
      A += val[i] / n;
    }
    return A;

  } else {
    return 0; // no values to average
  }
} // end average()

template <class U> U average(const std::vector<U> &v) {
  int n = v.size();
  U avr = 0; // returned average

  // true if all the elements are equal
  // to be precise, it checks if the first element which is equal to its
  // adjacent is the last one (thet is, there are no elements equal to their
  // adjacent)
  const bool all_equal =
      (std::adjacent_find(v.begin(), v.end(), std::not_equal_to<U>()) ==
       v.end());

  if (n != 0 && all_equal) {
    avr = v[0];
  } else {
    for (int i = 0; i < n; i++) {
      avr += v[i] / (n + 0.0);
    }
  }
  return avr;
}

template <class U>
U average_range(const std::vector<U> &v, const int &a, const int &b) {
  int N = v.size();
  int n = b - a;
  if (n > N) {
    std::cerr << "Indexes out of range:\n";
    std::cerr << "first(included) : v[" << a << "]\n";
    std::cerr << "last(not included) : v[" << b << "]\n";
    std::cerr << "size of vector : " << v.size() << "\n";
    abort();
  }

  std::vector<U> sub_v(v.begin() + a, v.begin() + b);
  return average(sub_v);
} // end average()

template <class U>
U weighted_average(const std::vector<U> &x, const std::vector<U> &w) {
  assert(x.size() == w.size() &&
         "Error: number of values != number of weights.");
  int N = x.size();
  U num = 0.0, den = 0.0;
  for (int i = 0; i < N; i++) {
    num += x[i] * w[i];
    den += w[i];
  }
  return (num / den);
} // end weighted_average()

template <class U> U average_rm(U *val, int n_tot, int n_i, int n_f) {

  int n1 = n_i;         // size of the first block of values
  int n2 = n_tot - n_f; // size of the second block of values

  // the "array" val[] is splitted in 2 pieces.
  // The average can be evaluated through the averages over the two pieces:

  U A = average(val, 0, n1);      // values from '0' to 'n1-1'
  U B = average(val, n_f, n_tot); // values from 'n_f' to 'n_tot-1'

  U n_cons = n2 + n1; // total number of points condidered
                      // (once removed the block)
  U result = (n1 * A + n2 * B) / n_cons;
  return result;
} // end average_rm()

template <class U> U sum_diff_squared(int n, U *p, U m) {
  U sum = 0;
  U diff; //!< Dummy variable
  for (int i = 0; i < n; i++) {
    diff = (p[i] - m);
    sum += pow(diff, 2.0);
  }
  return sum;
} // end sum_diff_squared()

#endif
} // namespace Functions
} // namespace Silibs2
