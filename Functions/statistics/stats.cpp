// stats.cpp
/* This file defines what is declared in stats.h */

#include "stats.hpp"

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_stats_cpp
#define Silibs2_Functions_stats_cpp
namespace cpp_partial_template_definition {

double (*average_double_1)(double *, int, int) = average<double>;
double (*average_double_2)(const std::vector<double> &) = average<double>;
auto average_range_double = average_range<double>;
auto weighted_average_double = weighted_average<double>;
auto average_rm_double = average_rm<double>;
auto sum_diff_squared_double = sum_diff_squared<double>;

#endif
} // namespace Functions
} // namespace Functions
} // namespace Silibs2
