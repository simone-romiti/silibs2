// physics.h
/* generic routines for physics: physical constants, etc. */

#include <cmath>
#include <iostream>
#include <vector>

#include "../math/math.h"

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_physics_h
#define Silibs2_Functions_physics_h

const double hbar_c_MeV_fm = 197.3269788;
const double hbar_c_GeV_fm = hbar_c_MeV_fm / 1000;

const double Mpi_plus_phys_MeV = 139.57039;
const double Mpi_0_phys_MeV = 134.9768;
const double Mpi_bar_phys_MeV =
    Functions::quadratic_average(Mpi_plus_phys_MeV, Mpi_0_phys_MeV);

const double MK_plus_phys_MeV = 493.677;
const double MK_0_phys_MeV = 497.611;
const double MK_bar_phys_MeV =
    Functions::quadratic_average(MK_plus_phys_MeV, MK_0_phys_MeV);

const double M_Omega_phys_MeV = 1672.45;
const double M_Omega_phys_GeV = M_Omega_phys_MeV / pow(10, 3);

/*
returns the value of the quantity in 'MeV', given its value in 'GeV'
i.e. returns x_GeV * 10^3
*/
template <class U = double> U GeV_to_MeV(const U &x_GeV);

/*
returns the value of the quantity in 'MeV^-1', given its value in 'GeV^-1'
i.e. returns x_GeV * 10^{-3}
*/
template <class U = double> U GeV_inv_to_MeV_inv(const U &x_GeV_inv);

/*
returns the value of the quantity in 'GeV', given its value in 'MeV'
i.e. returns x_GeV * 10^{-3}
*/
template <class U = double> U MeV_to_GeV(const U &x_MeV);

/*
returns the value of the quantity in 'GeV^-1', given its value in 'MeV^-1'
i.e. returns x_GeV * 10^{+3}
*/
template <class U = double> U MeV_inv_to_GeV_inv(const U &x_MeV_inv);

/*
returns the value of the quantity in 'GeV^-1', given its value in 'fm'
i.e. returns x_GeV_inv / hbar_c_GeV_fm
*/
template <class U = double> U fm_to_GeV_inv(const U &x_fm);

/*
returns the value of the quantity in 'fm', given its value in 'GeV^-1'
i.e. returns x_GeV_inv * hbar_c_GeV_fm
*/
template <class U = double> U GeV_inv_to_fm(const U &x_GeV_inv);

/*
returns the value of the quantity in 'MeV^-1', given its value in 'fm'
i.e. returns x_MeV_inv / hbar_c_MeV_fm
*/
template <class U = double> U fm_to_MeV_inv(const U &x_fm);

/*
returns the value of the quantity in 'fm', given its value in 'MeV^-1'
i.e. returns x_MeV_inv * hbar_c_MeV_fm
*/
template <class U = double> U MeV_inv_to_fm(const U &x_MeV_inv);

#endif
} // namespace Functions
} // namespace Silibs2