// physics.hpp

#include "../math/math.hpp"
#include "./physics.h"

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_physics_hpp
#define Silibs2_Functions_physics_hpp

template <class U = double> U GeV_to_MeV(const U &x_GeV) {
  return x_GeV * std::pow(10, 3);
}

template <class U = double> U GeV_inv_to_MeV_inv(const U &x_GeV_inv) {
  return x_GeV_inv * std::pow(10, -3);
}

template <class U = double> U MeV_to_GeV(const U &x_MeV) {
  return x_MeV * std::pow(10, -3);
}

template <class U = double> U MeV_inv_to_GeV_inv(const U &x_MeV_inv) {
  return x_MeV_inv * std::pow(10, +3);
}

template <class U = double> U fm_to_GeV_inv(const U &x_fm) {
  return x_fm / hbar_c_GeV_fm;
}

template <class U = double> U GeV_inv_to_fm(const U &x_GeV_inv) {
  return x_GeV_inv * hbar_c_GeV_fm;
}

template <class U = double> U fm_to_MeV_inv(const U &x_fm) {
  return x_fm / hbar_c_MeV_fm;
}

template <class U = double> U MeV_inv_to_fm(const U &x_MeV_inv) {
  return x_MeV_inv * hbar_c_MeV_fm;
}

#endif
} // namespace Functions
} // namespace Silibs2