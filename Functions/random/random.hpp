// random.hpp
/* This file defines what declared in random.h */

#include <random>

#include "./random.h"

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_random_hpp
#define Silibs2_Functions_random_hpp

template <class U> std::vector<U> gauss_distributed(int n, U m, U sigma) {

  std::vector<U> v(0);

  std::random_device rd;
  std::mt19937 gen(rd());
  std::normal_distribution<U> dis(m, sigma);

  U g;
  for (int i = 0; i < n; i++) {

    g = dis(gen);
    v.push_back(g);
  }

  return v;

} // end gauss_distributed()

#endif
} // namespace Functions
} // namespace Silibs2
