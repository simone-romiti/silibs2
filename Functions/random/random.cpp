// random.cpp
/* This file defines what declared in random.h */

#include "./random.hpp"

namespace Silibs2 {
namespace Functions {
namespace cpp_partial_template_definition {
#ifndef Silibs2_Functions_random_cpp
#define Silibs2_Functions_random_cpp

auto gauss_distributed_double = gauss_distributed<double>;

#endif
} // namespace cpp_partial_template_definition
} // namespace Functions
} // namespace Silibs2