// random.h
/*
this file defines functions which deal with
the propagation of the error in a multiplication
*/

#include <vector>

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_random_h
#define Silibs2_Functions_random_h

/*!
Generates a vector of 'n' gaussian numbers
with mean 'm' and std. dev. 'sigma'
*/
template <class U> std::vector<U> gauss_distributed(int n, U m, U sigma);

#endif
} // namespace Functions
} // namespace Silibs2
