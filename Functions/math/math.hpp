// math.hpp
/* definitions of what declared in math.h */

#include "./math.h"

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_math_hpp
#define Silibs2_Functions_math_hpp

template <class intgr = int> intgr factorial(intgr n) {
  if (n == 0) {
    return 1;
  } else if (n > 0) {
    return n * factorial(n - 1);
  } else {
    std::cerr << "Factorial doesn't accept negative numbers as arguments."
              << "\n";
    std::cerr << "Aborting"
              << "\n";
    abort();
  }
} // end factorial()

template <class U> U polynomial(const U &x, const std::vector<U> &a) {
  int N = a.size();
  U res = 0.0;
  for (int i = 0; i < N; ++i) {
    res += a[i] * std::pow(x, i);
  }
  return res;
} // end polynomial()

/* returns sqrt( (a^2 + b^2)/2 )*/
template <class U> U quadratic_average(const U &a, const U &b) {
  U a2 = pow(a, 2), b2 = pow(b, 2);
  return sqrt((a2 + b2) / 2);
} // end quadratic_average()

#endif
} // namespace Functions
} // namespace Silibs2
