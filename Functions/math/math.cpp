// math.cpp
/* definitions of what declared in math.h */

#include "./math.hpp"

namespace Silibs2 {
namespace Functions {

namespace cpp_partial_template_definition {
#ifndef Silibs2_Functions_math_cpp
#define Silibs2_Functions_math_cpp

auto factorial_int = factorial<int>;
auto polynomial_double = polynomial<double>;
auto quadratic_average_double = quadratic_average<double>;

#endif
} // namespace cpp_partial_template_definition
} // namespace Functions
} // namespace Silibs2
