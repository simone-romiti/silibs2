// math.h
/* declaration of generic functions */

#include <cmath>
#include <iostream>
#include <vector>

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_math_h
#define Silibs2_Functions_math_h

template <class intgr = int> intgr factorial(intgr n); // Factorial : n!

/*
Polynomial of degree n=a.size()-1,
i.e. the function returns P(x) = a[0] + a[1]*x + ... + a[n]*x^n
*/
template <class U> U polynomial(const U &x, const std::vector<U> &a);

/* returns sqrt( (a^2 + b^2)/2 )*/
template <class U> U quadratic_average(const U &a, const U &b);

#endif
} // namespace Functions
} // namespace Silibs2
