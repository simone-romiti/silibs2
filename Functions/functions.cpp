// functions.h
/*
Import this file if you want to have access
to all the functions in the namespace Silibs2::Functions
*/

#ifndef Silibs2_functions_cpp
#define Silibs2_functions_cpp

#include "./calculus/calculus.cpp"     /*!< numerical calculus */
#include "./containers/containers.cpp" /*!< containers manipulation */
#include "./files/files.cpp"           /*!< files manipulation */
#include "./files/smart_print.cpp" /*!< smart printing of average and error*/

#include "./lattice/lattice.cpp" /*!< functions useful for lattice correlators data analysis */

#include "./math/math.cpp"     /*!< generic mathematical functions */
#include "./random/random.cpp" /*!< random numbers */
#include "./roots/roots.cpp"   /*!< roots of functions */

#include "./statistics/jkf.cpp"   /*!< jackknifes */
#include "./statistics/stats.cpp" /*!< statistics */

#endif
