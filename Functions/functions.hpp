// functions.h
/*
Import this file if you want to have access
to all the functions in the namespace Silibs2::Functions
*/

#ifndef Silibs2_functions_hpp
#define Silibs2_functions_hpp

#include "./calculus/calculus.hpp"     /*!< numerical calculus */
#include "./containers/containers.hpp" /*!< containers manipulation */
#include "./files/files.hpp"           /*!< files manipulation */
#include "./files/smart_print.hpp" /*!< smart printing of average and error */
#include "./lattice/lattice.hpp"   /*!< analysis of lattice correlators */
#include "./math/math.hpp"         /*!< generic mathematical functions */
#include "./physics/physics.hpp"   /*!< generic physics functions */
#include "./random/random.hpp"     /*!< random numbers */
#include "./roots/roots.hpp"       /*!< roots of functions */
#include "./statistics/jkf.hpp"    /*!< jackknifes */
#include "./statistics/stats.hpp"  /*!< statistics */

#endif
