// file.cpp

#include "./files.hpp"

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_files_cpp
#define Silibs2_Functions_files_cpp

namespace cpp_partial_template_definition {

auto home_dir_0 = home_dir;
auto build_full_path_0 = build_full_path;
auto check_path_0 = check_path;
auto rows_file_0 = rows_file;
auto columns_file_0 = columns_file;
auto deduce_dir_file_0 = deduce_dir_file;
auto touch_0 = touch;
auto find_occurencies_0 = find_occurencies;
auto check_occurencies_0 = check_occurencies;
auto get_int = get<int>;
auto get_double = get<double>;
auto get_string = get<std::string>;
auto get_vector_int = get_vector<int>;
auto get_vector_double = get_vector<double>;
auto get_vector_string = get_vector<std::string>;
auto file_to_string_0 = file_to_string;
auto clear_content_0 = clear_content;
auto append_string_0 = append_string;
auto string_to_file_0 = string_to_file;
auto replace_substring_0 = replace_substring;
auto replace_substrings_0 = replace_substrings;

} // namespace cpp_partial_template_definition
#endif
} // namespace Functions
} // namespace Silibs2
