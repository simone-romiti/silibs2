// files.h
/* declarations of methos relative to input and output of files */

#include <iostream>
#include <vector>

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_files_h
#define Silibs2_Functions_files_h

/*!
returns the home directory of the user running the program,
e.g. '/home/user/'
*/
std::string home_dir();

/*!
Builds the full path to the file, i.e. returns: dir+rel_path,
Example:
rel_path = files/file1.txt, left = /home/user1/
--> returns /home/user1/files/file1.txt
*/
std::string build_full_path(const std::string &rel_path,
                            const std::string &dir = home_dir());

/*! ------- */
/*! Reading */
/*! ------- */

void check_path(const std::string &path, const bool &abort_if_not_found = true);

/*! Number of rows of the file, including empty lines */
int rows_file(const std::string &path, const int &ign_lines = 0);

/*!
Number of columns of the file:
they are calculated from the first non-empty row.
*/
int columns_file(const std::string &path, const std::string &sep = "\t",
                 const int &ign_lines = 0);

/*!
Deduction of directory and file names:
 dir/file --> (dir/, file)
 */
std::pair<std::string, std::string>
deduce_dir_file(const std::string &full_path);

/*!
Wrapper for the bash 'touch' command.
The directory 'dir' is deduced
and if mkdir==true created if not present
*/
void touch(const std::string &full_path, const bool &mkdir = true);

/*! Returns how many occurencies of the string 'word' appear in file 'path'*/
int find_occurencies(const std::string &word, const std::string &path);

/* checks if the string 'word' appears exactly n times in 'path')*/
void check_occurencies(const int &n, const std::string &word,
                       const std::string &path);

/*!
Returns the parameter following the string 'name_par' in the input file
if there is one (and no more) occurrency of "name_par"
(spaces, tabs or newlines are ignored ).
If 'name_par' is followed by more than one tokens, only the fist one is
returned. This is done in order to allow for comments after the value.
Example:
name_par   123.456   # this is an ignored comment
*/
template <class U>
U get(const std::string &name_par, const std::string &input_file);

/*!
Returns the vector of values (in the file 'input_file')
following the token 'word' until the newline
*/
template <class U>
std::vector<U> get_vector(const std::string &word,
                          const std::string &input_file);

/*! returns the content of a file into a std::string */
std::string file_to_string(const std::string &file);

/*! ------- */
/*! Writing */
/*! ------- */

/*! Clears the content of a file */
void clear_content(const std::string &file);

/*! appends a string to a file */
void append_string(const std::string &s, const std::string &file);

/*! prints the content of an std::string into a file */
void string_to_file(const std::string &s, const std::string &file);

/*!
Replaces all the occurrencies of the substring "sub" in 'file' with the
std::string 'rep'.
Then it saves the result in the file 'out'. If 'forced' is
true, the output can overwrite 'file' if the two coincide. Elsewise, an
exception is thrown.
*/
void replace_substring(const std::string &file, const std::string &sub,
                       const std::string &rep, const std::string &out,
                       const bool &forced = false);

/*! replace_substring() many couples of sub-rep strings */
void replace_substrings(const std::string &file,
                        const std::vector<std::string> &subs,
                        const std::vector<std::string> &reps,
                        const std::string &out, const bool &forced = false);

#endif
} // namespace Functions
} // namespace Silibs2
