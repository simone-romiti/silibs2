// smart_print.hpp

#include <string>
#include <vector>

namespace Silibs2 {
namespace Functions {
namespace smart_printing {
#ifndef Silibs2_Functions_files_smart_print_h
#define Silibs2_Functions_files_smart_print_h

//! keeping the dot fixed, shifts the number by 10 left (if digit is positive)
//! or right (if digit is negative) : (123.456, 2) --> 12345.6
template <class U = double> U shift_by_10(const U &x, const int &digit);

//! return x rounded to the digit (i.e. power of 10):
//! (18.32,-1) --> 18.3, (18.32,1) --> 20
template <class U = double> U round_to_digit(const U &x, const int &digit);

//! print the average and errors considering all rounding
template <class U = double>
std::string smart_print(const U &ave, const std::vector<U> &errors,
                        const int &ndigits);

#endif
} // namespace smart_printing
} // namespace Functions
} // namespace Silibs2
