// smart_print.hpp

#include <algorithm>
#include <cmath>
#include <sstream>

#include "smart_print.h"

namespace Silibs2 {
namespace Functions {
namespace smart_printing {
#ifndef Silibs2_Functions_files_smart_print_hpp
#define Silibs2_Functions_files_smart_print_hpp

//! keeping the dot fixed, shifts the number by 10 left (if digit is positive)
//! or right (if digit is negative) : (123.456, 2) --> 12345.6
template <class U> U shift_by_10(const U &x, const int &digit) {
  return x * pow(10, digit);
}

//! return x rounded to the digit (i.e. power of 10):
//! (18.32,-1) --> 18.3, (18.32,1) --> 20
template <class U> U round_to_digit(const U &x, const int &digit) {
  return shift_by_10(floor(shift_by_10(x, -digit) + 0.5), digit);
}

//! print the average and errors considering all rounding
template <class U>
std::string smart_print(const U &ave, const std::vector<U> &errors,
                        const int &ndigits) {
  // set fixed precision
  std::ostringstream out;
  out << std::fixed;

  // get the negative of pow of 10 needed to make all
  /// syst have at least 1 digit
  const U mel = *max_element(errors.begin(), errors.end());
  if (mel == 0)
    out << ave << "(0)";
  else if (isnan(mel))
    out << ave << "(nan)";
  else {
    const int lem = (int)ceil(log(mel) / M_LN10);
    const int digit_to_round = lem - ndigits;
    out.precision((lem < ndigits) * std::max(-digit_to_round, 0));

    // truncate
    out << round_to_digit(ave, digit_to_round);
    if (digit_to_round <= -ndigits)
      out.precision(0);
    for (auto err : errors)
      out << "("
          << shift_by_10(round_to_digit(err, digit_to_round),
                         (lem <= 0) * (ndigits - lem))
          << ")";
  }

  return out.str();
}

#endif
} // namespace smart_printing
} // namespace Functions
} // namespace Silibs2
