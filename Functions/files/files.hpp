// file.hpp
/* declarations relative to files.h */

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <sstream>
#include <string>

#include "./files.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem/string_file.hpp>
#include <boost/lexical_cast.hpp>

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_files_hpp
#define Silibs2_Functions_files_hpp

std::string home_dir() {
  return (boost::lexical_cast<std::string>(std::getenv("HOME")) + "/");
}

std::string build_full_path(const std::string &rel_path,
                            const std::string &dir) {
  return dir + rel_path;
}

/*! ------- */
/*! Reading */
/*! ------- */

void check_path(const std::string &path, const bool &abort_if_not_found) {
  std::ifstream IN(path);
  if (IN.fail()) {
    std::cerr << path << " : no such file or directory.\n";
    if (abort_if_not_found) {
      std::cerr << "Aborting."
                << "\n";
      abort();
    }
  }
  IN.close();
} // end check_path()

/* number of rows of the file, including empty lines */
int rows_file(const std::string &path, const int &ign_lines) {
  check_path(path);
  std::ifstream IN(path);
  int nr = 0;

  std::string line; /* dummy variable */
  for (int i = 0; i < ign_lines; i++) {
    getline(IN, line, '\n');
  }
  while (getline(IN, line, '\n')) {
    if (line.length() > 0) {
      nr++;
    }
  }

  return nr;
}

/* number of columns of the file, including empty lines*/
int columns_file(const std::string &path, const std::string &sep,
                 const int &ign_lines) {
  check_path(path);
  std::ifstream IN(path);
  int nc = 0;

  std::string line; /* dummy variable */
  for (int i = 0; i < ign_lines; i++) {
    getline(IN, line, '\n');
  }

  /* I take the first non-empty line to get the number of columns */
  std::vector<std::string> token;
  while (getline(IN, line, '\n')) {
    if (line.length() > 0) {
      boost::split(token, line, boost::is_any_of(sep));
      nc = token.size();
      break; /* exist the while loop: the number of columns has been found. */
    }
  }

  return nc;
} // end columns_file()

std::pair<std::string, std::string>
deduce_dir_file(const std::string &full_path) {
  std::vector<std::string> toks;
  boost::split(toks, full_path, boost::is_any_of("/"));
  int n_toks = toks.size();

  std::string dir = "";
  for (int i = 0; i < n_toks - 1; ++i) {
    dir += toks[i] + "/";
  }
  dir += "./";
  return make_pair(dir, toks.back());
} // end deduce_dir_file()

void touch(const std::string &full_path, const bool &mkdir) {

  const std::string dir = deduce_dir_file(full_path).first; /*!< directory */
  /*! Check for directory existence */
  std::ifstream DIR(full_path);
  if (DIR.fail() && mkdir) {
    std::string mkdir_cmd = "mkdir -p " + dir, touch_cmd = "touch " + full_path;

    int m = std::system(&mkdir_cmd[0]);
    m++; /* suppress warnings for unused variable */
    int f = std::system(&touch_cmd[0]);
    f++; /* suppress warnings for unused variable */
  }

  std::ifstream IN(full_path);
  if (IN.fail()) {
    std::cerr << "Cannot touch : " << full_path << "\n";
    abort();
  }
  IN.close();
} // end touch()

int find_occurencies(const std::string &word, const std::string &path) {
  check_path(path);
  std::ifstream IN(path);
  int n = 0;
  std::string token = "not " + word;
  while (!IN.eof()) {
    IN >> token;
    if (token == word) {
      ++n;
    }
  }
  IN.close();
  return n;
} // end find_occurencies()

void check_occurencies(const int &n, const std::string &word,
                       const std::string &path) {
  const int n_occ = find_occurencies(word, path);
  if (n_occ != n) {
    std::cerr << n_occ << " occurencies of "
              << "\'" << word << "\' in \'" << path << "\'\n";
    std::cerr << n << " expected.\n";
    abort();
  }
} // end check_occurencies()

template <class U>
U get(const std::string &name_par, const std::string &input_file) {
  check_occurencies(1, name_par, input_file);
  std::ifstream IN(input_file);
  std::string token =
      "not_" + name_par; /*!< forcing token != name_par at the beginning */
  while (token != name_par && !IN.eof()) {
    IN >> token;
  }
  U value;
  IN >> value;
  IN.close();
  return value;
} // end get()

template <class U>
std::vector<U> get_vector(const std::string &word,
                          const std::string &input_file) {
  check_occurencies(1, word, input_file);
  std::ifstream IN(input_file);
  std::string token = "not " + word; /* token != name_par at the beginning */
  while (token != word && !IN.eof()) {
    IN >> token;
  }

  std::string line;
  getline(IN, line);
  IN.close();

  std::istringstream ss(line);
  std::vector<U> v(0);
  while (ss >> token) {
    v.push_back(boost::lexical_cast<U>(token));
  }
  return v;
} // end get()

std::string file_to_string(const std::string &file) {
  std::string s = "";                           /*!< fill with file content */
  boost::filesystem::load_string_file(file, s); /*! filling */
  return s;
} // file_to_string()

/*! ------- */
/*! Writing */
/*! ------- */

void clear_content(const std::string &file) {
  std::ofstream ofs; /*!< creating the stream */
  ofs.open(file, std::ofstream::out | std::ofstream::trunc);
  ofs.close(); /*!< closing */
} // clear_content()

void append_string(const std::string &s, const std::string &file) {
  std::ofstream ofs;                  /*!< creating the stream */
  ofs.open(file, std::ios_base::app); /*!< opening with the appending option */
  ofs << s;                           /*!< appending the std::string */
  ofs.close();                        /*!< closing */
} // append_string()

void string_to_file(const std::string &s, const std::string &file) {
  touch(file);
  clear_content(file);    /*! clear residual content */
  append_string(s, file); /*! appending (to an empty file!) */
} // string_to_file()

void replace_substring(const std::string &file, const std::string &sub,
                       const std::string &rep, const std::string &out,
                       const bool &forced) {
  try {
    if (file == out && !forced) {
      std::string err =
          "Error in replace_substring(). Input and output file coincide:\n";
      err += file + "\n";
      err += "(To overwrite, call this functions with the forced "
             "option enabled)\n. Aborting.\n";
      throw err;
    }
  } catch (std::string e) {
    std::cerr << e;
    abort();
  }

  std::string s = Silibs2::Functions::file_to_string(file);
  boost::replace_all(s, sub, rep);
  string_to_file(s, out);
} // replace_substring()

void replace_substrings(const std::string &file,
                        const std::vector<std::string> &subs,
                        const std::vector<std::string> &reps,
                        const std::string &out, const bool &forced) {
  try {
    if (subs.size() != reps.size()) {
      std::string err_sz =
          "Error. The number of substring to replace doesn't equal the number "
          "of replacements.\n Aborting.\n";
      throw err_sz;
    }
    if (file == out && !forced) {
      std::string err_ow =
          "Error in replace_substrings(). Input and output file coincide:\n";
      err_ow += file + "\n";
      err_ow += "(To overwrite, call this functions with the forced "
                "option enabled).\n Aborting.\n";
      throw err_ow;
    }
  } catch (const std::string &e) {
    std::cerr << e << "\n";
    abort();
  }

  std::string s = Silibs2::Functions::file_to_string(file);
  int n = subs.size();
  for (int i = 0; i < n; ++i) {
    boost::replace_all(s, subs[i], reps[i]);
  }
  string_to_file(s, out);
} // replace_substrings()

#endif
} // namespace Functions
} // namespace Silibs2
