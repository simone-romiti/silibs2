// smart_print.hpp

#include "smart_print.hpp"

namespace Silibs2 {
namespace Functions {
namespace smart_printing {
namespace cpp_partial_template_definition {
#ifndef Silibs2_Functions_files_smart_print_cpp
#define Silibs2_Functions_files_smart_print_cpp

auto shift_by_10_double = shift_by_10<double>;
auto round_to_digit_double = round_to_digit<double>;
auto smart_print_double = smart_print<double>;

} // namespace cpp_partial_template_definition
#endif
} // namespace smart_printing
} // namespace Functions
} // namespace Silibs2
