// lattice.hpp
/* Definitions of lattice.h */

#include <cassert>
#include <cmath>
#include <iostream>

#include "lattice.h"

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_lattice_hpp
#define Silibs2_Functions_lattice_hpp

template <typename U> U leading_exp(const U &t, const U &A, const U &M) {
  return A * exp(-M * t);
} // end leading_exp()

template <typename U>
U leading_exp_back(const U &t, const U &A, const U &M, const U &p, const U &T) {
  U forward = leading_exp(t, A, M);
  U backward = p * leading_exp(T - t, A, M);
  return (forward + backward);
} // end leading_exp_back()

// /*!< Returns \sum_i Ai*( exp(-Ei*t) + p*exp(-Ei*(T-t)) ) */
// template <class U>
// U n_exp(const U &t, const std::vector<U> &Ai, const std::vector<U> &Ei) {
//   return n_exp_back(t, Ai, Ei, 0.0, 0.0); // p=0 -> T irrelevant
// }
//
// /*!< Returns \sum_i Ai*( exp(-Ei*t) + p*exp(-Ei*(T-t)) ) */
// template <class U>
// U n_exp_back(const U &t, const std::vector<U> &Ai, const std::vector<U> &Ei,
//              const U &p, const U &T) {
//   if (Ai.size() != Ei.size()) {
//     std::cerr << "Error: number of amplitudes != number of energies\n";
//     std::cerr << "Aborting.\n";
//     abort();
//   }
//   const int n = Ai.size();
//   U s = 0.0;
//   for (int i = 0; i < n; ++i) {
//     s += leading_exp_back(t, Ai[i], Ei[i], p, T);
//   }
//   return s;
// }

template <class U>
U lin_comb_decr_exponentials(const U &t, const std::vector<U> &A,
                             const std::vector<U> &E) {
  assert(A.size() == E.size() && "Cannot build linear combination.");
  int n = A.size();
  U res = 0.0;
  for (int i = 0; i < n; ++i) {
    res += A[i] * exp(-E[i] * t);
  }
  return res;
} // end lin_comb_exponentials()

template <class U> U symm_der(const U &y_for, const U &y_back) {
  return (y_for - y_back) / 2.0;
} // symm_der()

template <class U> U backward_der(const U &y_back, const U &y_0) {
  return (y_0 - y_back);
} // backward_der()

template <class U> U forward_der(const U &y_0, const U &y_forward) {
  return (y_forward - y_0);
} // backward_der()

template <class U>
std::vector<U> backward_der(const unsigned int &n, const std::vector<U> &y) {
  if (n > 0) {
    unsigned int T = y.size();
    std::vector<U> y_prime_def(T - 1);
    for (unsigned int i = 1; i < T; ++i) {
      y_prime_def[i] = backward_der(y[i - 1], y[i]);
    }
    std::vector<U> y_prime = backward_der(n - 1, y_prime_def);
    y_prime.insert(y_prime.begin(), 0.0);
    return y_prime;
  } else {
    return y;
  }
} // end backward_der()

template <class U>
std::vector<U> forward_der(const unsigned int &n, const std::vector<U> &y) {
  if (n > 0) {
    unsigned int T = y.size();
    std::vector<U> y_prime_def(T - 1);
    for (unsigned int i = 0; i < T - 1; ++i) {
      y_prime_def[i] = forward_der(y[i], y[i + 1]);
    }
    std::vector<U> y_prime = forward_der(n - 1, y_prime_def);
    y_prime.insert(y_prime.end(), 0.0);
    return y_prime;
  } else {
    return y;
  }
} // end forward_der()

template <class U>
std::vector<U> symm_der(const unsigned int &n, const std::vector<U> &y) {
  if (n > 0) {
    unsigned int T = y.size();
    std::vector<U> y_prime_def(T - 2);
    for (unsigned int i = 0; i < T - 2; ++i) {
      y_prime_def[i] = symm_der(y[i + 2], y[i]);
    }
    std::vector<U> y_prime = symm_der(n - 1, y_prime_def);
    y_prime.insert(y_prime.begin(), 0.0);
    y_prime.insert(y_prime.end(), 0.0);
    return y_prime;
  } else {
    return y;
  }
} // end symm_der()

template <class U>
std::vector<U> even_der_2n(const unsigned int &n, const std::vector<U> &y) {
  if (n > 0) {
    unsigned int T = y.size();
    std::vector<U> y_prime_def(T - 2);
    for (unsigned int i = 0; i < T - 2; ++i) {
      y_prime_def[i] = y[i + 2] + y[i] - 2.0 * y[i + 1];
    }
    std::vector<U> y_prime = even_der_2n(n - 1, y_prime_def);
    y_prime.insert(y_prime.begin(), 0.0);
    y_prime.insert(y_prime.end(), 0.0);
    return y_prime;
  } else {
    return y;
  }
} // end symm_der()

#endif
} // namespace Functions
} // namespace Silibs2
