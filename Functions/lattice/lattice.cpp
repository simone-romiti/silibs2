// lattice.cpp
/* Definitions of lattice.h */

#include "lattice.hpp"

namespace Silibs2 {
namespace Functions {
namespace cpp_partial_template_definition {
#ifndef Silibs2_Functions_lattice_cpp
#define Silibs2_Functions_lattice_cpp

auto leading_exp_double = leading_exp<double>;
auto leading_exp_back_double = leading_exp_back<double>;
auto lin_comb_decr_exponentials_double = lin_comb_decr_exponentials<double>;
double (*symm_der_double_1)(const double &, const double &) = symm_der<double>;
double (*backward_der_double_1)(const double &,
                                const double &) = backward_der<double>;
double (*forward_der_double_1)(const double &,
                               const double &) = forward_der<double>;
std::vector<double> (*symm_der_double_2)(
    const unsigned int &, const std::vector<double> &) = symm_der<double>;
std::vector<double> (*backward_der_double_2)(
    const unsigned int &, const std::vector<double> &) = backward_der<double>;
std::vector<double> (*forward_der_double_2)(
    const unsigned int &, const std::vector<double> &) = forward_der<double>;
auto even_der_2n_double = even_der_2n<double>;

#endif
} // namespace cpp_partial_template_definition
} // namespace Functions
} // namespace Silibs2
