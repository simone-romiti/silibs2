// lattice.h
/*
This file declares and defines some utility functions to deal with lattice
correlators
*/

#include <vector>

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_lattice_h
#define Silibs2_Functions_lattice_h

/*!< Returns A*exp(-M*t) */
template <class U = double> U leading_exp(const U &t, const U &A, const U &M);

/*!< Returns A*( exp(-M*t) + p*exp(-M*(T-t)) ) */
template <class U = double>
U leading_exp_back(const U &t, const U &A, const U &M, const U &p, const U &T);

// /*!< Returns \sum_i Ai*( exp(-Ei*t) + p*exp(-Ei*(T-t)) ) */
// template <class U=double>
// U n_exp(const U &t, const std::vector<U> &Ai, const std::vector<U> &Ei);
//
// /*!< Returns \sum_i Ai*( exp(-Ei*t) + p*exp(-Ei*(T-t)) ) */
// template <class U=double>
// U n_exp_back(const U &t, const std::vector<U> &Ai, const std::vector<U> &Ei,
//              const U &p, const U &T);

/* returns \sum_i A_i exp(-E_i * t) */
template <class U = double>
U lin_comb_decr_exponentials(const U &t, const std::vector<U> &A,
                             const std::vector<U> &E);

/* symmetric derivative in lattice units : (y(t+1) - y(t-1))/2*/
template <class U = double> U symm_der(const U &y_for, const U &y_back);

/* backward derivative in lattice units : y(t) - y(t-1) */
template <class U = double> U backward_der(const U &y_back, const U &y_0);

/* forward derivative in lattice units : y(t+1) - y(t) */
template <class U = double> U forward_der(const U &y_0, const U &y_forward);

/* vector of symmetric derivatives of order 'n' (in lattice units):
    y^{(n)}(x) = (y^{(n-1)}(x+1)  - y^{(n-1)}(x-1))/2
    In the points for which it's not defined it is set to equal to 0.
    */
template <class U = double>
std::vector<U> symm_der(const unsigned int &n, const std::vector<U> &y);

/* vector of backward derivatives :
y^{(n)}(x) = y^{(n-1)}(x)  - y^{(n-1)}(x+1)
In the points for which it's not defined it is set to equal to 0.
*/
template <class U = double>
std::vector<U> backward_der(const unsigned int &n, const std::vector<U> &y);

/* vector of forward derivatives :
y^{(n)}(x) = y^{(n-1)}(x+1)  - y^{(n-1)}(x)
In the points for which it's not defined it is set to equal to 0.
*/
template <class U = double>
std::vector<U> forward_der(const unsigned int &n, const std::vector<U> &y);

/* derivative of degree '2*n' */
template <class U = double>
std::vector<U> even_der_2n(const unsigned int &n, const std::vector<U> &y);

#endif
} // namespace Functions
} // namespace Silibs2
