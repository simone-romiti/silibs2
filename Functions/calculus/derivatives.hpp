// derivatives.hpp
/*! definitions of derivatives.h */

#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>

#include "derivatives.h"

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_calculus_derivatives_hpp
#define Silibs2_Functions_calculus_derivatives_hpp

template <typename U>
std::vector<U> gradient(const std::function<U(const std::vector<U> &)> &f,
                        const std::vector<U> &x, const std::vector<U> &delta) {

  const int n = x.size();
  std::vector<U> grad(n);

  std::vector<U> xi_1 = x, xi_2 = x;

  for (int i = 0; i < n; i++) {

    /* decrease/increase i-th variable */
    xi_1[i] = x[i] - delta[i];
    xi_2[i] = x[i] + delta[i];

    grad[i] = (f(xi_2) - f(xi_1)) / (2.0 * delta[i]);

    /* increase/decrease i-th variable */
    xi_1[i] = x[i] + delta[i];
    xi_2[i] = x[i] - delta[i];
  }

  return grad;
} // gradient()

template <typename U>
U derivative(const std::function<U(const U &)> &f, const U &x, const U &dx) {
  std::function<U(const std::vector<U> &)> fun = [&](const std::vector<U> &X) {
    return f(X[0]);
  };
  return Functions::gradient(fun, {x}, {dx})[0];
} // derivative()

#endif
} // namespace Functions
} // namespace Silibs2
