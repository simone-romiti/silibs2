// derivatives.cpp
/*! definitions of derivatives.h */

#include "derivatives.hpp"

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_calculus_derivatives_cpp
#define Silibs2_Functions_calculus_derivatives_cpp

// partial template definition
namespace cpp_partial_template_definition {

auto gradient_double = gradient<double>;
auto derivative_double = derivative<double>;

} // namespace cpp_partial_template_definition
#endif
} // namespace Functions
} // namespace Silibs2
