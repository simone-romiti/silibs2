// derivatives.h
/*! Numerical derivatives */

#include <functional>
#include <vector>

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_derivatives_h
#define Silibs2_Functions_derivatives_h

template <class U = double>
std::vector<U> gradient(const std::function<U(const std::vector<U> &)> &f,
                        const std::vector<U> &x, const std::vector<U> &delta);

template <class U = double>
U derivative(const std::function<U(const U &)> &f, const U &x, const U &dx);

#endif
} // namespace Functions
} // namespace Silibs2
