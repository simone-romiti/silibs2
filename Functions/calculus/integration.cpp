// inte.h
/*
This file declares functions which perform numerical integration
*/

#include "./integration.hpp"

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_inte_cpp
#define Silibs2_Functions_inte_cpp

namespace cpp_partial_template_definition {

auto inte_0_1_double = inte_0_1<double>;
auto inte_a_b_double = inte_a_b<double>;

auto inte_a_plusInf_double = inte_a_plusInf<double>;
auto inte_minusInf_b_double = inte_minusInf_b<double>;

auto inte_minusInf_plusInf_double = inte_minusInf_plusInf<double>;

} // namespace cpp_partial_template_definition
#endif
} // namespace Functions
} // namespace Silibs2
