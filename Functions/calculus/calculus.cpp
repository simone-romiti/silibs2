// calculus.h
/*!
Include this file to include the numerical derivation and integration routines
of Silisb2::Functions
*/

#include "./derivatives.cpp" /*!< numerical derivatives */
#include "./integration.cpp" /*!< numerical integration */