// inte.hpp
/* This file defines what is declared in inte.h */

#include <array>
#include <cmath>
#include <iostream>
#include <mutex>
#include <thread>

#include "integration.h"
namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_inte_hpp
#define Silibs2_Functions_inte_hpp

template <class U>
U inte_0_1(const std::function<U(const U &, U *)> &f, U *p, const int &N_div) {
  U delta = 1.0 / (N_div + 0.0);

  U x1, x2;
  U y1, y2;
  U sum_inte = 0.0;

  for (int i = 1; i < N_div - 1; i++) {

    x1 = i * delta, x2 = (i + 1) * delta;
    y1 = f(x1, p), y2 = f(x2, p);

    sum_inte += delta * (y1 + y2) / 2.0; // trapezium area
  }

  return sum_inte;
} // end inte_0_1()

template <class U>
U inte_a_b(const std::function<U(const U &, U *)> &f, U *p, const U &a,
           const U &b, const int &N_div) {

  std::function<U(const U &, U *)> g = [=](const U &t, U *p_g) {
    U z = a + (b - a) * t;
    U meas = (b - a); //!< measure change
    return meas * f(z, p_g);
  };

  U result = inte_0_1<U>(g, p, N_div);
  return result;
} // end inte_a_b()

template <class U>
U inte_a_plusInf(const std::function<U(const U &, U *)> &f, U *p, const U &a,
                 const int &N_div) {
  std::function<U(const U &, U *)> g = [=](const U &t, U *p_g) {
    U z = a + (1.0 - t) / t;
    U meas = pow(t, -2.0); //!< measure change
    return meas * f(z, p_g);
  };

  U result = inte_0_1<U>(g, p, N_div);
  return result;
} // end inte_a_plusInf()

template <class U>
U inte_minusInf_b(const std::function<U(const U &, U *)> &f, U *p, const U &b,
                  const int &N_div) {
  std::function<U(const U &, U *)> g = [=](const U &t, U *p_g) {
    U z = b - (1.0 - t) / t;
    U meas = pow(t, -2.0); //!< measure change
    return meas * f(z, p);
  };

  U result = inte_0_1<U>(g, p, N_div);
  return result;
}

template <class U>
U inte_minusInf_plusInf(const std::function<U(const U &, U *)> &f, U *p,
                        const int &N_div) {
  std::function<U(const U &, U *)> g1 = [=](const U &t, U *p_g) {
    U z = (1.0 - t) / t;
    U meas = pow(t, -2.0); //!< measure change
    return meas * f(z, p);
  };

  std::function<U(const U &, U *)> g2 = [=](const U &t, U *p_g) {
    U z = -(1.0 - t) / t;
    U meas = pow(t, -2.0); //!< measure change
    return meas * f(z, p);
  };

  std::function<U(U, U *)> g = [=](const U &t, U *p_g) {
    return g1(t, p) + g2(t, p);
  };

  return inte_0_1<U>(g, p, N_div); /*!< \e Note : the measure changes */
}

#endif
} // namespace Functions
} // namespace Silibs2
