// calculus.h
/*!
Include this file to include the numerical derivation and integration routines
of Silisb2::Functions
*/

#include "./derivatives.h" /*!< numerical derivatives */
#include "./integration.h" /*!< numerical integration */