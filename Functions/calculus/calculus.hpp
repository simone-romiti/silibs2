// calculus.hpp
/*!
Include this file to include the numerical derivation and integration routines
of Silisb2::Functions
*/

#include "./derivatives.hpp" /*!< numerical derivatives */
#include "./integration.hpp" /*!< numerical integration */