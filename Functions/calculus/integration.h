// inte.h
/*
This file declares functions which perform numerical integration
*/

#include <functional>
#include <vector>

namespace Silibs2 {
namespace Functions {
#ifndef Silibs2_Functions_inte_h
#define Silibs2_Functions_inte_h

/* \int_{0}^{1} dx f(x)*/
template <class U = double>
U inte_0_1(const std::function<U(const U &x, U *p0)> &f, U *p,
           const int &N_div);

/* \int_{a}^{b} dx f(x) */
template <class U = double>
U inte_a_b(const std::function<U(const U &x, U *p0)> &f, U *p, const U &a,
           const U &b, const int &N_div);

/* \int_{a}^{+\infty} dx f(x) */
template <class U = double>
U inte_a_plusInf(const std::function<U(const U &x, U *p0)> &f, U *p, const U &a,
                 const int &N_div);

/* \int_{-\infty}^{a} dx f(x) */
template <class U = double>
U inte_minusInf_b(const std::function<U(const U &x, U *p0)> &f, U *p,
                  const U &b, const int &N_div);

/* \int_{-\infty}^{+\infty} dx f(x) */
template <class U = double>
U inte_minusInf_plusInf(const std::function<U(const U &x, U *p0)> &f, U *p,
                        const int &N_div);

#endif
} // namespace Functions
} // namespace Silibs2
