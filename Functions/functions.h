// functions.h
/*
Import this file if you want to have access
to all the functions in the namespace Silibs2::Functions
*/
#ifndef Silibs2_functions_h
#define Silibs2_functions_h

#include "./calculus/calculus.h"     /*!< numerical calculus */
#include "./containers/containers.h" /*!< containers manipulation */
#include "./files/files.h"           /*!< files manipulation */
#include "./files/smart_print.h"     /*!< smart printing of average and error */
#include "./lattice/lattice.h"       // analysis of lattice correlators

#include "./math/math.h"        /*!< generic mathematical functions */
#include "./physics/physics.h"  /*!< generic physics functions */
#include "./random/random.h"    /*!< random numbers */
#include "./roots/roots.h"      /*!< roots of functions */
#include "./statistics/jkf.h"   /*!< jackknifes */
#include "./statistics/stats.h" /*!< statistics */
#endif
