// Silibs2.cpp
/*! Include this header if you want to include all Silibs2 libraries */

#include "Silibs2.hpp"
#ifndef Silibs2_cpp
#define Silibs2_cpp

namespace cpp_partial_template_specialization {

// NaRF = not a real file
// NaRP = not a real path

/* --------- */
/* Functions */
/* --------- */
namespace Functions {
using namespace Silibs2::Functions;
void calculus() {
  // derivatives
  auto gradient_double = gradient<double>;
  auto derivative_double = derivative<double>;
  // integrals
  auto inte_0_1_double = inte_0_1<double>;
  auto inte_a_b_double = inte_a_b<double>;

  auto inte_a_plusInf_double = inte_a_plusInf<double>;
  auto inte_minusInf_b_double = inte_minusInf_b<double>;

  auto inte_minusInf_plusInf_double = inte_minusInf_plusInf<double>;
}

void containers() {
  auto print_cout_int = print_cout<int>;
  auto print_cout_double = print_cout<double>;
  auto print_cout_string = print_cout<std::string>;

  auto print_cerr_int = print_cerr<int>;
  auto print_cerr_double = print_cerr<double>;
  auto print_cerr_string = print_cerr<std::string>;

  auto range_double_double = range<double, double>;
  auto range_double_int = range<double, int>;

  auto linspace_double = linspace<double>;
  auto prod_double = prod<double>;
  auto mult_double = mult<double>;
  auto dot_product_double = dot_product<double>;
  auto rm_comp_double = rm_comp<double>;
  auto cut_double = cut<double>;
  auto norm_squared_double = norm_squared<double>;
  auto concatenate_double = concatenate<double>;
  auto concatenate_string = concatenate<std::string>;
  auto rm_reps_double = rm_reps<double>;
  auto reps_double = reps<double>;
  auto multip_int = multip<int>;
  auto multips3_double = multips3<double>;

  // auto sum_double = sum<double>;

  double (*sum_double_1)(double (*f)(const int &, double *p0), double *p,
                         double Lambda, int in) = sum<double>;

  double (*sum_double_2)(const std::function<double(int, double *)> &, double *,
                         double, int) = sum<double>;

  auto min_value_double = min_value<double>;
  auto max_value_double = max_value<double>;

  auto boost_cast_double_string = boost_cast<double, std::string>;
  auto boost_cast_string_double = boost_cast<std::string, double>;

  auto occurrencies_string = occurrencies<std::string>;
  auto occurrencies_double = occurrencies<double>;
  auto occurrencies_int = occurrencies<int>;

  auto is_contained_string = is_contained<std::string>;
  auto is_contained_double = is_contained<double>;
  auto is_contained_int = is_contained<int>;

  auto change_type_double_int = change_type<double, int>;
  auto change_type_int_double = change_type<int, double>;
}

void files() {
  auto home_dir_0 = home_dir;
  auto build_full_path_0 = build_full_path;
  auto check_path_0 = check_path;
  auto rows_file_0 = rows_file;
  auto columns_file_0 = columns_file;
  auto deduce_dir_file_0 = deduce_dir_file;
  auto touch_0 = touch;
  auto find_occurencies_0 = find_occurencies;
  auto check_occurencies_0 = check_occurencies;
  auto get_int = get<int>;
  auto get_double = get<double>;
  auto get_string = get<std::string>;
  auto get_vector_int = get_vector<int>;
  auto get_vector_double = get_vector<double>;
  auto get_vector_string = get_vector<std::string>;
  auto file_to_string_0 = file_to_string;
  auto clear_content_0 = clear_content;
  auto append_string_0 = append_string;
  auto string_to_file_0 = string_to_file;
  auto replace_substring_0 = replace_substring;
  auto replace_substrings_0 = replace_substrings;
}

void smart_print() {
  using namespace Silibs2::Functions::smart_printing;
  auto shift_by_10_double = shift_by_10<double>;
  auto round_to_digit_double = round_to_digit<double>;
  auto smart_print_double = smart_print;
}

void lattice() {
  auto leading_exp_double = leading_exp<double>;
  auto leading_exp_back_double = leading_exp_back<double>;
  double lin_comb_decr_exponentials_double =
      lin_comb_decr_exponentials<double>(0.0, {0.0}, {0.0});

  double (*symm_der_double_1)(const double &, const double &) =
      symm_der<double>;
  double (*backward_der_double_1)(const double &, const double &) =
      backward_der<double>;
  double (*forward_der_double_1)(const double &, const double &) =
      forward_der<double>;
  std::vector<double> (*symm_der_double_2)(
      const unsigned int &, const std::vector<double> &) = symm_der<double>;
  std::vector<double> (*backward_der_double_2)(
      const unsigned int &, const std::vector<double> &) = backward_der<double>;
  std::vector<double> (*forward_der_double_2)(
      const unsigned int &, const std::vector<double> &) = forward_der<double>;
  auto even_der_2n_double = even_der_2n<double>;
}

void math() {
  auto factorial_int = factorial<int>;
  auto polynomial_double = polynomial<double>;
  auto quadratic_average_double = quadratic_average<double>;
}

void physics() {
  auto GeV_to_MeV_double = GeV_to_MeV<double>;
  auto GeV_inv_to_MeV_inv_double = GeV_inv_to_MeV_inv<double>;
  auto MeV_to_GeV_double = MeV_to_GeV<double>;
  auto MeV_inv_to_GeV_inv_double = MeV_inv_to_GeV_inv<double>;
  auto fm_to_GeV_inv_double = fm_to_GeV_inv<double>;
  auto GeV_inv_to_fm_double = GeV_inv_to_fm<double>;
  auto fm_to_MeV_inv_double = fm_to_MeV_inv<double>;
  auto MeV_inv_to_fm_double = MeV_inv_to_fm<double>;
}

void random() { auto gauss_distributed_double = gauss_distributed<double>; }

void roots() {
  auto N_Derive_double = N_Derive<double>;
  auto FindRootInRange_double = FindRootInRange<double>;
  auto root_on_the_left_double = root_on_the_left<double>;
  auto root_on_the_right_double = root_on_the_right<double>;
  auto roots_interval_double = roots_interval<double>;
  auto find_roots_double = find_roots<double>;
  auto find_root_double = find_root<double>;
}

void statistics() {
  // stats
  double (*average_double_1)(double *, int, int) = average<double>;
  double (*average_double_2)(const std::vector<double> &) = average<double>;
  auto average_range_double = average_range<double>;
  auto weighted_average_double = weighted_average<double>;
  auto average_rm_double = average_rm<double>;
  auto sum_diff_squared_double = sum_diff_squared<double>;
  // jkf
  auto make_jkf_double = make_jkf<double>;
  auto error_jackknives_0_double = error_jackknives_0<double>;
  auto error_jkf_double = error_jkf<double>;
}
} // namespace Functions

/* ----------- */
/* Minuit2_fit */
/* ----------- */
namespace Minuit2_fit {
using namespace Silibs2::Minuit2_fit;

void fit_1D_xexyey() {
  fit_xexyey<double> minu;
  const std::vector<double> t;
  minu.set_data(t, t, t, t);
  minu.set_n_dof_manually(1.0);
  minu.set_ansatz(
      [](const double &, const std::vector<double> &) { return 0.0; });
  minu.fit();
  std::vector<double> v = minu.get_vec_y_th();
}

void fit_1D_xyey() {
  fit_xyey<double> minu;
  const std::vector<double> t;
  minu.set_data(t, t, t);
  minu.set_n_dof_manually(1.0);
  minu.set_ansatz(
      [](const double &, const std::vector<double> &) { return 0.0; });
  minu.fit();
  std::vector<double> v = minu.get_vec_y_th();
}

void fit_nD_xexyey() {
  nD::fit_xexyey<double> minu;
  const Silibs2::Tables::table<double> x, ex;
  const std::vector<double> y, ey;
  minu.set_data(x, ex, y, ey);
  minu.set_n_dof_manually(1.0);
  minu.set_ansatz([](const std::vector<double> &, const std::vector<double> &) {
    return 0.0;
  });
  minu.fit();
  std::vector<double> v = minu.get_vec_y_th();
}

} // namespace Minuit2_fit

/* ------ */
/* Tables */
/* ------ */

namespace Tables {
using namespace Silibs2::Tables;

template <class U> void tables_U() {
  U x = boost::lexical_cast<U>(0.0);
  table<U> t1(0, 0);
  table<U> t2(0, 0, x);
  table<U> t3(t1);
  table<U> t4("NaRP");
  table<U> t5("NaRP");
  table<U> t6(0, 0, [&](const int &a, const int &b) { return x; });
  table<U> t7 = t6.transpose();
  t7.transposeInPlace();
  t7.loop_elements([&](const int &a, const int &b) { return t7(0, 0) = x; });
  t7.loop_elements([&](const int &a, const int &b) {});
  int n = t7.rows();
  n = t7.cols();
  n = t7.size();
  std::pair<int, int> shp = t7.shape();
  auto lor = t7.list_of_rows();
  t7.check_access(0, 0);
  U Mij = t7(0, 0);
  t7(0, 0) = x;
  t7 = t6;
  table<U> t8 = t6.block(0, 0, 0, 0);
  t8.remove_row(0);
  t8.remove_col(0);
  std::vector<U> v = t7.row(0);
  v = t7.col(0);
  t8.set_row(0, v);
  t8.set_row(v, 0);
  t8.set_col(0, v);
  t8.set_col(v, 0);
  t8.insert_row(v, 0);
  t8.insert_col(v, 0);
  std::string t_str = t8.to_string();
  t8.print_cout();
  t8.print_cerr();
  t8.print("NaRF");
  t8.append("NaRF");
}

void tables_double() {
  tables_U<double>();
  tables_U<std::string>();
}

void tables_functions() {

  auto print_tables_double = print_tables<double>;
  auto print_tables_string = print_tables<std::string>;

  const Tables::table<double> t0_double = read_table_sep<double>("NaRF");
  const Tables::table<std::string> t0_string =
      read_table_sep<std::string>("NaRF");

  Tables::table<double> t1_double = read_table<double>("NaRF");
  const Tables::table<std::string> t1_string = read_table<std::string>("NaRF");

  const std::vector<double> r1_double = read_row<double>(0, "NaRF");
  const std::vector<std::string> r1_string = read_row<std::string>(0, "NaRF");

  std::vector<double> c1_double = read_col<double>(0, "NaRF");
  std::vector<std::string> c1_string = read_col<std::string>(0, "NaRF");

  /*  ---------------------------- */
  /*! OPERATIONS on the components */
  /*  ---------------------------- */

  auto cast_table_double_string = cast_table<double, std::string>;
  auto cast_table_string_double = cast_table<std::string, double>;

  // auto t1_changed_1 = change_type<double>(t1_string);
  // auto t1_changed_2 = change_type<std::string>(t1_double);

  // auto convert_table_string_double =
  //     convert_table<table<std::string>, table<double>>;
  //
  // auto convert_table_double_string =
  //     convert_table<table<double>, table<std::string>>;

  std::function<double(const double &)> ocw = [&](const double &x) {
    return 0.0;
  };

  Tables::table<double> t2_double = operation_comp_wise<double>(t1_double, ocw);

  auto sqrt_comp_wise_double = sqrt_comp_wise<double>;
  auto pow_comp_wise_double_double = pow_comp_wise<double, double>;
  auto pow_comp_wise_double_int = pow_comp_wise<double, int>;
  auto square_comp_wise_double = square_comp_wise<double>;
  auto ratio_comp_wise_double = ratio_comp_wise<double>;
  auto mult_comp_wise_double = mult_comp_wise<double>;

  auto table_from_rows_string = table_from_rows<std::string>;
  auto table_from_rows_double = table_from_rows<double>;
  auto table_from_cols_string = table_from_cols<std::string>;
  auto table_from_cols_double = table_from_cols<double>;
}

} // namespace Tables

/* -- */
/* RC */
/* -- */

namespace RC_tables {
using namespace Silibs2;

template <class U> void tables_templ() {
  RC::table<U> tRC_1;
  tRC_1.check_sizes();

  std::vector<std::string> r_names = {"s1"};
  std::vector<std::string> c_names = {"c1"};

  Tables::table<U> t0_U;
  RC::table<U> table_U(t0_U, r_names, c_names);

  RC::table<U> t00_U(1, 1, r_names, c_names);

  RC::table<U> t1_U = t0_U;

  RC::table<U> t2_U(0, 0);
  RC::table<U> t2_U_val(0, 0, boost::lexical_cast<U>(0.0));

  RC::table<U> t3_U("NaRF");

  t3_U.set_r_names({"a", "b", "c"});
  t3_U.set_c_names({"a", "b", "c"});

  std::vector<U> r2_U = t3_U.row("name");
  std::vector<U> c2_U = t3_U.col("name");

  Tables::table<U> t4_U = t3_U.content();
  std::pair<int, int> p1 = t2_U.content_ij("r", "c");

  U e1 = t2_U("a", "b");
  t2_U("a", "b") = e1;

  std::string str1 = t2_U.to_string();
  t2_U.print("NaRF");

  const RC::table<U> t5_U = RC::read_table<U>("NaRF");
  U x = t5_U("a", "b");
}

void tables_calls() {
  tables_templ<int>();
  tables_templ<double>();
  tables_templ<std::string>();
}

} // namespace RC_tables

/* -- */
/* LA */
/* -- */

namespace LA {
using namespace Silibs2::LA;

void matrices() {

  Silibs2::Tables::table<double> t0;
  matrix<double> m0;
  matrix<double> m1 = t0;
  m1 = m0;

  matrix<double> m2(1, 1);
  matrix<double> m3 = 1.0 * (m2 * 2.0); // scalar multiplication
  m3 *= 1.0;

  matrix<double> m4 = (m2 / 2.0); // scalar multiplication
  m4 /= 1.0;

  matrix<double> m5 = (m2 + m3); // scalar multiplication
  m5 += m3;

  matrix<double> m6 = (m2 - m3); // scalar multiplication
  m6 -= m3;

  matrix<double> m7 = m6.inv();
}

void functions() {

  auto print_matrices_double = print_matrices<double>;
  auto read_matrix_sep_double = read_matrix_sep<double>;
  auto read_matrix_double = read_matrix<double>;
  auto read_row_double = read_row<double>;
  auto read_col_double = read_col<double>;

  auto cast_matrix_double_string = cast_matrix<double, std::string>;
  auto cast_matrix_string_double = cast_matrix<std::string, double>;

  // template <class M_type1, class M_type2>
  // M_type1 convert_matrix(const M_type2 &matr);
  //

  auto matrix_from_rows_double = matrix_from_rows<double>;
  auto matrix_from_cols_double = matrix_from_cols<double>;
  // auto lin_comb_double = lin_comb<double>;
  matrix<double> m1, m2, m3;
  m3 = 1.0 * m1 + 2.0 * m2 - 3.0 * m1;
  m3 = m1 * m2;

  auto tensor_prod_double = tensor_prod<double>;
  auto minor_matrix_double = minor_matrix<double>;
  auto cofactors_matrix_double = cofactors_matrix<double>;
  auto adjugate_matrix_double = adjugate_matrix<double>;
  auto det_2x2_double = det_2x2<double>;
  auto det_double = det<double>;
  auto inverse_matrix_double = inverse_matrix<double>;
  auto solve_double = solve<double>;
}
} // namespace LA

/* --- */
/* jkf */
/* --- */
namespace jkf {
using namespace Silibs2::jkf;

template <class U> void jkf_matrix_templ() {
  matrix<U> m1;

  m1.print_cout();

  Silibs2::Tables::table<U> t1;
  m1 = t1;
  m1 = m1;
  matrix<U> m2(1, 1);
  matrix<U> m3(1, 1, boost::lexical_cast<U>(0.0));
  matrix<U> m4("NaRF");
  matrix<U> m5(1, 1, [](const int &i, const int &j) { return 0.0; });

  /*! jackknife averages */
  std::vector<U> a1 = m5.avrs();
  std::vector<U> e1 = m5.errs();
  matrix<U> ae1 = m5.ae();

  m5.print_jkf_ae("NaRF");
  matrix<std::string> m6 = m5.scientific_digits(2);
  m5.smart_print_ae("NaRF", 2);

  matrix<U> m7 = m1 * m2 / 1.0 + m3 - m4;
  m7 += m1;
  m7 -= m1;
  m7 *= 1.0;
  m7 /= 1.0;
}

void jkf_matrix_double() { jkf_matrix_templ<double>(); }

template <class U> void functions_templ() {

  auto print_matrices_U = print_matrices<U>;
  auto read_matrix_sep_U = read_matrix_sep<U>;
  auto read_matrix_U = read_matrix<U>;

  auto read_jkf_U = read_jkf<U>;

  auto read_avr_err_U = read_avr_err<U>;
  auto read_row_U = read_row<U>;
  auto read_col_U = read_col<U>;

  auto cast_matrix_U_string = cast_matrix<U, std::string>;
  auto cast_matrix_string_U = cast_matrix<std::string, U>;

  auto matrix_from_rows_U = matrix_from_rows<U>;
  auto matrix_from_cols_U = matrix_from_cols<U>;

  auto build_xyey_U = build_xyey<U>;
  auto build_x_y_ey_yth_eyth_U = build_x_y_ey_yth_eyth<U>;
  auto build_xexyey_U = build_xexyey<U>;
  auto build_x_ex_y_ey_yth_eyth_U = build_x_ex_y_ey_yth_eyth<U>;
  auto build_xiyey_U = build_xiyey<U>;

  matrix<U> m1, m2, m3, m4;
  matrix<U> m5 = m1 * m2 / 1.0 + m3 - m4;
  m5 += m1;
  m5 -= m1;
  m5 *= 1.0;
  m5 /= 1.0;

  auto tensor_prod_U = tensor_prod<U>;

  std::function<U(const U &)> f1 = [](const U &x) -> U { return x; };
  matrix<U> m6 = operation_cwise(m5, f1);
  auto pow_cwise_U_U = pow_cwise<U, U>;
  auto pow_cwise_U_int = pow_cwise<U, int>;
  auto square_cwise_U = square_cwise<U>;
  auto sqrt_cwise_U = sqrt_cwise<U>;

  std::function<U(const U &, const U &)> f2 = [](const U &x, const U &y) {
    return x * y;
  };
  matrix<U> m7 = operation_cwise(m5, m6, f2);
  auto mult_cwise_U = mult_cwise<U>;
  auto ratio_cwise_U = ratio_cwise<U>;
  m7 = m1 / m2;

  auto average_matrices_U = average_matrices<U>;
  std::function<U(const U &, const std::vector<U> &)> f3 =
      [](const U &x, const std::vector<U> &p) { return x; };
  matrix<U> m8 = find_root<U>(f3, m7, 0.0, 0.0, 0.0, 0.0, 0.0);
  auto Gaussian_jkf_U = Gaussian_jkf<U>;

  auto jkf_matrix_from_gauge_skip_U = jkf_matrix_from_gauge_skip<U>;
  auto jkf_matrix_from_gauge_skip_ReIm_U = jkf_matrix_from_gauge_skip_ReIm<U>;

  auto gauge_to_jkf_U = gauge_to_jkf<U>;
  auto time_symm_cols_U = time_symm_cols<U>;

  auto trivial_jkf_U = trivial_jkf<U>;
  auto constant_jkf_U = constant_jkf<U>;
  auto trivial_constant_jkf_U = trivial_constant_jkf<U>;

  matrix<U> m9;
  m9 = backward_der<U>(1, m8);
  m9 = forward_der<U>(1, m8);
  m9 = symm_der<U>(1, m8);
  m9 = even_der_2n<U>(1, m8);

  matrix<U> m10 = GeV_to_MeV(m9);
  m10 = GeV_inv_to_MeV_inv(m9);
  m10 = MeV_to_GeV(m9);
  m10 = MeV_inv_to_GeV_inv(m9);
  m10 = fm_to_GeV_inv(m9);
  m10 = GeV_inv_to_fm(m9);
  m10 = fm_to_MeV_inv(m9);
  m10 = MeV_inv_to_fm(m9);
}

void functions_double() { functions_templ<double>(); }

void array3d_double() {

  using namespace Silibs2::jkf;

  array3d<double> a1;
  array3d<double> a2(1, 1, 1, 0.0);
  a2.resize(2, 2, 2);

  std::array<int, 3> sh1 = a2.shape();
  a2.check_access(0, 0, 0);

  jkf::matrix<double> J1 = a1[0];
  a1[0] = J1;

  double x = a1(0, 0, 0);
  a1(0, 0, 0) = 0.0;

  Tables::table<double> t1 = a1.avrs();
  Tables::table<double> t2 = a1.errs();

  a1.print_avrs("NaRP");
  J1 = a1.slice_0(0);
  J1 = a1.slice_1(0);
  J1 = a1.slice_2(0);
}
} // namespace jkf

namespace jkf_Minuit2_fit {

namespace oneD {
namespace jmf = Silibs2::jkf::Minuit2_fit;

void base_fit() {
  jmf::base_fit<double> minu;

  minu.set_verbose(true);
  minu.check_data_init();
  minu.check_residue_init();

  minu.check_fit_init();
  minu.check_fit_done();

  Silibs2::jkf::matrix<double> pars =
      minu.get_PARS(); /*!< jkf of best fit parameters */
  Silibs2::jkf::matrix<double> ch_red =
      minu.get_CH2_RED(); /*!< jkf of best fit ch2_red */

  Silibs2::jkf::matrix<double> X_extr;
  minu.set_X_extr(X_extr, 0.0);
  minu.find_X_extr(0.0, 0.0, 0.0, 0.0, 0.0);
  jkf::matrix<double> X0 = minu.get_X_extr();

  minu.find_Y_extr({0.0, 1.0});
  minu.find_Y_extr(0.0);
  jkf::matrix<double> Y0 = minu.get_Y_extr();

  minu.give_details("details");
  minu.add_dense_th(10, 0.0, 0.0, true, 100);
  minu.print_fit_results("NaRP");

  Silibs2::jkf::matrix<double> Yth = minu.get_Y_th();
  minu.store_conv_info();
}

void generic_fit_xyey() {
  jmf::fit_xyey<double> minu;

  std::vector<double> x;
  jkf::matrix<double> Y;
  minu.set_data(x, Y);

  int n_pts = minu.get_n_pts();
  int N_jkf = minu.get_N_jkf();

  minu.set_ansatz(
      [](const double &, const std::vector<double> &) { return 0.0; });

  minu.set_ch2_red_fun_single_jkf(
      [](const std::vector<double> &y, const std::vector<double> &ey,
         const std::vector<double> &p) { return 0.0; });

  minu.fit();
  minu.store_results();
}
void generic_fit_xexyey() {

  jmf::fit_xexyey<double> minu;

  jkf::matrix<double> X, Y;
  minu.set_data(X, Y);

  int n_pts = minu.get_n_pts();
  int N_jkf = minu.get_N_jkf();

  minu.set_ansatz(
      [](const double &, const std::vector<double> &) { return 0.0; });

  minu.set_ch2_red_fun_single_jkf(
      [](const std::vector<double> &x, const std::vector<double> &ex,
         const std::vector<double> &y, const std::vector<double> &ey,
         const std::vector<double> &p) { return 0.0; });

  minu.set_n_dof_manually(1.0);
  minu.set_n_sigma_Bayes(1.0);

  minu.fit();
  minu.store_results();
}
void polynomial_fit_xyey() {
  jmf::polynomial_fit_xyey<double> minu;
  minu.set_pars_auto({"a1", "a2"});
  minu.set_pars_auto({"a1", "a2"}, {0.0, 0.0});
}
void polynomial_fit_xexyey() {
  jmf::polynomial_fit_xexyey<double> minu;
  minu.set_pars_auto({"a1", "a2"});
  minu.set_pars_auto({"a1", "a2"}, {0.0, 0.0});
}
void constant_fit_xyey() {
  jmf::constant_fit_xyey<double> minu;
  minu.set_par("a1");
}
void constant_fit_xexyey() {
  jmf::constant_fit_xexyey<double> minu;
  minu.set_par("a1");
}

} // namespace oneD

namespace nD {
namespace jmfD = Silibs2::jkf::Minuit2_fit::nD;
void base_fit() {
  jmfD::base_fit<double> minu;

  minu.set_verbose(true);
  minu.check_data_init();
  minu.check_residue_init();

  minu.check_fit_init();
  minu.check_fit_done();

  Silibs2::jkf::matrix<double> pars =
      minu.get_PARS(); /*!< jkf of best fit parameters */
  Silibs2::jkf::matrix<double> ch_red =
      minu.get_CH2_RED(); /*!< jkf of best fit ch2_red */

  Silibs2::jkf::matrix<double> X_extr;
  std::vector<double> vec_x_extr;
  minu.find_Y_extr(X_extr);
  minu.find_Y_extr(vec_x_extr);
  jkf::matrix<double> Y0 = minu.get_Y_extr();

  minu.give_details("details");
  const std::vector<double> x_fixed;
  minu.add_dense_th_idx("info", 1, x_fixed, 10, 0.0, 1.0, 100);
  minu.print_fit_results("NaRP");

  Silibs2::jkf::matrix<double> Yth = minu.get_Y_th();
  // auto fr = minu.get_fit_results();
  minu.store_conv_info();
}

void fit_xexyey() {

  jmfD::fit_xexyey<double> minu;

  jkf::array3d<double> X;
  jkf::matrix<double> Y;
  minu.set_data(X, Y);

  int n_pts = minu.get_n_pts();
  int N_jkf = minu.get_N_jkf();

  minu.set_ansatz([](const std::vector<double> &, const std::vector<double> &) {
    return 0.0;
  });

  minu.set_ch2_red_fun_single_jkf(
      [](const Silibs2::Tables::table<double> &x,
         const Silibs2::Tables::table<double> &ex, const std::vector<double> &y,
         const std::vector<double> &ey,
         const std::vector<double> &p) { return 0.0; });

  Tables::table<double> t1 = minu.get_x(1);
  Tables::table<double> t2 = minu.get_ex();
  Tables::table<double> t3 = minu.get_cols_xy(X, Y);
  Tables::table<double> t4 = minu.get_cols_xyyth(X, Y, Y);

  minu.fit(); /*!< do the fit */
  minu.store_results();

  std::vector<double> x_fixed;
  minu.add_dense_th("info", 1, x_fixed, 100);
  minu.add_Yth_xi_fixed(0, 3.0, "label");
}

} // namespace nD

} // namespace jkf_Minuit2_fit

/* ------- */
/* Lattice */
/* ------- */

namespace Lattice {

namespace anomaly_l {
using namespace Silibs2::Lattice;
auto a_l_lattice_double = a_l_lattice<double>;

std::vector<double> times, V;
const std::function<double(const double &)> fV;

double (*a_l_theory_1)(const std::vector<double> &, const std::vector<double> &,
                       const double &, const int &) = a_l_theory<double>;
double (*a_l_theory_2)(const std::function<double(const double &)> &,
                       const double &, const int &) = a_l_theory<double>;

auto kernel_a_l_double = kernel_a_l<double>;

auto integrand_kernel_a_l_double = integrand_kernel_a_l<double>;

} // namespace anomaly_l

namespace correlators {

namespace lc = Silibs2::Lattice::correlators;
void free_corr_double() {

  lc::free<double> f1;
  lc::free<double> f2("name", "path_data");
  lc::free<double> f3("name", "path_data", +1);

  Silibs2::jkf::matrix<double> m1;
  lc::free<double> f4("name", m1);
  lc::free<double> f5("name", m1, +1);

  std::string a = f5.get_NAME();

  Silibs2::jkf::matrix<double> m2 = f5.get_JKF();
  int Nj = f5.N_jkf();
  int Th = f5.T_half();
  int T0 = f5.T();
  int t_rev = f5.get_T_REV();

  f5.check_init();
  f5.check_T_REV();

  f5.print_jkf_ae("NaRF");
  f5.set_and_print_effective(m2, m2, "info_method", "NaRF");

  f5.effective_no_back("NaRF");
  f5.effective_back_Gattringer("NaRF");
  f5.effective_back_II_der("NaRF");
  f5.check_effective();

  f5.check_t_min_t_max(0, 1, 2);
  f5.append_plateau_info("NaRF", 1, 2);

  f5.print_yes_no_back(true);
  f5.single_exp_fit("NaRF");

  f5.n_exp_fit(1, "NaRF");
  f5.fit_effective("NaRP", 1);
  f5.ODE_no_back("NaRF", 1);
  f5.ODE_back("NaRF", 1);

  f5.dual_PP_fit("NaRP");
}

void correction_corr_double() {
  lc::correction<double> c1;

  const jkf::matrix<double> dC, C0;
  lc::correction<double> c2 = lc::correction<double>(dC, C0, 1);

  c2.check_back();
  c2.check_no_back();

  jkf::matrix<double> dM_no_back = c2.dM_eff_no_back();
  jkf::matrix<double> dA_no_back = c2.dA_eff_no_back();

  jkf::matrix<double> dM_back = c2.dM_eff_back();
  jkf::matrix<double> dA_back = c2.dA_eff_back();

  auto obs_full_double = lc::obs_full<double>;
  auto obs_slope_double = lc::obs_slope<double>;
}

} // namespace correlators

namespace effective {

using namespace Silibs2::Lattice::effective;
void eff_curves() {
  auto effective_no_back_double = effective_no_back<double>;
  auto effective_back_II_der_double = effective_back_II_der<double>;

  const std::vector<double> y;
  std::pair<std::vector<double>, std::vector<double>>
      effective_back_Gattringer_vec = effective_back_Gattringer<double>(y);

  const jkf::matrix<double> Y;
  std::pair<jkf::matrix<double>, jkf::matrix<double>>
      effective_back_Gattringer_jkf = effective_back_Gattringer<double>(Y);
}

// void slope_eff() {
//   using namespace Silibs2::Lattice;
//   auto slopes_fwd_double = slopes_fwd<double>;
// }

} // namespace effective

namespace FVE {
using namespace Silibs2::Lattice::FVE;

void QCD_FVE() {
  auto fact_obs_L_had_double = fact_obs_L_had<double>;
  double O1 = obs_L_had<double>(0.0, 0.0, 0.0, 0.0, "Pion");
  double O2 = obs_L_had<double>(0.0, 0.1, 0.0);

  QCD<double> q1;

  int Nj = q1.N_jkf();
  int npars = q1.n_pars();
  int npts = q1.n_pts();

  jkf::matrix<double> m;
  QCD<double> q2("name", {1, 2, 3, 4}, m, "mesonic");

  q1.set_MP(m);
  q1.check_MP(); /* check if Mpi has beenn initialized */

  q1.find_PARS(); /* find the parameters of the ansatz*/

  jkf::matrix<double> m2 = q1.get_OBS_inf();

  jkf::matrix<double> m3 = q1.get_OBS_L_th();
  jkf::matrix<double> m4 = q1.get_OBS_inf_th();

  q1.print_obs_L("NaRP");
  q1.print_obs_inf("NaRP");

  jkf::matrix<double> m5 = q1.get_corrected(m2, 5, m3);
}

void QED_L_FVE() {

  auto fact_QED_L_pt_double = fact_QED_L_pt;
  auto fact_QED_L_sd_double = fact_QED_L_sd;
  auto fact_QED_L_double = fact_QED_L;
  auto ansatz_QED_L_pt_double = ansatz_QED_L_pt;
  auto ansatz_QED_L_sd_double = ansatz_QED_L_sd;
  auto ansatz_QED_L_double = ansatz_QED_L;

  QED_L<double> q1;

  int Nj = q1.N_jkf();
  int npars = q1.n_pars();
  int npts = q1.n_pts();

  std::vector<double> L_inv = q1.get_L_inv();

  jkf::matrix<double> m;
  QED_L<double> q2("name", {1, 2, 3}, m, 0.0);

  q2.set_MASS_L(m);

  q2.check_init();
  q2.use_pt_only();

  q2.find_PARS();
  q2.check_PARS();

  /*
  set the theoretical values
  SD == true --> the Sructure Dependent part is included
  */
  q2.find_corrected();

  jkf::matrix<double> m1, m2;

  q2.print_obs_L("NaRP");
  q2.print_obs_inf("NaRP");

  jkf::matrix<double> m5 = q2.get_corrected(m1, m2, 2);
}

} // namespace FVE

namespace Luscher {

template <class U, class I1, class I2> void V_PP_calls_templ() {
  using namespace Silibs2::Luscher;
  U MP, MV, g, cff;
  I1 t;
  I2 L;
  U y1 = V_PP(t, 1, L, MP, MV, g, cff);

  const std::vector<U> kn;
  U ratio_MV_MP;
  U y2 = V_PP(t, kn, L, MP, ratio_MV_MP, g, cff);
}

void V_PP_calls() {
  V_PP_calls_templ<double, int, int>();
  V_PP_calls_templ<double, double, int>();
  V_PP_calls_templ<double, int, double>();
  V_PP_calls_templ<double, double, double>();
}

} // namespace Luscher

namespace ODE {
using namespace Silibs2::ODE;
template <class U> void ODE_back_templ() {
  jkf::matrix<U> m;
  jkf_back<U> j1;
  jkf_back<U> j2(m, 1, 5);
  j2.setup(m, 1, 5);
  int N = j2.N(), Th = j2.T_half(), T = j2.T();
  int p = j2.p();
  int Nj = j2.N_jkf();
  jkf::matrix<U> m2 = j2.Mass_matrix(1);
  std::vector<U> v1 = j2.V_vector(1);
  jkf::matrix<U> xk = j2.get_xk();
  jkf::matrix<U> all_zi = j2.get_all_zi();
  jkf::matrix<U> zi = j2.get_zi();
  int N_phys = j2.N_phys();
  jkf::matrix<U> neg_zi = j2.get_negative_zi();
  jkf::matrix<U> all_masses = j2.get_all_masses();
  jkf::matrix<U> masses = j2.get_masses();
  jkf::matrix<U> img_masses = j2.get_imaginary_masses();
  jkf::matrix<U> Amatr = j2.Amplitude_matrix(1);
  std::vector<U> w_vec = j2.W_vector(1);
  jkf::matrix<U> all_ampl = j2.get_all_amplitudes();
  jkf::matrix<U> ampl = j2.get_amplitudes();
}

void ODE_back_calls() { ODE_back_templ<double>(); }

template <class U> void ODE_no_back_templ() {
  jkf::matrix<U> m;
  jkf_no_back<U> j1;
  jkf_no_back<U> j2(m, 1, 5);
  int Th = j2.T_half();
  int T = j2.T();
  jkf::matrix<U> xk = j2.get_xk();
  int N_phys = j2.N_phys();
  jkf::matrix<U> zm = j2.get_zm();
  jkf::matrix<U> Mm = j2.get_Mm();
  jkf::matrix<U> Am = j2.get_Am();
  jkf::matrix<U> M_for = j2.get_M_forward();
  jkf::matrix<U> M_back = j2.get_M_backward();
  jkf::matrix<U> A_for = j2.get_A_forward();
  jkf::matrix<U> A_back = j2.get_A_backward();
}

void ODE_no_back_calls() { ODE_no_back_templ<double>(); }

} // namespace ODE

// 1x10 jkf::matrix (cannot average null size matrices)
jkf::matrix<double> foo(1, 10);
auto pair_systematics = Silibs2::Lattice::combine_systematics<double>({foo});

} // namespace Lattice

} // namespace cpp_partial_template_specialization

#endif
