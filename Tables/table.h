// table.h
/*!
This header declares the base class table,
from which matrix classes can be derived
*/

#include <functional>
#include <vector>

#include "../Functions/functions.h"

namespace Silibs2 {
namespace Tables {
#ifndef Silibs2_Tables_table_h
#define Silibs2_Tables_table_h

template <typename U> class table {
protected:
  int ROWS = 0;                             // number of rows
  int COLS = 0;                             // number of columns
  std::vector<std::vector<U>> LIST_OF_ROWS; // list of rows

public:
  table();
  ~table();

  /* Create a table of size ro*co with all elements equal to val */
  table<U>(const int &ro, const int &co);
  table<U>(const int &ro, const int &co, const U &val);

  /*! Resize the table */
  void resize(const int &ro, const int &co);
  void resize(const int &ro, const int &co, const U &val);

  /*! Create a table from another */
  table<U>(const table<U> &table);

  /*! Create a table from a file */
  table<U>(const std::string &path, const std::string &sep = "\t",
           const int &ign_lines = 0);

  /*! Create a table from a function */
  table<U>(const int &ro, const int &co,
           const std::function<U(const int &, const int &)> &fun);

  /* Returns the traspose of the table */
  table<U> transpose() const;
  /* Transposes *this */
  void transposeInPlace();

  /* loop over rows and columns for const methods */
  void
  loop_elements(const std::function<void(const int &, const int &)> &fun) const;
  /* loop over rows and columns when calling non-const methods */
  void loop_elements(const std::function<void(const int &, const int &)> &fun);

  int rows() const;                  /*!< ROWS */
  int cols() const;                  /*!< COLS */
  int size() const;                  /*!< ROWS*COLS */
  std::pair<int, int> shape() const; /*!< (ROWS, COLS) */

  /*!< list of rows of the table */
  std::vector<std::vector<U>> list_of_rows() const;

  /* checks if (i,j) is a valid index of the table */
  void check_access(const int &i, const int &j) const;

  // /* returns the index of VALUES correponding to the element (i,j) */
  // int index(const int &i, const int &j) const;

  U &get(const int &i, const int &j);
  U &operator()(const int &i, const int &j);
  const U &operator()(const int &i,
                      const int &j) const; /* for const callings */

  /*! Assignment operator */
  void operator=(const table<U> &m);

  /*! returns a block of size (p,q), starting at (i,j) */
  table<U> block(const int &i, const int &j, const int &p, const int &q) const;
  void remove_row(const int &r);          /*!< removes the r-th row */
  void remove_col(const int &c);          /*!< removes the c-th column */
  std::vector<U> row(const int &r) const; /*!< r-th row of *this */
  std::vector<U> col(const int &c) const; /*!< c-th columns of *this */

  // Set r-th row of the table equal to the vector.
  void set_row(const std::vector<U> &v, const int &r);
  void set_row(const int &r, const std::vector<U> &v);
  // Set c-th col of the table equal to the vector.
  void set_col(const std::vector<U> &v, const int &c);
  void set_col(const int &c, const std::vector<U> &v);

  /*!
  Insert row on the top of the r-th one and returns the resulting table
  r=ROWS --> 'v' becomes the last row
  */
  void insert_row(const std::vector<U> &v, const int &r);
  /*!
  Insert column on the left of the c-th one and returns the resulting table
  c = COLS --> 'v' becomes the last column
  */
  void insert_col(const std::vector<U> &v, const int &c);

  /*!<
  Converts the table to a std::string
  in order to print it out on a file
  */
  std::string to_string(const std::string &sep = "\t",
                        const int &prec = 16) const;

  /*! print on std::cout with column separator 'sep' */
  void print_cout(const std::string &sep = "\t") const;
  /*! print on std::cerr with column separator 'sep' */
  void print_cerr(const std::string &sep = "\t") const;
  /*! prints on file with column separator 'sep' and precision 'prec'*/
  void print(const std::string &file, const std::string &sep = "\t",
             const int &prec = 16, const std::string &header = "") const;

  void append(const std::string &dest, const std::string &sep = "\t",
              const std::string &header = "", const int &prec = 16) const;
};

/* --------- */
/* functions */
/* --------- */

/*  -------------------- */
/*! READING and PRINTING */
/*  -------------------- */

template <class U>
void print_tables(const std::string &path, const std::vector<table<U>> &M,
                  const std::vector<std::string> &header = "",
                  const std::string &sep = "\t", const int &prec = 16);

/*!
Extract from the file in 'path' the table of values found after the line
equal to the header 'head' (head="" --> the file is read from the beginning ).
It stops at the first empty line of when EOF is reached
ign_lines = number of lines ignored after the line of the 'head'
sep = separation between columns
*/
template <class U>
table<U> read_table_sep(const std::string &path, const std::string &head = "",
                        const int &ign_lines = 0,
                        const std::string &sep = "\t");

/*!
Like read_table_sep(),
but ignoring multiple occurencies of tabs and spaces
*/
template <class U>
table<U> read_table(const std::string &path, const std::string &head = "",
                    const int &ign_lines = 0);

/*! uses read_table to extract the 'n'-th row*/
template <class U>
std::vector<U> read_row(const int &n, const std::string &path,
                        const std::string &head = "", const int &ign_lines = 0);

/*! uses read_table to extract the 'n'-th column */
template <class U>
std::vector<U> read_col(const int &n, const std::string &path,
                        const std::string &head = "", const int &ign_lines = 0);

/*  ---------------------------- */
/*! OPERATIONS on the components */
/*  ---------------------------- */

template <class U, class V> table<U> cast_table(const table<V> &matr);
template <class T1, class T2> table<T1> change_type(const table<T2> &m);
template <class M_type1, class M_type2>
M_type1 convert_table(const M_type2 &matr);

/* Returns a table table M whose elements are : M_ij = f(A_ij) */
template <class U>
table<U> operation_comp_wise(const table<U> &A,
                             const std::function<U(const U &)> &f);

/* Returns a table M whose elements are : M_ij = pow(A_ij, a) */
template <class U1, class U2>
table<U1> pow_comp_wise(const table<U1> &A, const U2 &a);

/* Returns a table M whose elements are : M_ij = pow(A_ij, 2) */
template <class U> table<U> square_comp_wise(const table<U> &A);

/* Returns a table M whose elements are : M_ij = sqrt(A_ij) */
template <class U> table<U> sqrt_comp_wise(const table<U> &A);

/* Returns a table M whose elements are : M_ij = f(A_ij, B_ij) */
template <class U>
table<U> operation_comp_wise(const table<U> &A, const table<U> &B,
                             const std::function<U(const U &, const U &)> &f);

/* Returns a table M whose elements are : M_ij = A_ij/B_ij */
template <class U>
table<U> ratio_comp_wise(const table<U> &A, const table<U> &B);
/* Returns a table M whose elements are : M_ij = A_ij * B_ij */
template <class U>
table<U> mult_comp_wise(const table<U> &A, const table<U> &B);

/*! BUILDING from rows and columns */
template <class U>
table<U> table_from_rows(const std::vector<std::vector<U>> &rows);
template <class U>
table<U> table_from_cols(const std::vector<std::vector<U>> &cols);

#endif
} // namespace Tables
} // namespace Silibs2
