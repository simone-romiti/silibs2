// table.hpp
/* This file defines what is declared in table.h */

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/multiprecision/gmp.hpp>
#include <cassert>
#include <fstream>
#include <random>
#include <string>
#include <thread>

#include "./table.h"

// #include "../Functions/functions.hpp"

namespace Silibs2 {
namespace Tables {
#ifndef Silibs2_Tables_table_hpp
#define Silibs2_Tables_table_hpp
using namespace Silibs2::Functions;

template <class U> table<U>::table() {}
template <class U> table<U>::~table() {}

template <class U> table<U>::table(const int &ro, const int &co) {
  resize(ro, co);
}

template <class U> table<U>::table(const int &ro, const int &co, const U &val) {
  resize(ro, co, val);
}

template <class U> void table<U>::resize(const int &ro, const int &co) {
  ROWS = ro;
  COLS = co;

  LIST_OF_ROWS.resize(ROWS);
  for (int i = 0; i < ROWS; ++i) {
    LIST_OF_ROWS[i].resize(COLS);
  }
} // resize()

template <class U>
void table<U>::resize(const int &ro, const int &co, const U &val) {
  ROWS = ro;
  COLS = co;

  LIST_OF_ROWS.resize(ROWS);
  for (int i = 0; i < ROWS; ++i) {
    LIST_OF_ROWS[i].resize(COLS);
  }

  for (int i1 = 0; i1 < ROWS; ++i1) {
    for (int i2 = 0; i2 < COLS; ++i2) {
      LIST_OF_ROWS[i1][i2] = boost::lexical_cast<U>(val);
    }
  }
} // resize()

template <class U> table<U>::table(const table<U> &matr) { *this = matr; }

template <class U>
table<U>::table(const std::string &path, const std::string &sep,
                const int &ign_lines) {
  using namespace boost;
  check_path(path);

  int n_rows = rows_file(path, ign_lines);
  int n_cols = columns_file(path, sep, ign_lines);
  this->resize(n_rows, n_cols);

  std::ifstream IN(path);
  std::string line;
  std::vector<std::string> token;
  for (int i = 0; i < ign_lines; i++) {
    getline(IN, line, '\n');
  }

  int a = 0; /* [non-empty row] index*/
  while (getline(IN, line, '\n')) {
    if (line.length() > 0) {
      boost::split(token, line, boost::is_any_of(sep));
      for (int b = 0; b < n_cols; b++) {
        (*this)(a, b) = boost::lexical_cast<U>(token[b]);
      }
      ++a;
    }
  }
  IN.close();
} // table()

template <class U>
table<U>::table(const int &ro, const int &co,
                const std::function<U(const int &, const int &)> &fun) {
  this->resize(ro, co);
  this->loop_elements(
      [&](const int &i, const int &j) { (*this)(i, j) = fun(i, j); });
} // table()

template <class U> table<U> table<U>::transpose() const {
  table<U> M_tr(COLS, ROWS);

  auto lp = [&](const int &i, const int &j) { M_tr(j, i) = (*this)(i, j); };
  this->loop_elements(lp);

  return M_tr;
} // transpose()

template <class U> void table<U>::transposeInPlace() {
  *this = this->transpose();
}

template <class U>
void table<U>::loop_elements(
    const std::function<void(const int &, const int &)> &fun) const {
  /* Single std::thread function */
  auto single_thread = [&](const int &i, const int &j) { fun(i, j); };

  std::vector<std::thread> thr(0);
  for (int i = 0; i < ROWS; ++i) {
    for (int j = 0; j < COLS; ++j) {
      std::thread t1(single_thread, i, j);
      thr.push_back(std::move(t1));
    }
  }

  for (int t = 0; t < ROWS * COLS; ++t) {
    thr[t].join();
  }

} // loop_elements()

template <class U>
void table<U>::loop_elements(
    const std::function<void(const int &, const int &)> &fun) {
  /* Single std::thread function */
  auto single_thread = [&](const int &i, const int &j) { fun(i, j); };

  std::vector<std::thread> thr(0);
  for (int i = 0; i < ROWS; ++i) {
    for (int j = 0; j < COLS; ++j) {
      std::thread t1(single_thread, i, j);
      thr.push_back(std::move(t1));
    }
  }

  for (int t = 0; t < ROWS * COLS; ++t) {
    thr[t].join();
  }

} // loop_elements()

template <class U> int table<U>::rows() const { return ROWS; }
template <class U> int table<U>::cols() const { return COLS; }
template <class U> int table<U>::size() const { return ROWS * COLS; }
template <class U> std::pair<int, int> table<U>::shape() const {
  return std::make_pair(ROWS, COLS);
}

template <class U> std::vector<std::vector<U>> table<U>::list_of_rows() const {
  return LIST_OF_ROWS;
}

template <class U>
void table<U>::check_access(const int &i, const int &j) const {
  assert(0 <= i && i < ROWS && "Invalid row index");
  assert(0 <= j && j < COLS && "Invalid column index");
} // check_access()

template <class U> U &table<U>::get(const int &i, const int &j) {
  this->check_access(i, j);
  return LIST_OF_ROWS[i][j];
} // get()

template <class U> U &table<U>::operator()(const int &i, const int &j) {
  return get(i, j);
} // operator ()
template <class U>
const U &table<U>::operator()(const int &i, const int &j) const {
  this->check_access(i, j);
  return LIST_OF_ROWS[i][j];
} // operator ()

template <class U> void table<U>::operator=(const table<U> &t) {
  (*this).LIST_OF_ROWS = t.list_of_rows(); //!< same elements
  (*this).ROWS = t.rows();                 //!< same rows
  (*this).COLS = t.cols();                 //!< same columns
}

/* returns block of size (p,q), starting at (i,j) */
template <class U>
table<U> table<U>::block(const int &i, const int &j, const int &p,
                         const int &q) const {
  assert(p >= 0 && j >= 0 && "Invalid block: negative block size.\n");
  assert(i + p <= ROWS && j + q <= COLS &&
         "Invalid block: block exceeds table sizes.\n");
  table<U> B(p, q);
  for (int r = 0; r < p; ++r) {
    for (int c = 0; c < q; ++c) {
      B(r, c) = (*this)(i + r, j + c);
    }
  }
  return B;
} // block()

template <class U> void table<U>::remove_row(const int &r) {
  table<U> matr(ROWS - 1, COLS);
  for (int j = 0; j < COLS; j++) {
    for (int i1 = 0; i1 < r; ++i1) {
      matr(i1, j) = (*this)(i1, j);
    }
    for (int i2 = r; i2 < ROWS - 1; ++i2) {
      matr(i2, j) = (*this)(i2 + 1, j);
    }
  }
  *this = matr;
} // remove_row()

template <class U> void table<U>::remove_col(const int &c) {
  table<U> matr(ROWS, COLS - 1);
  for (int i = 0; i < ROWS; i++) {
    for (int j1 = 0; j1 < c; j1++) {
      matr(i, j1) = (*this)(i, j1);
    }
    for (int j2 = c; j2 < COLS - 1; j2++) {
      matr(i, j2) = (*this)(i, j2 + 1);
    }
  }
  *this = matr;
} // remove_col()

template <class U> std::vector<U> table<U>::row(const int &r) const {
  return LIST_OF_ROWS[r];
} // row()

template <class U> std::vector<U> table<U>::col(const int &c) const {
  const int n_r = this->rows(); // number of rows
  std::vector<U> col_c(n_r);
  for (int i = 0; i < n_r; ++i) {
    col_c[i] = (*this)(i, c);
  }
  return col_c;
} // col()

template <class U>
void table<U>::set_row(const std::vector<U> &v, const int &r) {
  assert(0 <= r && r < ROWS && "Invalid row index.");
  assert((int)v.size() == COLS && "(vector size) != (number of cols).");
  for (int c = 0; c < COLS; c++) {
    (*this)(r, c) = v[c];
  }
} // set_row()

template <class U>
void table<U>::set_row(const int &r, const std::vector<U> &v) {
  this->set_row(v, r);
} // set_row()

template <class U>
void table<U>::set_col(const std::vector<U> &v, const int &c) {
  assert(0 <= c && c < COLS && "Invalid colum index.");
  assert((int)v.size() == ROWS && "(vector size) != (number of rows).");
  for (int r = 0; r < ROWS; r++) {
    (*this)(r, c) = v[r];
  }
}

template <class U>
void table<U>::set_col(const int &c, const std::vector<U> &v) {
  this->set_col(v, c);
}

template <class U>
void table<U>::insert_row(const std::vector<U> &v, const int &r) {
  assert(0 <= r && r <= ROWS && "Cannot insert row at the specified position");
  int C = COLS;
  if (C == 0) {
    C = v.size();
  }
  table<U> A(ROWS + 1, C);
  for (int i1 = 0; i1 < r; ++i1) {
    A.set_row(this->row(i1), i1);
  }
  A.set_row(v, r);
  for (int i2 = r + 1; i2 < ROWS + 1; ++i2) {
    A.set_row(this->row(i2 - 1), i2);
  }
  (*this) = A;
} // insert_row()

template <class U>
void table<U>::insert_col(const std::vector<U> &v, const int &c) {
  assert(0 <= c && c <= COLS &&
         "Cannot insert column at the specified position.");
  int R = ROWS;
  if (R == 0) {
    R = v.size();
  }
  table<U> A(R, COLS + 1);
  for (int j1 = 0; j1 < c; ++j1) {
    A.set_col(this->col(j1), j1);
  }
  A.set_col(v, c);
  for (int j2 = c + 1; j2 < COLS + 1; ++j2) {
    A.set_col(this->col(j2 - 1), j2);
  }
  (*this) = A;
} // insert_col()

template <class U>
std::string table<U>::to_string(const std::string &sep, const int &prec) const {
  std::ostringstream OUT;              /*!< stream */
  OUT.precision(prec);                 /*!< setting the precision */
  OUT.setf(std::ios_base::scientific); /*!< scientific notation */

  /*! loop over matrix components */
  for (int a = 0; a < ROWS; ++a) {
    for (int b = 0; b < COLS - 1; ++b) {
      OUT << (*this)(a, b) << sep;
    }
    OUT << (*this)(a, COLS - 1) << "\n";
  }
  return (std::string)OUT.str();
} // to_string()

template <class U> void table<U>::print_cout(const std::string &sep) const {
  std::cout << "\n";
  for (int i = 0; i < ROWS; ++i) {
    for (int j = 0; j < COLS; ++j) {
      std::cout << (*this)(i, j) << sep;
    }
    std::cout << "\n";
  }
  std::cout << "\n";
} // print_cout()

template <class U> void table<U>::print_cerr(const std::string &sep) const {
  std::cerr << "\n";
  for (int i = 0; i < ROWS; ++i) {
    for (int j = 0; j < COLS; ++j) {
      std::cerr << (*this)(i, j) << sep;
    }
    std::cerr << "\n";
  }
  std::cerr << "\n";
} // print_cerr()

template <class U>
void table<U>::print(const std::string &file, const std::string &sep,
                     const int &prec, const std::string &header) const {
  touch(file);
  std::ofstream OUT(file);
  OUT.precision(prec);
  OUT.setf(std::ios_base::scientific);

  if (header != "") {
    OUT << header << "\n";
  } /* printing the header if not empty */

  for (int a = 0; a < ROWS; ++a) {
    for (int b = 0; b < COLS - 1; ++b) {
      OUT << (*this)(a, b) << "\t";
    }
    OUT << (*this)(a, COLS - 1) << "\n";
  }

  OUT.close();
} // print()

template <class U>
void table<U>::append(const std::string &dest, const std::string &sep,
                      const std::string &header, const int &prec) const {
  std::ofstream OUT;
  OUT.precision(prec);
  OUT.setf(std::ios_base::scientific);

  OUT.open(dest, std::ios_base::app); // append instead of overwrite
  OUT << header << "\n";
  for (int i = 0; i < this->rows(); ++i) {
    for (int j = 0; j < this->cols(); ++j) {
      OUT << (*this)(i, j) << sep;
    }
    OUT << "\n";
  }
  OUT.close();
} // append()

/* --------- */
/* functions */
/* --------- */

template <class U>
void print_tables(const std::string &path, const std::vector<table<U>> &M,
                  const std::vector<std::string> &header,
                  const std::string &sep, const int &prec) {
  touch(path);
  assert(M.size() == header.size() &&
         "Cannot print matrices. Invalid number of headers.");

  // std::ofstream OUT(path);
  // OUT.precision(prec);
  // OUT.setf(std::ios_base::scientific);
  clear_content(path);
  int n = M.size(); /* number of matrices to print */
  for (int i = 0; i < n; ++i) {
    M[i].append(path, sep, header[i], prec);
    // OUT << "\n";
    append_string("\n", path);
  };
} // end print_tables()

template <class U>
table<U> read_table_sep(const std::string &path, const std::string &head,
                        const int &ign_lines, const std::string &sep) {
  check_path(path);
  std::ifstream IN(path);

  std::string line = "";
  std::vector<std::string> token;

  while (line != head) {
    if (IN.eof()) {
      std::cerr << "\'" << head << "\' not found in " << path << "\n";
      abort();
    }
    getline(IN, line, '\n');
  }
  for (int i = 0; i < ign_lines; ++i) {
    getline(IN, line, '\n');
  }

  getline(IN, line, '\n');
  boost::split(token, line, boost::is_any_of(sep), boost::token_compress_on);
  int C = token.size();
  table<std::string> matr(0, C);
  int r = 0;
  while (line.length() != 0) {
    matr.insert_row(token, r);
    getline(IN, line, '\n');
    boost::split(token, line, boost::is_any_of(sep));
    ++r;
  }
  IN.close();

  return cast_table<U>(matr);
} // end extract_table()

template <class U>
table<U> read_table(const std::string &path, const std::string &head,
                    const int &ign_lines) {
  check_path(path);
  std::ifstream IN(path);

  std::string line = "not " + head;
  if (head.length() != 0) { /* reads until it finds the header */
    while (boost::algorithm::trim_copy(line) != head) {
      if (IN.eof()) {
        std::cerr << "\'" << head << "\' not found in " << path << "\n";
        abort();
      }
      getline(IN, line, '\n');
    }
  }

  /* skips unwanted lines after the header*/
  for (int i = 0; i < ign_lines; ++i) {
    getline(IN, line, '\n');
  }

  /* counting the number of rows */
  int start = 0;
  line = "";
  int nR = 0;
  while (line.length() == 0) {
    start = IN.tellg();
    getline(IN, line, '\n');
  }

  /* deducing the number of columns from the first non-empty line */
  std::string word;
  int nC = -1;
  std::istringstream iss(line);
  while (iss) {
    iss >> word;
    ++nC;
  }

  /* continues reading until the line is empty or getline() = false */
  while (line.length() != 0) {
    ++nR;
    if (!getline(IN, line, '\n')) {
      break;
    }
  }

  /* comes back to the first line of the matrix */
  IN.clear();
  IN.seekg(start, IN.beg);

  table<std::string> matr(nR, nC);

  for (int i = 0; i < nR; ++i) {
    for (int j = 0; j < nC; ++j) {
      if (IN.eof()) {
        std::cerr << "Error : Invalid table. IN.eof() reached too early."
                  << "\n";
        abort();
      }
      IN >> matr(i, j);
    }
  }
  IN.close();

  return cast_table<U>(matr);
} // end read_table()

template <class U>
std::vector<U> read_row(const int &n, const std::string &path,
                        const std::string &head, const int &ign_lines) {
  return read_table<U>(path, head, ign_lines).row(n);
}

template <class U>
std::vector<U> read_col(const int &n, const std::string &path,
                        const std::string &head, const int &ign_lines) {
  return read_table<U>(path, head, ign_lines).col(n);
}

template <class U, class V> table<U> cast_table(const table<V> &matr) {
  int R = matr.rows();
  int C = matr.cols();
  table<U> M(R, C);

  /* Single thread function */
  auto single_thread = [&](const int &i, const int &j) {
    M(i, j) = boost::lexical_cast<U>(matr(i, j));
  };

  std::vector<std::thread> thr(0);
  for (int i = 0; i < R; ++i) {
    for (int j = 0; j < C; ++j) {
      std::thread t1(single_thread, i, j);
      thr.push_back(std::move(t1));
    }
  }

  for (int t = 0; t < R * C; ++t) {
    thr[t].join();
  }

  return M;
} // end cast_table()

template <class T1, class T2> table<T1> change_type(const table<T2> &m) {
  table<T1> M(m.rows(), m.cols());
  M.loop_elements(
      [&](const int &i, const int &j) { M(i, j) = static_cast<T1>(m(i, j)); });
  return M;
} // end change_type()

template <class M_type1, class M_type2>
M_type1 convert_table(const M_type2 &matr) {
  int R = matr.rows();
  int C = matr.cols();
  M_type1 M(R, C);

  /* Single thread function */
  auto single_thread = [&](const int &i, const int &j) {
    M(i, j) = matr(i, j);
  };

  std::vector<std::thread> thr(0);
  for (int i = 0; i < R; ++i) {
    for (int j = 0; j < C; ++j) {
      std::thread t1(single_thread, i, j);
      thr.push_back(std::move(t1));
    }
  }

  for (int t = 0; t < R * C; ++t) {
    thr[t].join();
  }

  return M;
} // end convert_table()

template <class U>
table<U> operation_comp_wise(const table<U> &A,
                             const std::function<U(const U &)> &f) {
  int R = A.rows(), C = A.cols();

  table<U> M(R, C);
  M.loop_elements([&](const int &i, const int &j) { M(i, j) = f(A(i, j)); });
  return M;
} // end operation_comp_wise()

template <class U1, class U2>
table<U1> pow_comp_wise(const table<U1> &A, const U2 &a) {
  std::function<U1(const U1 &)> f = [&](const U1 &x) { return pow(x, a); };
  return operation_comp_wise(A, f);
} // end pow_comp_wise()

template <class U> table<U> square_comp_wise(const table<U> &A) {
  return pow_comp_wise(A, 2.0);
} // end pow_comp_wise()

template <class U> table<U> sqrt_comp_wise(const table<U> &A) {
  std::function<U(const U &)> f = [&](const U &x) { return sqrt(x); };
  return operation_comp_wise(A, f);
} // end sqrt_comp_wise()

template <class U>
table<U> operation_comp_wise(const table<U> &A, const table<U> &B,
                             const std::function<U(const U &, const U &)> &f) {
  assert((A.shape() == B.shape()) && "Cannot compare the two matrices.");
  int R = A.rows();
  int C = A.cols();

  table<U> M(R, C);
  M.loop_elements(
      [&](const int &i, const int &j) { M(i, j) = f(A(i, j), B(i, j)); });
  return M;
} // end operation_comp_wise()

template <class U>
table<U> ratio_comp_wise(const table<U> &A, const table<U> &B) {
  std::function<U(const U &, const U &)> f = [&](const U &a, const U &b) {
    return a / b;
  };
  return operation_comp_wise(A, B, f);
} // end ratio_comp_wise()

template <class U>
table<U> mult_comp_wise(const table<U> &A, const table<U> &B) {
  std::function<U(const U &, const U &)> f = [&](const U &a, const U &b) {
    return a * b;
  };
  return operation_comp_wise(A, B, f);
} // end multiplication_comp_wise()

template <class U>
table<U> table_from_rows(const std::vector<std::vector<U>> &rows) {
  int C = rows[0].size(), R = rows.size();
  table<U> M(R, C);
  /* Single thread function */
  auto single_thread = [&](const int &i) {
    assert(C == (int)rows[i].size() && "Not all rows have the same size.");
    M.set_row(rows[i], i);
  };
  std::vector<std::thread> thr(0);
  for (int i = 0; i < R; ++i) {
    std::thread t1(single_thread, i);
    thr.push_back(std::move(t1));
  }
  for (int i = 0; i < R; ++i) {
    thr[i].join();
  }
  return M;
} // end table_from_rows()

template <class U>
table<U> table_from_cols(const std::vector<std::vector<U>> &cols) {
  int R = cols[0].size(), C = cols.size();
  table<U> M(R, C);
  /* Single thread function */
  auto single_thread = [&](const int &j) {
    assert(R == (int)cols[j].size() && "Not all columns have the same size.");
    M.set_col(cols[j], j);
  };
  std::vector<std::thread> thr(0);
  for (int j = 0; j < C; ++j) {
    std::thread t1(single_thread, j);
    thr.push_back(std::move(t1));
  }
  for (int j = 0; j < C; ++j) {
    thr[j].join();
  }
  return M;
} // end table_from_cols()

#endif
} // namespace Tables
} // namespace Silibs2
