// functions.cpp
/*! Definitions of functions.h */

#include "./functions.hpp"

namespace Silibs2 {
namespace Tables {
#ifndef Silibs2_Tables_functions_cpp
#define Silibs2_Tables_functions_cpp
using namespace Silibs2::Functions;

namespace cpp_partial_template_definition {
using namespace Silibs2::Functions;

auto print_tables_double = print_tables<double>;
auto print_tables_string = print_tables<std::string>;

auto read_table_sep_double = read_table_sep<double>;
auto read_table_sep_string = read_table_sep<std::string>;

auto read_table_double = read_table<double>;
auto read_table_string = read_table<std::string>;

auto read_row_double = read_row<double>;
auto read_row_string = read_row<std::string>;

auto read_col_double = read_col<double>;
auto read_col_string = read_col<std::string>;

auto cast_table_double_string = cast_table<double, std::string>;
auto cast_table_string_double = cast_table<std::string, double>;

auto change_type_double_string = change_type<double, std::string>;
auto change_type_string_double = change_type<std::string, double>;

auto convert_table_double_string = convert_table<double, std::string>;
auto convert_table_string_double = convert_table<std::string, double>;

table<double> (*operation_comp_wise_double_1)(
    const table<double> &A, const std::function<double(const double &)> &) =
    operation_comp_wise<double>;

table<double> (*operation_comp_wise_double_2)(
    const table<double> &, const table<double> &,
    const std::function<double(const double &, const double &)> &) =
    operation_comp_wise<double>;

auto pow_comp_wise_double_double = pow_comp_wise<double, double>;
auto pow_comp_wise_double_int = pow_comp_wise<double, int>;

auto ratio_comp_wise_double = ratio_comp_wise<double>;
auto mult_comp_wise_double = mult_comp_wise<double>;

auto table_from_rows_double = table_from_rows<double>;
auto table_from_rows_string = table_from_rows<std::string>;

auto table_from_cols_double = table_from_cols<double>;
auto table_from_cols_string = table_from_cols<std::string>;

} // namespace cpp_partial_template_definition
#endif
} // namespace Tables
} // namespace Silibs2
