// functions.h
/* utilities to interact with the table class objects */

#include "../Functions/functions.h"

#include "./table.h"

namespace Silibs2 {
namespace Tables {
#ifndef Silibs2_Tables_functions_h
#define Silibs2_Tables_functions_h
using namespace Silibs2::Functions;

#endif
} // namespace Tables
} // namespace Silibs2
