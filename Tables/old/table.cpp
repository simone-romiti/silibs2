// table.cpp
/*
This file defines what is declared in table.h
*/

#include "./functions.cpp"

namespace Silibs2 {
namespace Tables {
#ifndef Silibs2_Tables_table_cpp
#define Silibs2_Tables_table_cpp
using namespace Silibs2::Functions;

namespace cpp_partial_template_definition {

template <class U> void table_methods() {
  table<U> t0;
  table<U> t1(0, 0);
  table<U> t2 = t1;
  t1.resize(0, 0);
  table<U> t3("no_a_real_path", "", 0);
  table<U> t4(0, 0, [](const int &, const int &) -> U { return 0; });
  t4.transposeInPlace();
  t4.loop_elements([](const int &, const int &) -> void {}); // const method
  t4.loop_elements([&](const int &, const int &) -> void {
    t4(0, 0) = 0.0;
  }); // non-const method
  int a = t4.rows();
  a = t4.cols();
  a = t4.size();
  auto ab = t4.shape();
  auto rows = t4.list_of_rows();
  t4.check_access(0, 0);
  auto x = t4.get(0, 0);
  x = t4(0, 0);
  t4(0, 0) = 1;
  table<U> t5 = t4.block(0, 0, 1, 1);
  t5.remove_row(0);
  t5.remove_col(0);
  auto v0 = t5.row(0);
  v0 = t5.col(0);
  t5.set_row(0, v0);
  t5.set_col(0, v0);
  t5.insert_row(v0, 0);
  t5.insert_col(v0, 0);
  auto st5 = t5.to_string();
  t5.print_cout();
  t5.print_cerr();
  t5.print("not_a_file");
  t5.append("not_a_path");
}

auto table_methods_double = table_methods<double>;
auto table_methods_string = table_methods<std::string>;

} // namespace cpp_partial_template_definition
#endif
} // namespace Tables
} // namespace Silibs2
