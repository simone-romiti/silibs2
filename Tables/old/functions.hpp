// functions.hpp
/*! Definitions of functions.h */

#include "./functions.h"

namespace Silibs2 {
namespace Tables {
#ifndef Silibs2_Tables_functions_hpp
#define Silibs2_Tables_functions_hpp
using namespace Silibs2::Functions;

#endif
} // namespace Tables
} // namespace Silibs2
