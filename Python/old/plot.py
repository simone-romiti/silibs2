# plot.py
# utilities for plotting data

import matplotlib
# matplotlib.rcParams['text.usetex'] = True # output latex
from matplotlib import pyplot as plt
import pandas as pd
import numpy as np

def plot_corr(filepath, name_corr, a, b, separator="\t"):
	lat_pts = pd.read_csv(filepath, header=None, sep=separator).to_numpy()
	all_y = lat_pts[:, 0]
	all_err_y = lat_pts[:, 1]
	all_t = range(1, len(all_y)+1)
	
	t = []
	y = []
	err_y = []
	for tau in range(a, b):
		t.append(tau)
		y.append(all_y[tau-1])
		err_y.append(all_err_y[tau-1])
		
	# inward ticks of axes
	plt.tick_params(direction='in', top=True, right=True)
	
	# plot of data of the correlator
	
	plt.plot(t, y, color='k', marker = ".", markersize = 0.5 , linestyle='None', label=name_corr)
	plt.errorbar(t, y, yerr=err_y, capsize=1, elinewidth=0.5, ls='none', color="indigo")
	plt.autoscale(enable=True, axis="y", tight=False)
	
	plt.grid(linestyle='--', linewidth=0.05);
	plt.legend();
	
	plt.savefig(name_corr + ".pdf")
	
	plt.cla()
	plt.clf()
	plt.close()

