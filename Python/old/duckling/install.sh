#!/bin/bash

chmod +x duckling.py

# create the installation directory and copying the hole duckling source code in it
inst_dir=$(cat ./inst_dir.txt)
sudo mkdir -p $inst_dir
sudo cp  -r  ./  $inst_dir

# creating a global executable
glob_exe=/usr/bin/duckling

glob_exe_example=./glob_exe.sh
echo $inst_dir'duckling.py $@' > $glob_exe_example

sudo cp $glob_exe_example $glob_exe
sudo chmod +x $glob_exe

