#!/usr/bin/python3 

import sys
args = sys.argv;

if (len(args)==1):
    print("No arguments passed. Use --help for usage informations.")
    quit()
if (args[1]=="--help"):
        from help import help
        help()
elif (args[1]=="-xy"):
    from xy import plot
elif (args[1]=="-yey"):
    from yey import plot
# elif (args[1]=="-xexy"):
#     from xexy import plot
# elif (args[1]=="-xyey"):
#     from xyey import plot
elif (args[1]=="-xexyey"):
    from xexyey import plot
else:
    print("Invalid option. Type --help for help")
       
for i in range(2,len(args)):
    plot(args[i])

