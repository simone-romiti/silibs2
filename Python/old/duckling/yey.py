# xy.py
# plot of data in the yey format

import matplotlib
from matplotlib import pyplot as plt
import os

def plot(file):
	FILE = open(file, 'r'); Lines = FILE.readlines()
	x = []; y = []; ey = []
	t = 1;
	for l in Lines:
			values = l.strip().split();
			if(len(values)>0):
				x.append(t); t += 1; 
				y.append(float(values[0])); ey.append(float(values[1]));

	plt.plot(x, y, marker='.', markersize=0.7, linestyle='None')
	plt.errorbar(x, y, yerr=ey, linestyle='None');
	plt.xlabel("t"); plt.ylabel("y(t)"); plt.title(os.path.abspath(file))	
	
	plt.show()
	plt.close()


