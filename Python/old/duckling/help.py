# help.py
# print the help message

help_string = """
duckling plotting utility
USAGE : duckling [OPTION] [FILES]...

Options:

    --help   :   Return this help page
    -xy      :   Plot data in the xy format 
    -yey     :   Plot data in the yey format (assuming x=[1,...,N]) 

Examples:

    duckling -xy  file1.dat file2.dat file3.dat
    duckling -yey file.dat
"""

def help():
    print(help_string)
    
    