# xy.py
# plot of data in the xy format

import matplotlib
from matplotlib import pyplot as plt
import os

def plot(file):
	FILE = open(file, 'r'); Lines = FILE.readlines()
	x = []; y = []; ex = []; ey = []
	for l in Lines:
			values = l.strip().split();	
			if(len(values)>0):
				x.append(float(values[0])); 
				ex.append(float(values[1])); 
				y.append(float(values[2])); 
				ey.append(float(values[3]));
				
	plt.plot(x, y, marker='.', markersize=0.7, linestyle='None')
	plt.errorbar(x, y, xerr=ex, yerr=ey, linestyle='None');
	plt.xlabel("x"); plt.ylabel("y(x)"); plt.title(os.path.abspath(file))	
	
	plt.show()
	plt.close()

