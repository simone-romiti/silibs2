# xy.py
# plot of data in the xy format

import matplotlib
from matplotlib import pyplot as plt
import os

def plot(file):
	FILE = open(file, 'r'); Lines = FILE.readlines()
	x = []; y = [];
	for l in Lines:
			xy = l.strip().split();	
			if(len(xy)>0):
				x.append(float(xy[0])); 
				y.append(float(xy[1]));

	plt.plot(x, y)
	plt.xlabel("x"); plt.ylabel("y"); plt.title(file);
	
	plt.show()	
	plt.close()

