# Duckling plot generator

## Fast installation

``` bash
chmod +x ./install.sh # making it executable
./install.sh # installation
```

The latter commands do automatically what is described below.

## Custom installation

This folder contains **duckling.py**,
a python script for reading text files organized in columns corresponding to data
It can be executed providing any number of files as arguments.
Make **duckling.py** executable with:

``` bash
chmod +x duckling.py
```

In order to install duckling ad then execute it independently of the location, follow these steps:

``` bash
sudo touch /usr/local/bin/duckling
sudo cat ./duckling.py > /usr/local/bin/duckling
sudo chmod +x /usr/local/bin/duckling
```

## Uninstall duckling

In order to uninstall duckling give the following commands:

``` bash
chmod +x ./uninstall.sh # making it executable
./uninstall.sh # uninstallation
```
