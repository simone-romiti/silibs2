# yey.py

import matplotlib
# matplotlib.rcParams['text.usetex'] = True # output latex
from matplotlib import pyplot as plt
# import numpy as np
import os
import pickle

from siplot.ranges import automatic_range

def plot(file, xa=0, xb=0, ya=0, yb=0, t_first = 1, show=False, title="", xname="", yname="", ext=".dat"):
	out_dir = os.path.dirname(file) + '/';
	
	FILE = open(file, 'r'); Lines = FILE.readlines()
	x = []; y = []; ey = []
	t = t_first;
	for l in Lines:
			values = l.strip().split();
			if(len(values)>0):
				x.append(t); t += 1; 
				y.append(float(values[0])); ey.append(float(values[1]));

	# inward ticks of axes
	plt.tick_params(direction='in', top=True, right=True)
	plt.grid(linestyle='--', linewidth=0.05);
	
	plt.plot(x, y, color='indigo', marker = ".", markersize = 0.5 , linestyle='None')	
	plt.errorbar(x, y, yerr=ey, elinewidth=0.5, ls='none', color="indigo")

	# adding margins for better visualization
	plot_margin = 0.25

	x0, x1, y0, y1 = plt.axis()
	plt.axis((x0 - plot_margin,
          x1 + plot_margin,
          y0 - plot_margin,
          y1 + plot_margin))

	range_auto = automatic_range(x,xa,xb, y,ya,yb);
	xa_out = range_auto[0]; xb_out = range_auto[1];
	ya_out = range_auto[2]; yb_out = range_auto[3];

	plt.xlim(xa_out,xb_out); 
	plt.ylim(ya_out,yb_out);
	
	out_pickle = os.path.basename(file).replace(ext, ".pickle")
	pickle.dump(plt.figure(1), open(out_dir+out_pickle, 'wb')) # interactive plot
	
	# Title and axis labels	
	plt.xlabel(xname)
	plt.ylabel(yname)
	plt.title(title)

	# saving the plot
	out_pdf = os.path.basename(file).replace(ext, ".pdf")
	plt.savefig(out_dir+out_pdf) # static pdf
	

	plt.show() if show else None
	plt.close()

