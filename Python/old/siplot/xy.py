# siplot.py
# utilities for plotting data

import matplotlib
# matplotlib.rcParams['text.usetex'] = True # output latex
from matplotlib import pyplot as plt
# import pandas as pd
# import numpy as np
import os
import pickle


def plot(file, xa=0, xb=0, ya=0, yb=0, show=False, title="", xname="", yname="", ext=".dat"):
	FILE = open(file, 'r'); Lines = FILE.readlines()
	x = []; y = [];
	for l in Lines:
			xy = l.strip().split();	
			if(len(xy)>0):
				x.append(float(xy[0])); y.append(float(xy[1]));

	# inward ticks of axes
	plt.tick_params(direction='in', top=True, right=True)
	plt.grid(linestyle='--', linewidth=0.05);
	plt.plot(x, y, color='indigo', marker = "o", markersize = 1.0 , linestyle='None')	
	
	plt.xlim(xa,xb) if (xa != xb) else None; 
	plt.ylim(ya,yb) if (ya != yb) else None;

	plt.autoscale();
	plt.xlabel(xname)
	plt.ylabel(yname)
	plt.title(title)

	# adding margins for better visualization
	plot_margin = 0.25

	x0, x1, y0, y1 = plt.axis()
	plt.axis((x0 - plot_margin,
          x1 + plot_margin,
          y0 - plot_margin,
          y1 + plot_margin))

	# saving the plot
	out_dir = os.path.dirname(file) + '/';
	out_pdf = os.path.basename(file).replace(ext, ".pdf")
	out_pickle = os.path.basename(file).replace(ext, ".pickle")
	
	plt.savefig(out_dir+out_pdf) # static pdf
	pickle.dump(plt.figure(1), open(out_dir+out_pickle, 'wb')) # interactive plot

	# show
	if show:
		plt.show()
	
	plt.close()

