# xyexey.py

import matplotlib
# matplotlib.rcParams['text.usetex'] = True # output latex
from matplotlib import pyplot as plt
# import numpy as np
import os
import pickle

from siplot.ranges import automatic_range


def plot(file, xa=0, xb=0, ya=0, yb=0, show=False, title="", xname="", yname="", ext=".dat"):
	FILE = open(file, 'r'); Lines = FILE.readlines()
	x = []; y = []; ex = []; ey = []
	for l in Lines:
			values = l.strip().split();	
			if(len(values)>0):
				x.append(float(values[0])); 
				ex.append(float(values[1])); 
				y.append(float(values[2])); 
				ey.append(float(values[3]));

	# inward ticks of axes
	plt.tick_params(direction='in', top=True, right=True)
	plt.grid(linestyle='--', linewidth=0.05);
	plt.plot(x, y, color='indigo', marker = ".", markersize = 1.0 , linestyle='None')	
	plt.errorbar(x, y, xerr=ex, yerr=ey, capsize=1, elinewidth=0.5, ls='none', color="indigo")
	
	range_auto = automatic_range(x,xa,xb, y,ya,yb);
	xa_out = range_auto[0]; xb_out = range_auto[1];
	ya_out = range_auto[2]; yb_out = range_auto[3];
	
	plt.xlabel(xname)
	plt.ylabel(yname)
	plt.title(title)

	# adding margins for better visualization
	plot_margin = 0.25

	x0, x1, y0, y1 = plt.axis()
	plt.axis((x0 - plot_margin,
          x1 + plot_margin,
          y0 - plot_margin,
          y1 + plot_margin))

	plt.xlim(xa_out,xb_out);
	plt.ylim(ya_out,yb_out);
	
	# print(xa_out,xb_out)
	# print(ya_out,yb_out)
	
	# saving the plot
	out_dir = os.path.dirname(file) + '/';
	out_pdf = os.path.basename(file).replace(ext, ".pdf")
	out_pickle = os.path.basename(file).replace(ext, ".pickle")
	
	plt.savefig(out_dir+out_pdf) # static pdf
	pickle.dump(plt.figure(1), open(out_dir+out_pickle, 'wb')) # interactive plot

	if show:
		plt.show()
	plt.close()

