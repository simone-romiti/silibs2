#!/usr/bin/env python3

def automatic_range(x, xa, xb, y, ya, yb):
	nx = len(x); ny = len(y);
	if nx!=ny:
		print("invalid data. len(x)!=len(y)")
		quit()
	
	xa_out=0.0; xb_out=0.0; 
	ya_out=0.0; yb_out=0.0;
	
	# xa==xb and ya!=yb
	if(xa==xb and ya!=yb):
		ya_out=ya; yb_out=yb;
		x_eff = []
		for i in range(0,ny):
			if (y[i]<=yb and y[i]>=ya):
				x_eff.append(x[i])
		xa_out=min(x_eff)
		xb_out=max(x_eff)
	
	# xa!=xb and ya==yb
	if(xa!=xb and ya==yb):
		xa_out=xa; xb_out=xb;
		y_eff = []
		for i in range(0,nx):
			if (x[i]<=xb and x[i]>=xa):
				y_eff.append(y[i])
		ya_out=min(y_eff)
		yb_out=max(y_eff)
	
	
	# xa==xb and ya==yb
	if (xa==xb and ya==yb):
		Deltax = max(x) - min(x); Deltay = max(y) - min(y)
		xa_out= min(x) - 0.10*Deltax; xb_out=max(x) + 0.10*Deltax;
		ya_out= min(y) - Deltay; yb_out=max(y) + Deltay;
		# print(" range dedotto")
		# print(xa_out, xb_out, ya_out, yb_out)
	
	# xa!=xb and ya!=yb
	if (xa!=xb and ya!=yb):
		xa_out = xa; xb_out = xb;
		ya_out = ya; yb_out = yb;
	
	
	return [xa_out,xb_out, ya_out,yb_out]
	
