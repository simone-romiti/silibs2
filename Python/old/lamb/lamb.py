#!/usr/bin/python3

import sys
import os.path
from common import deduce_format

args = sys.argv


if (len(args) == 1):  # check if al least one argument has been provided
    print("No arguments passed. Use --help for usage informations.")
    quit()
if (args[1] == "--help"):
    from help import help
    help()  # printing the help message
    quit()

for i in range(1, len(args)):
    file = args[i]
    if os.path.isfile(file):
        p = deduce_format(file)
        p(file)
    else:
        print("Invalid file : \'", file, "\'")
        print("Type --help for help")
        quit()
