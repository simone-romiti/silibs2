# lamb plot generator

## Fast installation

``` bash
chmod +x ./install.sh # making it executable
./install.sh # installation
```

The latter commands do automatically what is described below.

## Custom installation

This folder contains **lamb.py**,
a python script for reading text files organized in columns corresponding to data
It can be executed providing any number of files as arguments.
Make **lamb.py** executable with:

``` bash
chmod +x lamb.py
```

In order to install lamb ad then execute it independently of the location, follow these steps:

``` bash
sudo touch /usr/local/bin/lamb
sudo cat ./lamb.py > /usr/local/bin/lamb
sudo chmod +x /usr/local/bin/lamb
```

## Uninstall lamb

In order to uninstall lamb give the following commands:

``` bash
chmod +x ./uninstall.sh # making it executable
./uninstall.sh # uninstallation
```
