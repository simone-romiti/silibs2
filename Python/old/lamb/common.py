# common.py
""" Common routines for reading data from files """


def check_header(file, header):
    """ Returns True if header is in file """
    L = open(file, 'r').readlines()
    for line in L:  # iterates over the lines of the file
        if line.strip() == header:
            return True  # header found
    return False  # header not found
# check_header()


def get_data(file, header):
    """ Returns the list of lines containing the data """
    L = open(file, 'r').readlines()
    rows = []  # rows containing data
    for i in range(len(L)):  # iterates over the lines of the file
        if L[i].strip() == header:
            i += 1  # jump to the first line with data
            while len(L[i].strip()) != 0:  # continue up to an empty line
                rows.append(L[i])  # append the
                i += 1
    return rows
# get_data()


def deduce_format(file):
    """ Deduces the format of the data """
    if check_header(file, "#x_ex_y_ey_yth_eyth"):
        from x_ex_y_ey_yth_eyth import plot
        return plot
    if check_header(file, "#x_ex_y_ey"):
        from x_ex_y_ey import plot
        return plot
    if check_header(file, "#x_y_ey_yth_eyth"):
        from x_y_ey_yth_eyth import plot
        return plot
    else:
        mess = "The file you passed,\n"
        mess += file
        mess += ",\ndoesn't contain a valid header."
        raise Exception(mess)  # print error message
# deduce_format()
