# help.py
# print the help message

help_string = """
duckling plotting utility
USAGE : duckling [OPTION] [FILES]...

Options:

    --help   :   Return this help page
    -xyey    :   Plot data in the xyey format
    -xexyey    :   Plot data in the xexyey format (assuming x=[1,...,N])

Examples:

    duckling -xyey  file1.dat file2.dat file3.dat
    duckling -xexyey file.dat
"""

def help():
    print(help_string)
