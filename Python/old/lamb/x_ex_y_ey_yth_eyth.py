""" file name """

# plot of data in the x ex y ey yth eyth forma

from matplotlib import pyplot as plt
import os

from common import get_data


def plot(file):
    x, ex, y, ey, yth, eyth = [], [], [], [], [], []
    rows = get_data(file, "#x_ex_y_ey_yth_eyth")
    for r in rows:
        v = r.strip().split()
        x.append(float(v[0]))
        ex.append(float(v[1]))

        y.append(float(v[2]))
        ey.append(float(v[3]))

        yth.append(float(v[4]))
        eyth.append(float(v[5]))

    plt.plot(x, y, marker='.', markersize=0.7, linestyle='None')
    plt.errorbar(x, y, xerr=ex, yerr=ey, linestyle='None')

    plt.plot(x, yth, marker='.', markersize=0.7, linestyle='dashed')
    plt.errorbar(x, yth, yerr=eyth, linestyle='None')

    plt.xlabel("x")
    plt.ylabel("y(x)")
    plt.title(os.path.abspath(file))

    plt.show()
    plt.close()

# end of file
