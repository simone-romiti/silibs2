#!/bin/bash


# remove the directory with the program files
inst_dir=$(cat ./inst_dir.txt)
sudo rm -rf $inst_dir

# remove the global executable
sudo rm -f /usr/bin/lamb
