#!/bin/bash

echo "Installing the lamb plotting package"
chmod +x lamb.py

echo "creating the installation directory"
inst_dir=$(cat ./inst_dir.txt)
sudo mkdir -p $inst_dir
echo "copying the necessary scripts"
sudo cp  -r  ./  $inst_dir

echo "Creating the global executable"
glob_exe=/usr/bin/lamb

glob_exe_example=./glob_exe.sh
echo $inst_dir'lamb.py $@' > $glob_exe_example

sudo cp $glob_exe_example $glob_exe
sudo chmod +x $glob_exe
echo "Installation successfull"
