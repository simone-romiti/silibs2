# **sand** plotting library

Plotting library based on **matplotlib**.

## System-wide configuration

In order to install/uninstall **sand** on your Linux machine run the corresponding installer in the **CONFIG/** directory:

``` bash
cd INSTALL/
```

* **Install**

  ``` bash
  chmod +x install.sh
  ./install.sh
  ```

* **Uninstall**

  ``` bash
  chmod +x uninstall.sh
  ./uninstall.sh
  ```

## Usage

The information about how to use the library can be retrieved from the _help_ page. Run the command.

``` bash
sand --help
```

## Rationale

This library is intended to be very simple to use. The package contains:

*   Some python modules which can be imported in other python scripts
*   A program to be installed globally for doing plots on the fly.

For the moment only 2-D plot are supported.
These can be of various kinds, and

*   Include or not the errors on the _x_, _y_
*   Plot different vectors of _x_ and _y_ values
*   Include the theoretical curve for _y_ with or without a band for the associated uncertainty.
*   Use a wider range, even if points are confined in a smaller region

For this reason the different kinds of plots are categorized according to codes. These are described in the **help** page.
