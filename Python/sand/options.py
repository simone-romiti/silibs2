""" Definition of class cli_opt """

import os
import sys
import common.help as help
import common.options as opt

from plot import plot_format_auto as pf

class cli_opt:
    """class for handling cli parameter options"""

    def __init__(self, args):
        if (len(args) == 1):
            """ Check if at least one argument was passed"""
            print("sand: No arguments passed")
            help.say_print_help()
            quit()
        self.FILES = args[1:] # default
        # Handling options passed
        h_opt = opt.get_opt(args, "--header", "-h")
        self.HEAD = h_opt[1]
        if self.HEAD != "":
            self.FILES.remove(h_opt[0])
            self.FILES.remove(self.HEAD)

        f_opt = opt.get_opt(args, "--format", "-f")
        self.FORMAT = f_opt[1]
        if self.FORMAT != "":
            self.FILES.remove(f_opt[0])
            self.FILES.remove(self.FORMAT)

        # Handling help request
        if opt.opt_present(args, "--help") :
            help.help_message()  # printing the help message
            quit()
        
        
    def plot(self):
        for f in self.FILES:
            if os.path.isfile(f):
                print("Plotting the content of", f)
                pf(f, self.HEAD, self.FORMAT)
            else:
                print("sand: Invalid file : \'", f, "\'", sep="")
                help.say_print_help()
                quit()

        




        