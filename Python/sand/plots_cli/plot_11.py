""" 11.py : plot in the format x y """

from matplotlib import pyplot as plt

def plot(m):
    " Error bar plot using the first 2 columns of the matrix m "
    x  = m[:,0]
    y  = m[:,1]

    plt.plot(x, y, marker='.', linestyle='-')

    plt.xlabel("x")
    plt.ylabel("y(x)")
    # plt.title(os.path.abspath(file))

    plt.show()
    plt.close()
# end of file
