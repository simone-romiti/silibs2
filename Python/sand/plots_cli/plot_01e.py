""" 1e1e.py : plot in the format x ex y ey """

from matplotlib import pyplot as plt

def plot(m):
    " Error bar plot using the first 3 columns of the matrix m "
    n = m.shape[0]
    x = range(1,n+1)
    y  = m[:,0]
    ey = m[:,1]

    plt.plot(x, y, marker='.', markersize=0.7, linestyle='None')
    plt.errorbar(x, y, yerr=ey, linestyle='None')

    plt.xlabel("x")
    plt.ylabel("y(x)")
    # plt.title(os.path.abspath(file))

    plt.show()
    plt.close()
# end of file
