""" 11e.py : plot in the format x y ey """

from matplotlib import pyplot as plt

def plot(m):
    " Error bar plot using the first 3 columns of the matrix m "
    x  = m[:,0]
    y  = m[:,1]
    ey = m[:,2]

    plt.plot(x, y, marker='.', markersize=0.7, linestyle='None')
    plt.errorbar(x, y, yerr=ey, linestyle='None')

    plt.xlabel("x")
    plt.ylabel("y(x)")
    # plt.title(os.path.abspath(file))

    plt.show()
    plt.close()
# end of file
