""" 1e1e.py : plot in the format x ex y ey """

from matplotlib import pyplot as plt

def plot(m):
    " Error bar plot using the first 3 columns of the matrix m "
    x  = m[:,0]
    ex = m[:,1]
    y  = m[:,2]
    ey = m[:,3]

    plt.plot(x, y, marker='.', markersize=0.7, linestyle='None')
    plt.errorbar(x, y, xerr=ex, yerr=ey, linestyle='None')

    plt.xlabel("x")
    plt.ylabel("y(x)")
    # plt.title(os.path.abspath(file))

    plt.show()
    plt.close()
# end of file
