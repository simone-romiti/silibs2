""" plot_11n.py : plot in the format x y ey """

from matplotlib import pyplot as plt
import numpy as np

from os.path import expanduser
parent = expanduser("../")
import sys
sys.path.append(parent)

from common.spline import get_spline as get_spline

def plot(m):
    "Plot of the columns of the matrix using the first column as 'x' "
    x = m[:,0]
    n_cols = m.shape[1]
    for i in range(1, n_cols):
        y  = m[:,i]
        plt.plot(x, y, marker='.', markersize=1.0, linestyle='-', label="y_"+str(i))
    plt.xlabel("x")
    plt.ylabel("y(x)")
    plt.legend(loc="best")
 
    plt.show()
    plt.close()
# end of file
