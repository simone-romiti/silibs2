""" plot_1n.py : plot the curves y1, ... , yn """

from matplotlib import pyplot as plt
import numpy as np

from os.path import expanduser
parent = expanduser("../")
import sys
sys.path.append(parent)

from common.spline import get_spline as get_spline

def plot(m):
    " Plot of the columns of the matrix"
    nx = m.shape[0] 
    x = [i for i in range(1, nx + 1)]
    n_cols = m.shape[1] 
    for i in range(n_cols):
        y  = m[:,i]
        plt.plot(x, y, marker='.', markersize=1.0, linestyle='-', label="y_"+str(i+1))
    plt.xlabel("x")
    plt.ylabel("y(x)")
    plt.legend(loc="best")
 
    plt.show()
    plt.close()
# end of file
