""" 11e.py : plot in the format x y ey """

from matplotlib import pyplot as plt
import numpy as np

from os.path import expanduser
parent = expanduser("../")
import sys
sys.path.append(parent)

from common.spline import get_spline as get_spline

def plot(m):
    " Error bar plot using the first 5 columns of the matrix m "
    x  = m[:,0]

    y  = m[:,1]
    ey = m[:,2]
    # plot of the data
    plt.plot(x, y, marker='.', markersize=0.7, linestyle='None')
    plt.errorbar(x, y, yerr=ey, linestyle='None')

    # plot of the theoretical points
    yth = m[:,3]
    eyth = m[:,4]
    plt.errorbar(x, yth, yerr=eyth, linestyle='None')

    y_up = [yth[i]+eyth[i] for i in range(0,len(yth))]
    y_down = [yth[i]-eyth[i] for i in range(0,len(yth))]

    # splines
    x_dense = np.linspace(min(x), max(x), 100)
    spl_0 = get_spline(x, yth)
    spl_up = get_spline(x, y_up)
    spl_down = get_spline(x, y_down)
    yth_spl = spl_0(x_dense)
    y_up_spl = spl_up(x_dense)
    y_down_spl = spl_down(x_dense)

    plt.plot(x_dense, yth_spl, marker='.', markersize=0.7, linestyle='--')
    plt.fill_between(x_dense, y_up_spl, y_down_spl, alpha=0.5)

    plt.xlabel("x")
    plt.ylabel("y(x)")
 
    plt.show()
    plt.close()
# end of file
