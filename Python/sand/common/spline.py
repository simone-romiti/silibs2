""" Generation of the point of a spline, given x and y """

import numpy as np
from scipy.interpolate import make_interp_spline, BSpline

def sort_xy(x, y):
    """ Sort x and y according to the x """
    x_ord, y_ord =[ x[0] ], [ y[0] ]
    n = len(x)
    for i in (range(1,n)):
        if(x[i]<x_ord[0]):
            """ If the element is smaller, it's put at the beginning """
            x_ord.insert(0,x[i])
            y_ord.insert(0,y[i])
        else:
            """ If the element is larger, it's put at the end """
            x_ord.append(x[i])
            y_ord.append(y[i])
    return [x_ord,y_ord]
# sort_xy()


def get_spline(x, y, n_deg=None):
    """ n_pts points distributed along the spline of the (x[i],y[i]) points """
    ord = sort_xy(x,y)
    x_ord, y_ord = ord[0], ord[1]
    n = 0
    if n_deg == None:
        n = len(x_ord)-1
    else:
        n = n_deg
    if n%2 == 0 and n>0:
        n -= 1 # only odd degreee is supported by the library
    spl = make_interp_spline(x_ord, y_ord, k=n)  # type: BSpline
    return spl
# spline_gen
