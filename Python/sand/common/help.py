# help.py
# print the help message


help_msg = """
███████╗ █████╗ ███╗   ██╗██████╗
██╔════╝██╔══██╗████╗  ██║██╔══██╗
███████╗███████║██╔██╗ ██║██║  ██║
╚════██║██╔══██║██║╚██╗██║██║  ██║
███████║██║  ██║██║ ╚████║██████╔╝
╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═════╝

'sand' plotting utility
USAGE : sand [FILES, OPTIONS]...

Options:

    --help           : Return this help page
    -f, --format [F] : Set 'F' as the format for plotting
    -h, --header [H] : Set 'H' as the header to look for
"""

from . import formats
frm = formats.valid_formats

examples = """
EXAMPLES:

    sand 11   file1.dat file2.dat file3.dat
    sand 1e1  --header "#data" file.dat
    sand 1e1e file1.dat file2.dat --format "01e"

Note: files and options can be given in any order.
"""

help_string = help_msg + frm + examples

def help_message():
    print(help_string)

def say_print_help():
    print("Type --help for help")
