# formats.py

valid_formats = """
FORMATS: Valid formats for plotting

#valid_formats
11     # x y
11e    # x y  ey
1e1e   # x ex y  ey
01e    # y ey (x=1,..,n)
11eth  # x    y  ey yth(dashed) yth+/-eyth(filled)
11eth  # x ex y  ey yth(dashed) yth+/-eyth(filled)
1n     #   y_1 ... y_n (x=1,...,nx)
11n    # x y_1 ... y_n
"""

import io
def is_valid(s):
    """ Returns true if format 's' is valid,
    i.e is present in the string 'valid_formats'.
    """
    F = valid_formats.partition("#valid_formats")[2]
    if (F.find(s) == -1):
        return False
    else:
        return True
# is_valid()

from . import common
def get_format(file, plot_tag=common.plot_tag):
    """ Format specified after the plot_tag """
    F = open(file, 'r').read() # file content
    if F.find(plot_tag)==-1:
        print("Error: '", plot_tag, "' absent in ", file, sep="")
        quit()
    if plot_tag!="":
        format = (F.partition(plot_tag)[2]).split()[0]
        if is_valid(format):
            return format
        else:
            return "None"
    else:
        return "None"
# get_format()
