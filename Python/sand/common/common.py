# common.py
""" Common routines for reading data from files """

plot_tag = "#plot" # plot tag in file after which data are present

def check_header(file, header):
    """ Returns True if header is in file """
    F = open(file, 'r').read() # file content
    if (F.find(header) == -1):
        print("Error.", header, "not present in", file)
        quit()
# check_header()

import numpy as np

def get_matrix_string(file, h, sep):
    """ Stores the columns found after 'h' in 'file' """
    check_header(file, h) # check if 'h' is present
    F = open(file, 'r').read() # file content
    i = 0
    D1 = F.partition(h)[2] # content of the file after 'h'
    D2 = D1.split("\n")
    rows = []
    while True:
        if(len(D2[i]) != 0):
            break # continue until first non-empty line
        i +=  1
    while True:
        if(len(D2[i]) == 0):
            break # first empty line after data was found
        rows.append(D2[i])
        i += 1
    data = np.array([[x.strip() for x in r.strip().split(sep=sep) if len(x)!=0] for r in rows], dtype=object)
    return data

def get_matrix_float(file, h):
    """ Stores the columns found after 'h' in 'file' """
    check_header(file, h) # check if 'h' is present
    F = open(file, 'r').read() # file content
    i = 0
    D1 = F
    if h != "":
        D1 = F.partition(h)[2]
    D2 = D1.split("\n")
    rows = []
    while (len(D2[i]) == 0): # continue until first non-empty line
        i +=  1
    while (len(D2[i]) != 0): # first empty line after data was found
        rows.append(D2[i])
        i += 1
    data = np.array([[float(x) for x in r.strip().split()] for r in rows])
    return data
