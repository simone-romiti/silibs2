# options.py
# Routines for handiling parameter options 
# such as the header to look for in the file

def opt_present(args, opt):
    """ Check if the option was passed"""
    if (opt in args):
        return True
    else:
        return False

def get_opt(args, opt, opt_short):
    """ If the format gets specified, sets the header to use"""
    opt_str = ""
    if opt in args:
        opt_str = opt
    if opt_short in args:
        opt_str = opt_short
    if opt_str != "":
        ih = args.index(opt_str)
        if len(args)<ih+2:
            print("Error. You didn't specify the header to look for.")
            quit()
        return [opt_str, args[ih+1]]
    else:
        return [opt_str, ""]