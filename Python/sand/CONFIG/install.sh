#!/bin/bash

sudo echo "Installing the sand plotting package" || exit 1
chmod +x ../sand.py # making the sand.py executable

inst_dir=$(cat ./inst_dir.txt) # installation directory
echo "Creating the installation directory : " $inst_dir  
sudo mkdir -p $inst_dir # creating the installation directory
sudo rm -r $inst_dir/* # removing the old version of the program
echo "Copying the necessary scripts"
sudo cp  -r  ../  $inst_dir # copying the whole package in the installation directory

glob_exe_example=./glob_exe.sh # local copy of the global executable
echo $inst_dir'sand.py $@' > $glob_exe_example # writing the content

glob_exe=/usr/bin/sand
echo "Creating the global executable : " $glob_exe
sudo mv $glob_exe_example $glob_exe
sudo chmod +x $glob_exe # making the global script file executable

echo "Installation successfull."
