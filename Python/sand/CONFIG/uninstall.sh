#!/bin/bash

sudo echo "Uninstalling the sand plotting package" || exit 1

inst_dir=$(cat ./inst_dir.txt) # installation directory
echo "Removing the installation directory : " $inst_dir  
sudo rm -r $inst_dir # creating the installation directory

glob_exe=/usr/bin/sand
echo "Removing the global executable : " $glob_exe
sudo rm $glob_exe

echo "Installation successfull."
