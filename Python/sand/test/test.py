from os.path import expanduser
home = expanduser("~")

import sys
sys.path.append(home + "/Documents/Silibs2/Python/sand")

import matplotlib
import numpy as np

import lattice.effective as le

e1 = le.effective(latex=False)

e1.set_info(title="Titolo", xlabel="$x$", ylabel="$\\chi \\Delta$")
e1.set_grid(linestyle=":", linewidth=1)


M = np.array([
            [0.9,1.5,2.4,0.9,2.4,0.9,2.4], 
            [1,1.5,2.4,2.4,1,1.5,1.5], 
            [0.1,0.1,0.2,0.1,0.1,0.1,0.1]
             ])

e1.set_data(M)

Md = np.array([[1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0], 
     [1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5], 
     [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]])

e1.set_curve_plateau(Md)

e1.save_plot("./test.pdf")
e1.close()


