# Plots for Lattice QCD

This module contains routines for plots used in the analysis of lattice correlators.

The classes and functionalities are the following:

* [in progress] Plot of effective curves.
  It's possible plot also the theoretical curve in the plateau.

* [in progress] Plot of extrapolations