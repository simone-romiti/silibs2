# effective.py 

import numpy as np
from lattice.generic import generic

class effective(generic):
    """class for plotting effective mass curves"""
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
    
    def set_data(self, M0, t1=0, t2=0, color="red", markersize=3, capsize=2):
        """ Plotting data points in the interval [t1, t2] """
        if (t1==t2 and t1==0):
            t1, t2 = min(M0[:,0]), max(M0[:,0])
        M = np.array([m for m in M0 if m[0]>=t1 and m[0]<=t2])
        self.set_xy(M, marker='.', markersize=markersize, color=color, linestyle='None')
        self.set_xyey(M, capsize=capsize, color=color, linestyle='None')
    
    def set_curve_plateau(self, M, CH2_RED="", color="green", alpha=0.3):
        self.set_dense_xyey(M, linestyle="--", color=color, label="$\chi^2_{d.o.f}=$"+CH2_RED)
        self.set_dense_band_xyey(M, alpha = alpha, color=color)
# class effective
    