# generic

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os
 
class generic:
    """class for plotting generic data"""
    def __init__(self, **kwargs):
        """ initialize references to pyplot figure and axes"""
        if "latex" in kwargs:
            matplotlib.rcParams['text.usetex'] = kwargs["latex"] 
        fig, ax = plt.subplots() #getting fig, and ax
        self.fig, self.ax = fig, ax # referencing
    
    def set_info(self, **kwargs):
        if "title" in kwargs:
            (self.ax).set_title(kwargs["title"])
        if "xlabel" in kwargs:
            (self.ax).set_xlabel(kwargs["xlabel"])
        if "ylabel" in kwargs:
            (self.ax).set_ylabel(kwargs["ylabel"])
    
    def set_grid(self, **kwargs):
        plt.grid(**kwargs)
    
    def set_xy(self, M, **kwargs):
        """ Plotting data points"""
        x, y = np.array(M[:,0]), np.array(M[:,1]) # getting 'x' and 'y'
        (self.ax).plot(x, y, **kwargs) # plotting

    def set_xexyey(self, M, **kwargs):
        """error bars on the 'x' and 'y' """
        x, ex = np.array(M[:,0]), np.array(M[:,1]) # x, ex
        y, ey = np.array(M[:,2]), np.array(M[:,3]) # y, ey
        (self.ax).errorbar(x, y, xerr=ex, yerr=ey, **kwargs) # plotting

    def set_xexy(self, M, **kwargs):
        """error bars on the 'x' """
        x, ex, y = np.array(M[0]), np.array(M[1]), np.array(M[2])
        (self.ax).errorbar(x, y, xerr=ex, **kwargs)

    def set_xyey(self, M, **kwargs):
        """error bars on the 'y' """
        x, y, ey = np.array(M[:,0]), np.array(M[:,1]), np.array(M[:,2])
        (self.ax).errorbar(x, y, yerr=ey, **kwargs)

    def set_dense_xyey(self, M, **kwargs):
        """theoretical curve"""
        x, y, ey = np.array(M[:,0]), np.array(M[:,1]), np.array(M[:,2])
        (self.ax).plot(x, y, **kwargs)

    def set_dense_band_xyey(self, M, **kwargs):
        """error band for theoretical curve"""
        x, y, ey = np.array(M[:,0]), np.array(M[:,1]), np.array(M[:,2])
        (self.ax).fill_between(x, y+ey, y-ey, **kwargs)
    
    def save_plot(self, output):
        plt.tight_layout() # adjusting the margins
        plt.legend(loc="best") # plotting the legend

        # saving the plot
        fld = os.path.dirname(output) ## directory of file
        if not os.path.exists(fld):
            os.makedirs(fld)

        plt.savefig(output)
    
    def close(self):
        matplotlib.rcParams['text.usetex'] = True # output latex
        plt.close("all")
