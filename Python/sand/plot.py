""" plotting functions based on format passed """
# plot.py

import common.common as common

# plotting modules
import plots_cli.plot_11 as plot_11
import plots_cli.plot_11e as plot_11e
import plots_cli.plot_1e1e as plot_1e1e
import plots_cli.plot_01e as plot_01e
import plots_cli.plot_11eth as plot_11eth
import plots_cli.plot_1e1eth as plot_1e1eth
import plots_cli.plot_1n as plot_1n
import plots_cli.plot_11n as plot_11n


def plot_format(data, fmt, plot_tag=common.plot_tag):
    """ Plot the data in a given format """
    if fmt == "11":
        plot_11.plot(data)
    elif fmt == "11e":
        plot_11e.plot(data)
    elif fmt == "1e1e":
        plot_1e1e.plot(data)
    elif fmt == "01e":
        plot_01e.plot(data)
    elif fmt == "11eth":
        plot_11eth.plot(data)
    elif fmt == "1e1eth":
        plot_1e1eth.plot(data)
    elif fmt == "1n":
        plot_1n.plot(data)
    elif fmt == "11n":
        plot_11n.plot(data)
    else:
        print("Invalid format : ", fmt)
        quit()
# plot_format

from common.formats import get_format

def plot_format_auto(file, plot_tag=common.plot_tag, format=""):
    """ Plot deducing the format if nc=2,3,4 """
    data = common.get_matrix_float(file, plot_tag)
    nc = data.shape[1]  # number of columns
    fmt = format
    if format=="": # if no format has been provided
        fmt = get_format(file, plot_tag)
    if fmt == "None":  # default plots
        if nc == 2:  # default : x y plot
           plot_format(data, "11", plot_tag)
        if nc == 3:  # default : x y ey plot
            plot_format(data, "11e", plot_tag)
        if nc == 4:  # default : x ex y ey plot
            plot_format(data, "1e1e", plot_tag)
        if nc == 4:  # default : 11eth plot
            plot_format(data, "11eth", plot_tag)
    else:
        plot_format(data, fmt, plot_tag)
# plot_format_auto()
