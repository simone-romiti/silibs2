// Silibs2.hpppp
/*! Include this header if you want to include all Silibs2 libraries */

#ifndef Silibs2_hpp
#define Silibs2_hpp

#include "./Functions/functions.hpp" /*!< generic tables */

#include "./Minuit2_fit/fit.hpp" /*!< Fit with Minuit2 */

#include "./Tables/table.hpp" /*!< generic tables */

#include "./RC/table.hpp" /*!< generic tables */

#include "./LA/matrix.hpp" /*!< matrices for linear algebra */
#include "./jkf/jkf.hpp"   /*!< matrices for jacknifes */

#include "./Lattice/lattice.hpp" /*!< lattice correlators */

// #include "./scripts/python.hpp" /*!< python scripts */

#endif
