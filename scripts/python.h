// python.h
/*!
Declaration of a class for running python scripts. The user must supply the
commands, which are stored in a std::vector. The user can run them using the
run() method, which stores all of them in a std::string and passes it to the
system() function.
 */

#ifndef Silibs2_scripts_python_h
#define Silibs2_scripts_python_h
#include "../Functions/functions.h"

namespace Silibs2 {
namespace scripts {

class python {
protected:
  std::string EXE = "/usr/bin/python3"; /*!< default executable */
  std::vector<std::string> COMMANDS;    /*!< list of commands */

public:
  python();  /*!< default constructor */
  ~python(); /*!< default destructor */

  void set_exe(const std::string &exe); /*!< set executable's location */

  /*! add command to the list */
  void add(const std::string &cmd, const std::string &end = "\n");

  /*! single std::string containing the script */
  std::string gen_script() const;

  /*!
  run the whole script (verbosely if verb==true).
  path = path of the file where the script is saved.
   */
  void run(const bool &verb = false) const;

  /*! Save the script into a file */
  void save(const std::string &file) const;

  void reset(); /*!< erase the list of commands */
};              // class python

} // namespace scripts
} // namespace Silibs2
#endif
