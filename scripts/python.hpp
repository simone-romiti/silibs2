// python.hpp
/*! definitions of python.h */

#ifndef Silibs2_scripts_python_hpp
#define Silibs2_scripts_python_hpp
#include "./python.h"

namespace Silibs2 {
namespace scripts {

python::python() {}
python::~python() {}

void python::set_exe(const std::string &exe) { EXE = exe; } // set_exe()

void python::add(const std::string &cmd, const std::string &end) {
  COMMANDS.push_back(cmd + end);
} // add()

std::string python::gen_script() const {
  std::string script = "";
  for (std::string c : COMMANDS) {
    script += c;
  }
  return script;
} // gen_script()

void python::run(const bool &verb) const {
  const std::string script = this->gen_script();
  const std::string file = "/tmp/Silibs2/scripts/python/temp.py";
  Functions::string_to_file(script, file);
  std::string cmd = EXE + " " + file;
  if (verb) {
    std::cout << "You're giving the command:\n" + cmd + "\n";
  }
  int i = system(&cmd[0]);
  i++; /*!< suppress warnings for unused variable */
} // run()

void python::save(const std::string &file) const {
  const std::string script = this->gen_script();
  Functions::string_to_file(script, file);
} // save()

void python::reset() { COMMANDS.resize(0); } // reset()

} // namespace scripts
} // namespace Silibs2
#endif
