var searchData=
[
  ['qcd_1678',['QCD',['../class_silibs2_1_1_lattice_1_1_f_v_e_1_1_q_c_d.html#a57f1a7c6334a031e76c41c5c507b5bf7',1,'Silibs2::Lattice::FVE::QCD::QCD()'],['../class_silibs2_1_1_lattice_1_1_f_v_e_1_1_q_c_d.html#aec94222ac9955d9d5f7e551aba59f3cf',1,'Silibs2::Lattice::FVE::QCD::QCD(const std::string &amp;name, const std::vector&lt; int &gt; L_values, const jkf::matrix&lt; U &gt; &amp;obs_L, const std::string &amp;type)']]],
  ['qed_5fl_1679',['QED_L',['../class_silibs2_1_1_lattice_1_1_f_v_e_1_1_q_e_d___l.html#ae882bd48e25b37ef91d9c3c6e937eb42',1,'Silibs2::Lattice::FVE::QED_L::QED_L()'],['../class_silibs2_1_1_lattice_1_1_f_v_e_1_1_q_e_d___l.html#a039007291db939a37ab371f8226b5f1b',1,'Silibs2::Lattice::FVE::QED_L::QED_L(const std::string &amp;name, const std::vector&lt; int &gt; &amp;L_values, const jkf::matrix&lt; U &gt; &amp;obs_L, const U &amp;q)']]],
  ['quadratic_5faverage_1680',['quadratic_average',['../namespace_silibs2_1_1_functions.html#aba1bb3330af543851b9df368e1b525a4',1,'Silibs2::Functions']]]
];
