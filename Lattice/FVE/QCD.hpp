// QCD.hpp
/* definitions of QCD.h */

#include "../../Minuit2_fit/fit.hpp"
#include "../../jkf/matrix.hpp"
#include "../../jkf/results.hpp"

#include "./QCD.h"

namespace Silibs2 {
namespace Lattice {
namespace FVE {
#ifndef Silibs2_Lattice_FVE_QCD_hpp
#define Silibs2_Lattice_FVE_QCD_hpp

std::string fL_string(const std::string &had) {
  if (std::count(had_list_1.begin(), had_list_1.end(), had)) {
    return "MP^2 * exp(-MP * L) / (MP*L)^(3/2)";
  } else if (std::count(had_list_2.begin(), had_list_2.end(), had)) {
    return "MP^3 * exp(-MP * L) / (MP*L)";
  } else {
    std::cerr << "Error. Invalid hadron : " << had << "\n";
    abort();
  }
} // fL_string()

template <class U>
U fL_val(const U &MP, const U &L_inv, const std::string &had) {
  if (L_inv == 0.0) {
    return 0.0;
  } else {
    U L = std::pow(L_inv, -1.0);
    if (std::count(had_list_1.begin(), had_list_1.end(), had)) {
      return std::pow(MP, 2) * exp(-MP * L) / std::pow(MP * L, 3.0 / 2.0);
    } else if (std::count(had_list_2.begin(), had_list_2.end(), had)) {
      return std::pow(MP, 3) * exp(-MP * L) / (MP * L);
    } else {
      std::cerr << "Error. Invalid hadron : \'" << had << "\' \n";
      abort();
    }
  }
} // fL_val()

template <class U>
U fact_obs_L_had(const U &MP, const U &L_inv, const U &C,
                 const std::string &had) {
  return (1.0 + C * fL_val(MP, L_inv, had));
}

template <class U>
U obs_L_had(const U &obs_inf, const U &MP, const U &L_inv, const U &C,
            const std::string &had) {
  return obs_inf * fact_obs_L_had(MP, L_inv, C, had);
}

template <class U> U obs_L_had(const U &obs_inf, const U &fL, const U &C) {
  return obs_inf * (1.0 + C * fL);
}

template <class U> QCD<U>::QCD() {}
template <class U> QCD<U>::~QCD() {}

template <class U> int QCD<U>::N_jkf() const { return OBS_L.cols(); }
template <class U> int QCD<U>::n_pars() const { return (int)PAR_NAMES.size(); }
template <class U> int QCD<U>::n_pts() const { return OBS_L.rows(); }

template <class U>
QCD<U>::QCD(const std::string &name, const std::vector<int> L_values,
            const jkf::matrix<U> &obs_L, const std::string &had_name) {
  OBS_L = obs_L;
  L = Functions::boost_cast<U>(L_values);

  if ((int)L.size() != n_pts()) {
    std::cerr << "Error: number of volumes != number of observables\n";
    std::cerr << "Aborting.\n";
    abort();
  }

  const int n_pts = (int)L.size();
  L_inv.resize(n_pts);
  for (int i = 0; i < n_pts; ++i) {
    L_inv[i] = std::pow(L[i], -1.0);
  }
  NAME = name;
  HAD = had_name;
} // FVE()

template <class U> void QCD<U>::set_MP(const jkf::matrix<U> &m_pi) {
  MP = m_pi;
  MP_init = true;
} // set_Mpi()

template <class U> void QCD<U>::check_MP() {
  if (!MP_init) {
    std::cerr << "Error. Pion masses haven't been provided.\n";
    std::cerr << "Aborting.\n";
    abort();
  }
} // set_Mpi()

template <class U> void QCD<U>::find_PARS() {
  this->check_MP();
  /*
  the value at L->inf would be(presumably) near the values analyzed, so I
  use one of them as an ansatz
  */
  const std::vector<U> guesses = {OBS_L.avrs()[0], 1.0};
  const int n_deg = n_pts() - n_pars(); /* degrees of freedom of the fit */

  /* loop over the jkf */
  PARS.resize(n_pars(), N_jkf());
  const std::vector<U> ey = OBS_L.errs(); /* errors */

  for (int j = 0; j < N_jkf(); ++j) {
    const std::vector<U> mP = MP.col(j);
    const std::vector<U> y = OBS_L.col(j);

    Minuit2_fit::fit_xyey<U> minu;

    minu.set_pars(PAR_NAMES, guesses);
    minu.set_data(L, OBS_L.col(j), ey);

    std::function<U(const std::vector<U> &)> rsd =
        [=](const std::vector<U> &p) {
          U ch2 = 0.0;
          for (int i = 0; i < n_pts(); ++i) {
            U y_th = obs_L_had(p[0], mP[i], L_inv[i], p[1], HAD);
            ch2 += pow((y_th - y[i]) / ey[i], 2);
          }
          return (ch2 / n_deg); // reduced chi squared
        };

    minu.set_residue(rsd);
    minu.fit();
    PARS.set_col(minu.get_PARS(), j);
  }
} // find_PARS()

template <class U> jkf::matrix<U> QCD<U>::get_fL() {
  const int n1 = n_pts();
  const int Nj = N_jkf();
  jkf::matrix<U> fL(n1, Nj);
  for (int i = 0; i < n1; ++i) {
    for (int j = 0; j < Nj; ++j) {
      fL(i, j) = fL_val(MP(i, j), L_inv[i], HAD);
    }
  }
  return fL;
} // get_fL()

template <class U> jkf::matrix<U> QCD<U>::get_OBS_L_th() {
  const int n1 = n_pts();
  const int Nj = N_jkf();
  jkf::matrix<U> OBS_L_th(n1, Nj);
  for (int j = 0; j < Nj; ++j) {
    U obs_inf = PARS(0, j);
    U C = PARS(1, j);
    for (int i = 0; i < n1; ++i) {
      U mP = (*this).MP(i, j);
      OBS_L_th(i, j) = obs_inf * fact_obs_L_had(mP, L_inv[i], C, HAD);
    }
  }
  return OBS_L_th;
} // get_OBS_L_th()

template <class U> jkf::matrix<U> QCD<U>::get_OBS_inf() {
  const int n1 = n_pts();
  const int Nj = N_jkf();
  jkf::matrix<U> OBS_inf(n1, Nj);
  for (int j = 0; j < Nj; ++j) {
    U C = PARS(1, j);
    for (int i = 0; i < n1; ++i) {
      U mP = MP(i, j);
      OBS_inf(i, j) = OBS_L(i, j) / fact_obs_L_had(mP, L_inv[i], C, HAD);
    }
  }
  return OBS_inf;
} // get_OBS_inf()

template <class U> jkf::matrix<U> QCD<U>::get_OBS_inf_th() {
  return jkf::constant_jkf(PARS.row(0), n_pts());
} // get_OBS_inf_th()

/* ------------------------------- */
/* Printing the results of the fit */
/* ------------------------------- */

template <class U>
void QCD<U>::print_obs_L(const std::string &fld, const int &N) {
  jkf::results<U> rL;                       /* data at finite volume */
  rL.set_title(NAME + " at finite volume"); /*!< Title */
  rL.set_subtitle(NAME + " as a function of " +
                  fL_string(HAD)); /*!< subtitle */
  /*! Parameters with their names */
  rL.add_jkf_rows(PARS, PAR_NAMES);
  rL.add(OBS_L, "#jkf#data_y"); /*!< adding jkf of y values */

  const jkf::matrix<U> fL = this->get_fL();
  const jkf::matrix<U> OBS_L_th = this->get_OBS_L_th();

  /*! data in the "x ex y ey y_th ey_th" format */
  const jkf::matrix<U> x_ex_y_ey_yth_eyth =
      jkf::build_x_ex_y_ey_yth_eyth(fL, OBS_L, OBS_L_th);
  const std::string head_x_ex_y_ey_yth_eyth = "#x_ex_y_ey_yth_eyth";
  rL.add(x_ex_y_ey_yth_eyth, head_x_ex_y_ey_yth_eyth);

  /* plotting a dense list of theoretical ponits */

  // finding the minimum and maximum value of the 'x'
  const U a = 0.0;
  const U b = Functions::max_value(fL.avrs());

  std::vector<U> x_dense(N + 1);
  const int Nj = this->N_jkf();
  jkf::matrix<U> Y_th_dense(N + 1, Nj); // jkf of the 'N' points
  // filling the matrix
  for (int j = 0; j < Nj; ++j) {
    const U A0 = PARS(0, j);
    const U C = PARS(1, j);
    for (int i = 0; i <= N; ++i) {
      const U fLi = a + i * ((b - a) / (N + 0.0));
      x_dense[i] = fLi;
      Y_th_dense(i, j) = obs_L_had(A0, fLi, C);
    }
  }

  /*! data in the "x y_th ey_th" format */
  jkf::matrix<U> dense_x_yth_eyth = jkf::build_xyey(x_dense, Y_th_dense);
  rL.add(dense_x_yth_eyth, {"#dense#x_yth_eyth"});

  rL.print_summary(fld + NAME + "_L.dat");
} // print_obs_L()

template <class U>
void QCD<U>::print_obs_inf(const std::string &fld, const int &N) {
  jkf::results<U> r_inf;                          /* data at finite volume */
  r_inf.set_title("Observable at finite volume"); /*!< Title */
  r_inf.set_subtitle(NAME + " as a function of " +
                     fL_string(HAD)); /*!< subtitle */

  /*! Parameters with their names */
  r_inf.add_jkf_rows(PARS, PAR_NAMES);
  const jkf::matrix<U> OBS_inf = get_OBS_inf();
  r_inf.add(OBS_inf, "#jkf#data_y"); /*!< adding jkf of y values */

  const jkf::matrix<U> fL = this->get_fL();
  const jkf::matrix<U> OBS_inf_th = jkf::constant_jkf(PARS.row(0), n_pts());

  /*! data in the "x ex y ey y_th ey_th" format */
  const jkf::matrix<U> x_ex_y_ey_yth_eyth =
      jkf::build_x_ex_y_ey_yth_eyth(fL, OBS_inf, OBS_inf_th);
  const std::string head_x_ex_y_ey_yth_eyth = "#x_ex_y_ey_yth_eyth";
  r_inf.add(x_ex_y_ey_yth_eyth, head_x_ex_y_ey_yth_eyth);

  /* plotting a dense list of theoretical points */

  // finding the minimum and maximum value of the 'x'
  const U a = 0.0;
  const U b = Functions::max_value(fL.avrs());
  const U delta = (b - a) / (N + 0.0);

  std::vector<U> x_dense(N + 1);
  jkf::matrix<U> Y_th_dense(N + 1, this->N_jkf()); // jkf of the 'N' points
  // filling the matrix Y_th_dense
  for (int i = 0; i <= N; ++i) {
    x_dense[i] = a + i * delta;
    Y_th_dense.set_row(i, PARS.row(0));
  }

  /*! data in the "x y_th ey_th" format */
  jkf::matrix<U> dense_x_yth_eyth = jkf::build_xyey(x_dense, Y_th_dense);
  r_inf.add(dense_x_yth_eyth, {"#dense#x_yth_eyth"});

  r_inf.print_summary(fld + NAME + "_inf.dat");
} // print_obs_L()

/* ----------------------------------------------------- */
/* Correcting an observable using the results of the fit */
/* ----------------------------------------------------- */

template <class U>
jkf::matrix<U> QCD<U>::get_corrected(const jkf::matrix<U> &obs_L, const int &l,
                                     const jkf::matrix<U> &m_pi) const {
  const int Nj = N_jkf();
  jkf::matrix<U> obs_inf(1, Nj);   /* infinite volume limit */
  const U l_inv = 1.0 / (0.0 + l); // 1/l
  for (int j = 0; j < Nj; ++j) {
    const U C = PARS(1, j);
    const U m = MP(0, j);
    obs_inf(0, j) = obs_L(0, j) / fact_obs_L_had(m, l_inv, C, HAD);
  }
  return obs_inf;
} // get_OBS_inf())

#endif
} // namespace FVE
} // namespace Lattice
} // namespace Silibs2
