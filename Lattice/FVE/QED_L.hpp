// QED_L.hpp
/* definitions of QED_L.h */

#include "../../Minuit2_fit/fit.hpp"
// #include "../../jkf/Minuit2_fit/fit.hpp"
#include "../../jkf/matrix.hpp"
#include "../../jkf/results.hpp"

#include "./QED_L.h"

namespace Silibs2 {
namespace Lattice {
namespace FVE {
#ifndef Silibs2_Lattice_FVE_QED_L_hpp
#define Silibs2_Lattice_FVE_QED_L_hpp

double fact_QED_L_pt(const std::map<std::string, double> &args) {
  const double L_inv = args.at("L_inv"), Q = args.at("Q"), M = args.at("M");

  const double ML_inv = L_inv / M;
  const double Q2 = std::pow(Q, 2);

  const double kappa_half = kappa / 2.0;
  /* note the '-' */
  const double fact =
      -Q2 * alpha_EM * (kappa_half * L_inv * (1 + 2.0 * ML_inv));
  return fact;
} // fact_QED_L_pt()

double fact_QED_L_sd(const std::map<std::string, double> &args) {
  const double L_inv = args.at("L_inv"), M = args.at("M"), a3 = args.at("a3"),
               a4 = args.at("a4");

  const double ML_inv = L_inv / M;

  const double ML3_inv = std::pow(ML_inv, 3); // (M*L)^(-3)
  const double ML4_inv = std::pow(ML_inv, 4); // (M*L)^(-4)
  const double f = (a3 * ML3_inv + a4 * ML4_inv);
  return alpha_EM * f;
} // fact_QED_L_sd()

double fact_QED_L(const std::map<std::string, double> &args) {
  const double pt = fact_QED_L_pt(args);
  const double sd = fact_QED_L_sd(args);
  return (pt + sd);
} // fact_QED_L()

double ansatz_QED_L_pt(const std::map<std::string, double> &args) {
  const double &obs_inf = args.at("obs_inf");
  return obs_inf + fact_QED_L_pt(args);
} // ansatz_QED_L_pt()

double ansatz_QED_L_sd(const std::map<std::string, double> &args) {
  const double &obs_inf = args.at("obs_inf");
  return obs_inf + fact_QED_L_sd(args);
} // ansatz_QED_L_sd()

double ansatz_QED_L(const double &L_inv, const double &Q, const double &M,
                    const std::vector<double> &p) {
  const std::map<std::string, double> args = {
      {"L_inv", L_inv}, {"Q", Q}, {"M", M}, {"a3", p[1]}, {"a4", p[2]}};
  return p[0] + fact_QED_L(args);
} // ansatz_QED_L()

/* ----------- */
/* class QED_L */
/* ----------- */

template <class U> QED_L<U>::QED_L() {}
template <class U> QED_L<U>::~QED_L() {}

template <class U> int QED_L<U>::N_jkf() const { return OBS_L.cols(); }
template <class U> int QED_L<U>::n_pars() const {
  return (int)PAR_NAMES.size();
}

template <class U> int QED_L<U>::n_pts() const { return OBS_L.rows(); }

template <class U> std::vector<U> QED_L<U>::get_L_inv() const {
  const int n = L.size();
  std::vector<U> L_inv(n);
  for (int i = 0; i < n; ++i) {
    L_inv[i] = std::pow((U)L[i], -1);
  }
  return L_inv;
} // get_L_inv()

template <class U>
QED_L<U>::QED_L(const std::string &name, const std::vector<int> &L_values,
                const jkf::matrix<U> &obs_L, const U &q) {
  L = Functions::boost_cast<U>(L_values);
  OBS_L = obs_L;
  NAME = name;
  Q = q;
} // FVE()

template <class U> void QED_L<U>::set_MASS_L(const jkf::matrix<U> &mass_L) {
  MASS_L = mass_L;
  MASS_L_init = true;
} // set_MASS_L()

template <class U> void QED_L<U>::check_init() const {
  const int nL = L.size();
  const int n1 = MASS_L.rows();
  const int n2 = OBS_L.rows();

  if (!MASS_L_init) {
    std::cerr
        << "Error: you must give the jkf for the mass associated to the slope."
        << "\n";
    std::cerr << "Aborting."
              << "\n";
    abort();
  }

  if (n1 != n2 || nL != n2) {
    std::cerr << "Invalid initialization of object.\n";
    std::cerr << "Number of volumes " << nL << "\n";
    std::cerr << "Number of masses " << n1 << "\n";
    std::cerr << "Number of observables " << n2 << "\n";
    std::cerr << "Aborting.\n";
    abort();
  }
} // check_init()

template <class U> void QED_L<U>::use_pt_only() {
  pt_only = true;
  this->check_init();
  pars_found = true;
} // check_init()

template <class U> void QED_L<U>::find_PARS() {
  this->check_init();
  const std::vector<double> L_inv = this->get_L_inv();
  const int Nj = N_jkf();
  /*
  the value at L->inf would be(presumably) near the values analyzed, so I
  use one of them as an ansatz
  */
  const std::vector<U> guesses = {OBS_L.avrs()[0], 1.0, 1.0};
  const int n_pars = PAR_NAMES.size();
  const int n_deg = n_pts() - n_pars;

  /* loop over the jkf */
  PARS.resize(n_pars, Nj);
  const std::vector<U> ey = OBS_L.errs(); /* errors */
  for (int j = 0; j < Nj; ++j) {
    const std::vector<U> mass = MASS_L.col(j);
    const std::vector<U> y = OBS_L.col(j);

    Minuit2_fit::fit_xyey<U> minu;

    minu.set_pars(PAR_NAMES, guesses);
    minu.set_data(L_inv, OBS_L.col(j), ey);

    std::function<U(const std::vector<U> &)> rsd =
        [=](const std::vector<U> &p) {
          U ch2 = 0.0;
          for (int i = 0; i < n_pts(); ++i) {
            const U y_th = ansatz_QED_L(L_inv[i], Q, mass[i], p);
            ch2 += pow((y_th - y[i]) / ey[i], 2);
          }
          return (ch2 / n_deg); // reduced chi squared
        };

    minu.set_residue(rsd);
    minu.fit();
    PARS.set_col(minu.get_PARS(), j);
  }

  pars_found = true;
} // find_PARS()

template <class U> void QED_L<U>::check_PARS() const {
  if (!pars_found && !pt_only) {
    std::cerr << "Error. Theoretical observables can be calculated only "
                 "after the fit.\n";
    std::cerr << "Aborting.\n";
    abort();
  }
} // check_PARS()

template <class U> void QED_L<U>::find_corrected() {
  if (!pt_only) {
    this->find_PARS();
  }

  const std::vector<double> L_inv = this->get_L_inv();
  /* setting up the components of the theoretical values */
  const int n1 = n_pts();
  const int Nj = N_jkf();

  OBS_L_th.resize(n1, Nj);
  OBS_inf.resize(n1, Nj);
  for (int j = 0; j < Nj; ++j) {
    const std::vector<U> p = PARS.col(j);
    for (int i = 0; i < n1; ++i) {
      const double m = MASS_L(i, j);
      if (pt_only) {
        OBS_L_th(i, j) = OBS_L(i, j);
        OBS_inf(i, j) =
            OBS_L(i, j) -
            fact_QED_L_pt({{"L_inv", L_inv[i]}, {"Q", Q}, {"M", m}});
      } else {
        OBS_L_th(i, j) = ansatz_QED_L(L_inv[i], Q, m, p);
        OBS_inf(i, j) = OBS_L(i, j) - fact_QED_L({{"L_inv", L_inv[i]},
                                                  {"Q", Q},
                                                  {"M", m},
                                                  {"a3", p[1]},
                                                  {"a4", p[2]}});
      }
    }
  }

  // theoretical value of the observable at L=inf
  OBS_inf_th.resize(n1, Nj);
  if (pt_only) { // point-like only
    for (int j = 0; j < Nj; ++j) {
      const double OBS_inf_avr = Functions::average(OBS_inf.col(j));
      for (int i = 0; i < n1; ++i) {
        OBS_inf_th(i, j) = OBS_inf_avr;
      }
    }
  } else { // pt and SD
    OBS_inf_th = jkf::constant_jkf(PARS.row(0), n1);
  }
} // find_corrected()

/* ------------------------------- */
/* Printing the results of the fit */
/* ------------------------------- */

template <class U>
void QED_L<U>::print_obs_L(const std::string &fld, const int &N) const {
  this->check_PARS();
  jkf::results<U> rL;                              /* data at finite volume */
  rL.set_title(NAME + " at finite volume");        /*!< Title */
  rL.set_subtitle(NAME + " as a function of 1/L"); /*!< Subtitle */
  /*! Parameters with their names */
  if (!pt_only) {
    rL.add_jkf_rows(PARS, PAR_NAMES);
  }
  rL.add(OBS_L, "#jkf#data_y"); /*!< adding jkf of y values */

  const std::vector<U> L_inv = this->get_L_inv();
  const std::vector<U> y = OBS_L.avrs();
  const std::vector<U> ey = OBS_L.errs();
  const std::vector<U> yth = OBS_L_th.avrs();
  const std::vector<U> eyth = OBS_L_th.errs();

  const jkf::matrix<U> data_plot =
      jkf::matrix_from_cols({L_inv, y, ey, yth, eyth});
  const std::string head_plot = "#L_inv_y_ey_yth_eyth";
  rL.add(data_plot, head_plot);

  // finding the minimum and maximum value of the 'x'
  const U a = 0.0;
  const U b = Functions::max_value(L_inv);

  // index of the minimum L_inv (i.e. the point closer to L=inf limit)
  const int ci = std::min_element(L_inv.begin(), L_inv.end()) - L_inv.begin();

  std::vector<U> x_dense(N + 1);
  const int Nj = this->N_jkf();
  jkf::matrix<U> Y_th_dense(N + 1, Nj); // jkf of the 'N' points

  // filling the matrix
  for (int i = 0; i <= N; ++i) {
    const U L_inv_i = a + i * ((b - a) / (N + 0.0));
    x_dense[i] = L_inv_i;
    for (int j = 0; j < Nj; ++j) {
      const double m = MASS_L(ci, j);
      if (pt_only) {
        Y_th_dense(i, j) =
            OBS_inf(ci, j) +
            fact_QED_L_pt({{"L_inv", L_inv_i}, {"Q", Q}, {"M", m}});
      } else {
        Y_th_dense(i, j) = ansatz_QED_L(L_inv_i, Q, m, PARS.col(j));
      }
    }
  }

  /*! data in the "x y_th ey_th" format */
  jkf::matrix<U> dense_x_yth_eyth = jkf::build_xyey(x_dense, Y_th_dense);
  rL.add(dense_x_yth_eyth, {"#dense#x_yth_eyth"});

  rL.print_summary(fld + NAME + "_L.dat");
} // print_obs_L()

template <class U>
void QED_L<U>::print_obs_inf(const std::string &fld, const int &N) const {
  this->check_PARS();
  jkf::results<U> r_inf;                          /* data at finite volume */
  r_inf.set_title("Observable at finite volume"); /*!< Title */
  r_inf.set_subtitle(NAME + " as a function of 1/L"); /*!< Subtitle */

  /*! Parameters with their names */
  if (!pt_only) {
    r_inf.add_jkf_rows(PARS, PAR_NAMES);
  }
  r_inf.add(OBS_inf, "#jkf#data_y"); /*!< adding jkf of y values */

  const std::vector<U> L_inv = this->get_L_inv();

  /*! data in the "x ex y ey y_th ey_th" format */
  const jkf::matrix<U> x_y_ey_yth_eyth =
      jkf::build_x_y_ey_yth_eyth(L_inv, OBS_inf, OBS_inf_th);
  const std::string head_x_y_ey_yth_eyth = "#L_inv_y_ey_yth_eyth";
  r_inf.add(x_y_ey_yth_eyth, head_x_y_ey_yth_eyth);

  // finding the minimum and maximum value of the 'x'
  const U a = 0.0;
  const U b = Functions::max_value(L_inv);

  // index of the minimum L_inv (i.e. the point closer to L=inf limit)
  const int ci = std::min_element(L_inv.begin(), L_inv.end()) - L_inv.begin();

  const int Nj = this->N_jkf();

  std::vector<U> x_dense(N + 1);
  jkf::matrix<U> Y_th_dense(N + 1, Nj); // jkf of the 'N' points
  // filling the matrix
  for (int i = 0; i <= N; ++i) {
    x_dense[i] = a + i * (b - a) / (N + 0.0);
    for (int j = 0; j < Nj; ++j) {
      if (pt_only) {
        Y_th_dense(i, j) = OBS_inf(ci, j);
      } else {
        Y_th_dense(i, j) = PARS(0, j);
      }
    }
  }

  /*! data in the "x y_th ey_th" format */
  jkf::matrix<U> dense_x_yth_eyth = jkf::build_xyey(x_dense, Y_th_dense);

  r_inf.add(dense_x_yth_eyth, {"#dense#x_yth_eyth"});

  r_inf.print_summary(fld + NAME + "_inf.dat");
} // print_obs_L()

/* ----------------------------------------------------- */
/* Correcting an observable using the results of the fit */
/* ----------------------------------------------------- */

template <class U>
jkf::matrix<U> QED_L<U>::get_corrected(const jkf::matrix<U> &obs_L,
                                       const jkf::matrix<U> &mass,
                                       const int &l) const {
  this->check_PARS();
  const int Nj = N_jkf();
  jkf::matrix<U> obs_inf(1, Nj); /* infinite volume limit */
  const double l_inv = 1.0 / l;
  for (int j = 0; j < Nj; ++j) {
    const std::vector<U> p = PARS.col(j);
    const double m = mass(0, j);
    if (pt_only) {
      obs_inf(0, j) =
          obs_L(0, j) - fact_QED_L_pt({{"L_inv", l_inv}, {"Q", Q}, {"M", m}});
    } else {
      obs_inf(0, j) = obs_L(0, j) - fact_QED_L({{"L_inv", l_inv},
                                                {"Q", Q},
                                                {"M", m},
                                                {"a3", p[1]},
                                                {"a4", p[2]}});
    }
  }
  return obs_inf;
} // get_corrected()

#endif
} // namespace FVE
} // namespace Lattice
} // namespace Silibs2
