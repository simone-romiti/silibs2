// QED_L.h
/*
class for the correction of QED_L Finite Volume Effects
for a given particle or electromagnetic mass variarion with respect to the
isoQCD background.
These effect comprehend both a universal (point like)
and a structure dependent contribution
*/

#include "../../Minuit2_fit/fit.h"
// #include "../../jkf/Minuit2_fit/fit.h"
#include "../../jkf/matrix.h"
#include "../../jkf/results.h"
#include <map>

namespace Silibs2 {
namespace Lattice {
namespace FVE {
#ifndef Silibs2_Lattice_FVE_QED_L_h
#define Silibs2_Lattice_FVE_QED_L_h

const double alpha_EM = 7.2973525693e-3; /* from PDG*/
const double kappa = 2.837297; /* kappa factor in the QED_L correction */

double fact_QED_L_pt(const std::map<std::string, double> &args);

double fact_QED_L_sd(const std::map<std::string, double> &args);

double fact_QED_L(const std::map<std::string, double> &args);

double ansatz_QED_L_pt(const std::map<std::string, double> &args);

double ansatz_QED_L_sd(const std::map<std::string, double> &args);

double ansatz_QED_L(const double &L_inv, const double &Q, const double &M,
                    const std::vector<double> &p);

/* ----------- */
/* class QED_L */
/* ----------- */

template <class U = double> class QED_L {
private:
  std::string NAME; /* name of the observable */

  U Q; /* charge in units of 'e' */

  std::vector<U> L; /* sizes */

  jkf::matrix<U> MASS_L; /* Mass linked to the observable */
  bool MASS_L_init = false;

  jkf::matrix<U> OBS_L;      /* observables differing for the volume only */
  jkf::matrix<U> OBS_inf;    /* OBL_L corrected from FVEs */
  jkf::matrix<U> OBS_inf_th; /* theoretical limit L->\inf OBL_L */

  /* Theoretical prediction */
  jkf::matrix<U> OBS_L_th_pt; /* point-like contribution */
  jkf::matrix<U> OBS_L_th_sd; /* structure dependent contribution */
  jkf::matrix<U> OBS_L_th;    /* point-like + structure-dep. contribution */

  std::vector<std::string> PAR_NAMES = {"OBS_inf", "c1", "c2"};
  jkf::matrix<U> PARS;
  bool pars_found = false;

  // true when we consider only the universal, point-like, correction
  bool pt_only = false;

public:
  QED_L();
  ~QED_L();

  int N_jkf() const;
  int n_pars() const;
  int n_pts() const;

  std::vector<U> get_L_inv() const;

  QED_L(const std::string &name, const std::vector<int> &L_values,
        const jkf::matrix<U> &obs_L, const U &q);

  void set_MASS_L(const jkf::matrix<U> &mass_L);

  void check_init() const; // check the correct initialization fo the object
  void use_pt_only();      // set pt_only=true

  void find_PARS();        /* find the parameters of the ansatz*/
  void check_PARS() const; /* check if PARS have been found with the fit*/
  // void use_PARS();         /*  set the theoretical values*/

  /*
  set the theoretical values
  SD == true --> the Sructure Dependent part is included
  */
  void find_corrected();

  // jkf::matrix<U> get_OBS_inf() const; /* OBS_L corrected from FVEs */
  //
  // /* OBS_L corrected from only point-like (universal) FVEs */
  // jkf::matrix<U> get_OBS_inf_pt() const;
  //
  // jkf::matrix<U> get_OBS_L_th() const;
  //
  // jkf::matrix<U> get_OBS_inf_th() const;

  /* ------------------------------- */
  /* Printing the results of the fit */
  /* ------------------------------- */

  /* OBS_L printed as a function of fL */
  void print_obs_L(const std::string &fld, const int &N = 100) const;
  /* OBS_L corrected from FVEs, printed as a function of fL */
  void print_obs_inf(const std::string &fld, const int &N = 100) const;

  /* ----------------------------------------------------- */
  /* Correcting an observable using the results of the fit */
  /* ----------------------------------------------------- */

  jkf::matrix<U> get_corrected(const jkf::matrix<U> &obs_L,
                               const jkf::matrix<U> &mass, const int &l) const;

}; // class QED_L

#endif
} // namespace FVE
} // namespace Lattice
} // namespace Silibs2
