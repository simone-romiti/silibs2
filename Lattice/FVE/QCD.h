// QCD.h
/*
class for the correction of QCD Finite Volume Effects
for a given observable.
These effect arise (at LO) from the presence of virtual pion/kaon loops in ChPT.
Mesons: https://arxiv.org/abs/hep-lat/0409111
Nucleon: https://arxiv.org/pdf/1005.1485.pdf
Omega: https://arxiv.org/abs/1401.7805
*/

#include <algorithm>

#include "../../Minuit2_fit/fit.h"
#include "../../jkf/matrix.h"
#include "../../jkf/results.h"

namespace Silibs2 {
namespace Lattice {
namespace FVE {

#ifndef Silibs2_Lattice_FVE_QCD_h
#define Silibs2_Lattice_FVE_QCD_h

// 1st type of ansatz
const std::vector<std::string> had_list_1 = {
    "Pion",  "Pi",      "pi",      "pion",    "K",
    "Kaon",  "kaon",    "Omega",   "omega",   "Delta",
    "delta", "Delta_2", "delta_2", "Delta_3", "delta_3"};

// 2nd type of ansatz
const std::vector<std::string> had_list_2 = {"N", "Nucleon", "nucleon"};

/*! returns string:
pi,K,Omega --> mP^2 * exp(-mP * L) / pow(mP * L, 3.0/2.0)
N          --> mP^3 * exp(-mP * L) / (mP * L)
*/
std::string fL_string(const std::string &had);

template <class U = double>
U fL_val(const U &MP, const U &L_inv, const std::string &had);

//
// template <class U = double>
// U had_fact(const U &L, const U &C, const U &m_pi, const U &alpha);
//
// template <class U> U had_fact_fL(const U &fL, const U &C);

// factor multiplying the observable at L=inf
// int the expression for the observable at finite volume
template <class U = double>
U fact_obs_L_had(const U &MP, const U &L_inv, const U &C,
                 const std::string &had);

// expression for the observable at finite volume
template <class U = double>
U obs_L_had(const U &obs_inf, const U &MP, const U &L_inv, const U &C,
            const std::string &had);

/*
Overload of obs_L_had but with fL given.
This is for dense points generation
*/
template <class U> U obs_L_had(const U &obs_inf, const U &fL, const U &C);

template <class U = double> class QCD {
private:
  std::string NAME;     /* name of the observable */
  std::vector<U> L;     /* sizes L/a */
  std::vector<U> L_inv; /* inverse sizes a/L */
  jkf::matrix<U> OBS_L; /* observables differing for the volume only */

  std::string HAD; // hadron name (pion, kaon, nucleon, Omega)
  // U ALPHA;

  jkf::matrix<U> MP;
  bool MP_init = false;

  std::vector<std::string> PAR_NAMES = {"OBS_inf", "C"};
  jkf::matrix<U> PARS;

public:
  QCD();
  ~QCD();

  int N_jkf() const;
  int n_pars() const;
  int n_pts() const;

  QCD(const std::string &name, const std::vector<int> L_values,
      const jkf::matrix<U> &obs_L, const std::string &had_name);

  // MP = mass of the pseudoscalar meson giving the leading contribution
  void set_MP(const jkf::matrix<U> &mp);
  void check_MP(); /* check if MP has been initialized */

  void find_PARS(); /* find the parameters of the ansatz*/

  jkf::matrix<U> get_fL();      /* returns the jkf::matrix of fL */
  jkf::matrix<U> get_OBS_inf(); /* OBS_L corrected from FVEs */

  /* ------------------------------------------------------------- */
  /* Theoretical prediction of the L dependence of the observable: */
  /* ------------------------------------------------------------- */
  jkf::matrix<U> get_OBS_L_th();   // with FVEs
  jkf::matrix<U> get_OBS_inf_th(); // without FVEs (should be flat)

  /* ------------------------------- */
  /* Printing the results of the fit */
  /* ------------------------------- */

  /*
  OBS_L printed as a function of fL
  N = number of points for the theoretical curve
  */
  void print_obs_L(const std::string &fld, const int &N = 100);
  /* OBS_L corrected from FVEs, printed as a function of fL */
  void print_obs_inf(const std::string &fld, const int &N = 100);

  /* ----------------------------------------------------- */
  /* Correcting an observable using the results of the fit */
  /* ----------------------------------------------------- */

  jkf::matrix<U> get_corrected(const jkf::matrix<U> &obs_L, const int &l,
                               const jkf::matrix<U> &m_pi) const;

}; // class QCD

#endif
} // namespace FVE
} // namespace Lattice
} // namespace Silibs2
