// al.hpp
/*! Definitions of al.h */

#include "../../Functions/functions.hpp"

#include "./al.h"

namespace Silibs2 {
namespace Lattice {
#ifndef Silibs2_Lattice_al_hpp
#define Silibs2_Lattice_al_hpp

namespace a_l {
const double inv_alpha_em = 137.035999139; // taken from wikipedia
const double alpha_em = 1.0 / inv_alpha_em;
} // namespace a_l

template <class U>
U a_l_lattice(const std::vector<U> &V, const U &m_l, const int &N_div) {
  U result = 0.0;
  int n = V.size();
  for (int i = 0; i < n; ++i) {
    U t = i + 1;
    result += V[i] * kernel_a_l(t, m_l, N_div);
  }

  U alphaEM = a_l::alpha_em;
  U A = 4.0 * pow(alphaEM, 2.0);
  return A * result;
} // end a_l_lattice()

template <class U>
U a_l_theory(const std::vector<U> &times, const std::vector<U> &V, const U &m_l,
             const int &N_div) {
  int nt = times.size();
  int nV = V.size();
  if (nt != nV) {
    std::cerr << "Error :\n";
    std::cerr << "Numer of times = " << nt << "\n";
    std::cerr << "Numer of values = " << nV << "\n";
    abort();
  }
  U result = 0.0;
  for (int i = 0; i < nt; ++i) {
    U t = times[i];
    result += V[i] * kernel_a_l(t, m_l, N_div);
  }
  return result;
} // end a_l_theory()

template <class U>
U a_l_theory(const std::function<U(const U &)> &V, const U &m_l,
             const int &N_div) {
  using namespace Silibs2::Functions;
  U alpha2 = pow(a_l::alpha_em, 2);
  std::function<U(const U &, U *)> integrand = [&](const U &t, U *p0) {
    return kernel_a_l(t, m_l, N_div) * V(t);
  };

  U dummy_ptr[] = {};
  return 4.0 * alpha2 * inte_a_plusInf(integrand, dummy_ptr, 0.0, N_div);
} // end a_l_theory()

template <class U> U kernel_a_l(const U &t, const U &m_l, const int &N_div) {
  using namespace Silibs2::Functions;
  U A = pow(2.0 / m_l, 2);
  std::function<U(const U &, U *)> f = [&](const U &omega, U *p) {
    U tau = p[0], mass_l = p[1];
    return integrand_kernel_a_l(omega, tau, mass_l);
  };

  U pars[2] = {t, m_l};
  U I = inte_a_plusInf(f, pars, 0.0, N_div);

  return A * I;
} // end kernel_a_l()

template <class U>
U integrand_kernel_a_l(const U &z, const U &t, const U &m_l) {
  U z2 = pow(z, 2.0);   /* \z^2 */
  U R = sqrt(4.0 + z2); /* \sqrt{4 + \z^2} */

  U one = 1.0 / R; /* 1/R */
  U two = pow((R - z) / (R + z), 2);

  U three1 = (cos(z * m_l * t) - 1.0) / z2;
  U three2 = pow(m_l * t, 2) / 2.0;
  U three = three1 + three2;

  U res = one * two * three;
  return res;
} // end integrand_kernel_a_l()

#endif
} // namespace Lattice
} // namespace Silibs2
