// a_l.h
/*!
This file declares the functions which calculate a_l
(l= generic lepton, e.g. the muon)
given the vector corelator V(t)
[reference](https://arxiv.org/abs/1707.03019)
*/

#include "../../Functions/functions.h"

namespace Silibs2 {
namespace Lattice {
#ifndef Silibs2_Lattice_al_h
#define Silibs2_Lattice_al_h

/*
contribution to a_l from the lattice data points;
t=0 is not considered because of the contact divergence:
the user is supposed to pass the vector [V(1), V(2), ..., V(n)]
*/
template <class U = double>
U a_l_lattice(const std::vector<U> &V, const U &m_l, const int &N_div = 1e+2);

/*
contribution to a_l from the theory;
the user must supply the times and the corresponding values of V(t)
*/
template <class U = double>
U a_l_theory(const std::vector<U> &times, const std::vector<U> &V, const U &m_l,
             const int &N_div = 1e+2);

template <class U = double>
U a_l_theory(const std::function<U(const U &)> &V, const U &m_l,
             const int &N_div = 1e+2);

/* kernel function f(t) which appears in the integral of a_l */
template <class U = double>
U kernel_a_l(const U &t, const U &m_l, const int &N_div);

/*
integrand of the kernel function f(t) which appears in the integral of a_l
the mass of the lepton must be in lattice units
*/
template <class U = double>
U integrand_kernel_a_l(const U &z, const U &t, const U &m_l);

#endif

} // namespace Lattice
} // namespace Silibs2
