// systematics.h
// combining two or more results to get the average and systematic error
// using eq. 28 of https://arxiv.org/abs/1403.4504

#include "./systematics.h"

namespace Silibs2 {
namespace Lattice {

template <class U>
std::pair<U, U> combine_systematics(const std::vector<jkf::matrix<U>> &v) {
  const int N = v.size(); // number of observable to combine
  const U x = (jkf::average_matrices(v)).avrs()[0]; // average

  U sigma2 = 0.0; //  sigma^2
  for (int i = 0; i < N; ++i) {
    if (v[i].rows() != 1) {
      std::cerr
          << "Invalid vector of jkf::matrix . They must be all 1-row matrices."
          << "\n";
      std::cerr << "i : " << i << " , v[i].rows() : " << v[i].rows() << "\n";
      std::cerr << "Aborting."
                << "\n";
      abort();
    }

    U sigmai = v[i].errs()[0];
    U ei = v[i].avrs()[0] - x;

    sigma2 += (std::pow(sigmai, 2) + std::pow(ei, 2)) / (N + 0.0);
  }

  U sigma = std::sqrt(sigma2);
  return std::make_pair<U, U>((U)x, (U)sigma);
} // combine_systematics()

} // namespace Lattice
} // namespace Silibs2