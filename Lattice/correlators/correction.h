// correction.h
/*!
Routines for the extraction of mass and amplitudes corrections from the
corrections to the correlators
*/

#include <iostream>
#include <vector>

#include "../../jkf/matrix.h"

namespace Silibs2 {
namespace Lattice {
namespace correlators {
#ifndef Silibs2_Lattice_correlators_correction_h
#define Silibs2_Lattice_correlators_correction_h

/* ---------------- */
/* class correction */
/* ---------------- */

template <class U = double> class correction {
private:
  /* sign under time reversal:
  0 = undefined time parity (no backward signal)
  1,-1: positive/negative time parity (yes backward signal)
  */
  int t_rev = 0;
  jkf::matrix<U> dC;
  jkf::matrix<U> C0;

public:
  correction();
  ~correction();

  correction(const jkf::matrix<U> &dC_corr, const jkf::matrix<U> &C0_corr,
             const int &tp);

  /* returns true if (*this).tp == +1,-1 */
  void check_back() const;

  /* returns true if (*this).tp == 0 */
  void check_no_back() const;

  /*!
  effective curve of the correction to the leading mass
  (with M = M0 + dM)
  without accounting for the backward signal (i.e. for bayonic correlators)
  */
  jkf::matrix<U> dM_eff_no_back() const;

  /*!
  effective curve of the correction to the leading amplitude
  (with A = A0*(1 + dA) )
  without accounting for the backward signal (i.e. for bayonic correlators)
  */
  jkf::matrix<U> dA_eff_no_back() const;

  /*!
  returns the effective curve of the correction to the leading mass
  (with M = M0 + dM)
  accounting for the backward signal (i.e. for mesonic correlators).
  t_rev = +1,-1 : sign under time reversal
  */
  jkf::matrix<U> dM_eff_back() const;

  /*!
  returns the effective curve of the correction to the leading Amplitude
  (with A = A0*(1 + dA) )
  accounting for the backward signal (i.e. for mesonic correlators).
  t_rev = +1,-1 : sign under time reversal
  */
  jkf::matrix<U> dA_eff_back() const;

}; // class correction

/* -------------------- */
/* full-theory routines */
/* -------------------- */

/*!
observable in the full theory.
g = vector of counterterms
slp = vector of slopes
returns obs_0 + g[i]*slp[i]
*/
template <class U = double>
jkf::matrix<U> obs_full(const jkf::matrix<U> &obs_0,
                        const std::vector<jkf::matrix<U>> &g,
                        const std::vector<jkf::matrix<U>> &slp);

/*!
slope of a generic observable with respect to the counterterm g[i].
g = vector of counterterms
slp = vector of slopes
returns obs_0 + g[i]*slp[i]
*/
template <class U = double>
jkf::matrix<U> obs_slope(const jkf::matrix<U> &obs_0,
                         const std::vector<jkf::matrix<U>> &g,
                         const std::vector<jkf::matrix<U>> &slp, const int &i,
                         const U &eps = 1e-10);
#endif
} // namespace correlators
} // namespace Lattice
} // namespace Silibs2
