// correction.hpp
/* Definitions of correction.h */

#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include "../../jkf/Minuit2_fit/fit.hpp"
#include "../../jkf/matrix.hpp"
#include "../Luscher/dual_PP.hpp"
#include "../ODE/ODE.hpp"
#include "../effective/effective.hpp"

#include "./correction.h"

namespace Silibs2 {
namespace Lattice {
namespace correlators {
#ifndef Silibs2_Lattice_correlators_correction_hpp
#define Silibs2_Lattice_correlators_correction_hpp

/* ---------------- */
/* class correction */
/* ---------------- */

template <class U> correction<U>::correction() {}  // correction()
template <class U> correction<U>::~correction() {} // correction()

template <class U>
correction<U>::correction(const jkf::matrix<U> &dC_corr,
                          const jkf::matrix<U> &C0_corr, const int &tp) {
  try {
    const std::string e0 = "Error in construction of correction correlator.\n";
    if (dC_corr.shape() != C0_corr.shape()) {
      throw e0 + "Correction and free correlator have different sizes.\n";
    }
    if (!(tp == 1 || tp == -1 || tp == 0)) {
      std::string info =
          "Time parity can either be 1,-1 or 0 (i.e. undefined).\n";
      info += "You passed " + std::to_string(tp) + "\n";
      throw e0 + info;
    }
  } catch (const std::string &err) {
    std::cerr << err << "\n";
    std::cerr << "Aborting.\n";
    abort();
  }
  (*this).dC = dC_corr;
  (*this).C0 = C0_corr;
  (*this).t_rev = tp;
} // correction()

template <class U> void correction<U>::check_back() const {
  if (t_rev == 0) {
    std::cerr << "Time parity shouldn't equal " << t_rev << " here.\n";
    std::cerr << "You can call only methods using backward signal.\n";
    std::cerr << "Aborting.\n";
    abort();
  }
} // check_back()

template <class U> void correction<U>::check_no_back() const {
  if (t_rev != 0) {
    std::cerr << "Time parity shouldn't equal " << t_rev << " here.\n";
    std::cerr << "You can call only methods not using backward signal.\n";
    std::cerr << "Aborting.\n";
    abort();
  }
} // check_no_back()

template <class U> jkf::matrix<U> correction<U>::dM_eff_no_back() const {
  this->check_no_back();
  return jkf::forward_der(1, (-1.0) * dC / C0);
} // dM_eff_no_back()

template <class U> jkf::matrix<U> correction<U>::dA_eff_no_back() const {
  this->check_no_back();
  const jkf::matrix<U> R = dC / C0;
  const jkf::matrix<U> dM = this->dM_eff_no_back();
  const int T_ext = C0.rows(), N_jkf = C0.cols();
  jkf::matrix<U> dA(T_ext, N_jkf);
  for (int i = 0; i < T_ext; ++i) {
    const int t = i + 1; // physical time
    for (int j = 0; j < N_jkf; ++j) {
      dA(i, j) = R(i, j) + dM(i, j) * t;
    }
  }
  return dA;
} // dA_eff_no_back()

template <class U> jkf::matrix<U> correction<U>::dM_eff_back() const {
  this->check_back();
  /*
  NOTE: I use E0_eff instead of E0 already fitted, becase:
  1 - the formula for dM_eff here is valid when C0 is dominated by E0
  2 - it's not necessary to fit C0 before calculating dM_eff
  */
  const jkf::matrix<U> E0_eff =
      Lattice::effective::effective_back_Gattringer(C0, 1e-4, 1e-8, t_rev)
          .first;

  const int T_half = C0.rows();
  const int N_jkf = C0.cols();
  const jkf::matrix<U> R = dC / C0;
  jkf::matrix<U> dM(T_half, N_jkf);

  U tau0 = T_half - 1;
  for (int i = 0; i < T_half; ++i) {
    for (int j = 0; j < N_jkf; ++j) {
      U t0 = tau0;
      const U t = i + 1; /* physical time */
      if (t == T_half) {
        /* t == T_half -> undefined effective mass curve E0_eff */
        dM(i, j) = 0.0;
        continue;
      }
      if (t == t0 && t_rev == -1) {
        dM(i, j) = 0.0; /* dM undefined */
        continue;
      }
      if (t == t0 && t_rev == +1) {
        /* T/2 -1 reached */
        t0 = 1; /* dM is defined, but t0 can't equal 't' */
      }

      const U num = R(i, j) - R(t0 - 1, j);
      U E0 = E0_eff(i, j);
      const U t_left = (t - T_half);
      const U t0_left = (t0 - T_half);
      const U tanh_t = tanh(E0 * t_left);
      const U tanh_t0 = tanh(E0 * t0_left);
      const U den =
          std::pow(tanh_t, t_rev) * t_left - std::pow(tanh_t0, t_rev) * t0_left;
      dM(i, j) = num / den;
    }
  }

  return dM;
} // dM_eff_back()

template <class U> jkf::matrix<U> correction<U>::dA_eff_back() const {
  this->check_back();
  /*
  NOTE: I use E0_eff instead of E0 already fitted, becase:
  1 - the formula for dM_eff here is valid when C0 is dominated by E0
  2 - it's not necessary to fit C0 before calculating dM_eff
  */
  const jkf::matrix<U> E0_eff =
      Lattice::effective::effective_back_Gattringer(C0, 1e-4, 1e-8, t_rev)
          .first;
  const jkf::matrix<U> dM_jkf = this->dM_eff_back();

  const int T_half = C0.rows(); // number of times
  const int N_jkf = C0.cols();  // number of jkf
  const jkf::matrix<U> R = dC / C0;
  jkf::matrix<U> dA(T_half, N_jkf);
  for (int i = 0; i < T_half; ++i) {
    for (int j = 0; j < N_jkf; ++j) {
      const U t = i + 1; /* physical time */
      if (t == T_half) {
        /* t == T_half -> undefined effective mass curve E0_eff */
        dA(i, j) = 0.0;
        continue;
      }

      const U E0 = E0_eff(i, j);
      const U dM = dM_jkf(i, j);
      const U t_left = (t - T_half);
      const U first = dM * T_half;
      const U second = -dM * t_left * std::pow(tanh(E0 * t_left), t_rev);
      dA(i, j) = R(i, j) + first + second;
    }
  }

  return dA;
} // dA_eff_back()

/* -------------------- */
/* full-theory routines */
/* -------------------- */

template <class U>
jkf::matrix<U> obs_full(const jkf::matrix<U> &obs_0,
                        const std::vector<jkf::matrix<U>> &g,
                        const std::vector<jkf::matrix<U>> &slp) {
  jkf::matrix<U> obs_result = obs_0;
  int n = g.size();
  assert(g.size() == slp.size() &&
         "The number of countertems is different from the number of slopes");
  for (int i = 0; i < n; ++i) {
    obs_result += g[i] * slp[i];
  }
  return obs_result;
} // obs_full()

template <class U>
jkf::matrix<U>
obs_slope(const jkf::matrix<U> &obs_0, const std::vector<jkf::matrix<U>> &g,
          const std::vector<jkf::matrix<U>> &slp, const int &i, const U &eps) {
  const int T_ext = obs_0.rows(); /*!< rows */
  const int N_jkf = obs_0.cols(); /*!< columns */
  const jkf::matrix<U> eps_matr = jkf::trivial_constant_jkf(eps, T_ext, N_jkf);
  std::vector<jkf::matrix<U>> g_left = g;
  g_left[i] = g[i] - eps_matr; /*!< {g1,... gi-eps, ..., gn} */
  std::vector<jkf::matrix<U>> g_right = g;
  g_right[i] = g[i] + eps_matr; /*!< {g1,... gi-eps, ..., gn} */

  const jkf::matrix<U> left = obs_full(obs_0, g_left, slp);
  const jkf::matrix<U> right = obs_full(obs_0, g_right, slp);
  return (right - left) / (2 * eps);
} // obs_slope()

#endif
} // namespace correlators
} // namespace Lattice
} // namespace Silibs2
