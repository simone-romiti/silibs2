// free.hpp
/* Definitions of free.h */

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "../../jkf/Minuit2_fit/fit.hpp"
#include "../../jkf/matrix.hpp"
#include "../Luscher/dual_PP.hpp"
#include "../ODE/ODE.hpp"
#include "../effective/effective.hpp"

#include "./free.h"

namespace Silibs2 {
namespace Lattice {
namespace correlators {
#ifndef Silibs2_Lattice_correlators_free_hpp
#define Silibs2_Lattice_correlators_free_hpp

template <class U> free<U>::free() {}  //! free()
template <class U> free<U>::~free() {} //! ~free()

/* ------------ */
/* Constructors */
/* ------------ */

template <class U>
free<U>::free(const std::string &name, const std::string &path_data,
              const std::string &head) {
  (*this).NAME = name;
  JKF = jkf::read_jkf<U>(path_data);
} // free()

template <class U>
free<U>::free(const std::string &name, const std::string &path_data,
              const int &rev, const std::string &head) {
  (*this).NAME = name;
  (*this).JKF = jkf::read_jkf<U>(path_data);
  (*this).T_REV = rev;
} // free()

template <class U>
free<U>::free(const std::string &name, const jkf::matrix<U> &jkf_matrix) {
  (*this).NAME = name;
  (*this).JKF = jkf_matrix;
} //! free()

template <class U>
free<U>::free(const std::string &name, const jkf::matrix<U> &jkf_matrix,
              const int &rev) {
  (*this).NAME = name;
  (*this).JKF = jkf_matrix;
  (*this).T_REV = rev;
} //! free()

/* ------- */
/* Members */
/* ------- */

template <class U> std::string free<U>::get_NAME() const {
  return (*this).NAME;
} // get_NAME()

template <class U> jkf::matrix<U> free<U>::get_JKF() const {
  return JKF;
} // get_JKF()

template <class U> int free<U>::N_jkf() const { return JKF.cols(); } // N_jkf()

template <class U> int free<U>::T_half() const {
  return JKF.rows();
} // T_half()

template <class U> int free<U>::T() const {
  return 2 * (this->T_half());
} // T()

template <class U> int free<U>::get_T_REV() const {
  this->check_init();
  return T_REV;
} // T_REV()

template <class U>
void free<U>::print_jkf_ae(const std::string &dest, const int &prec) const {
  this->check_init();
  /*! printing out */
  std::string data_file = dest + (*this).NAME + ".dat";
  JKF.print_jkf_ae(dest + (*this).NAME + ".dat", "\t", prec);
} // print_jkf()

/* ----- */
/* Check */
/* ----- */

template <class U> void free<U>::check_init() const {
  if (this->N_jkf() == 0 || ((*this).NAME) == "unnamed") {
    std::cerr << "Error : You haven't still initialized the object yet.\n"
              << "\n";
    std::cerr << "jkf matrix size : " << ((*this).JKF).rows() << " x "
              << ((*this).JKF).cols() << "\n";
    std::cerr << "Name of the correlator : " << (*this).NAME << "\n";
    std::cerr << "Aborting."
              << "\n";
    abort();
  }
} // check_init()

template <class U> void free<U>::check_T_REV() const {
  if ((*this).T_REV == 0) {
    std::cerr << "Error : You haven't specified the sign under time reversal."
              << "\n"
              << "Sign under time reversal : " << (*this).T_REV << "\n";
    std::cerr << "Aborting."
              << "\n";
    abort();
  }

} // check_T_REV()

/* ---------------------------- */
/* Effective mass and amplitude */
/* ---------------------------- */

template <class U>
void free<U>::set_and_print_effective(const jkf::matrix<U> &m_eff,
                                      const jkf::matrix<U> &a_eff,
                                      const std::string &info_method,
                                      const std::string &dest,
                                      const int &prec) {
  /*! - Setting - */

  (*this).M_EFF = m_eff;
  (*this).A_EFF = a_eff;

  /*! - Printing - */

  jkf::results<U> eff; /*!< results of the calculation */

  /*! title */
  std::string title =
      "Effective mass and amplitude curves for the correlator : " +
      this->get_NAME() + "\n.";
  title += info_method;
  eff.set_title("Effective mass and amplitude curves for the correlator " +
                this->get_NAME() + ".\n");

  const std::vector<double> times =
      Functions::range<double>(1, T_half() + 1, 1);

  // effective mass curve
  eff.add_jkf((*this).M_EFF, "M_eff"); /*!< jkf eff. mass curve */
  eff.add(jkf::build_xyey(times, (*this).M_EFF), "#xyey#M_eff");

  // effective amplitude curve
  eff.add_jkf((*this).A_EFF, "A_eff"); /*!< jkf eff. amplitude curve */
  eff.add(jkf::build_xyey(times, (*this).A_EFF), "#xyey#A_eff");

  /*!< printing */
  eff.print_summary(dest, "\t", prec);
} // set_and_print_effective()

template <class U>
void free<U>::effective_no_back(const std::string &dest_fld, const int &prec) {
  this->check_init();

  jkf::matrix<U> m_eff(T_half(), N_jkf());
  jkf::matrix<U> a_eff(T_half(), N_jkf());

  std::vector<U> y;
  std::pair<std::vector<U>, std::vector<U>> pa;
  for (int k = 0; k < N_jkf(); ++k) {
    pa = Lattice::effective::effective_no_back(((*this).JKF).col(k));

    m_eff.set_col(Functions::concatenate<U>({std::get<0>(pa), {0.0}}), k);
    a_eff.set_col(Functions::concatenate<U>({std::get<1>(pa), {0.0}}), k);
  }

  const std::string dest = dest_fld + NAME + ".dat";
  this->set_and_print_effective(m_eff, a_eff, "Backward signal neglected.",
                                dest, prec);
} // effective_no_back()

template <class U>
void free<U>::effective_back_Gattringer(const std::string &dest_fld,
                                        const U &dx, const U &dy,
                                        const U &M_max, const int &prec) {
  this->check_init();
  this->check_T_REV();

  jkf::matrix<U> m_eff(T_half(), N_jkf()), a_eff(T_half(), N_jkf());

  std::vector<U> y;
  std::pair<std::vector<U>, std::vector<U>> pa;
  for (int k = 0; k < N_jkf(); ++k) {
    pa = Lattice::effective::effective_back_Gattringer(
        this->get_JKF().col(k), dx, dy, this->get_T_REV(), M_max);

    m_eff.set_col(pa.first, k);
    a_eff.set_col(pa.second, k);
  }

  const std::string dest = dest_fld + NAME + ".dat";
  this->set_and_print_effective(m_eff, a_eff,
                                "Gattringer formula. Backward signal included.",
                                dest, prec);
} // effective_back_Gattringer()

template <class U>
void free<U>::effective_back_II_der(const std::string &dest_fld,
                                    const int &prec) {
  this->check_init();
  this->check_T_REV();
  int p = get_T_REV();

  jkf::matrix<U> m_eff(T_half(), N_jkf());
  jkf::matrix<U> a_eff(T_half(), N_jkf());

  std::vector<U> y;
  std::pair<std::vector<U>, std::vector<U>> pa;
  for (int k = 0; k < N_jkf(); ++k) {
    pa = Lattice::effective::effective_back_II_der(JKF.col(k), p);

    m_eff.set_col(std::get<0>(pa), k);
    a_eff.set_col(std::get<1>(pa), k);
  }

  const std::string dest = dest_fld + NAME + ".dat";
  this->set_and_print_effective(
      m_eff, a_eff, "Second derivative formula. Backward signal included.",
      dest, prec);
} // effective_II_der()

template <class U> void free<U>::check_effective() const {
  this->check_init();
  if (M_EFF.size() == 0 || A_EFF.size() == 0) {
    std::cerr << "Effective mass and amplitudes haven't been calculated yet.\n";
    std::cerr << "Aborting."
              << "\n";
    abort();
  }
} // check effective()

/* ------------------------------- */
/* Extraction of the leading state */
/* ------------------------------- */

template <class U>
void free<U>::check_t_min_t_max(const int &t_min, const int &t_max,
                                const int &T_ext) const {
  assert(0 < t_min && t_min < t_max && t_max <= T_ext &&
         "Invalid times passed to the fit.");
  if (t_min == t_max) {
    std::cerr << "Error : The plateau must consist of at least 2 points."
              << "\n";
    std::cerr << "Aborting."
              << "\n";
    abort();
  }

} // check_t_min_t_max()

template <class U>
void free<U>::append_plateau_info(const std::string &dest, const int &t_min,
                                  const int &t_max) const {
  /*! print out the information about the plateau */
  Functions::append_string("#plateau:\n", dest);
  Functions::append_string("t_min = " + std::to_string(t_min) + "\n", dest);
  Functions::append_string("t_max = " + std::to_string(t_max) + "\n", dest);
  Functions::append_string("T/2 = " + std::to_string(this->T_half()) + "\n",
                           dest);
} // append_plateau_info()

template <class U> void free<U>::print_yes_no_back(const bool &back_sig) const {
  if (back_sig) {
    this->check_T_REV();
    std::clog << "(YES ";
  } else {
    std::clog << "(NO ";
  }
  std::clog << " backward signal).\n";
} // print_yes_no_back()

template <class U>
void free<U>::single_exp_fit(const std::string &dest_fld, const int &t_min,
                             const int &t_max, const bool &back_sig,
                             const int &prec) const {

  std::clog << "Single exponential fit ";
  this->print_yes_no_back(back_sig);
  this->check_effective();

  int t1 = t_min, t2 = t_max;

  check_t_min_t_max(t1, t2, this->T_half());
  std::clog << "Plateau : " << t1 << " " << t2
            << " .\t T_half : " << this->T_half() << "\n";

  int n_pts = t2 - t1 + 1; /* t2 is included */
  std::vector<U> x(n_pts);
  std::iota(x.begin(), x.end(), t1);

  std::function<U(const U &, const std::vector<U> &)> ansatz =
      [&](const U &time, const std::vector<U> &p) {
        U M = p[0];
        U A = p[1];
        U rev = (U)this->get_T_REV(); /* sign under time reversal */
        U T_ext = (U)this->T();       /* T */

        if (back_sig) {
          return Functions::leading_exp_back(time, A, M, rev, T_ext);
        } else {
          return Functions::leading_exp(time, A, M);
        }
      };

  /*! As a guess it's taken the average of the values in the chosen plateau */
  std::vector<U> guesses = {
      Functions::average_range(((*this).M_EFF).avrs(), t1, t2),
      Functions::average_range(((*this).A_EFF).avrs(), t1, t2)};

  jkf::matrix<U> Y = ((*this).JKF).block(t1 - 1, 0, n_pts, this->N_jkf());

  /*! Doing the fit */

  jkf::Minuit2_fit::fit_xyey<U> minu;

  minu.set_pars({"M", "A"}, guesses, {0.0, 0.0},
                {0.0, 0.0}); /*!< parameters info */
  minu.set_data(x, Y);       /*1 data to be fitted */

  minu.set_ansatz(ansatz); /*! fit ansatz */

  minu.fit(); /*!< fitting */
  minu.store_results();

  /*! Printing the results */
  std::string dest_file =
      dest_fld + this->get_NAME() + ".dat";           /*!< output file */
  minu.print_fit_results(dest_file);                  /*! parameters and data */
  this->append_plateau_info(dest_file, t_min, t_max); /*!< Plateau info */
} // end single_exp_fit()

template <class U>
void free<U>::n_exp_fit(const int &n, const std::string &dest_fld,
                        const int &t_min, const int &t_max,
                        const int &prec) const {

  std::clog << n << " exponential fit ";
  const bool back_sig = (this->get_T_REV() != 0);
  this->print_yes_no_back(back_sig);
  this->check_effective();

  int t1 = t_min, t2 = t_max;
  check_t_min_t_max(t1, t2, this->T_half());
  std::clog << "Plateau : " << t1 << " " << t2
            << " .\t T_half : " << this->T_half() << "\n";

  int n_pts = t2 - t1 + 1; /* t2 is included */
  std::vector<U> x(n_pts);
  std::iota(x.begin(), x.end(), t1);

  std::function<U(const U &, const std::vector<U> &)> ansatz =
      [&](const U &time, const std::vector<U> &p) {
        const U rev = (U)this->get_T_REV(); /* sign under time reversal */
        const U T_ext = (U)this->T();       /* T */
        U s = 0.0;
        for (int i = 0; i < n; ++i) {
          U Ai = p[2 * i], Ei = p[2 * i + 1];
          s += Functions::leading_exp_back(time, Ai, Ei, rev, T_ext);
        }
        return s;
      };
  /*! As a guess it's taken the average of the values in the chosen plateau */

  U E0_guess = Functions::average_range(M_EFF.avrs(), t1, t2);
  U A0_guess = Functions::average_range(A_EFF.avrs(), t1, t2);
  std::vector<U> guesses = {A0_guess, E0_guess};
  std::vector<std::string> par_names = {"A0", "E0"};
  for (int i = 1; i < n; ++i) {
    guesses.push_back(0.9 * guesses[2 * i - 2]); // 90% previous amplitude
    guesses.push_back(1.1 * guesses[2 * i - 1]); // 110% previous energy
    par_names.push_back("A" + std::to_string(i));
    par_names.push_back("E" + std::to_string(i));
  }

  const jkf::matrix<U> Y = ((*this).JKF).block(t1 - 1, 0, n_pts, this->N_jkf());

  /*! Doing the fit */

  jkf::Minuit2_fit::fit_xyey<U> minu;

  minu.set_pars(par_names, guesses); /*!< parameters info */
  minu.set_data(x, Y);               /*1 data to be fitted */

  minu.set_ansatz(ansatz); /*! fit ansatz */

  minu.fit(); /*!< fitting */
  minu.store_results();

  /*! Printing the results */
  std::string dest_file =
      dest_fld + this->get_NAME() + ".dat";           /*!< output file */
  minu.print_fit_results(dest_file);                  /*! parameters and data */
  this->append_plateau_info(dest_file, t_min, t_max); /*!< Plateau info */
} // end single_exp_fit()

template <class U>
void free<U>::fit_effective(const std::string &dest_fld, const int &t_min,
                            const int &t_max, const int &prec_output) const {
  std::clog << "Fitting the effective mass and amplitude curves to a constant."
            << "\n";
  this->check_effective();

  /*!
  NOTE: The effective mass isn't defined for the last point,
  that's why the check is done up to 'T/2-1' and not 'T/2'
  */
  this->check_t_min_t_max(t_min, t_max, this->T_half() - 1);

  int n_pts = t_max - t_min + 1; /* t_max is included */
  std::vector<U> x(n_pts);
  iota(x.begin(), x.end(), t_min);

  std::function<U(const U &, const std::vector<U> &)> ansatz =
      [&](const U &time, const std::vector<U> &p) { return p[0]; };

  /*! Fit of the effective curve of the variable named 'par_name' */
  auto fit_eff_curve = [&](const jkf::matrix<U> &eff_curve,
                           const std::string &par_name) {
    /*! As a guess it's taken the average of the values in the chosen plateau */
    std::vector<U> guess = {
        Functions::average_range(eff_curve.avrs(), t_min, t_max)};

    jkf::matrix<U> Y = eff_curve.block(t_min - 1, 0, n_pts, this->N_jkf());

    std::string dest_file =
        dest_fld + this->get_NAME() + "_" + par_name + ".dat";
    jkf::Minuit2_fit::fit_xyey<U> minu;

    minu.set_pars({par_name}, guess, {0.0}, {0.0});
    minu.set_data(x, Y);

    minu.set_ansatz(ansatz);

    minu.fit();
    minu.store_results();

    /*! Printing the results */

    minu.print_fit_results(dest_file, "\t",
                           prec_output); /*! parameters and data */
    this->append_plateau_info(dest_file, t_min, t_max); /*!< Plateau info */
  };

  fit_eff_curve((*this).M_EFF, "M"); /*!< fitting the effective mass */
  fit_eff_curve((*this).A_EFF, "A"); /*!< fitting the effective amplitude */

  std::clog
      << "Fit of the effective mass and amplitude curves to a constant done."
      << "\n";

} // end fit_effective()

/*! ---------- */
/*! ODE method */
/*! ---------- */

template <class U>
void free<U>::ODE_no_back(const std::string &dest, const int &n_states,
                          const int &n_prec, const U &M_max, const U &dx,
                          const U &dy, const bool &verbose,
                          const int &prec_out) {
  ODE::jkf_no_back<U> ode(this->get_JKF(), n_states, n_prec);
  std::clog << "Initializing the ODE analysis of the correlator.";
  std::clog << " No particular time symmetry is assumed."
            << "\n";

  jkf::results<U> ODE_results;
  ODE_results.set_title(
      "ODE method (no backward signals) results for the correlator " +
      this->get_NAME());

  std::clog << "Finding the forward propagating signals.\n";
  jkf::matrix<U> M_fwd = ode.get_M_forward(M_max, dx, dy, true);
  jkf::matrix<U> A_fwd = ode.get_A_forward(M_max, dx, dy, true);
  ODE_results.add_jkf(M_fwd, "M_forward");
  ODE_results.add_jkf(A_fwd, "A_forward");

  std::clog << "Finding the backward propagating signals.\n";
  jkf::matrix<U> M_bkd = ode.get_M_backward(M_max, dx, dy, true);
  jkf::matrix<U> A_bkd = ode.get_A_backward(M_max, dx, dy, true);
  ODE_results.add_jkf(M_bkd, "M_backward");
  ODE_results.add_jkf(A_bkd, "A_backward");

  std::clog << "Printing out.\n";
  ODE_results.print_summary(dest + (*this).NAME + "_signals.dat", "\t",
                            prec_out);

  std::clog << "ODE analysis of the correlator done."
            << "\n";
} // end ODE_no_back

template <class U>
void free<U>::ODE_back(const std::string &dest, const int &n_states,
                       const int &n_prec, const U &M_min, const U &M_max,
                       const U &dx, const U &dy, const int &prec_out) {
  this->check_T_REV();

  ODE::jkf_back<U> ode(this->get_JKF(), this->get_T_REV(), n_states, n_prec);
  std::clog << "Initializing the ODE analysis of the correlator."
            << "\n";

  jkf::results<U> ODE_results;
  ODE_results.set_title(
      "ODE method (backward signals included) results for the correlator " +
      this->get_NAME());

  std::clog << "Finding the masses.\n";
  jkf::matrix<U> masses = ode.get_masses(M_max, dx, dy);
  ODE_results.add_jkf(masses, "masses");

  std::clog << "Finding the amplitudes.\n";
  jkf::matrix<U> amplitudes = ode.get_amplitudes(M_min, M_max, dx, dy);
  ODE_results.add_jkf(amplitudes, "amplitudes");

  std::clog << "Printing out.\n";
  ODE_results.print_summary(dest + this->get_NAME() + "_signals.dat", "\t",
                            prec_out);

  std::clog << "ODE analysis of the correlator with backward signal done."
            << "\n";
} // end ODE_back()

/* ----------- */
/* dual+PP fit */
/* ----------- */

template <typename U> void free<U>::dual_PP_fit(const std::string &path) {
  using namespace Functions;

  int t_min = get<int>("t_min", path), t_max = get<int>("t_max", path);
  int N_states = get<int>("N_states", path);
  U MP = get<U>("MP", path);
  int L = get<int>("L", path);
  U q_quark = get<U>("q_quark", path);
  U cff = get<U>("coeff_form_factor", path);

  std::vector<U> guess = get_vector<U>("guess", path);
  std::vector<U> boundL = get_vector<U>("boundL", path);
  std::vector<U> boundR = get_vector<U>("boundR", path);

  std::string destination = get<std::string>("destination", path);
  int prec_output = get<int>("prec_output", path);
  int N_iter = get<int>("N_iter", path);
  U step_size = get<U>("step_size", path);
  int n_seek = get<int>("n_seek", path);
  int print_lv = get<int>("print_lv", path);

  std::cout << "Doing the fit for : (V_dual(t) + V_PP(t))(1 + D*a^2) \n";

  int n_pars = guess.size();
  assert(n_pars == 4 && "This fit requires 4 parameters.");
  this->check_init();
  this->check_T_REV();
  this->check_t_min_t_max(t_min, t_max, this->T_half());

  int n_pts = t_max - t_min;
  std::vector<U> x = range<U>(t_min, t_max);

  std::vector<U> all_e_y = (this->get_JKF()).errs(); /* all errors */

  // jkf::matrix<U> energy(N_states, N_jkf());
  std::vector<std::string> names = {"R_dual", "E_dual", "MV/MP", "g"};

  /* doing the fit */
  int Nj = this->N_jkf();
  jkf::matrix<U> Y = (this->get_JKF()).block(t_min - 1, 0, n_pts, Nj);

  std::function<U(const std::vector<U> &, const std::vector<U> &,
                  const std::vector<U> &)>
      CH2_RED_single_jkf = [&](const std::vector<U> &y_k,
                               const std::vector<U> &ey_k,
                               const std::vector<U> &p) {
        U R_dual = p[0], E_dual = p[1], rVP = p[2], g = p[3];
        U MV = rVP * MP;

        std::vector<U> k_Luscher =
            Luscher::k_Luscher_roots(N_states, (U)L, MP, MV, g, cff);

        U V_value = 0.0, residue = 0.0; /* dummy variables */
        for (int t = 0; t < n_pts; t++) {
          V_value += Luscher::V_dual(x[t], R_dual, MV, E_dual,
                                     q_quark); /* V_dual(t) */
          V_value +=
              Luscher::V_PP(x[t], k_Luscher, L, MP, rVP, g, cff); /* V_PP(t) */
          residue += pow((y_k[t] - V_value) / ey_k[t], 2.0);
          V_value = 0.0;
        }

        /*! check the number of degrees of freedom */
        int n_par = p.size();
        U n_deg = n_pts - n_par;
        if (n_deg == 0) {
          std::cerr << "0 degrees of freedom for the fit!\n";
          abort();
        }
        return (residue / n_deg); /*!< return reduced chi squared */
      };

  jkf::Minuit2_fit::fit_xyey<U> minu;

  minu.set_pars(names, guess, boundL, boundR);
  minu.set_data(x, Y);

  minu.set_ch2_red_fun_single_jkf(CH2_RED_single_jkf);

  minu.fit();
  minu.store_results();

  /*! Printing the results */

  minu.print_fit_results(destination); /*! parameters and data */
  this->append_plateau_info(destination, t_min, t_max); /*!< Plateau info */

  std::cout << "Fit completed."
            << "\n";
} // end dual_PP_fit()

#endif
} // namespace correlators
} // namespace Lattice
} // namespace Silibs2
