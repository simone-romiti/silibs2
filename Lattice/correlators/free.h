// free.h
/*!
Declaration of a class free : a standard analysis tool
for the jackknifes values of lattice correlators.

The rows (t) are the times (in lattice units), while columns (k) are the
jackknifes. Indexes start from 0, as usual, but the correspond to k = 1, ...,
N_jkf t = 1, ..., T/2 (T is the temporal extension of the ensemble)

*/

#include <iostream>

#include "../../jkf/Minuit2_fit/fit.h"
#include "../../jkf/matrix.h"
#include "../Luscher/dual_PP.h"
#include "../ODE/ODE.h"
#include "../effective/effective.h"

namespace Silibs2 {
namespace Lattice {
namespace correlators {
#ifndef Silibs2_Lattice_correlators_free_h
#define Silibs2_Lattice_correlators_free_h

template <class U = double> class free {

private:
  std::string NAME = "unnamed"; /*!< Name of the correlator */
  Silibs2::jkf::matrix<U> JKF;  /*!< jackknifes: [t=time][k=jackknife] */
  int T_REV = 0; /* sign of the correlator under time reversal (+1 or -1) */

  Silibs2::jkf::matrix<U> M_EFF; /* jackknifes: [k=jackknife][t=time] */
  Silibs2::jkf::matrix<U> A_EFF; /* jackknifes: [k=jackknife][t=time] */

public:
  free();
  ~free();

  /*!
  Initialize the object with a name and retrieving data from a file
  T_REV is left unspecified
  */
  free(const std::string &name, const std::string &path_data,
       const std::string &head = "");

  /*! Initialize the object with a name and retrieving data from a file */
  free(const std::string &name, const std::string &path_data, const int &t_rev,
       const std::string &head = "");

  /*!
  Initialize the object with a name the jkf::matrix
  T_REV is left unspecified
  */
  free(const std::string &name, const Silibs2::jkf::matrix<U> &jkf);

  /*! Initialize the object with a name the jkf::matrix */
  free(const std::string &name, const Silibs2::jkf::matrix<U> &jkf_matrix,
       const int &t_rev);

  std::string get_NAME() const;

  Silibs2::jkf::matrix<U> get_JKF() const;
  int N_jkf() const;     /*!< number of jackknifes (rows of 'jkf')*/
  int T_half() const;    /*!< number of time value (columns of 'jkf')*/
  int T() const;         /*!< 2*T_half */
  int get_T_REV() const; /*!< sign under time_reversal */

  void check_init() const;  /*!< Check if matrix 'jkf' is still empty */
  void check_T_REV() const; /*!< check if T_REV has been specified */

  /*! Print out the jkf with their average and error */
  void print_jkf_ae(const std::string &dest, const int &prec = 16) const;

  void set_and_print_effective(const Silibs2::jkf::matrix<U> &m_eff,
                               const Silibs2::jkf::matrix<U> &a_eff,
                               const std::string &info_method,
                               const std::string &dest, const int &prec = 16);

  /*! Effective mass and amplitude (the backward signal is neglected) */
  void effective_no_back(const std::string &dest_fld, const int &prec = 16);

  /*! Effective mass and amplitude with the Gattringer formula */
  void effective_back_Gattringer(const std::string &dest_fld,
                                 const U &dx = 1e-4, const U &dy = 1e-4,
                                 const U &M_max = 1.0, const int &prec = 16);

  /*!
  Evaluates the effective mass and amplitude with the method of the II
  derivative (the backward signal is taken into account)
  */
  void effective_back_II_der(const std::string &dest_fld, const int &prec = 16);

  /*! Check if effective mass and amplitude have been calculated */
  void check_effective() const;

  /* ------------------------------- */
  /* Extraction of the leading state */
  /* ------------------------------- */

  /*! Check if the choices of t_min and t_max are valid */
  void check_t_min_t_max(const int &t_min, const int &t_max,
                         const int &T_ext) const;

  /*! append informations abount the plateau to the file 'dest' */
  void append_plateau_info(const std::string &dest, const int &t_min,
                           const int &t_max) const;

  // prints (YES/NO backward signal) on std::clog
  void print_yes_no_back(const bool &back_sig) const;

  /*!
  fit with the hypotesis
  [ A*exp(-M*t) + (if back_sig==true) T_REV*A*exp(-M(T-t)) ]
  from t_min (included) to t_max(excluded).
  NOTE: Since the value of the correlator t=0 is not present,
  t_min must be at least 1.
  */
  void single_exp_fit(const std::string &dest_fld, const int &t_min = 0,
                      const int &t_max = 0, const bool &back_sig = true,
                      const int &prec_output = 16) const;

  /*!
  n-exp fit with the ansatz:
  \sum_i [ Ai*exp(-Ei*t) + (if back_sig==true) T_REV*A*exp(-M(T-t)) ]
  from t_min (included) to t_max(excluded).
  NOTE: Since the value of the correlator t=0 is not present,
  t_min must be at least 1.
  */
  void n_exp_fit(const int &n, const std::string &dest_fld,
                 const int &t_min = 0, const int &t_max = 0,
                 const int &prec_output = 16) const;

  /* fit the effective mass and amplitude to a constant */
  void fit_effective(const std::string &dest_fld, const int &t_min = 0,
                     const int &t_max = 0, const int &prec_output = 16) const;

  /*!
  Find masses and amplitudes with the ODE method
  without assuming any particular time symmetry
  */
  void ODE_no_back(const std::string &dest, const int &n_states,
                   const int &n_prec = 32, const U &M_max = 1.0,
                   const U &dx = 1e-4, const U &dy = 1e-10,
                   const bool &verbose = true, const int &prec_output = 16);
  /*!
  Find masses and amplitudes with the ODE method
  for a correlator with definite time parity (i.e. acconting for the backward
  signal)
  */
  void ODE_back(const std::string &dest, const int &n_states,
                const int &n_prec = 32, const U &M_min = -1.0,
                const U &M_max = 1.0, const U &dx = 1e-4, const U &dy = 1e-10,
                const int &prec_output = 16);

  /* fit considering the correlator as V_dual + V_PP */
  /* (for the structure of the input file see ./template/ )*/
  void dual_PP_fit(const std::string &path);

  // /* fit considering the correlator as (V_dual + V_PP)*(1 + D*a) */
  // void dual_PP_discr(const std::string &path);

}; // class free

#endif
} // namespace correlators
} // namespace Lattice
} // namespace Silibs2
