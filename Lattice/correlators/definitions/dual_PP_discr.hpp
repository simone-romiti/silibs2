// dual_PP_discr.hpp
/* This file defines the method jkf_analysis::dual_PP_discr(). */

template<typename U> void jkf_analysis<U>::dual_PP_discr(const string& path)
{
	int t_min = get<int>("t_min", path), t_max = get<int>("t_max", path);
	int N_states = get<int>("N_states", path); U MP = get<U>("MP", path); int L = get<int>("L", path);
	U q_quark = get<U>("q_quark", path); U cff = get<U>("coeff_form_factor", path);
	
	vector<U> guess = get_vector<U>("guess", path);
	vector<U> boundL = get_vector<U>("boundL", path);
	vector<U> boundR = get_vector<U>("boundR", path);
	
	string destination = get<string>("destination", path); int prec_output = get<int>("prec_output", path);
	int N_iter = get<int>("N_iter", path);	U step_size = get<U>("step_size", path); 
	int n_seek = get<int>("n_seek", path);	int print_lv = get<int>("print_lv", path);
	bool use_Migrad = get<bool>("use_Migrad", path);
	
	cout<<"Doing the fit for : V_dual(t) + V_PP(t)*(1 + D) \n";
	
	int n_pars = guess.size(); assert(n_pars==5 && "This fit requires 5 parameters.");	
	check_init(); 
	check_t_rev();
	check_t_min_t_max(t_min, t_max, this->T_half());
	
	int n_pts = t_max - t_min; vector<U> x = range<U>(t_min, t_max);
	
	vector<U> all_e_y = jkf.errs(); /* all errors */
	
	/* (R_dual, E_dual, MV, rVP, g, D) obtaind from the fit */
	Silibs::Matrix<U> PARS(n_pars, N_jkf());
	Silibs::Matrix<U> ch2(1, N_jkf());
	Silibs::Matrix<U> energy(N_states, N_jkf());
	vector<string> names = {"R_dual", "E_dual", "MV/MP", "g", "D"};
	
	/* doing the fit */
	for(int k=0; k<N_jkf(); k++)
	{
		std::cout << "Jackknife number "<< k+1 << "\n\n";
		
		vector<U> all_y = jkf.col(k);
		vector<U> y(all_y.begin() + t_min - 1, all_y.begin() + t_max - 1);		
		vector<U> e_y(all_e_y.begin() + t_min - 1, all_e_y.begin() + t_max - 1);
		
		function<void(int&, double*, double&, double*, int)> ch2_reduced = 
		[&](int &npar, double *fuf, double &ch2_reduced, double *p, int flag)
		{
			double R_dual=p[0], E_dual=p[1], rVP=p[2], g=p[3], D=p[4];
			double MV = rVP*MP;
			
			vector<U> k_Luscher = Luscher::k_Luscher_roots(N_states, (double) L, MP, MV, g, cff);
			
			double V_value = 0.0, residue = 0.0; /* dummy variables */
			for (int t = 0; t < n_pts; t++) 
			{
				V_value += Luscher::V_dual(x[t], R_dual, MV, E_dual, q_quark); /* V_dual(t) */
				V_value += Luscher::V_PP(x[t], k_Luscher, (U) L, MP, rVP, g, cff)*(1 + D); /* V_PP(t) * (discretization effects) */
				residue += pow( (y[t] - V_value)/e_y[t], 2.0);
				V_value = 0.0;
			}
			
			int n_deg = n_pts - npar;
			if (n_deg==0){ std::cerr << "0 degrees of freedom for the fit!" << "\n"; }
			ch2_reduced = residue/n_deg;	
		};
		
		fit_Minuit2_1D mini;
		
		mini.set_ch2_red(ch2_reduced);
		mini.set_pars(guess, {}, names, boundL, boundR );
		mini.do_the_fit(N_iter, step_size, n_seek, print_lv, use_Migrad);
		
		/* best fit (BF) values */
		PARS.set_col(mini.get_best_fit(), k);
		
		U MV_BF = MP*PARS(2,k), g_BF = PARS(3,k);
		vector<U> k_Luscher = Luscher::k_Luscher_roots( N_states, (U) L, MP, MV_BF, g_BF, cff);
		for(int n = 0; n < N_states; n++){ energy(n, k) = Luscher::omega_PP(k_Luscher[n], MP); }
		ch2(0,k) = mini.get_ch2();
	}
	
	/* printing data */
	string out_base = destination + Name_corr + "_";
	
	/* summary of the results of the fit : Parameters and Luscher energies */
	string out_summary = destination + "summary/" + Name_corr + "_";
	std::string path_fit_results = out_summary + "fit_results.dat"; 
	Silibs::touch(path_fit_results); ofstream FIT_results(path_fit_results);
	FIT_results.precision(prec_output); FIT_results.setf(ios_base::scientific);
	
	FIT_results<<"Results of the fit for "<<Name_corr<<": V(t) = V_dual(t) + V_PP(t)*(1 + D)\n\n";
	FIT_results<<"L\t"<<L<<"\n"<<"MP\t"<<MP<<"\n"<<"N_states\t"<<N_states<<"\n\n"
	<<"step size\t"<<step_size<<"\n"<<"n_seek\t"<<n_seek<<"\n\n";
	
	Silibs::Matrix<U> res_tab(n_pars, 5);
	res_tab.set_col(PARS.avrs(),1);res_tab.set_col(PARS.errs(),2);res_tab.set_col(boundL,3);res_tab.set_col(boundR,4);
	Silibs::Matrix<std::string> res_tab_str = Silibs::cast_matrix<std::string>(res_tab); res_tab_str.set_col(names, 0);
	
	res_tab_str.append(FIT_results, "\t", "\nParameter\tBest fit\terror\tleft bound\tright bound\n");
	ch2.avr_and_err().append(FIT_results, "\t", "\nresidue\terr_residue");
	
	PARS.print_all_auto(out_base + "PARS.dat", "\t", prec_output);
	ch2.print_all_auto( out_base + "ch2.dat" , "\t", prec_output);
	
	/* printing the energies */
	energy.print_all_auto(out_base +  + "E_n.dat", "\t", prec_output);
	Silibs::Matrix<U> E_n_ae = energy.avr_and_err();
	
	FIT_results<<"\nEnergies (lattice units):\n\n"<<"n\t"<<"E_n\t"<<"error\n\n";
	for (int r0 = 0; r0 < N_states; r0++){ FIT_results<<r0+1<<"\t"<<E_n_ae(r0, 0)<<"\t"<<E_n_ae(r0, 1)<<"\n"; }	
	
	FIT_results	<<"\nRange for the fit :\n";
	FIT_results	<<"t_min"<<"\t"<<"t_max"<<"\n";
	FIT_results	<<t_min<<"\t"<<t_max<<"\n\n";
	
	FIT_results	<<"\nValues (lattice units) :\n\n"
	<<"t\t"<<"V(t)\t"<<"err_V(t)\t"<<"(dual + PP*(1+D))\t"<<"dual\t"<<"PP\n";
	std::vector<U> VV = jkf.avrs(), err_VV = jkf.errs();
	for (int t = 0; t < this->T_half(); t++)
	{
		U Rd = PARS.avrs()[0], Ed = PARS.avrs()[1], 
		rVP = PARS.avrs()[2], MV = rVP*MP, Gvpp = PARS.avrs()[3],
		Discr = PARS.avrs()[4];
		
		vector<U> KL = Luscher::k_Luscher_roots(N_states, (U) L, MP, MV, Gvpp, cff);
		U Vd  = Luscher::V_dual((U) t+1, Rd, MV, Ed, q_quark);
		U Vpp = Luscher::V_PP((U) t+1, KL, (U) L, MP, rVP, Gvpp, cff);
		FIT_results<<(t+1)<<"\t"<<VV[t]<<"\t"<<err_VV[t]<<"\t"<<Vd + Vpp*(1+Discr)<<"\t"<<Vd<<"\t"<<Vpp<<"\n";
	}
	FIT_results.close();
	std::cout<<"Fit completed."<<endl;	
}// end dual_PP_discr()

