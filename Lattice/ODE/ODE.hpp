// ODE.hpp
/* inclusion of all implemented ODE algorithms */

#include "./jkf_back.hpp"    /* ODE algorithm (yes backward signal) */
#include "./jkf_no_back.hpp" /* ODE algorithm (no backward signal) */
