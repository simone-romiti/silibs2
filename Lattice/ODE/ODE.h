// ODE.h
/* inclusion of all implemented ODE algorithms */

#include "./jkf_back.h"    /* ODE algorithm (yes backward signal) */
#include "./jkf_no_back.h" /* ODE algorithm (no backward signal) */
