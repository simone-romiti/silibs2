// jkf_no_back.h
/*!
This header declares a class jkf_no_back,
which is the tool to apply the ODE algorithm described in the following paper:
PHYSICAL REVIEW D 100, 054515 (2019)
*/

#include <cassert>
#include <complex>
#include <functional>
#include <random>
#include <string>

#include "./method.h"

namespace Silibs2 {
namespace ODE {
#ifndef Silibs2_ODE_jkf_no_back_h
#define Silibs2_ODE_jkf_no_back_h

template <class U> class jkf_no_back {

protected:
  int L = 0;

  int N_states = 0; /*! number of states */
  int N_prec = 32;  /*! numerical precision in the inversion of mass and
                       amplitude  matrix */

  jkf::matrix<U> C_jkf; /*! correlator [t][jkf]. t=1,...N */

public:
  jkf_no_back();
  ~jkf_no_back();

  /*! n_states = number of states, corr = jackknifes of correlator (time, jkf)*/
  jkf_no_back<U>(const jkf::matrix<U> &corr, const int &n_states,
                 const int &n_prec = 32);

  int T_half() const; /*! returns the total number of times */
  int T() const;      /*! returns the total number of times */
  int N_jkf() const;  /*! returns the total number of times */

  /*! finds the jkf of coefficients x_k of eq. (22) */
  jkf::matrix<U> get_xk() const;

  /*! returns the number of physical states found */
  int N_phys(const U &M_max = 1.0, const U &dx = 1e-4,
             const U &dy = 1e-10) const;

  /*! return jkf of the z_m of equation (6) found with this->get_M_tilde() */
  jkf::matrix<U> get_zm(const U &M_max = 1.0, const U &dx = 1e-4,
                        const U &dy = 1e-10, const bool &verbose = false) const;

  /*!
  Finds the masses from the roots z_m_tilde of eq. (6)
  Mass_max is the maximum positive M_tilde */
  jkf::matrix<U> get_Mm(const U &M_max = 1.0, const U &dx = 1e-4,
                        const U &dy = 1e-10, const bool &verbose = false) const;

  /*! finds jkf of the coefficients A_m_tilde of eq. (28) */
  jkf::matrix<U> get_Am(const U &M_max = 1.0, const U &dx = 1e-4,
                        const U &dy = 1e-10, const bool &verbose = false) const;

  /*!
  returns the jkf of the masses of the forward signals
  according to eq. (3) and (4)
  */
  jkf::matrix<U> get_M_forward(const U &M_max = 1.0, const U &dx = 1e-4,
                               const U &dy = 1e-10,
                               const bool &verbose = false) const;

  /*!
  returns the jkf of the masses of the backward signals
  according to eq. (3) and (4)
  */
  jkf::matrix<U> get_M_backward(const U &M_max = 1.0, const U &dx = 1e-4,
                                const U &dy = 1e-10,
                                const bool &verbose = false) const;

  /*! jkf of the forward amplitudes */
  jkf::matrix<U> get_A_forward(const U &M_max = 1.0, const U &dx = 1e-4,
                               const U &dy = 1e-10,
                               const bool &verbose = false) const;

  /*! jkf of the backward amplitudes */
  jkf::matrix<U> get_A_backward(const U &M_max = 1.0, const U &dx = 1e-4,
                                const U &dy = 1e-10,
                                const bool &verbose = false) const;
};

#endif
} // namespace ODE
} // namespace Silibs2
