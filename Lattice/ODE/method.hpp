// method.hpp
/* This file defines what is declared in method.h */

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/multiprecision/gmp.hpp>

#include "../../Functions/functions.hpp"
#include "../../LA/matrix.hpp"
#include "../../jkf/matrix.hpp"

#include "./method.h"

namespace Silibs2 {
namespace ODE {
#ifndef Silibs2_ODE_method_hpp
#define Silibs2_ODE_method_hpp

template <class U> method<U>::method() {}
template <class U> method<U>::~method() {}

template <class U>
method<U>::method(const int &n_states, const std::vector<U> &corr,
                  const std::vector<U> &sig) {
  setup(n_states, corr, sig);
} // method()

template <class U>
void method<U>::setup(const int &n_states, const std::vector<U> &corr,
                      const std::vector<U> &sig) {
  assert(corr.size() == sig.size() &&
         "The number of values and errors are not the same.\n");
  (*this).N_states = n_states;
  (*this).C = corr;
  (*this).sigma = sig;
} // setup()

template <class U> int method<U>::N() { return N_states; }
template <class U> int method<U>::T() { return (int)C.size(); }

template <class U> std::vector<U> method<U>::get_xk(const int &n_prec) {
  LA::matrix<U> M(N(), N());
  std::vector<U> V(N());

  for (int k1 = 0; k1 < N(); ++k1) {
    for (int k2 = 0; k2 < N(); ++k2) {
      std::vector<U> C_k1 = symm_der(k1, C);
      std::vector<U> C_k2 = symm_der(k2, C);
      M(k1, k2) = 0.0;
      for (int n = N(); n < T() - N(); ++n) {
        M(k1, k2) += (C_k1[n] * C_k2[n]) / pow(sigma[n], 2.0);
      }
    }
  }

  std::vector<U> C_N = symm_der(N(), C);
  for (int k1 = 0; k1 < N(); ++k1) {
    std::vector<U> C_k1 = symm_der(k1, C);
    V[k1] = 0.0;
    for (int n = N(); n < T() - N(); ++n) {
      V[k1] -= (C_k1[n] * C_N[n]) / pow(sigma[n], 2.0);
    }
  }

  std::vector<U> x = solve_with_precision(n_prec, M, V);
  x.push_back(1.0); /* x_N is set to 0 by convention (see eq. (11) ) */
  return x;
} // get_xk()

template <class U>
std::vector<U> method<U>::get_masses(const int &n_prec, const U &mL,
                                     const U &mR, const U &dx, const U &dy) {
  U z_min = -sinh(mR);
  U z_max = -sinh(mL);

  std::vector<U> x = this->get_xk(n_prec);
  std::function<U(const U &)> P = [&](const U &z) {
    U res = 0.0;
    for (int i = 0; i <= N(); ++i) {
      res += x[i] * pow(z, i);
    }
    return res;
  };

  std::vector<U> Z = roots_interval(P, z_min, z_max, dx, dy);

  std::vector<U> masses(0);
  for (U z_m : Z) {
    masses.push_back(asinh(-z_m));
  }

  if ((int)masses.size() < N()) {
    std::cerr << "Warning : The algorithm found less states than expected.\n";
  }

  return masses;
} // get_masses()

template <class U>
std::vector<U> method<U>::get_zm(const int &n_prec, const U &mL, const U &mR,
                                 const U &dx, const U &dy) {
  std::vector<U> masses_found = this->get_masses(n_prec, mL, mR, dx, dy);
  std::vector<U> z(0);
  for (U m : masses_found) {
    z.push_back(-sinh(m));
  }
  return z;
}

template <class U>
std::vector<U> method<U>::get_Am(const int &n_prec, const U &mL, const U &mR,
                                 const U &dx, const U &dy) {
  LA::matrix<U> A_matr(N(), N());
  std::vector<U> W(N());

  std::vector<U> z = this->get_zm(n_prec, mL, mR, dx, dy);
  std::vector<U> masses = this->get_masses(n_prec, mL, mR, dx, dy);

  for (int m1 = 0; m1 < N(); ++m1) {
    for (int m2 = 0; m2 < N(); ++m2) {
      A_matr(m1, m2) = 0.0;
      for (int k = 0; k <= N(); ++k) {
        for (int t = k; t < T() - k; ++t) {
          A_matr(m1, m2) += pow(z[m1] * z[m2], k) *
                            (exp(-(masses[m1] + masses[m2]) * (t + 1))) /
                            pow(sigma[t], 2.0);
        }
      }
    }
  }

  for (int m1 = 0; m1 < N(); ++m1) {
    W[m1] = 0.0;
    for (int k = 0; k <= N(); ++k) {
      std::vector<U> C_k = symm_der(k, C);
      for (int t = k; t < T() - k; ++t) {
        W[m1] += pow(z[m1], k) * (exp(-masses[m1] * (t + 1)) * C_k[t]) /
                 pow(sigma[t], 2.0);
      }
    }
  }

  std::vector<U> Am = solve_with_precision(n_prec, A_matr, W);

  return Am;
} // get_xk()

#endif
} // namespace ODE
} // namespace Silibs2
