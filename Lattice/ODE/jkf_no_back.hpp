// jkf_no_back.hpp
/* Definitions of jkf_no_back.h */

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/multiprecision/gmp.hpp>

#include "./method.hpp"

#include "./jkf_no_back.h"

namespace Silibs2 {
namespace ODE {
#ifndef Silibs2_ODE_jkf_no_back_hpp
#define Silibs2_ODE_jkf_no_back_hpp
using namespace Silibs2::Functions;

template <class U> jkf_no_back<U>::jkf_no_back() {}
template <class U> jkf_no_back<U>::~jkf_no_back() {}

template <class U>
jkf_no_back<U>::jkf_no_back(const jkf::matrix<U> &corr, const int &n_states,
                            const int &n_prec) {
  C_jkf = corr;
  N_states = n_states;
  N_prec = n_prec;
} // end ODE_jkf_back()

template <class U> int jkf_no_back<U>::T_half() const { return C_jkf.rows(); }
template <class U> int jkf_no_back<U>::T() const { return 2 * T_half(); }
template <class U> int jkf_no_back<U>::N_jkf() const { return C_jkf.cols(); }

template <class U> jkf::matrix<U> jkf_no_back<U>::get_xk() const {
  /* multiprecision vesions of M and V */
  using namespace boost::multiprecision;
  mpf_float::default_precision(N_prec);

  jkf::matrix<mpf_float> xk(N_states + 1, N_jkf());
  jkf::matrix<mpf_float> C_jkf_mp =
      Silibs2::Tables::change_type<mpf_float>(C_jkf);
  std::vector<mpf_float> sigma = C_jkf_mp.errs();

  for (int k = 0; k < N_jkf(); ++k) {
    std::vector<mpf_float> C = C_jkf_mp.col(k);
    LA::matrix<mpf_float> M(N_states, N_states);

    for (int k1 = 0; k1 < N_states; ++k1) {
      std::vector<mpf_float> C_k1 = symm_der(k1, C);
      for (int k2 = 0; k2 < N_states; ++k2) {
        std::vector<mpf_float> C_k2 = symm_der(k2, C);
        M(k1, k2) = 0.0;
        for (int t = N_states + 1; t <= T_half() - N_states; ++t) {
          M(k1, k2) += C_k1[t - 1] * C_k2[t - 1] / pow(sigma[t - 1], 2);
        }
      }
    }

    std::vector<mpf_float> V(N_states);
    std::vector<mpf_float> C_N = symm_der(N_states, C);
    for (int k1 = 0; k1 < N_states; ++k1) {
      std::vector<mpf_float> C_k1 = symm_der(k1, C);
      V[k1] = 0.0;
      for (int t = N_states + 1; t <= T_half() - N_states; ++t) {
        V[k1] -= C_k1[t - 1] * C_N[t - 1] / pow(sigma[t - 1], 2);
      }
    }

    std::vector<mpf_float> xk_single_jkf = LA::solve(M, V);
    xk_single_jkf.push_back(
        1.0); /* x_N is set to 1 by convention (see eq. (11) ) */

    xk.set_col(xk_single_jkf, k);
  }
  return change_type<U>(xk);
} // end get_xk()

template <class U>
int jkf_no_back<U>::N_phys(const U &M_max, const U &dx, const U &dy) const {
  std::vector<U> x = this->get_xk().avrs();
  std::function<U(const U &)> P = [&](const U &z) { return polynomial(z, x); };

  U M_min = (2.0 * M_PI) / (T() + 0.0); /* 2*\pi/T is the lowest frequency */
  U M_bound = std::max(M_min, M_max);

  U z_min = -sinh(M_bound);
  U z_max = +sinh(M_bound);

  std::vector<U> z_roots = roots_interval(P, z_min, z_max, dx, dy);
  int nf = z_roots.size();
  return nf;
} // end N_phys()

template <class U>
jkf::matrix<U> jkf_no_back<U>::get_zm(const U &M_max, const U &dx, const U &dy,
                                      const bool &verbose) const {
  jkf::matrix<U> xk = this->get_xk();
  jkf::matrix<U> zm(N_states, N_jkf());

  U M_min = (2.0 * M_PI) / (T() + 0.0); /* 2*\pi/T is the lowest frequency */
  U M_bound = std::max(M_min, M_max);

  U z_min = -sinh(M_bound);
  U z_max = +sinh(M_bound);

  bool less_states = false;
  for (int k = 0; k < N_jkf(); ++k) {
    std::vector<U> x = xk.col(k);
    std::function<U(const U &)> P = [&](const U &z) {
      return polynomial(z, x);
    };

    std::vector<U> z_roots = roots_interval(P, z_min, z_max, dx, dy);
    while ((int)z_roots.size() < N_states) {
      z_roots.push_back(0.0);
      less_states = true;
    }
    zm.set_col(z_roots, k);
  }

  if (verbose && less_states) {
    std::clog << "Warning : You found " << N_phys() << " states of " << N_states
              << " expected to be in the correlator."
              << "\n";
  }

  return zm;
} // end get_zm()

template <class U>
jkf::matrix<U> jkf_no_back<U>::get_Mm(const U &M_max, const U &dx, const U &dy,
                                      const bool &verbose) const {
  jkf::matrix<U> zm = this->get_zm(M_max, dx, dy, verbose);
  jkf::matrix<U> M_tilde(N_states, N_jkf());
  for (int n = 0; n < N_states; ++n) {
    for (int j = 0; j < N_jkf(); ++j) {
      M_tilde(n, j) = -asinh(zm(n, j));
    }
  }
  return M_tilde;
} // end get_M_tilde()

template <class U>
jkf::matrix<U> jkf_no_back<U>::get_Am(const U &M_max, const U &dx, const U &dy,
                                      const bool &verbose) const {
  using namespace std;

  using namespace boost::multiprecision;
  mpf_float::default_precision(N_prec);
  U tol = pow(10, -N_prec / 2); /* tolerance under with a number is zero */

  jkf::matrix<mpf_float> Am(N_states, N_jkf());

  jkf::matrix<mpf_float> zm = Silibs2::Tables::change_type<mpf_float>(
      this->get_zm(M_max, dx, dy, verbose));
  jkf::matrix<mpf_float> Mm = Silibs2::Tables::change_type<mpf_float>(
      this->get_Mm(M_max, dx, dy, verbose));

  for (int j = 0; j < N_jkf(); ++j) {
    std::vector<mpf_float> C =
        Silibs2::Tables::change_type<mpf_float>(C_jkf).col(j);

    LA::matrix<mpf_float> A_matr(N_states, N_states);
    std::vector<mpf_float> W(N_states);

    std::vector<mpf_float> z = zm.col(j);
    std::vector<mpf_float> masses = Mm.col(j);

    jkf::matrix<mpf_float> C_jkf_mp =
        Silibs2::Tables::change_type<mpf_float>(C_jkf);

    for (int m1 = 0; m1 < N_states; ++m1) {
      for (int m2 = 0; m2 < N_states; ++m2) {
        A_matr(m1, m2) = 0.0;
        for (int k = 0; k <= N_states; ++k) {
          jkf::matrix<mpf_float> matr_C_k = jkf::symm_der(k, C_jkf_mp);
          std::vector<mpf_float> C_k = matr_C_k.col(j);
          std::vector<mpf_float> sigma = matr_C_k.errs();
          for (int t = k + 1; t <= T_half() - k; ++t) {
            U num =
                pow(z[m1] * z[m2], k) * (exp(-(masses[m1] + masses[m2]) * t));
            U den = pow(sigma[t - 1], 2);
            if (fabs(num) >= tol && fabs(den) >= tol) {
              A_matr(m1, m2) += num / den;
            }
          }
        }
      }
    }

    for (int m = 0; m < N_states; ++m) {
      W[m] = 0.0;
      for (int k = 0; k <= N_states; ++k) {
        jkf::matrix<mpf_float> matr_C_k = symm_der(k, C_jkf_mp);
        std::vector<mpf_float> C_k = matr_C_k.col(j);
        std::vector<mpf_float> sigma = matr_C_k.errs();
        for (int t = k + 1; t <= T_half() - k; ++t) {
          U num = pow(z[m], k) * exp(-masses[m] * t) * C_k[t - 1];
          U den = pow(sigma[t - 1], 2);
          if (fabs(num) >= tol && fabs(den) >= tol) {
            W[m] += num / den;
          }
        }
      }
    }

    std::vector<mpf_float> Am_single_jkf = LA::solve(A_matr, W);
    Am.set_col(Am_single_jkf, j);
  }

  return change_type<U>(Am);
} // end get_Am_tilde()

template <class U>
jkf::matrix<U> jkf_no_back<U>::get_M_forward(const U &M_max, const U &dx,
                                             const U &dy,
                                             const bool &verbose) const {
  jkf::matrix<U> M0 = this->get_Mm(M_max, dx, dy, verbose);
  std::vector<U> M0_avrs = M0.avrs();

  int n_found = M0.rows();
  jkf::matrix<U> M_for(n_found, N_jkf());
  for (int i = 0; i < n_found; ++i) {
    if (M0_avrs[i] >= 0) {
      M_for.set_row(M0.row(i), i);
    }
  }

  return M_for;
} // end get_M_forward()

template <class U>
jkf::matrix<U> jkf_no_back<U>::get_M_backward(const U &M_max, const U &dx,
                                              const U &dy,
                                              const bool &verbose) const {
  jkf::matrix<U> M0 = this->get_Mm(M_max, dx, dy, verbose);
  std::vector<U> M0_avrs = M0.avrs();

  int n_found = M0.rows();
  jkf::matrix<U> M_back(n_found, N_jkf());
  for (int i = 0; i < n_found; ++i) {
    if (M0_avrs[i] < 0) {
      M_back.set_row(((-1.0) * M0).row(i), i);
    }
  }

  return M_back;
} // end get_M_backward()

template <class U>
jkf::matrix<U> jkf_no_back<U>::get_A_forward(const U &M_max, const U &dx,
                                             const U &dy,
                                             const bool &verbose) const {
  jkf::matrix<U> M0 = this->get_Mm(M_max, dx, dy, verbose);
  jkf::matrix<U> A0 = this->get_Am(M_max, dx, dy, verbose);
  std::vector<U> M0_avrs = M0.avrs();

  int n_found = M0.rows();
  jkf::matrix<U> A_for(n_found, N_jkf());
  for (int i = 0; i < n_found; ++i) {
    if (M0_avrs[i] >= 0) {
      for (int j = 0; j < N_jkf(); ++j) {
        A_for(i, j) = A0(i, j);
      }
    }
  }

  return A_for;
} // end get_A_forward()

template <class U>
jkf::matrix<U> jkf_no_back<U>::get_A_backward(const U &M_max, const U &dx,
                                              const U &dy,
                                              const bool &verbose) const {
  jkf::matrix<U> M0 = this->get_Mm(M_max, dx, dy, verbose);
  jkf::matrix<U> A0 = this->get_Am(M_max, dx, dy, verbose);
  std::vector<U> M0_avrs = M0.avrs();

  int n_found = M0.rows();
  jkf::matrix<U> A_back(n_found, N_jkf());
  for (int i = 0; i < n_found; ++i) {
    if (M0_avrs[i] < 0) {
      for (int j = 0; j < N_jkf(); ++j) {
        A_back(i, j) = A0(i, j) * exp(-M0(i, j) * T());
      }
    }
  }

  return A_back;
} // end get_A_backward()

#endif
} // namespace ODE
} // namespace Silibs2
