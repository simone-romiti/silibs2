// method.h
/*!
This header declares a class method,
which is the tool to apply the ODE algorithm described in the following paper:
PHYSICAL REVIEW D 100, 054515 (2019)
*/

#include <cassert>
#include <complex>
#include <functional>
#include <string>
#include <vector>

#include "../../Functions/functions.h"
#include "../../LA/matrix.h"
#include "../../jkf/matrix.h"

namespace Silibs2 {
namespace ODE {
#ifndef Silibs2_ODE_method_h
#define Silibs2_ODE_method_h

template <typename U> class method {
protected:
  int N_states = 0;     /* number of states */
  std::vector<U> C;     /* correlator t = 1, ..., N */
  std::vector<U> sigma; /* errors of  correlator */

public:
  method();
  ~method();

  /*!
  n_states = number of states,
  T_ext =time extension ,
  corr = single jackkfine (or boostrap) of the correlator as a function of time
  */
  method<U>(const int &n_states, const std::vector<U> &corr,
            const std::vector<U> &sig);
  void setup(const int &n_states, const std::vector<U> &corr,
             const std::vector<U> &sig);

  int N(); /* returns the number of states */
  int T(); /* returns the time extension of the lattice */

  /*! finds the coefficients x_k of eq. (22) */
  std::vector<U> get_xk(const int &n_prec);

  /*! finds the masses from the roots z_m of eq. (6)
  mL and mR are the left and right bounds for the masses */
  std::vector<U> get_masses(const int &n_prec, const U &mL, const U &mR,
                            const U &dx = 1e-4, const U &dy = 1e-4);

  /*! return the z_m of equation (6) found with this->get_masses() */
  std::vector<U> get_zm(const int &n_prec, const U &mL, const U &mR,
                        const U &dx = 1e-4, const U &dy = 1e-4);

  /*! finds the coefficients Am of eq. (28) */
  std::vector<U> get_Am(const int &n_prec, const U &mL, const U &mR,
                        const U &dx = 1e-4, const U &dy = 1e-4);
};

#endif
} // namespace ODE
} // namespace Silibs2
