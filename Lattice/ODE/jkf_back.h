// jkf_back.h
/*!
This header declares a classjkf_back,
which is the tool to apply the ODE algorithm for correlators with definite time
parity. For more details see PHYSICAL REVIEW D 100, 054515 (2019)
*/

#include <cassert>
#include <complex>
#include <functional>
#include <random>
#include <string>

#include "./method.h"

namespace Silibs2 {
namespace ODE {
#ifndef Silibs2_ODE_jkf_back_h
#define Silibs2_ODE_jkf_back_h

template <class U> class jkf_back {

protected:
  int N_states = 0;    /*!< number of states */
  jkf::matrix<U> CORR; /*!< correlator [t][jkf]. t = 1, ..., T/2 */
  int T_REV = 0;       /*!< sign under time reversal */

  /*! precision digits in the inversion of mass and amplitude matrices */
  int N_prec = 16;

public:
  jkf_back();
  ~jkf_back();

  /*! n_states = number of states, corr = jackknifes of correlator (time, jkf)
   */
  jkf_back<U>(const jkf::matrix<U> &corr, const int &t_rev, const int &n_states,
              const int &N_prec = 16);
  void setup(const jkf::matrix<U> &corr, const int &t_rev, const int &n_states,
             const int &N_prec = 16);

  int N() const; /*! returns the number of states */

  int T_half()
      const; /*! returns T/2, which is equal to the times in the correlator */
  int T() const; /*! returns T */
  int p() const; /*! returns the sign under time reversal */

  int N_jkf() const; /*! returns the total number of times */

  /*! mass matrix of eq. (37) for the j-th jackknife */
  jkf::matrix<U> Mass_matrix(const int &j) const;

  /*! vector of eq. (38) for the j-th jackknife */
  std::vector<U> V_vector(const int &j) const;

  /*
  returns the jackknifes matrix of the coefficients xk of eq. (35)
  inverting the mass matrix with precision n_prec
  */
  jkf::matrix<U> get_xk() const;

  /*! returns all the roots z_i of eq. (33), even the negative ones */
  jkf::matrix<U> get_all_zi(const U &M_min = -1.0, const U &M_max = 1.0,
                            const U &dx = 1e-5, const U &dy = 1e-10,
                            const bool &verbose = false);

  /*! returns the physical roots z_i of eq. (33) */
  jkf::matrix<U> get_zi(const U &M_max = 1.0, const U &dx = 1e-5,
                        const U &dy = 1e-10, const bool &verbose = false);

  /*! returns the number of physical states in the correlator. They are found on
   * the average values of the xk */
  int N_phys(const U &M_min = -1.0, const U &M_max = 1.0, const U &dx = 1e-5,
             const U &dy = 1e-10, const bool &verbose = false) const;

  /*! returns the unphysical roots z_i of eq. (33) */
  jkf::matrix<U> get_negative_zi(const U &M_min = -1.0, const U &dx = 1e-5,
                                 const U &dy = 1e-10,
                                 const bool &verbose = false);

  /*! returns all the masses of eqs. (30) and (33) */
  jkf::matrix<U> get_all_masses(const U &M_min = -1.0, const U &M_max = 1.0,
                                const U &dx = 1e-5, const U &dy = 1e-10);

  /*! returns the positive (i.e. physical) masses found in the correlator */
  jkf::matrix<U> get_masses(const U &M_max = 1.0, const U &dx = 1e-5,
                            const U &dy = 1e-10);

  /*!
  Returns the imaginary (i.e. unphysical) masses
  in the correlator (Fourier modes of the noise)
  */
  jkf::matrix<U> get_imaginary_masses(const U &M_min = -1.0, const U &dx = 1e-5,
                                      const U &dy = 1e-10);

  /*! amplitude matrix of eq. (41) */
  jkf::matrix<U> Amplitude_matrix(const int &j, const U &M_min = -1.0,
                                  const U &M_max = 1.0, const U &dx = 1e-5,
                                  const U &dy = 1e-10);

  /*! W vector of eq. (42) */
  std::vector<U> W_vector(const int &j, const U &M_min = -1.0,
                          const U &M_max = 1.0, const U &dx = 1e-5,
                          const U &dy = 1e-10);

  /*! returns all the amplitudes (physical and unphysical)*/
  jkf::matrix<U> get_all_amplitudes(const U &M_min = -1.0, const U &M_max = 1.0,
                                    const U &dx = 1e-5, const U &dy = 1e-10,
                                    const bool &verbose = false);

  /*! returns only the physical amplitudes */
  jkf::matrix<U> get_amplitudes(const U &M_min = -1.0, const U &M_max = 1.0,
                                const U &dx = 1e-5, const U &dy = 1e-10,
                                const bool &verbose = true);

  // /*! returns the unphysical amplitudes */
  // jkf::matrix<U> get_unphysical_amplitudes(const U &M_min = -1.0,
  //                                          const U &M_max = 1.0,
  //                                          const U &dx = 1e-5,
  //                                          const U &dy = 1e-10);
}; // class jkf_back

#endif
} // namespace ODE
} // namespace Silibs2
