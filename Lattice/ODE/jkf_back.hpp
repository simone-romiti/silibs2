// jkf_back.hpp
/* This file defines what is declared in jkf_back.h */

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/multiprecision/gmp.hpp>

#include "./method.hpp"

#include "./jkf_back.h"

namespace Silibs2 {
namespace ODE {
#ifndef Silibs2_ODE_jkf_back_hpp
#define Silibs2_ODE_jkf_back_hpp
using namespace Silibs2::Functions;
template <typename U> jkf_back<U>::jkf_back() {}
template <typename U> jkf_back<U>::~jkf_back() {}

template <typename U>
jkf_back<U>::jkf_back(const jkf::matrix<U> &corr, const int &t_rev,
                      const int &n_states, const int &n_prec) {
  setup(corr, t_rev, n_states, n_prec);
} // jkf_back()

template <typename U>
void jkf_back<U>::setup(const jkf::matrix<U> &corr, const int &t_rev,
                        const int &n_states, const int &n_prec) {
  CORR = corr;
  T_REV = t_rev;
  N_states = n_states;
  N_prec = n_prec;
} // setup()

template <typename U> int jkf_back<U>::N() const { return N_states; }

template <typename U> int jkf_back<U>::T_half() const { return CORR.rows(); }
template <typename U> int jkf_back<U>::T() const { return 2 * T_half(); }
template <typename U> int jkf_back<U>::p() const { return T_REV; }

template <typename U> int jkf_back<U>::N_jkf() const { return CORR.cols(); }

template <typename U>
jkf::matrix<U> jkf_back<U>::Mass_matrix(const int &j) const {
  jkf::matrix<U> M(N_states, N_states);
  int T_ext = T_half();
  std::vector<U> C0_jack = CORR.col(j), sigma_0 = CORR.errs();
  for (int k1 = 0; k1 < N_states; ++k1) {
    for (int k2 = 0; k2 < N_states; ++k2) {
      std::vector<U> C_2k1 = even_der_2n(k1, C0_jack),
                     C_2k2 = even_der_2n(k2, C0_jack);
      M(k1, k2) = 0.0;
      /* recall: t=0 component correspond to physical t=1 */
      for (int t = N_states + 1; t <= T_ext - N_states; ++t) {
        M(k1, k2) += (C_2k1[t - 1] * C_2k2[t - 1]) / pow(sigma_0[t - 1], 2);
      }
    }
  }
  return M;
} // Mass_matrix()

template <typename U> std::vector<U> jkf_back<U>::V_vector(const int &j) const {
  std::vector<U> V(N_states);
  int T_ext = T_half();
  std::vector<U> C0_jack = CORR.col(j), sigma_0 = CORR.errs();
  for (int k = 0; k < N_states; ++k) {
    std::vector<U> C_2k = even_der_2n(k, C0_jack),
                   C_2N = even_der_2n(N_states, C0_jack);
    V[k] = 0.0;
    /* recall: t=0 component correspond to physical t=1 */
    for (int t = N_states + 1; t <= T_ext - N_states; ++t) {
      V[k] -= (C_2k[t - 1] * C_2N[t - 1]) / pow(sigma_0[t - 1], 2);
    }
  }
  return V;
} // V_vector()

template <typename U> jkf::matrix<U> jkf_back<U>::get_xk() const {
  /* multiprecision vesions of M and V */
  using namespace boost::multiprecision;
  mpf_float::default_precision(N_prec);

  jkf::matrix<U> xk(N_states + 1, N_jkf());

  for (int j = 0; j < N_jkf(); ++j) {
    LA::matrix<mpf_float> M =
        Tables::change_type<mpf_float, U>(this->Mass_matrix(j));
    std::vector<mpf_float> V =
        Functions::change_type<mpf_float, U>(this->V_vector(j));
    std::vector<mpf_float> xk_jack = LA::solve(M, V);

    for (int n = 0; n < N_states; ++n) {
      xk(n, j) = static_cast<U>(xk_jack[n]);
    }
    xk(N_states, j) = 1.0;
  }

  return xk;
} // get_xk()

template <typename U>
jkf::matrix<U> jkf_back<U>::get_all_zi(const U &M_min, const U &M_max,
                                       const U &dx, const U &dy,
                                       const bool &verbose) {
  /* multiprecision vesions of M and V */
  using namespace boost::multiprecision;
  mpf_float::default_precision(N_prec);

  bool less_states =
      false; /* check if th enumber of roots found for 'P(z)' equal N_states '*/

  jkf::matrix<U> zi(N_states, N_jkf());
  jkf::matrix<U> xk = this->get_xk();

  U z_min = -pow(2.0 * sinh(M_min / 2.0), 2);
  U z_max = +pow(2.0 * sinh(M_max / 2.0), 2);

  for (int j = 0; j < N_jkf(); ++j) {
    const std::vector<U> x = xk.col(j);
    const std::function<U(const U &)> P = [&](const U &z) {
      return polynomial(z, x);
    };
    std::vector<U> z_roots = roots_interval(P, z_min, z_max, dx, dy);

    const int n_found = z_roots.size();
    /*
    in case not enough roots were found the vector z_roots
    is filled with '0.0' until its size equals N_states
    */
    while ((int)z_roots.size() < N_states) {
      z_roots.push_back(0.0);
      less_states = true;
    }
    if (n_found > N_states) {
      std::cerr << "They have been found more roots than the number of states: "
                << "\n";
      print_cerr(z_roots);
      std::cerr
          << "This is impossible since the polynomial is of degree N_states."
          << "\n";
      std::cerr << "Try to reduce the cutoff \'dy\' on the \'y\' values."
                << "\n";
      std::cerr << "Aborting."
                << "\n";
      abort();
    }

    zi.set_col(z_roots, j);
  }

  if (verbose && less_states) {
    std::clog << "Warning : You found " << N_phys() << " states of " << N_states
              << " expected to be in the correlator."
              << "\n";
  }

  return zi;
}

template <typename U>
jkf::matrix<U> jkf_back<U>::get_zi(const U &M_max, const U &dx, const U &dy,
                                   const bool &verbose) {
  return this->get_all_zi(0.0, M_max, dx, dy, verbose);
} // get_zi()

template <typename U>
int jkf_back<U>::N_phys(const U &M_min, const U &M_max, const U &dx,
                        const U &dy, const bool &verbose) const {
  U z_max = +pow(2.0 * sinh(M_max / 2.0), 2);

  std::vector<U> x = (this->get_xk()).avrs();
  std::function<U(const U &)> P = [&](const U &z) { return polynomial(z, x); };
  std::vector<U> z_roots = roots_interval(P, 0.0, z_max, dx, dy);

  int nf = z_roots.size();
  return nf;
} // N_phys()

template <typename U>
jkf::matrix<U> jkf_back<U>::get_negative_zi(const U &M_min, const U &dx,
                                            const U &dy, const bool &verbose) {
  return this->get_all_zi(M_min, 0.0, dx, dy, verbose);
} // get_zi()

template <typename U>
jkf::matrix<U> jkf_back<U>::get_all_masses(const U &M_min, const U &M_max,
                                           const U &dx, const U &dy) {
  jkf::matrix<U> zi = this->get_all_zi(M_min, M_max, dx, dy);
  jkf::matrix<U> Mi(N_states, N_jkf());
  auto lp = [&](const int &n, const int &j) {
    U z_half = zi(n, j) / 2.0;
    if (z_half >= 0) {
      Mi(n, j) = acosh(1.0 + z_half);
    } else {
      Mi(n, j) = acos(1.0 + z_half);
    } /* taking only the solution +(i*M) */
  };

  Mi.loop_elements(lp);
  return Mi;
} // get_masses()

template <typename U>
jkf::matrix<U> jkf_back<U>::get_masses(const U &M_max, const U &dx,
                                       const U &dy) {
  jkf::matrix<U> zi = this->get_zi(M_max, dx, dy);
  jkf::matrix<U> Mi(N_states, N_jkf());
  auto lp = [&](const int &n, const int &j) {
    U z_half = zi(n, j) / 2.0;
    Mi(n, j) = acosh(1.0 + z_half);
  };
  Mi.loop_elements(lp);
  return Mi;
} // get_masses()

template <typename U>
jkf::matrix<U> jkf_back<U>::get_imaginary_masses(const U &M_min, const U &dx,
                                                 const U &dy) {
  jkf::matrix<U> zi = this->get_negative_zi(M_min, dx, dy);
  jkf::matrix<U> Mi(N_states, N_jkf());
  auto lp = [&](const int &n, const int &j) {
    U z_half = zi(n, j) / 2.0;
    Mi(n, j) = acos(1.0 + z_half);
  };
  Mi.loop_elements(lp);
  return Mi;
} // get_imaginary_masses()

template <typename U>
jkf::matrix<U> jkf_back<U>::Amplitude_matrix(const int &j, const U &M_min,
                                             const U &M_max, const U &dx,
                                             const U &dy) {
  int T_ext = T_half(), T_tot = 2 * T_ext;
  jkf::matrix<U> A(N_states, N_states);

  std::vector<std::vector<U>> sigma_2k(N_states +
                                       1); /* vector of vectors of the errors */
  for (int k = 0; k <= N_states; ++k) {
    sigma_2k[k] = even_der_2n(k, CORR).errs();
  }

  std::vector<U> z = this->get_all_zi(M_min, M_max, dx, dy).col(j);
  std::vector<U> M = this->get_all_masses(M_min, M_max, dx, dy).col(j);

  U t_rev = this->p();
  auto f = [&](const int &tau, const U &Mi) {
    return Silibs2::Functions::leading_exp_back((U)tau, (U)1.0, Mi, t_rev,
                                                (U)T_tot);
  };

  for (int i = 0; i < N_states; i++) {
    for (int j = 0; j < N_states; j++) {
      A(i, j) = 0.0;
      for (int k = 0; k <= N_states; k++) {
        for (int t = k + 1; t <= T_ext - k; t++) {
          A(i, j) += pow(z[i] * z[j], k) * (f(t, M[i]) * f(t, M[j])) /
                     pow(sigma_2k[k][t - 1], 2);
        }
      }
    }
  }

  return A;
} // Amplitude_matrix()

template <typename U>
std::vector<U> jkf_back<U>::W_vector(const int &j, const U &M_min,
                                     const U &M_max, const U &dx, const U &dy) {
  int T_ext = T_half(), T_tot = 2 * T_ext;
  std::vector<U> W(N_states);

  std::vector<U> C0_jack = CORR.col(j);

  std::vector<std::vector<U>> C_2k(
      N_states + 1); /* vector of vectors of the even derivatives */
  std::vector<std::vector<U>> sigma_2k(N_states +
                                       1); /* vector of vectors of the errors */
  for (int k = 0; k <= N_states; ++k) {
    jkf::matrix<U> C_2k_matr = even_der_2n(k, CORR);
    C_2k[k] = C_2k_matr.col(j);
    sigma_2k[k] = C_2k_matr.errs();
  }

  std::vector<U> z = this->get_all_zi(M_min, M_max, dx, dy).col(j);
  std::vector<U> M = this->get_all_masses(M_min, M_max, dx, dy).col(j);

  U t_rev = this->p();
  auto f = [&](const int &tau, const U &Mi) {
    return Silibs2::Functions::leading_exp_back((U)tau, (U)1.0, Mi, t_rev,
                                                (U)T_tot);
  };

  for (int i = 0; i < N_states; i++) {
    W[i] = 0.0;
    for (int k = 0; k <= N_states; ++k) {
      for (int t = k + 1; t <= T_ext - k; ++t) {
        W[i] += pow(z[i], k) * (f(t, M[i]) * C_2k[k][t - 1]) /
                pow(sigma_2k[k][t - 1], 2);
      }
    }
  }

  return W;
} // W_vector()

template <typename U>
jkf::matrix<U> jkf_back<U>::get_all_amplitudes(const U &M_min, const U &M_max,
                                               const U &dx, const U &dy,
                                               const bool &verbose) {
  /* multiprecision vesions of M and V */
  using namespace boost::multiprecision;
  mpf_float::default_precision(N_prec);

  jkf::matrix<U> Ai(N_states, N_jkf());
  for (int j = 0; j < N_jkf(); ++j) {
    LA::matrix<mpf_float> Aij = change_type<mpf_float, U>(
        this->Amplitude_matrix(j, M_min, M_max, dx, dy));
    std::vector<mpf_float> W =
        change_type<mpf_float, U>(this->W_vector(j, M_min, M_max, dx, dy));
    std::vector<mpf_float> Ai_jack = LA::solve(Aij, W);
    for (int n = 0; n < N_states; ++n) {
      Ai(n, j) = static_cast<U>(Ai_jack[n]);
    }
  }

  return Ai;
} // get_all_amplitudes()

template <typename U>
jkf::matrix<U> jkf_back<U>::get_amplitudes(const U &M_min, const U &M_max,
                                           const U &dx, const U &dy,
                                           const bool &verbose) {

  int n_phys = N_phys(M_min, M_max, dx, dy); /* number of physical signals */
  jkf::matrix<U> Ai = this->get_all_amplitudes(M_min, M_max, dx, dy);

  if (verbose && (N_states != n_phys)) {
    std::clog << "Warning : You found " << n_phys << " states of " << N_states
              << " expected to be in the correlator."
              << "\n";
  }

  return Ai.block(N_states - n_phys, 0, n_phys, N_jkf());
} // get_amplitudes()

#endif
} // namespace ODE
} // namespace Silibs2
