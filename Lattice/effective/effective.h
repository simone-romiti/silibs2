// effective.h
/*!
Functions which calculate the effective mass and amplitude of a correlator
*/

#include <iostream>

#include "point.h"

namespace Silibs2 {
namespace Lattice {
namespace effective {
#ifndef Silibs2_Lattice_effective_effective_h
#define Silibs2_Lattice_effective_effective_h

/*! Finds the effective mass using point::effective_no_back() */
template <class U = double>
std::pair<std::vector<U>, std::vector<U>>
effective_no_back(const std::vector<U> &y);

/*!
Finds the effective mass(in lattice units) and effective amplitude curves
using point::effective_back_II_der()
*/
template <class U = double>
std::pair<std::vector<U>, std::vector<U>>
effective_back_II_der(const std::vector<U> &y, const int &p);

/*!
Finds the effective mass and effective amplitude curves
using point::effective_back_Gattringer()
*/
template <class U = double>
std::pair<std::vector<U>, std::vector<U>>
effective_back_Gattringer(const std::vector<U> &y, const U &dx = 1e-4,
                          const U &dy = 1e-4, const int &p = 1,
                          const U &M_max = 1.0);

/* overload for jkf::matrix objects */
template <class U = double>
std::pair<jkf::matrix<U>, jkf::matrix<U>>
effective_back_Gattringer(const jkf::matrix<U> &Y, const U &dx = 1e-4,
                          const U &dy = 1e-8, const int &p = 1,
                          const U &M_max = 1.0);

#endif
} // namespace effective
} // namespace Lattice
} // namespace Silibs2
