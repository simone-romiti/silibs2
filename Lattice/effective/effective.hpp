// M_eff_DEF.h
/* This file defines what is declared in M_eff.h*/

#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>

#include "point.hpp"

#include "./effective.h"

namespace Silibs2 {
namespace Lattice {
namespace effective {
#ifndef Silibs2_Lattice_effective_effective_hpp
#define Silibs2_Lattice_effective_effective_hpp

template <class U>
std::pair<std::vector<U>, std::vector<U>>
effective_no_back(const std::vector<U> &y) {
  std::vector<U> M(0), A(0);

  int t;
  U y1, y2;

  int T_half = y.size();
  std::pair<U, U> pa;
  for (int tau = 0; tau < T_half - 1; ++tau) {
    t = tau + 1;
    y1 = y[tau];
    y2 = y[tau + 1];

    pa = point::effective_no_back(y1, y2, t);
    M.push_back(pa.first);
    A.push_back(pa.second);
  }

  return make_pair(M, A);
} // effective_no_back()

template <class U>
std::pair<std::vector<U>, std::vector<U>>
effective_back_II_der(const std::vector<U> &y, const int &p) {
  std::vector<U> M(0), A0(0);

  int t;
  U y_middle, y_back, y_for;

  int T_half = y.size();
  std::pair<U, U> pa;
  U m, a0;
  for (int tau = 0; tau < T_half; tau++) {
    t = tau + 1;

    if (t != 1) {
      y_back = y[tau - 1];
    } else {
      /* when t==1 y_back is not even condidered,
      so I put it =0 in order to not leave it uninitialized. */
      y_back = 0;
    }

    y_middle = y[tau];

    y_for = y[tau + 1];

    pa = point::effective_back_II_der(y_back, y_middle, y_for, t, T_half, p);

    m = std::get<0>(pa);
    a0 = std::get<1>(pa);

    M.push_back(m);
    A0.push_back(a0);
  }

  return make_pair(M, A0);
} // effective_back_II_der()

template <class U>
std::pair<std::vector<U>, std::vector<U>>
effective_back_Gattringer(const std::vector<U> &y, const U &dx, const U &dy,
                          const int &p, const U &M_max) {
  std::vector<U> M_eff(0), A_eff(0);

  int T_half = y.size();
  for (int i = 0; i < T_half - 1; ++i) {
    U y1 = y[i], y2 = y[i + 1];
    int t = i + 1; /* times start from 1, not from 0 */
    std::pair<U, U> pa =
        point::effective_back_Gattringer(y1, y2, t, T_half, dx, dy, p, M_max);
    M_eff.push_back(pa.first);
    A_eff.push_back(pa.second);
  }
  /*!
  For the last point the effective mass is undefined.
  Here it's set equal to 0
  */
  M_eff.push_back(0.0);
  A_eff.push_back(0.0);

  return make_pair(M_eff, A_eff);
} // effective_back_Gattringer()

/* overload for jkf */
template <class U = double>
std::pair<jkf::matrix<U>, jkf::matrix<U>>
effective_back_Gattringer(const jkf::matrix<U> &Y, const U &dx, const U &dy,
                          const int &p, const U &M_max) {

  int T_half = Y.rows();
  int N_jkf = Y.cols();
  jkf::matrix<U> M(T_half, N_jkf);
  jkf::matrix<U> A(T_half, N_jkf);

  std::pair<std::vector<U>, std::vector<U>> pa;
  for (int k = 0; k < N_jkf; ++k) {
    pa = effective_back_Gattringer(Y.col(k), dx, dy, p, M_max);
    M.set_col(pa.first, k);
    A.set_col(pa.second, k);
  }
  return std::make_pair(M, A);
}

#endif
} // namespace effective
} // namespace Lattice
} // namespace Silibs2
