// slope_eff.h
/* utilities for effective slope of a straight line */

#include <vector>

namespace Silibs2 {
namespace Lattice {
#ifndef Silibs2_Lattice_slope_eff_h
#define Silibs2_Lattice_slope_eff_h

/* effective slope with forward derivative */
template <class U = double>
std::vector<U> slopes_fwd(const std::vector<U> &x, const std::vector<U> &y);

#endif
} // namespace Lattice
} // namespace Silibs2
