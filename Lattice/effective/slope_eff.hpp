// slope_eff.hpp
/* utilities for effective slope of a straight line */

#include <cassert>
#include <vector>

#include "./slope_eff.h"

namespace Silibs2 {
namespace Lattice {
#ifndef Silibs2_Lattice_slope_eff_hpp
#define Silibs2_Lattice_slope_eff_hpp

/* effective slope with forward derivative */
template <class U>
std::vector<U> slopes_fwd(const std::vector<U> &x, const std::vector<U> &y) {
  std::vector<U> sl(0);
  assert(x.size() == y.size() &&
         "The number of x and y values are not the same.");
  int N = x.size();

  U dx, dy;
  for (int i = 0; i < N - 1; ++i) {
    dx = x[i + 1] - x[i];
    dy = y[i + 1] - y[i];
    sl.push_back(dy / dx);
  }
  return sl;
} // end slope_eff()

#endif
} // namespace Lattice
} // namespace Silibs2
