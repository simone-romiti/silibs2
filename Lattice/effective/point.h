// point.h
/*!
Functions that find the effective mass and amplitude at a given time
*/

#ifndef Silibs2_Lattice_point_h
#define Silibs2_Lattice_point_h

#include <functional>
#include <iostream>
#include <vector>

#include "../../jkf/jkf.h"

namespace Silibs2 {
namespace Lattice {
namespace point {

/*!
Finds the effective mass and amplitude
without taking into account the backward signal */
template <class U = double>
std::pair<U, U> efective_no_back(const U &y1, const U &y2, const int &t);

/*
Finds the effective mass(in lattice units) and amplitude
of the correlator at time 't',
using the 2nd order ODE method approximation:

        acosh[1 + C^(2)(t)/C(t)] --> M

NOTES:
- The backward signal is taken into account
- The method is applicable only from t=2 to t=(T/2-1) (both included).
*/
template <class U = double>
std::pair<U, U> effective_back_II_der(const U &y_back, const U &y_middle,
                                      const U &y_for, const int &t,
                                      const int &T_half, const int &p);

/*
Finds the effective mass and effective amplitude
of the correlator in the point 't'.

The 1st backward signal is taken into account.
y1 = C(t), y2 = C(t+1)
C(t) ~ A0*exp(-M_eff*t) + (-)^p A0*exp(-M_eff*(T-t) )

The reference formula is eq. (6.57) of
Gattringer, C. and Lang, C., 2009.
Quantum chromodynamics on the lattice: an introductory presentation (Vol. 788).
Springer Science & Business Media.
*/
template <class U = double>
std::pair<U, U> effective_back_Gattringer(const U &y1, const U &y2,
                                          const int &t, const int &T_half,
                                          const U &dx = 1e-4,
                                          const U &dy = 1e-4, const int &p = 1,
                                          const U &M_max = 1.0);

} // namespace point
} // namespace Lattice
} // namespace Silibs2
#endif
