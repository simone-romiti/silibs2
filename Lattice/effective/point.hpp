// point.hpp
/* Definitions of point.h */

#include <algorithm>
#include <cassert>
#include <cmath>

#include "../../Functions/functions.hpp"
#include "../../jkf/jkf.hpp"

#include "./point.h"

namespace Silibs2 {
namespace Lattice {
namespace point {
#ifndef Silibs2_Lattice_point_hpp
#define Silibs2_Lattice_point_hpp

template <typename U>
std::pair<U, U> effective_no_back(const U &y1, const U &y2, const int &t) {
  U M = log(y1 / y2);
  if (y2 == 0.0) {
    M = 0.0;
  }
  U A = y1 / exp(-M * t);

  return std::make_pair(M, A);
} // effective_no_back()

template <typename U>
std::pair<U, U> effective_back_II_der(const U &y_back, const U &y_middle,
                                      const U &y_for, const int &t,
                                      const int &T_half, const int &p) {

  U y1 = y_back, y2 = y_middle, y3 = y_for;
  U M = 0;
  int T = 2 * T_half;
  if (t == 1) {
    /* the M >0 --> y2/y3 > 1 */
    M = (y2 / y3 > 1) ? log(y2 / y3) : 0;
  } else if (t > 1) {
    if (t == T_half) {
      y3 = p * y_back;
    }

    U der_II = y1 + y3 - 2 * y2;
    U arg = 1 + der_II / (2 * y2);

    /* the argument of acosh() must be >1 */
    M = (arg < 1 || y2 == 0) ? 0 : acosh(arg);
  }

  U den = exp(-M * t) + p * exp(-M * (T - t));

  U A0 = y2 / den;

  /*! The denominator may be too small --> unphysical Amplitude */
  if (A0 > 1) {
    A0 = 1.0;
  }

  return std::make_pair(M, A0);
} // effective_back()

template <typename U>
std::pair<U, U> effective_back_Gattringer(const U &y1, const U &y2,
                                          const int &t, const int &T_half,
                                          const U &dx, const U &dy,
                                          const int &p, const U &M_max) {
  using namespace Silibs2;
  std::function<U(const U &)> f = [&](const U &m) {
    U res;
    if (p == 1) {
      res = cosh(m * (t - T_half)) / cosh(m * (t + 1 - T_half)) - y1 / y2;
    } else if (p == -1) {
      res = sinh(m * (t + 1 - T_half)) / sinh(m * (t - T_half)) - y2 / y1;
    } else {
      std::cerr << "Error: time reversal can be either 1 or -1 . You gave: "
                << p << "\n";
      abort();
    }
    return res;
  };

  std::vector<U> roots = Functions::roots_interval(f, 1e-16, M_max, dx, dy);

  assert((int)roots.size() < 2 && "More that one root was found.");
  U M = roots[0];

  U T = 2 * T_half;
  U den = Functions::leading_exp_back((U)t, 1.0, M, (U)p, T);
  U A = y1 / den;

  return std::make_pair(M, A);
} // effective_back()

#endif
} // namespace point
} // namespace Lattice
} // namespace Silibs2
