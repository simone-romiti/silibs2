// systematics.h
// combining two or more results to get the average and systematic error
// using eq. 28 of https://arxiv.org/abs/1403.4504

#include "../jkf/matrix.h"

namespace Silibs2 {
namespace Lattice {

template <class U = double>
std::pair<U, U> combine_systematics(const std::vector<jkf::matrix<U>> &v);

}
} // namespace Silibs2