// dual.h
/*
This header declares the function giving the dual contribution to the
correlator. [https://arxiv.org/abs/1808.00887]
*/

#include <cmath>
#include <complex>

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_dual_h
#define Silibs2_Luscher_dual_h

/* Dual contribution to the Vector correlator */
template <class U = double>
U V_dual(const U &t, const U &R_dual, const U &MV, const U &E_dual,
         const U &q_quark);

template <class U = double>
U V_dual(const int &t, const U &R_dual, const U &MV, const U &E_dual,
         const U &q_quark);

#endif
} // end namespace Luscher

} // namespace Silibs2
