// dual_DEF.h
/* This header defines what is declared in dual.h */

#include "./dual.h"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_dual_hpp
#define Silibs2_Luscher_dual_hpp

template <typename U>
U V_dual(const U &t, const U &R_dual, const U &MV, const U &E_dual,
         const U &q_quark) {

  U q_quark_sqr = std::pow(q_quark, 2.0);
  U a = q_quark_sqr * (1.0 / (2.0 * std::pow(M_PI, 2.0)));
  U b = R_dual / std::pow(t, 3.0);

  U Gamma = MV + E_dual;
  U c = std::exp(-Gamma * t);
  U Gamma_squared = std::pow(Gamma, 2.0);
  U t_squared = std::pow(t, 2.0);
  U d = 1.0 + Gamma * t + (Gamma_squared * t_squared) / 2.0;

  U v = a * b * c * d;
  return v;

} // V_dual()

template <typename U>
U V_dual(const int &t, const U &R_dual, const U &MV, const U &E_dual,
         const U &q_quark) {
  return V_dual((U)t, R_dual, MV, E_dual, q_quark);
} // V_dual

#endif
} // namespace Luscher
} // namespace Silibs2