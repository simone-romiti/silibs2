// phi_DEF.h
/*! definitions of phi.h */

#include "../numerics/nums.hpp"
#include "Z_00.hpp"

#include "./phi.h"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_phi_hpp
#define Silibs2_Luscher_phi_hpp

template <class U> U tan_phi(const U &q) {
  U eps = NL.eps;
  U q_sqr = pow(q, 2.0);
  U int_q_sqr = round(q_sqr);
  U diff = fabs(q_sqr - int_q_sqr);
  bool flag = !(diff < eps); // true if q^2 is not an integer

  if (flag) {
    U num = -2.0 * pow(M_PI, 2.0) * q;
    U den = Z_00(q_sqr);
    U result = num / den;
    return result;
  } else {
    return 0;
  }
} // tan_phi()

template <class U> U phi(const U &q) {
  U z = tan_phi(q);
  U res = atan(z);
  if (res > 0) {
    res -= M_PI;
  }
  // res += ( (int) pow(q,2.0) )*M_PI; //!< Luscher convention
  return res;
} // phi()

#endif
} // namespace Luscher
} // namespace Silibs2
