// phi.h
/*
This file declares the function phi(z)
[https://arxiv.org/abs/1808.00887]
*/

#include <cmath>

#include "Z_00.h"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_phi_h
#define Silibs2_Luscher_phi_h

/*! \tan{\phi(z)} */
template <class U = double> U tan_phi(const U &q);

/*!
\phi(q) : -\pi <= \phi < 0
then \delta(k) shoud be 0 < \delta< \pi
*/
template <class U = double> U phi(const U &q);

#endif
} // namespace Luscher
} // namespace Silibs2
