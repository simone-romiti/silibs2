// Z_00.hpp
/*!
Definitions of Z_00.h
[paper](https://projecteuclid.org/euclid.cmp/1104115329)
*/

#include "../numerics/nums.hpp"
#include "F_00.hpp"

#include "./Z_00.h"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_Z_00_hpp
#define Silibs2_Luscher_Z_00_hpp

template <class U> U arg_sum_Z_00(const int &n, const U &Q2) {
  U result = std::pow(n - Q2, -1.0);
  U res_multip = M3[n] * result;
  return res_multip;
} // arg_sum_Z_00()

template <class U> U arg_sum_Z_00(const int &n, U *p) {
  U Q2 = p[0];
  return arg_sum_Z_00(n, Q2);
} // arg_sum_Z_00()

template <class U> U Z_00_first(const U &Q2) {
  U p[1] = {Q2};
  U epsilon = NL.eps;
  U result = Functions::sum(arg_sum_Z_00, p, Q2 - epsilon); // 'q' is excluded
  return result;
} // Z_00_first()

template <class U>
U Z_00_second_one_integrand(const U &t, const U &Q2, const U &lam) {
  return F_00_trunc(t, Q2, lam);
} // Z_00_second_one_integrand()

template <class U> U Z_00_second_one_integrand(const U &t, U *p) {
  U Q2 = p[0];
  U lam = p[1];
  return Z_00_second_one_integrand(t, Q2, lam);
} // Z_00_second_one_integrand()

template <class U> U fun_Z_00_second_one_integrand(const U &t, U *p) {
  return Z_00_second_one_integrand(t, p);
} // fun_Z_00_second_one_integrand()

template <class U> U Z_00_second_one_integrand(const U &t, void *p) {
  U *Q2 = (U *)p;
  return Z_00_second_one_integrand(t, Q2);
} // Z_00_second_one_integrand()

template <class U> U Z_00_second_one(const U &Q2) {
  U lambda = NL.lambda;
  U p[2] = {Q2, lambda};

  U a = 0;
  U b = 1;

  std::function<U(const U &, U *)> fn1 =
      (U(*)(const U &, U *)) & fun_Z_00_second_one_integrand;
  int N_div = NL.N_div_integration;
  U result = Functions::inte_a_b(fn1, p, a, b, N_div);
  return result;
}

template <class U> U Z_00_second_two_integrand(const U &t, const U &Q2) {

  return F_00_large(t, Q2);
}

template <class U> U Z_00_second_two_integrand(const U &t, U *p) {

  U Q2 = p[0];
  return Z_00_second_two_integrand(t, Q2);
}

template <class U> U fun_Z_00_second_two_integrand(const U &t, U *p) {
  return Z_00_second_two_integrand(t, p);
}

template <class U> U Z_00_second_two_integrand(const U &t, void *p) {
  U *r = (U *)p;
  return Z_00_second_two_integrand(t, r);
}

template <class U> U Z_00_second_two(const U &Q2) {

  U p[1] = {Q2};

  U result;
  U a = 1;

  std::function<U(const U &, U *)> fn1 =
      (U(*)(const U &, U *)) & fun_Z_00_second_two_integrand;
  int N_div = NL.N_div_integration;
  result = Silibs2::Functions::inte_a_plusInf<U>(fn1, p, a, N_div);
  return result;
} // Z_00_second_two()

template <class U> U Z_00_second(const U &Q2) {

  U result = Z_00_second_one(Q2) + Z_00_second_two(Q2);
  return result;
} // Z_00_second()

template <class U> U arg_Z_00_third(const int &j, const U &Q2) {

  U a = A_j(j, Q2) / (j + 1.0);
  U b = B_j(j, Q2) / (j - 0.5);

  U result = a + b;
  return result;
} // arg_Z_00_third()

template <class U> U arg_Z_00_third(const int &j, U *p) {

  U Q2 = p[0];
  return arg_Z_00_third(j, Q2);
} // arg_Z_00_third()

template <class U> U Z_00_third(const U &Q2) {

  U lambda = NL.lambda;
  U p[1] = {Q2};
  U result = Functions::sum(arg_Z_00_third, p, lambda);
  return result;
} // Z_00_third

template <class U> U Z_00(const U &Q2) {

  U first = Z_00_first(Q2);
  U second = Z_00_second(Q2);
  U third = Z_00_third(Q2);

  U result = first + second + third;
  return result;

} // Z_00()

#endif
} // namespace Luscher
} // namespace Silibs2
