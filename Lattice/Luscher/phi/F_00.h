// F_00.h
/*
This file declares the function F_00 (and functions needed),
necessary in order to to define Z_00
[https://projecteuclid.org/euclid.cmp/1104115329]
*/

#include <cmath>
#include <complex>

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_F_00_h
#define Silibs2_Luscher_F_00_h

template <class U = double>
U arg_sum_A_j(const int &n, const int &j,
              const U &Q2); // 'n' is the norm squared

template <class U = double> U arg_sum_A_j(const int &n, U *p);

template <class U = double> U A_j(const int &j, const U &Q2);

template <class U = double> U B_j(const int &j, const U &Q2);

/*! --------------------------------------- */
/*! Kernel K (in another paper is called F) */
/*! --------------------------------------- */

// small 't'

template <class U = double>
U arg_sum_F_00_small_one(const int &n, const U &t, const U &Q2);

template <class U = double> U arg_sum_F_00_small_one(const int &n, U *Q2);

template <class U = double> U F_00_small_one(const U &t, const U &Q2);

template <class U = double> U arg_sum_F_00_small_two(const int &n, const U &t);

template <class U = double> U arg_sum_F_00_small_two(const int &n, U *p);

template <class U = double> U F_00_small_two(const U &t, const U &Q2);

template <class U = double> U F_00_small(const U &t, const U &Q2);

// large 't'
template <class U = double>
U arg_sum_F_00_large(const int &n, const U &t, const U &Q2);
template <class U = double> U arg_sum_F_00_large(const int &n, U *p);
template <class U = double> U F_00_large(const U &t, const U &Q2);

template <class U = double>
U arg_F_00_t_to_zero(const int &j, const U &t, const U &Q2);
template <class U = double> U arg_F_00_t_to_zero(const int &j, U *p);

template <class U = double>
U F_00_t_to_zero(const U &t, const U &Q2, const U &lam);
template <class U = double> U F_00_trunc(const U &t, const U &Q2, const U &lam);

#endif
} // end namespace Luscher
} // namespace Silibs2
