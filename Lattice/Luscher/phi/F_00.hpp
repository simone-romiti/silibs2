// F_00.hpp
/* Definitions of F_00.h  */

#include "../numerics/nums.hpp"

#include "./F_00.h"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_F_00_hpp
#define Silibs2_Luscher_F_00_hpp

template <class U> U arg_sum_A_j(const int &n, const int &j, const U &Q2) {
  U basis = Q2 - n;
  U result = pow(basis, j);
  return M3[n] * result;
}

template <class U> U arg_sum_A_j(const int &n, U *p) {
  int j = (int)round(p[0]);
  U Q2 = p[1];
  return arg_sum_A_j(n, j, Q2);
}

template <class U> U A_j(const int &j, const U &Q2) {
  U den = Silibs2::Functions::factorial(j);
  U p[2] = {(U)j, Q2};
  U s = Functions::sum(arg_sum_A_j, p, Q2);
  U result = -(s / den);
  return result;
}

template <class U> U B_j(const int &j, const U &Q2) {
  U num, den;
  num = pow(M_PI, 3.0 / 2.0) * pow(Q2, j);
  den = Silibs2::Functions::factorial(j);
  U result = num / den;
  return result;
}

template <class U>
U arg_sum_F_00_small_one(const int &n, const U &t, const U &Q2) {
  U result = exp(t * (Q2 - n));
  U res_multip = M3[n] * result;
  return res_multip;
}

template <class U> U arg_sum_F_00_small_one(const int &n, U *p) {
  U t = p[0];
  U Q2 = p[1];
  return arg_sum_F_00_small_one(n, t, Q2);
}

template <class U> U F_00_small_one(const U &t, const U &Q2) {
  U p[2] = {t, Q2};
  U result = -Functions::sum(arg_sum_F_00_small_one, p, Q2);
  return result;
}

template <class U> U arg_sum_F_00_small_two(const int &n, const U &t) {
  U result = exp(-(pow(M_PI, 2) * n) / t);
  U res_multip = M3[n] * result;
  return res_multip;
}

template <class U> U arg_sum_F_00_small_two(const int &n, U *p) {
  return arg_sum_F_00_small_two(n, p[0]);
}

template <class U> U F_00_small_two(const U &t, const U &Q2) {
  U Lambda = NL.Lambda;
  U a = (pow(M_PI / t, 3.0 / 2.0)) * exp(t * Q2);
  U p[1] = {t};
  U s = Functions::sum(arg_sum_F_00_small_two, p, Lambda);
  U result = a * s;
  return result;
}

template <class U> U F_00_small(const U &t, const U &Q2) {
  U result = F_00_small_one(t, Q2) + F_00_small_two(t, Q2);
  return result;
}

template <class U> U arg_sum_F_00_large(const int &n, const U &t, const U &Q2) {
  U result = exp(-t * (n - Q2));
  U res_multip = M3[n] * result;
  return res_multip;
}

template <class U> U arg_sum_F_00_large(const int &n, U *p) {
  U t = p[0];
  U Q2 = p[1];
  return arg_sum_F_00_large(n, t, Q2);
}

template <class U> U F_00_large(const U &t, const U &Q2) {
  U Lambda = NL.Lambda;
  U p[2] = {t, Q2};
  U s0 =
      Functions::sum(arg_sum_F_00_large, p, Lambda, Q2 + 1); // 'q' is excluded
  return s0;
}

template <class U> U arg_F_00_t_to_zero(const int &j, const U &t, const U &Q2) {
  U a = A_j(j, Q2) * pow(t, j);
  U b = B_j(j, Q2) * pow(t, j - 3.0 / 2.0);
  U result = a + b;
  return result;
}

template <class U> U arg_F_00_t_to_zero(const int &j, U *p) {
  U t = p[0];
  U Q2 = p[1];
  return arg_F_00_t_to_zero(j, t, Q2);
}

template <class U> U F_00_t_to_zero(const U &t, const U &Q2, const U &lam) {
  U p[2] = {t, Q2};
  U result = Functions::sum(arg_F_00_t_to_zero, p, lam);
  return result;
}

template <class U> U F_00_trunc(const U &t, const U &Q2, const U &lam) {
  U a = F_00_small(t, Q2);
  U b = F_00_t_to_zero(t, Q2, lam);
  U result = a - b;
  return result;
}

#endif
} // namespace Luscher
} // namespace Silibs2