// Z_00.h
/*
This file declares the function Z_00 (and functions needed)
[https://projecteuclid.org/euclid.cmp/1104115329]
*/

#include <functional>

#include "F_00.h"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_Z_00_h
#define Silibs2_Luscher_Z_00_h

template <class U = double>
U arg_sum_Z_00(const int &n,
               const U &Q2); // n = |{n1,n2,n3}|^2 ... times its multiplicity
template <class U = double> U arg_sum_Z_00(const int &n, U *p);
template <class U = double> U Z_00_first(const U &Q2); // first part (sum)

template <class U = double>
U Z_00_second_one_integrand(const U &t, const U &Q2,
                            const U &lam); // second part (integral)
template <class U = double> U Z_00_second_one_integrand(const U &t, const U *p);
template <class U = double> U fun_Z_00_second_one_integrand(const U &t, U *p);
template <class U = double> U Z_00_second_one_integrand(const U &t, void *p);

template <class U = double>
U Z_00_second_one(const U &Q2); //!< integral from '0' to '1'

template <class U = double>
U Z_00_second_two_integrand(const U &t,
                            const U &Q2); //!< second part (integral)
template <class U = double> U Z_00_second_two_integrand(const U &t, U *p);
template <class U = double> U fun_Z_00_second_two_integrand(const U &t, U *p);
template <class U = double> U Z_00_second_two_integrand(const U &t, void *p);

template <class U = double>
U Z_00_second_two(const U &Q2); //!< integral from 1 to infinity

template <class U = double> U Z_00_second(const U &Q2);

template <class U = double> U arg_Z_00_third(const int &j, const U &Q2);
template <class U = double> U arg_Z_00_third(const int &j, const U *p);
template <class U = double> U Z_00_third(const U &Q2);

template <class U = double> U Z_00(const U &Q2); // Z_00

#endif
} // end namespace Luscher
} // namespace Silibs2
