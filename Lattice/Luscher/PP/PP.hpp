// PP.hpp
/*! Definitions of PP.h */

#include "../roots/roots.hpp"

#include "./PP.h"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_PP_hpp
#define Silibs2_Luscher_PP_hpp

template <class U, class I1, class I2>
U V_PP(const I1 &t, const int &N_states, const I2 &L, const U &MP, const U &MV,
       const U &g, const U &cff) {
  U V = 0.0;
  std::vector<U> k_Luscher = k_Luscher_roots(N_states, (U)L, MP, MV, g, cff);
  int n_av = k_Luscher.size(); // available roots
  if (n_av < N_states) {
    std::cerr << N_states << " " << L << " " << MP << " " << MV << " " << g
              << "\n";
    std::cerr << "Insufficient number of states found."
              << "\n";
    abort();
  }
  U tau = (U)t;
  U k;     /*!< impulse k_n */
  U alpha; /*!< coefficient of the spectral decomposition */
  U omega; /*!< energy of the two PP */
  for (int i = 0; i < N_states; i++) {
    /* Note on enumeration: the k_Luscher[0] corresponds to the 1st impulse*/
    k = k_Luscher[i];
    alpha = alpha_n(k, (U)L, MP, MV, g, cff);
    omega = omega_PP(k, MP);
    V += alpha * (std::exp(-omega * tau));
  }
  return V;
} // end V_PP()

template <class U, class I1, class I2>
U V_PP(const I1 &t, const std::vector<U> &k_n, const I2 &L, const U &MP,
       const U &ratio_MV_MP, const U &g, const U &cff) {
  U V = 0.0;
  int N_states = k_n.size();
  U MV = MP * ratio_MV_MP;
  U tau = (U)t, k = 0.0, omega = 0.0, alpha = 0.0;
  for (int i = 0; i < N_states; i++) {
    k = k_n[i];

    omega = omega_PP(k, MP);
    alpha = alpha_n(k, (U)L, MP, MV, g, cff);

    V += alpha * (std::exp(-omega * tau));
  }

  return V;
} // end V_PP()

#endif
} // namespace Luscher
} // namespace Silibs2
