// PP.h
/*
This file declares the function giving
the Luscher representation of the PP contribution
[https://arxiv.org/abs/1808.00887]
*/

#include "../roots/roots.h"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_PP_h
#define Silibs2_Luscher_PP_h

/*!< V_{\pi \pi}(t) = \sum_{n} \alpha_n e^{-omega_n t} */
template <class U = double, class I1, class I2>
U V_PP(const I1 &t, const int &N_states, const I2 &L, const U &MP, const U &MV,
       const U &g, const U &cff);

/*
V_PP used when one wants to pass the vector of impulses k_n
insted of evaluating them each time V_PP is called
*/
template <class U = double, class I1, class I2>
U V_PP(const I1 &t, const std::vector<U> &k_n, const I2 &L, const U &MP,
       const U &ratio_MV_MP, const U &g, const U &cff);

#endif
} // namespace Luscher
} // namespace Silibs2
