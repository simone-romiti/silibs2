// form_factor.h
/*
This file declares the functions giving the form factor
[paper](https://arxiv.org/abs/1808.00887)
*/

#include <cmath>
#include <complex>

#include "./form_factor.hpp"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_phase_shift_h
#define Silibs2_Luscher_phase_shift_h

/*!
returns the phase shift \delta, such that
0 < \delta < \pi
(then phi(q) should be -\pi <phi(q)< 0 )
*/
template <class U = double>
U delta_11(const U &k, const U &MP, const U &MV, const U &g, const U &cff);

/* \tan{\delta_{11}} */
template <class U = double>
U tan_delta_11(const U &k, const U &MP, const U &MV, const U &g, const U &cff);

#endif
} // namespace Luscher
} // namespace Silibs2
