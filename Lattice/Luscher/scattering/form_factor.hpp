// form_factor.hpp
/*! Definitions of form_factor.h */

#include "./form_factor.h"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_form_factor_hpp
#define Silibs2_Luscher_form_factor_hpp

template <class U>
std::complex<U> Gamma(const U &omega, const U &MP, const U &g) {
  U c0 = pow(g, 2.0) / (6.0 * M_PI);   // g^2/(6*\pi)
  std::complex<U> k = k_PP(omega, MP); // k

  std::complex<U> num = pow(k, 3); // k^3
  U den = pow(omega, 2);           // \omega^2
  std::complex<U> x = c0 * (num / den);
  return x;
} // end Gamma()

template <class U> std::complex<U> h(const U &omega, const U &MP, const U &g) {
  std::complex<U> p1 = Gamma(omega, MP, g); // \omega * \Gamma
  std::complex<U> k = k_PP(omega, MP);      // PP impulse
  std::complex<U> arg =
      (omega + 2.0 * k) / (2.0 * MP); // argument of the logarithm
  std::complex<U> p2 = (2.0 * omega / M_PI) * log(arg);

  std::complex<U> x = p1 * p2;
  return x;

} // end h()

template <class U> std::complex<U> h(const U &omega, U *p) {
  U MP = p[0], g = p[1];
  return h(omega, MP, g);
} // end h()

template <class U>
std::complex<U> h_prime(const U &omega, const U &MP, const U &g) {
  std::complex<U> k = k_PP(omega, MP);

  U p1 = pow(g, 2.0) / (6.0 * M_PI);                 // g^2/(6*\pi)
  std::complex<U> p2 = pow(k, 2.0) / (M_PI * omega); // \frac{k^2}{\pi*\omega}

  U f1 = 1.0 + 2.0 * pow(MP / omega, 2.0); // 1 + 2 (M_{\pi}/\omega)
  std::complex<U> f2 = k / M_PI;
  std::complex<U> arg_log = (omega + 2.0 * k) / (2.0 * MP);
  std::complex<U> f3 = log(arg_log);
  std::complex<U> f = f1 * f2 * f3;

  std::complex<U> x = p1 * p2 + p1 * f;
  return x;
} // end h_prime()

template <class U> std::complex<U> A_0(const U &MP, const U &MV, const U &g) {
  std::complex<U> H_MV = h(MV, MP, g);
  std::complex<U> H_prime_MV = h_prime(MV, MP, g);
  std::complex<U> one = H_MV;
  std::complex<U> two = -(MV / 2.0) * H_prime_MV;
  std::complex<U> three = (pow(g, 2.0) / (6.0 * M_PI)) * (pow(MP, 2.0) / M_PI);

  std::complex<U> result = one + two + three;
  return result;
}

template <class U>
std::complex<U> A(const U &omega, const U &MP, const U &MV, const U &g) {

  if (omega == 0) {
    std::complex<U> Res0 = A_0(MP, MV, g);
    return Res0;
  } else {
    std::complex<U> H = h(omega, MP, g);
    std::complex<U> H_MV = h(MV, MP, g);
    std::complex<U> H_prime_MV = h_prime(MV, MP, g);

    std::complex<U> a0 = H_MV - H;

    U p0 = pow(omega, 2.0) - pow(MV, 2.0);
    std::complex<U> p1 = H_prime_MV / (2.0 * MV);
    std::complex<U> a1 = p0 * p1;

    std::complex<U> a = a0 + a1;
    std::complex<U> b = omega * Gamma(omega, MP, g);

    std::complex<U> i(0, 1);
    std::complex<U> z = a + i * b;
    return z;
  }

} // end A()

// form factor
template <class U>
std::complex<U> F(const U &omega, const U &MP, const U &MV, const U &g,
                  const U &coeff_form_factor) {
  std::complex<U> result;

  if (fabs(MV) < NL.epsilon) {
    result = 0.0;
  } else {
    U MV_sqr = pow(MV, 2.0);
    std::complex<U> A_sqr = A(0.0, MP, MV, g);
    std::complex<U> num = MV_sqr - A_sqr;

    U omega_sqr = pow(omega, 2.0);
    std::complex<U> den0 = MV_sqr - omega_sqr;
    std::complex<U> den1 = A(omega, MP, MV, g);
    std::complex<U> den = den0 - den1;

    result = (num / den);
  }

  return coeff_form_factor * result;
} // end F()

template <class U>
U F_squared(const U &omega, const U &MP, const U &MV, const U &g,
            const U &coeff_form_factor) {
  return std::norm(F(omega, MP, MV, g, coeff_form_factor));
} // end F_squared()

#endif
} // namespace Luscher
} // namespace Silibs2
