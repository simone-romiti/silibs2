// // form_factor.h
/*!
This file defines what is declared in phase_shift.h
[paper](https://arxiv.org/abs/1808.00887)
*/

#include "./phase_shift.h"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_phase_shift_hpp
#define Silibs2_Luscher_phase_shift_hpp

template <class U>
U delta_11(const U &k, const U &MP, const U &MV, const U &g, const U &cff) {

  U omega = omega_PP(k, MP);
  std::complex<U> f = F(omega, MP, MV, g, cff);
  U d = std::arg(f);

  if (d < 0) {
    d += M_PI;
  }

  // if( fabs(d) > M_PI/2.0 ){ d -= M_PI; }
  // else if(fabs(d) < -M_PI/2.0 ){ d += M_PI; }

  return d;

} // end delta_11()

template <class U>
U tan_delta_11(const U &k, const U &MP, const U &MV, const U &g, const U &cff) {
  return tan(delta_11(k, MP, MV, g, cff));
} // end tan_delta_11()

#endif
} // namespace Luscher
} // namespace Silibs2
