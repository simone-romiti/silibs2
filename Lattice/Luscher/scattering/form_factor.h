// form_factor.h
/*
This file declares the functions giving the form factor
[https://arxiv.org/abs/1808.00887]
*/

#include <cmath>
#include <complex>

#include "../kinematics/kinematics.hpp"
#include "../numerics/nums.hpp"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_form_factor_h
#define Silibs2_Luscher_form_factor_h

template <class U = double>
std::complex<U> Gamma(const U &omega, const U &MP,
                      const U &g); //!< \Gamma(\omega)

template <class U = double>
std::complex<U> h(const U &omega, const U &MP,
                  const U &g); // analytic prolongation of h
template <class U = double> std::complex<U> h(const U &omega, U *p);

template <class U = double>
std::complex<U> h_prime(const U &omega, const U &MP,
                        const U &g); //!< h'(\omega) (analytic prolongation)

template <class U = double>
std::complex<U> A_0_c(const U &MP, const U &MV, const U &g); // A(\omega = 0 )
template <class U = double>
std::complex<U> A(const U &omega, const U &MP, const U &MV,
                  const U &g); // A(\omega)

template <class U = double>
std::complex<U> F(const U &omega, const U &MP, const U &MV, const U &g,
                  const U &coeff_form_factor); // form factor
template <class U = double>
U F_squared(const U &omega, const U &MP, const U &MV, const U &g,
            const U &coeff_form_factor); // mod squared of the form factor

#endif
} // namespace Luscher
} // namespace Silibs2
