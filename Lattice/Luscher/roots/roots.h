// roots.h
/*!
Declaration of functions which find the Luscher energies ad roots
from the Lellouch-Luscher formula
[paper](https://arxiv.org/abs/1808.00887)
*/

#include <cmath>
#include <mutex>
#include <thread>

#include "../kinematics/kinematics.h"
#include "../phi/phi.h"
#include "../scattering/phase_shift.h"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_roots_h
#define Silibs2_Luscher_roots_h

template <class U = double>
U Lusch_cond(const U &k, const U &L, const U &MP, const U &MV, const U &g,
             const U &cff); //!< Luscher condition

//! Tangent of the Luscher condition

template <class U = double, class I>
std::vector<U> k_Luscher_roots(const int &n_wanted, const I &L, const U &MP,
                               const U &MV, const U &g, const U &cff);

/*!
alpha_n coefficients which are in front of the exponentials
in the spectral decomposition of the correlator.
*/
template <class U = double, class I>
U alpha_n(const U &k, const I &L, const U &MP, const U &MV, const U &g,
          const U &cff);

#endif
} // end namespace Luscher
} // namespace Silibs2
