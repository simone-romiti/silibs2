// roots.hpp
/*! Definition of roots_h */

#include "../kinematics/kinematics.hpp"
#include "../numerics/nums.hpp"
#include "../phi/phi.hpp"
#include "../scattering/phase_shift.hpp"

#include "./roots.h"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_roots_hpp
#define Silibs2_Luscher_roots_hpp

template <class U>
U Lusch_cond(const U &k, const U &L, const U &MP, const U &MV, const U &g,
             const U &cff) {
  U q = (k * L) / (2.0 * M_PI);
  U phase_shift = delta_11(k, MP, MV, g, cff);
  U phi_function = phi(q);

  U x = phase_shift + phi_function;
  return x;

} // end Lusch_cond()

template <class U> U Lusch_cond(const U &k, U *p) {
  U L = p[0], MP = p[1], MV = p[2], g = p[3], cff = p[4];
  return Lusch_cond(k, L, MP, MV, g, cff);
} // end Lusch_cond()

template <class U, class I>
std::vector<U> k_Luscher_roots(const int &n_wanted, const I &L, const U &MP,
                               const U &MV, const U &g, const U &cff) {
  using namespace Silibs2::Functions;

  std::vector<U> k_Luscher(0);
  U p[5] = {(U)L, MP, MV, g, cff};

  U epsilon_x = NL.epsilon_x;
  U epsilon_y = NL.epsilon_y;

  std::mutex mutex_k;

  std::function<U(const U &, U *)> LC = [&](const U &k0, U *p0) {
    return Lusch_cond(k0, p0);
  };

  /* Single thread function which finds the 1st root (0-th component of
   * k_Luscher)*/
  auto thread_1st_root = [&]() {
    U q_n = 0.0;                                    // Luscher::norms3[0];
    U start = epsilon_x + ((2.0 * M_PI) / L) * q_n; /* i.e. start=1 */
    // U start = sqrt( (pow(MV, 2.0)/4) - pow(MP, 2.0) );

    U k_r = root_on_the_right(LC, p, start, epsilon_x, epsilon_y);

    mutex_k.lock();
    k_Luscher.push_back(k_r);
    mutex_k.unlock();
  };

  /* Single thread function which finds the n-th root (n!=0)*/
  auto single_thread = [&](const int &r) {
    U q_n = Luscher::norms3[r];
    U start = epsilon_x + ((2.0 * M_PI) / L) * q_n;

    U k_r = root_on_the_right(LC, p, start, epsilon_x, epsilon_y);

    mutex_k.lock();
    k_Luscher.push_back(k_r);
    mutex_k.unlock();
  };

  std::vector<std::thread> thr(0);

  std::thread t0(thread_1st_root);
  thr.push_back(std::move(t0));

  for (int i = 0; i < n_wanted - 1; ++i) {
    std::thread t1(single_thread, i);
    thr.push_back(std::move(t1));
  }

  for (int i = 0; i < n_wanted; ++i) {
    thr[i].join();
  }

  /*!
  sorting the values
  NOTE: threads may have written the roots on the vector such that
  they are not in ascending order
  */
  std::sort(k_Luscher.begin(), k_Luscher.end());
  return k_Luscher;
} // end k_Luscher_roots()

template <class U, class I>
U alpha_n(const U &k, const I &L, const U &MP, const U &MV, const U &g,
          const U &cff) {
  U p[5] = {(U)L, MP, MV, g, cff};
  U omega = omega_PP(k, MP);

  U a1 = (2.0 * pow(k, 5.0)) / (3.0 * M_PI * pow(omega, 2.0));
  U a2 = F_squared(omega, MP, MV, g, cff);

  U epsilon = NL.epsilon;
  U b = k * Silibs2::Functions::N_Derive<U>(Lusch_cond, k, p, epsilon);

  U result = (a1 * a2) / b;
  return result;
} // end alpha_n()

#endif
} // end namespace Luscher
} // namespace Silibs2
