// Kinematics.h
/*
This file declares functions about the kinematics
of the decay V --> PP
[https://arxiv.org/abs/1808.00887]
*/

#include <cmath>
#include <complex>

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_kinematics_h
#define Silibs2_Luscher_kinematics_h

//! Returns the energy of two pseudoscalar mesons of impulse 'k'
template <class U = double> U omega_PP(const U &k, const U &MP);

//! impulse as a function of energy (analytic prolongation)
template <class U = double> std::complex<U> k_PP(const U &omega, const U &MP);

#endif
} // end namespace Luscher
} // namespace Silibs2
