// kinematics.hpp
/* Definitions of kinematics.h */

#include "./kinematics.h"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_kinematics_hpp
#define Silibs2_Luscher_kinematics_hpp

template <class U> U omega_PP(const U &k, const U &MP) {
  U a1 = pow(MP, 2.0); // MP^2
  U a2 = pow(k, 2.0);  // k^2
  U arg = a1 + a2;     // argument of the square root

  U x = 2.0 * sqrt(arg);
  return x;
} // omega()

template <class U> std::complex<U> k_PP(const U &omega, const U &MP) {
  U b1 = pow((omega / 2.0), 2.0); /*!< (\omega/2)^2 */
  U b2 = pow(MP, 2.0);            /*!< MP^2 */
  std::complex<U> arg = b1 - b2;
  std::complex<U> x = sqrt(arg);
  return x;
} // k_PP()

#endif
} // namespace Luscher
} // namespace Silibs2
