// nums.h
/*
This header defines numerical values needed by the functions
which evaluate the Luscher representation of the correlator
[https://arxiv.org/abs/1808.00887]
*/

#include <cmath>
#include <vector>

#include "../../../Functions/functions.h"

namespace Silibs2 {
namespace Luscher {
#ifndef Silibs2_Luscher_nums_h
#define Silibs2_Luscher_nums_h

struct Num_Luscher {

  const double eps = 1e-14; //!< difference under which a 'double' is an 'int'

  // Constraints for numerical methods (default values)
  /*double epsilon0 = 1e-4; // limit under which a function is considered to be
   * zero */
  const double epsilon = 1e-8; // increment for the approximation of derivatives

  // + 0.1 in order to avoid exclusion when they are converted into 'int' type
  const double Lambda = 15 + 0.1; // cutoff for infinite sums
  const double lambda = 1 + 0.1;  // separation for truncated kernel.
  // 'lambda = -1' means no truncation

  /*double Cut_t = 1.0; // separation for small and large values of 't'*/

  // number of sub-intervals of the integration region (0,1) [see inte_0_1()]
  const double N_div_integration = 1e+2;

  const double epsilon_x = 1e-4;
  const double epsilon_y = 1e-7; /* rule of thumb : 100*epsilon_x */

  // default constructors and destructors
  Num_Luscher() {}
  ~Num_Luscher() {}
};

const Num_Luscher NL;
const double Max_norm_squared =
    std::pow(NL.Lambda, 2.0); // largest norm squared condidered in sums
const std::vector<int> M3 = Functions::multips3(
    Max_norm_squared); // vector of multiplicities for the norm squared

/* ------------------------- */
template <class U = double> std::vector<U> vector_of_norms3() {
  std::vector<U> norms_Z3_vectors(0);
  const int n = M3.size();
  U mu = 0;
  for (int i = 0; i < n; ++i) {
    mu = i;
    if (mu != 0) {
      norms_Z3_vectors.push_back(sqrt(mu));
    }
  }
  return norms_Z3_vectors;
}

/* vector of norms of Z^3 vectors */
const std::vector<double> norms3 = vector_of_norms3<double>();

// const double quark_charge = 1.0/3.0;
// const double q2 = pow(quark_charge, 2.0);
// const double coeff_form_factor = 2.0*q2; //Contribution from two channels:
// K0K0 and K+K-
//
namespace roots_Luscher {
/* k_min and k_max in which the Luscher roots k_n are sought */
const double k_min = 1e-8, k_max = 10.0;

//!< number of points in which
//!< the interval [k_min, k_max] is divided
// const int iter_find_k_Luscher = k_max*100;

} // end namespace roots_Luscher

#endif
} // end namespace Luscher

} // namespace Silibs2
