# Makefile
# makefile for the maintenance of the git repository

CC = g++ -std=c++11 -g -Ofast
CFLAGS =  -I /opt/Minuit2/inc/

ld_boost = -lboost_filesystem -lboost_system
ld_gmp = -lgmp
ld_Minuit2 = -fopenmp -L /opt/Minuit2/src/.libs/  -lMinuit2
LDFLAGS = $(ld_boost) $(ld_gmp) $(ld_Minuit2)

# cration of a static library
Silibs2.o: Silibs2.cpp
	g++ -c -fPIC $(CFLAGS) Silibs2.cpp -o Silibs2.o

# creation of a shared library
libSilibs2.so: Silibs2.o Silibs2.cpp
	g++ -shared Silibs2.o $(LDFLAGS) -o libSilibs2.so
	
# creating a precompiled hader for faster compile times
gch:
	$(CC) $(CFLAGS) -c ./Silibs2.h -o Silibs2.h.gch 

pull:
	git pull origin master

add:
	git add -A

commit: add
	git commit -a

push: commit
	git push origin master

