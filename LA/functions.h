// functions.h
/*! Utilities to interact with the matrix class objects */

#include "../Functions/functions.hpp"
#include "./matrix.h"

namespace Silibs2 {
namespace LA {
#ifndef Silibs2_LA_functions_h
#define Silibs2_LA_functions_h
using namespace Silibs2::Functions;

#endif
} // namespace LA
} // namespace Silibs2
