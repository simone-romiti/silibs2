// matrix.hpp
/*! Definitions for matrix.h */

#include "../Tables/table.hpp"
#include "./matrix.h"

namespace Silibs2 {
namespace LA {
#ifndef Silibs2_LA_matrix_hpp
#define Silibs2_LA_matrix_hpp
using namespace Functions;

template <class U> matrix<U>::matrix() {}
template <class U> matrix<U>::~matrix() {}

template <class U> matrix<U>::matrix(const Tables::table<U> &t) {
  (*this).LIST_OF_ROWS = t.list_of_rows(); //!< same elements
  (*this).ROWS = t.rows();                 //!< same rows
  (*this).COLS = t.cols();                 //!< same columns
}
template <class U> matrix<U>::matrix(const matrix<U> &m) { (*this) = m; }

template <class U>
matrix<U>::matrix(const int &ro, const int &co, const U &val) {
  Tables::table<U> t(ro, co, val);
  (*this) = t;
}

template <class U> matrix<U> matrix<U>::operator*(const U &lambda) const {
  matrix<U> matr((*this).ROWS, (*this).COLS);
  auto lp = [&](const int &i, const int &j) {
    matr(i, j) = (lambda) * ((*this)(i, j));
  };
  this->loop_elements(lp);
  return matr;
} // operator *()

template <class U> void matrix<U>::operator*=(const U &lambda) {
  *this = (*this) * lambda;
} // operator -=()

template <class U> matrix<U> matrix<U>::operator/(const U &lambda) const {
  matrix<U> matr((*this).ROWS, (*this).COLS);
  auto lp = [&](const int &i, const int &j) {
    matr(i, j) = ((*this)(i, j)) / (lambda);
  };
  this->loop_elements(lp);
  return matr;
} // operator / ()

template <class U> void matrix<U>::operator/=(const U &lambda) {
  *this = (*this) / lambda;
} // operator -= ()

template <class U> void matrix<U>::operator+=(const matrix<U> &m) {
  *this = *this + m;
} // operator +=()

template <class U> void matrix<U>::operator-=(const matrix<U> &m) {
  *this = (*this - m);
} // operator -=()

template <typename U> matrix<U> matrix<U>::inv() const {
  return inverse_matrix(*this);
} // inv()

/* --------- */
/* functions */
/* --------- */

/*!  -------------------- */
/*! READING and PRINTING */
/*!  -------------------- */

template <class U>
void print_matrices(const std::string &path, const std::vector<matrix<U>> &M,
                    const std::vector<std::string> &header,
                    const std::string &sep, const int &prec) {
  const int n = (int)M.size();
  std::vector<Tables::table<U>> T(n);
  for (int i = 0; i < n; ++i) {
    T[i] = M[i];
  }
  Tables::print_tables(path, T, header, sep, prec);
} // print_matrices()

template <class U>
matrix<U> read_matrix_sep(const std::string &path, const std::string &head,
                          const int &ign_lines, const std::string &sep) {
  return Tables::read_table_sep<U>(path, head, ign_lines, sep);
} // read_matrix_sep()

template <class U>
matrix<U> read_matrix(const std::string &path, const std::string &head,
                      const int &ign_lines) {
  return Tables::read_table<U>(path, head, ign_lines);
} // read_matrix()

template <class U>
std::vector<U> read_row(const int &n, const std::string &path,
                        const std::string &head, const int &ign_lines) {
  return read_matrix<U>(path, head, ign_lines).row(n);
}

template <class U>
std::vector<U> read_col(const int &n, const std::string &path,
                        const std::string &head, const int &ign_lines) {
  return read_matrix<U>(path, head, ign_lines).col(n);
}

/*!  ---------------------------- */
/*!  OPERATIONS on the components */
/*!  ---------------------------- */

template <class U, class V> matrix<U> cast_matrix(const matrix<V> &matr) {
  return Tables::cast_table<U>(matr);
} // cast_matrix()

template <class M_type1, class M_type2>
M_type1 convert_matrix(const M_type2 &matr) {
  return Tables::convert_table<M_type1, M_type2>(matr);
} // convert_matrix()

/*!  ------------------------------ */
/*!  BUILDING from rows and columns */
/*!  ------------------------------ */

template <class U>
matrix<U> matrix_from_rows(const std::vector<std::vector<U>> &rows) {
  return Tables::table_from_rows(rows);
} // matrix_from_rows()

template <class U>
matrix<U> matrix_from_cols(const std::vector<std::vector<U>> &cols) {
  return Tables::table_from_cols(cols);
} // matrix_from_cols()

/*!  -------------- */
/*!  Linear Algebra */
/*!  -------------- */

template <class U>
matrix<U> lin_comb(const matrix<U> &m1, const matrix<U> &m2, const U &a,
                   const U &b) {
  assert(m1.rows() == m2.rows() && m1.cols() == m2.cols() &&
         "Invalid linear combination.");

  int R = m1.rows(), C = m1.cols();
  matrix<U> matr(R, C);
  matr.loop_elements([&](const int &i, const int &j) {
    matr(i, j) = a * m1(i, j) + b * m2(i, j);
  });
  return matr;
} // lin_comb()

template <class U>
std::pair<matrix<U>, matrix<U>>
lin_comb(const std::pair<matrix<U>, matrix<U>> &m1,
         const std::pair<matrix<U>, matrix<U>> &m2, const U &a, const U &b) {
  return make_pair(lin_comb(m1.first, m2.first, a, b),
                   lin_comb(m1.second, m2.second, a, b));
} // lin_comb();

template <class U>
matrix<U> operator+(const matrix<U> &m1, const matrix<U> &m2) {
  return lin_comb(m1, m2, +1.0, +1.0);
}

template <class U>
matrix<U> operator-(const matrix<U> &m1, const matrix<U> &m2) {
  return lin_comb(m1, m2, +1.0, -1.0);
}

template <class U>
matrix<U> operator*(const matrix<U> &m1, const matrix<U> &m2) {
  assert(m1.cols() == m2.rows() && "Cannot multiply the two matrices.");
  int N = m1.cols();

  int R = m1.rows(), C = m2.cols();
  matrix<U> matr(R, C);

  auto lambda_set_ij_elem = [&](const int &i, const int &j) {
    matr(i, j) = 0.0;
    for (int k = 0; k < N; k++) {
      matr(i, j) += m1(i, k) * m2(k, j);
    }
  };

  matr.loop_elements(
      [&](const int &i, const int &j) { lambda_set_ij_elem(i, j); });
  return matr;
} // operator *()

template <class U> matrix<U> operator*(const U &lambda, const matrix<U> &m) {
  return (m * lambda);
} // operator * ()

template <class U>
matrix<U> tensor_prod(const std::vector<U> &v, const std::vector<U> &w) {
  int n = v.size();
  int m = w.size();
  matrix<U> M(n, m);
  M.loop_elements([&](const int &i, const int &j) { M(i, j) = v[i] * w[j]; });
  return M;
} // tensor_prod()

template <class U>
matrix<U> minor_matrix(const matrix<U> &M, const int &i, const int &j) {
  matrix<U> M_ij = M;
  M_ij.remove_row(i);
  M_ij.remove_col(j);
  return M_ij;
} // minor_matrix()

template <class U> matrix<U> cofactors_matrix(const matrix<U> &M) {
  int R = M.rows(), C = M.cols();
  assert(R == C &&
         "Cannot find cofactors_matrix. The matrix is not a square matrix.");

  matrix<U> cofs(R, C);
  if (R == 1) {
    cofs(0, 0) = 1.0;
  } else {
    auto lp = [&](const int &i, const int &j) {
      cofs(i, j) = pow(-1, i + j) * det(minor_matrix(M, i, j));
    };
    cofs.loop_elements(lp);
  }
  return cofs;
} // cofactors_matrix

template <class U> matrix<U> adjugate_matrix(const matrix<U> &M) {
  return (cofactors_matrix(M).transpose());
} // adjugate_matrix()

template <class U> U det_2x2(const matrix<U> &M) {
  assert(M.rows() == 2 && M.cols() == 2 && "The matrix is not 2x2.");
  const U a00 = M(0, 0);
  const U a11 = M(1, 1);
  const U a01 = M(0, 1);
  const U a10 = M(1, 0);
  const U A = a00 * a11;
  const U B = a01 * a10;
  return (A - B);
} // det_2x2()

template <class U> U det(const matrix<U> &M) {
  int R = M.rows(), C = M.cols();

  matrix<U> cofs = cofactors_matrix(M);
  if (R == 0) {
    std::cerr << "Error: Cannot find the determinant of a matrix of null size. "
                 "Aborting."
              << "\n";
    abort();
  } else if (R == 1) {
    return M(0, 0);
  } else if (R == 2) {
    return det_2x2(M);
  } else { /* calculating the determinant with respect to the first row (0-th
              row) */
    int i = 0;
    U sum = 0.0;
    for (int j = 0; j < C; ++j) {
      sum += M(i, j) * cofs(i, j);
    }
    return sum;
  }
} // det()

template <class U> matrix<U> inverse_matrix(const matrix<U> &M) {
  U det_M = det(M);
  if (det_M == 0.0) {
    std::cerr << "Cannot invert matrix. Determinant is 0. Abort."
              << "\n";
    abort();
  }
  return (adjugate_matrix(M) / det_M);
} // inverse_matrix

template <class U>
std::vector<U> solve(const matrix<U> &M, const std::vector<U> &c) {
  int N = c.size();
  matrix<U> V(N, 1);
  V.set_col(c, 0);
  matrix<U> x = inverse_matrix(M) * V; // M.inv()*V;
  return x.col(0);
} // solve()

#endif
} // namespace LA
} // namespace Silibs2
