// matrix.h
/*! This header declares the class LA::matrix, used for Linear Algebra */

#include "../Tables/table.h"

namespace Silibs2 {
namespace LA {
#ifndef Silibs2_LA_matrix_h
#define Silibs2_LA_matrix_h

template <class U = double> class matrix : public Tables::table<U> {

public:
  matrix();
  ~matrix();

  /*! Copy contructor */
  matrix<U>(const Tables::table<U> &t);
  /*! Copy contructor */
  matrix<U>(const matrix<U> &matrix);

  /* Create a table of size ro*co with all elements equal to val */
  matrix<U>(const int &ro, const int &co, const U &val = 0.0);

  /*! Multiplication by a scalar from the right */
  matrix<U> operator*(const U &lambda) const;
  void operator*=(const U &lambda);
  /*! Division by a scalar */
  matrix<U> operator/(const U &lambda) const;
  void operator/=(const U &lambda);
  /*! Addition and subtraction*/
  void operator+=(const matrix<U> &m);
  void operator-=(const matrix<U> &m);

  /*! Inverse */
  matrix<U> inv() const;
};

/* --------- */
/* functions */
/* --------- */

/*!  -------------------- */
/*! READING and PRINTING */
/*!  -------------------- */

template <class U>
void print_matrices(const std::string &path, const std::vector<matrix<U>> &M,
                    const std::vector<std::string> &header = "",
                    const std::string &sep = "\t", const int &prec = 16);

/*!
Extract from the file in 'path' the matrix of values found after the line
equal to the header 'head' (head="" --> the file is read from the beginning ).
It stops at the first empty line of when EOF is reached
ign_lines = number of lines ignored after the line of the 'head'
sep = separation between columns
*/
template <class U>
matrix<U> read_matrix_sep(const std::string &path, const std::string &head = "",
                          const int &ign_lines = 0,
                          const std::string &sep = "\t");

/*!
like read_matrix_sep(),
but ignoring multiple occurencies of tabs and spaces
*/
template <class U>
matrix<U> read_matrix(const std::string &path, const std::string &head = "",
                      const int &ign_lines = 0);

/*! uses read_matrix to extract the 'n'-th row*/
template <class U>
std::vector<U> read_row(const int &n, const std::string &path,
                        const std::string &head = "", const int &ign_lines = 0);

/*! uses read_matrix to extract the 'n'-th column */
template <class U>
std::vector<U> read_col(const int &n, const std::string &path,
                        const std::string &head = "", const int &ign_lines = 0);

/*!  ---------------------------- */
/*!  OPERATIONS on the components */
/*!  ---------------------------- */

template <class U, class V> matrix<U> cast_matrix(const matrix<V> &matr);
template <class M_type1, class M_type2>
M_type1 convert_matrix(const M_type2 &matr);

/*!  ------------------------------ */
/*!  BUILDING from rows and columns */
/*!  ------------------------------ */

template <class U>
matrix<U> matrix_from_rows(const std::vector<std::vector<U>> &rows);
template <class U>
matrix<U> matrix_from_cols(const std::vector<std::vector<U>> &cols);

/*!  -------------- */
/*!  Linear Algebra */
/*!  -------------- */

/* linear combination a*m1 + b*m2 */
template <class U>
matrix<U> lin_comb(const matrix<U> &m1, const matrix<U> &m2, const U &a,
                   const U &b);

/* pair of linear combinations: a*m1<i> + b*m2<i> for i=0,1 */
template <class U>
std::pair<matrix<U>, matrix<U>>
lin_comb(const std::pair<matrix<U>, matrix<U>> &m1,
         const std::pair<matrix<U>, matrix<U>> &m2, const U &a, const U &b);

template <class U>
matrix<U> operator+(const matrix<U> &m1, const matrix<U> &m2);
template <class U>
matrix<U> operator-(const matrix<U> &m1, const matrix<U> &m2);

/* Multiplication rows * columns between matrices */
template <class U>
matrix<U> operator*(const matrix<U> &m1, const matrix<U> &m2);

/* multiplication by a scalar from the left */
template <class U> matrix<U> operator*(const U &lambda, const matrix<U> &m);

/* tensor product of 2 std::vectors : M_ij = v_i * w_j */
template <class U>
matrix<U> tensor_prod(const std::vector<U> &v, const std::vector<U> &w);

/*!
ij minor of the matrix :
matrix obteined removing the i-th row and j-th column
*/
template <class U>
matrix<U> minor_matrix(const matrix<U> &M, const int &i, const int &j);

/*! cofactors matrix :  C_ij = det(Minor_ij) */
template <class U> matrix<U> cofactors_matrix(const matrix<U> &M);

/*! adjugate of M : transpose of the cofactors matrix */
template <class U> matrix<U> adjugate_matrix(const matrix<U> &M);

/*! determinant of a 2x2 matrix */
template <class U> U det_2x2(const matrix<U> &M);
/*! determinant of a NxN matrix */
template <class U> U det(const matrix<U> &M);

/*! Inverse of the matrix */
template <class U> matrix<U> inverse_matrix(const matrix<U> &M);

/*! Solution to M*x=c , i.e.  x = (M^{-1})*c */
template <class U>
std::vector<U> solve(const matrix<U> &M, const std::vector<U> &c);

#endif
} // namespace LA
} // namespace Silibs2
