#!/bin/bash
# script for compiling, linking and and running .cpp file using the cl.py script for compiling

main_cpp=$1
name=$(echo "$main_cpp" | cut -f 1 -d '.')
exe="$name"".exe"

echo "running " $exe # name of the executable

home_dir="$(eval echo ~$USER)/Documents/"
cl=$home_dir"Silibs2/cpp_scripts/cl_cpp.sh"

$cl $main_cpp
./$exe # running the executable
