#!/bin/bash
# script for compiling and linking a .cpp file using the cl.py script for compiling

main_cpp=$1
# name=$(echo "$main_cpp" | cut -f 1 -d '.')
# exe="$name"".exe"

echo "compiling and linking " $main_cpp # name of the .cpp

home_dir="$(eval echo ~$USER)/Documents/"
cl=$home_dir"Silibs2/cpp_scripts/cl.py"

$cl $main_cpp # compiling and linking

