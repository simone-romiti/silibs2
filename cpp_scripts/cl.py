#!/usr/bin/python3
# compilation and linking using Silibs2
# Create a symbolic link in your project folder
# and do the compile-link process passing it the "".cpp" program you want.


CC = "g++ -std=c++11 -Wall -Wno-misleading-indentation -g -Ofast" # compiler

import os
home_dir = os.path.expanduser("~") + "/"
Silibs2_dir = home_dir + "Documents/Silibs2/"

CFLAGS = " -I /opt/Minuit2/inc/ "
CFLAGS += " -I " + Silibs2_dir

def compile(dot_cpp, dot_o):
    cmd = CC + " " + CFLAGS + " -c " + dot_cpp + " -o " + dot_o
    print("Compiling:")
    print(cmd)
    os.system(cmd)

ld_boost = "-lboost_filesystem -lboost_system"
ld_gmp = "-lgmp"
ld_Minuit2 = "-fopenmp -L /opt/Minuit2/src/.libs/  -lMinuit2"
ld_Silibs2 = "-L " + Silibs2_dir + " -lSilibs2"
LDFLAGS = ld_boost+" "+ld_gmp+" "+ld_Silibs2+" "+ld_Minuit2

def link(dot_o, dot_exe):
    cmd = CC + " " + dot_o + " " + LDFLAGS + " -o " + dot_exe
    print("Linking:")
    print(cmd)
    os.system(cmd)

# compile and link
def cl(dot_cpp):
    name = os.path.splitext(dot_cpp)[0]
    dot_o = name + ".o" # exemple: main.o
    dot_exe = name + ".exe" # example: main.exe
    compile(dot_cpp, dot_o)
    link(dot_o, dot_exe)

import sys
args = sys.argv # arguments passed to this executable
cpp_file = args[1] # example: main.cpp
cl(cpp_file)
