// test.cpp

#include "../Silibs2.hpp"
#include <iostream>
using namespace Silibs2;
namespace sf = Silibs2::Functions;
namespace slat = Silibs2::Lattice;

int main() {
  std::cout << "running test.cpp\n";

  const std::function<double(const double &)> f = [](const double &x) {
    return x + 4;
  };

  const double x0 = 10.0;
  const double d1 = sf::derivative(f, 10.0, 1e-4);

  std::cout << "derivative: " << d1 << "\n";
  sf::string_to_file("ciao!", "./out.dat");

  const double r1 = sf::find_root(f, 0.0, -x0, x0, 1e-6, 1e-6);
  std::cout << "root at " << r1 << "\n";

  jkf::matrix<double> m = jkf::Gaussian_jkf<double>(15, {1.0}, {0.1});
  m.print_cout();

  std::cout << Functions::GeV_inv_to_MeV_inv(1.0) << "\n";

  jkf::matrix<double> m_MeV_inv = jkf::GeV_inv_to_MeV_inv(m);
  std::cout << "main ends.\n";

  m_MeV_inv.ae().print_cout();
  auto pr1 = slat::combine_systematics<double>({m_MeV_inv});
  std::cout << pr1.first << " " << pr1.second << "\n";

  return 0;
}