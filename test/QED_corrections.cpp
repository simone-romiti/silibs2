// test.cpp

#include "../Silibs2.h"
using namespace Silibs2;

const double A0 = 50.0;
const double dA = 0.1;
const double A = A0 * (1.0 + dA);

const double M0 = 0.7;
const double dM = 0.005;
const double M = M0 + dM;

const double T = 40;
const double T_half = T / 2;
int main() {
  std::cout << "ciao Simone!" << '\n';

  const double p = +1;
  std::cout << "p : " << p << "\n";

  jkf::matrix<double> C0(T_half, 1);
  jkf::matrix<double> C(T_half, 1);
  for (int i = 0; i < T_half; ++i) {
    C0(i, 0) = Functions::leading_exp_back(i + 1.0, A0, M0, p, T);
    C(i, 0) = Functions::leading_exp_back(i + 1.0, A, M, p, T);
  }
  jkf::matrix<double> dC = C - C0;

  Lattice::correlators::correction<double> crn(dC, C0, p);
  jkf::matrix<double> dA_jkf = crn.dA_eff_back();
  jkf::matrix<double> dM_jkf = crn.dM_eff_back();

  std::cout << "Effective mass curve"
            << "\n";
  jkf::matrix<double> M_eff =
      Lattice::effective::effective_back_Gattringer<double>(C0, 1e-4, 1e-8, p,
                                                            1.0)
          .first;
  // M_eff.print_cout();

  std::cout << "You should get " << dM << " " << T_half << " times. "
            << "\n";
  dM_jkf.print_cout();

  std::cout << "You should get " << dA << " " << T_half << " times. "
            << "\n";
  dA_jkf.print_cout();

  return 0;
}
