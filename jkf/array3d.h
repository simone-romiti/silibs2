// array3d.h
/*!
This header declares the class jkf::array3d, a 3-dim tensor
whose last index runs over the jackknife values
*/

#include "./matrix.h"

namespace Silibs2 {
namespace jkf {
#ifndef Silibs2_jkf_array3d_h
#define Silibs2_jkf_array3d_h

template <class U = double> class array3d {
private:
  std::vector<jkf::matrix<U>> ARR; // vector of jkf::matrix
  int D1, D2, D3;                  // size of dimensions

public:
  array3d();
  ~array3d();

  /* Create a jkf::array3d of size ro*co with all elements equal to val */
  array3d<U>(const int &a, const int &b, const int &c, const U &val = 0.0);

  void resize(const int &a, const int &b, const int &c, const U &val = 0.0);

  std::array<int, 3> shape() const;

  /* checks if (i,j) is a valid index of the table */
  void check_access(const int &i, const int &j, const int &k) const;

  jkf::matrix<U> &operator[](const int &i);             // i-th jkf::matrix
  const jkf::matrix<U> &operator[](const int &i) const; // const callings

  U &operator()(const int &i, const int &j, const int &k);
  const U &operator()(const int &i, const int &j,
                      const int &k) const; /* for const callings */

  Tables::table<U> avrs() const; /*! jackknife averages */
  Tables::table<U> errs() const; /*! jackknife errors */

  void print_avrs(const std::string &file, const std::string &sep = "\t",
                  const int &prec = 16, const std::string &header = "") const;

  // (*this)(i,j,k), with i fixed to i0
  jkf::matrix<U> slice_0(const int &i0);
  // (*this)(i,j,k), with j fixed to j0
  jkf::matrix<U> slice_1(const int &j0);
  // (*this)(i,j,k), with k fixed to k0
  jkf::matrix<U> slice_2(const int &k0);
};

#endif
} // namespace jkf
} // namespace Silibs2
