// deps.h
// dependencies all in one file

/*! base class for 1D ansatz fitting */
#include "../../../Minuit2_fit/fit.h"
#include "../../array3d.h"
#include "../../matrix.h"
#include "../../results.h"
