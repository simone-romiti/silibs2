// base_fit.h
/*
This file declares routines allowing to fit a generic std::function
using the Minuit library, with data in the format xyey (i.e. no error on the x)
*/

#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <numeric>
#include <string>

#include "../deps.h"

namespace Silibs2 {
namespace jkf {
namespace Minuit2_fit {
namespace nD {

#ifndef Silibs2_jkf_Minuit2_fit_nD_base_fit_h
#define Silibs2_jkf_Minuit2_fit_nD_base_fit_h

//! 1-dimensional fit
template <class U = double>
class base_fit : public Silibs2::Minuit2_fit::base<U> {
protected:
  /*! control variables */
  bool data_init = false;    /*!< true if data have been set */
  bool residue_init = false; /*!< true if the residue function has been set */
  bool fit_done = false;     /*!< true when the fit has been done */
  bool verbose = false;      // when true, info is printed on std::cout

  /*! Ansatz to be fitted (if unspecified it is the null function)*/
  std::function<U(const std::vector<U> &, const std::vector<U> &)> ANSATZ =
      [](const std::vector<U> &, const std::vector<U> &) { return 0; };

  /*! jkf of Best-fit values for the parameters */
  Silibs2::jkf::matrix<U> PARS;

  /*!
  jkf of residue (reduced chi squared) i.e. the value resulting from the
  minimization.
  */
  Silibs2::jkf::matrix<U> CH2_RED;

  /*!
  Theoretical values obtained from the ansatz
  with the best fit values for the parameters
  */
  Silibs2::jkf::matrix<U> Y_th;

  Silibs2::jkf::matrix<U> X_extr_imp; /*!< imposed value of x to find Y_extr */
  Silibs2::jkf::matrix<U> Y_extr;     /*!< y_th(X_extr_imp.col(j)), \forall j*/

  bool extr_x = false; /*!< true if 'y' has been extrapolated wrt 'x' */
  bool extr_y = false; /*!< true if 'x' has been extrapolated wrt 'y' */

  std::vector<int> jkf_not_conv = {};
  Silibs2::jkf::results<U> fit_results; /*!< results of the fit in one file */
  std::string results_subtit = "";      // subtitle of the fit results

public:
  base_fit();
  ~base_fit();

  void check_data_init() const;    /*!< Checks if data were passed */
  void check_residue_init() const; /*!< Checks if residue_init is true */

  /*!
  Checks if all the conditions necessary
  to do the fit have been fullfilled
  */
  void check_fit_init() const;
  void check_fit_done() const; /*!< checks if the fit was done */

  void set_verbose(const bool &v);

  Silibs2::jkf::matrix<U> get_PARS() const; /*!< jkf of best fit parameters */
  int get_n_par() const;                    // number of parameters

  Silibs2::jkf::matrix<U> get_CH2_RED() const; /*!< jkf of best fit ch2_red */

  /*!< find y extrapolated to the jkf of x_extr */
  void find_Y_extr(const jkf::matrix<U> &x_extr);
  /*!< find y extrapolated to x_extr */
  void find_Y_extr(const std::vector<U> &x_extr);
  jkf::matrix<U> get_Y_extr() const; /*!< return the y extrapolated */

  /*!< adding a string to the subtitle of the fit_results */
  void give_details(const std::string &details);

  /*
  Add to fit_results the dendely spaced points obtained fixing all the
  variables but the idx-th. For this one, the values are found between 'a' and
  'b'.
  Example: f(x,y,z) . If idx=1 and x_fixed={1,2}, then: eps=(b-a)/N and the
  calculated values are f(1,a,2), f(1,a+eps,2), ..., f(1,b,2).
  */
  void add_dense_th_idx(const std::string &info, const int &idx,
                        const std::vector<U> &x_fixed, const int &N_jkf,
                        const U &a, const U &b, const int &N);

  /*! prints out the results of the fit */
  void print_fit_results(const std::string &dest, const std::string &sep = "\t",
                         const int &prec = 16, const int &nd = 2);

  Silibs2::jkf::matrix<U> get_Y_th(); /*!< returns Y_th */

  //! adds info about the convergence to the fit results
  void store_conv_info();

  // // reference to (*this).fit_results
  // Silibs2::jkf::results<U> &get_fit_results() const;
};

#endif
} // namespace nD
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2