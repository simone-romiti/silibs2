// fit_Minuit_v2.hpp
/*! This file defines what is declared in fit_Minuit2.h */

#include "./base_fit.hpp"

#include "./fit_xexyey.h"

namespace Silibs2 {
namespace jkf {
namespace Minuit2_fit {
namespace nD {
#ifndef Silibs2_jkf_Minuit2_fit_nD_fit_xexyey_hpp
#define Silibs2_jkf_Minuit2_fit_nD_fit_xexyey_hpp

template <class U> fit_xexyey<U>::fit_xexyey() {}  // end default contructor
template <class U> fit_xexyey<U>::~fit_xexyey() {} // end default contructor

template <class U>
void fit_xexyey<U>::set_data(const jkf::array3d<U> &X_jkf,
                             const jkf::matrix<U> &Y_jkf) {
  (*this).X = X_jkf;        /*!< jackknifes for the x values */
  (*this).Y = Y_jkf;        /*!< jackknifes for the y values */
  (*this).data_init = true; /*!< data have been initialized */
} // set_data()

template <class U> int fit_xexyey<U>::get_n_var() const {
  return X[0].rows(); // number of independent variables
}

template <class U> int fit_xexyey<U>::get_n_pts() const {
  this->check_data_init();
  return ((*this).Y).rows();
} // get_n_pts()

template <class U> int fit_xexyey<U>::get_n_dof() const {
  const int npt = this->get_n_pts();
  const int npa = this->get_n_pars();
  return npt - npa;
} // get_n_pts()

template <class U> int fit_xexyey<U>::get_N_jkf() const {
  this->check_data_init();
  return ((*this).Y).cols();
} // get_N_jkf()

template <class U> void fit_xexyey<U>::set_n_dof_auto() {
  this->check_data_init(); /*!< data initialization */
  this->check_pars_init(); /*!< parameters initialization */

  if (!(*this).n_dof_maually_set) { // if n_dof_maually_set == false ...
    const int n_pts = this->get_n_pts();
    const int n_pars = this->get_n_pars();
    (*this).n_dof = Silibs2::Minuit2_fit::get_dof(n_pts, n_pars);
  }
}

template <class U>
void fit_xexyey<U>::set_ansatz(
    const std::function<U(const std::vector<U> &, const std::vector<U> &)>
        &ansatz) {
  (*this).ANSATZ = ansatz; /*!< setting the ansatz */
  this->set_n_dof_auto();  // check also data and parameters

  /*! automatic deduction of the CH2_RED_FUN function */
  auto ch2_fun = [&](const Tables::table<U> &x, const Tables::table<U> &ex,
                     const std::vector<U> &y, const std::vector<U> &ey,
                     const std::vector<U> &p) {
    return Silibs2::Minuit2_fit::nD::ch2_red_ansatz_xexyey_value(
        (*this).ANSATZ, x, ex, y, ey, p, (*this).n_dof);
  };
  this->set_ch2_red_fun_single_jkf(ch2_fun);
} // end set_ansatz()

template <class U>
void fit_xexyey<U>::set_ch2_red_fun_single_jkf(
    const std::function<U(const Tables::table<U> &x, const Tables::table<U> &ex,
                          const std::vector<U> &y, const std::vector<U> &ey,
                          const std::vector<U> &p)> &res) {
  (*this).ch2_red_fun_single_jkf = res;
  (*this).residue_init = true;
} // set_ch2_red_single_jkf()

template <class U> Tables::table<U> fit_xexyey<U>::get_x(const int &k) const {
  const int n_var = this->get_n_var();
  const int n_pts = this->get_n_pts();
  Tables::table<U> x(n_var, n_pts);
  for (int j = 0; j < n_pts; ++j) {
    for (int i = 0; i < n_var; ++i) {
      x(i, j) = X(j, i, k); // j-th point, i-th variable, k-th jkf
    }
  }
  return x;
}

template <class U> Tables::table<U> fit_xexyey<U>::get_ex() const {
  const int n_var = this->get_n_var();
  const int n_pts = this->get_n_pts();
  Tables::table<U> ex(n_var, n_pts); // erros on the xi for each point
  for (int j = 0; j < n_pts; ++j) {
    ex.set_col(j, X[j].errs());
  }
  return ex;
}

template <class U>
Tables::table<U> fit_xexyey<U>::get_cols_xy(const jkf::array3d<U> &X_vec,
                                            const jkf::matrix<U> &Y_jkf) const {
  const int n_pts = X_vec.shape()[0];
  const int n_var = X_vec[0].rows();
  Tables::table<U> cols_xy(n_pts, 2 * (n_var + 1));
  const jkf::matrix<U> ae_Y = Y_jkf.ae(); // avr+err for each y point
  for (int j = 0; j < n_pts; ++j) {
    // average and error for each x point
    const jkf::matrix<U> ae_Xj = X_vec[j].ae();
    for (int i = 0; i < n_var; ++i) {
      cols_xy(j, 2 * i) = ae_Xj(i, 0);     // average
      cols_xy(j, 2 * i + 1) = ae_Xj(i, 1); // error
    }
    cols_xy(j, 2 * n_var) = ae_Y(j, 0);     // average
    cols_xy(j, 2 * n_var + 1) = ae_Y(j, 1); // error
  }
  return cols_xy;
}

template <class U>
Tables::table<U>
fit_xexyey<U>::get_cols_xyyth(const jkf::array3d<U> &X_vec,
                              const jkf::matrix<U> &Y_jkf,
                              const jkf::matrix<U> &Y_th_jkf) const {
  const std::array<int, 3> shp = X_vec.shape();
  const int n_pts = shp[0];
  const int n_var = shp[1];
  Tables::table<U> cols_xyyth(n_pts, 2 * (n_var + 2));
  const jkf::matrix<U> ae_Y = Y_jkf.ae();      // avr+err for each y point
  const jkf::matrix<U> ae_Yth = Y_th_jkf.ae(); // avr+err for each yth point
  for (int j = 0; j < n_pts; ++j) {
    // average and error for each x point
    const jkf::matrix<U> ae_Xj = X_vec[j].ae();
    for (int i = 0; i < n_var; ++i) {
      cols_xyyth(j, 2 * i) = ae_Xj(i, 0);     // average
      cols_xyyth(j, 2 * i + 1) = ae_Xj(i, 1); // error
    }
    cols_xyyth(j, 2 * n_var) = ae_Y(j, 0);       // average
    cols_xyyth(j, 2 * n_var + 1) = ae_Y(j, 1);   // error
    cols_xyyth(j, 2 * n_var + 2) = ae_Yth(j, 0); // average
    cols_xyyth(j, 2 * n_var + 3) = ae_Yth(j, 1); // error
  }
  return cols_xyyth;
}

template <class U> void fit_xexyey<U>::fit() {
  const int n_pars = this->get_n_pars(); /*!< number of parameters */
  const int n_pts = this->get_n_pts();   /*!< number of points */
  const int N_jkf = this->get_N_jkf();   /*!< number of jackknifes */
  this->set_n_dof_auto();

  ((*this).PARS).resize(n_pars, N_jkf); /*! jkf of fit parameters */
  ((*this).CH2_RED).resize(1, N_jkf);   /*!< jkf of reduced chi squadred */

  Tables::table<U> ex = this->get_ex();
  const std::vector<U> ey = Y.errs();  /*!< errors on the y */
  ((*this).Y_th).resize(n_pts, N_jkf); /*!< jkf of the theoretical values */

  // jkf indices for which the fit did not converge
  const bool verb = (*this).verbose;
  for (int k = 0; k < N_jkf; k++) { /*! loop over jackknifes */
    if (verb) {
      std::cout << "Jackknife number : " << k << "\n";
    }

    const Tables::table<U> x = this->get_x(k);
    const std::vector<U> y = Y.col(k); /*!< k-th jackknife */

    Silibs2::Minuit2_fit::nD::fit_xexyey<U> mini; /*!< fit class */
    mini.set_verbose(verb);                       // same verbosity

    mini.set_pars((*this).NAMES, (*this).GUESS, (*this).BOUND_L,
                  (*this).BOUND_R); /*!< specifying the parameters */

    mini.set_data(x, ex, y, ey); /*!< setting the data */

    mini.set_n_dof_manually((*this).n_dof);
    mini.set_ansatz((*this).ANSATZ); /*!< setting the ansatz */

    mini.fit(); /*!< doing the fit */

    if (!mini.fit_did_converge()) {
      (*this).jkf_not_conv.push_back(k);
    }

    /*! setting the k-th jkf of: */

    ((*this).PARS).set_col(mini.get_PARS(), k);     /*!< parameters */
    ((*this).CH2_RED)(0, k) = mini.get_CH2_RED();   /*!< reduced ch2 */
    ((*this).Y_th).set_col(mini.get_vec_y_th(), k); /*!< theoretical y */
  }

  /*! The fit has been done */
  (*this).fit_done = true;
} // fit()

template <class U> void fit_xexyey<U>::store_results() {
  this->check_fit_done();              // check if the fit was done
  const int n_pts = this->get_n_pts(); // number of points

  ((*this).fit_results).set_title("Results of the xexyey fit"); /*!< Title */
  this->store_conv_info();

  std::stringstream ipf;                          // Info Points of the Fit
  ipf << "n_var : " << this->get_n_var() << "\n"; // number of variables
  ipf << "n_pts : " << this->get_n_pts() << "\n"; // number of points
  ipf << "n_par : " << this->get_n_par() << "\n"; // number of parameters
  // ipf << "n_dof : " << this->get_n_dof() << "\n"; // degrees of freedom
  this->give_details(ipf.str()); // adding to the log file

  /*! Parameters with their names */
  ((*this).fit_results).add_jkf_rows((*this).PARS, (*this).NAMES);
  ((*this).fit_results).add_jkf((*this).CH2_RED, "CH2_RED"); /*!< chi^2_red*/
  for (int j = 0; j < n_pts; ++j) {
    /*! jkf of the n_var-tuples for each point */
    ((*this).fit_results).add(X[j], "#jkf#data_x_" + std::to_string(j + 1));
  }
  ((*this).fit_results).add(Y, "#jkf#data_y"); /*!< adding jkf of y values */

  /*
  columns with the individual variables for each point and the yth
  are added to the fit results
  */
  const Tables::table<U> cols_xy = this->get_cols_xyyth(X, Y, (*this).Y_th);
  ((*this).fit_results).add(cols_xy, "#plot#xiexiyeyytheyth");

} // store_results()

template <class U>
void fit_xexyey<U>::add_dense_th(const std::string &info, const int &idx,
                                 const std::vector<U> &x_fixed, const int &N) {
  /* storing a dense list of theoretical points */
  if ((int)x_fixed.size() != this->get_n_var() - 1) {
    std::cerr << "Error. You sould fix (n_var-1) variables.\n";
    std::cerr << "Number of fixed variables :" << x_fixed.size() << "\n";
    std::cerr << "n_var - 1 :" << this->get_n_var() - 1 << "\n";
    std::cerr << "Aborting.\n";
    abort();
  }

  jkf::matrix<U> X_idx = X.slice_1(idx);
  // finding the minimum and maximum value of the 'x'
  U a = Functions::min_value(X_idx.avrs());
  U b = Functions::max_value(X_idx.avrs());

  if ((*this).extr_x) { // dense points include the extrapolation point
    const U x0 = (*this).X_extr_imp.avrs()[idx];
    a = Functions::min_value<U>({a, x0});
    b = Functions::max_value<U>({b, x0});
  }

  this->add_dense_th_idx(info, idx, x_fixed, this->get_N_jkf(), a, b, N);
} // add_dense_th()

template <class U>
void fit_xexyey<U>::add_Yth_xi_fixed(const int &i, const U &x0,
                                     const std::string &label) {
  const int n_pts = this->get_n_pts(); // number of points
  const int Nj = this->get_N_jkf();
  jkf::array3d<U> X_new = (*this).X;
  jkf::matrix<U> Y_new(n_pts, Nj);

  for (int p = 0; p < n_pts; ++p) {
    for (int j = 0; j < Nj; ++j) {
      // point p, i-th variable, j-th jackknife
      X_new(p, i, j) = x0;
      Y_new(p, j) =
          (*this).ANSATZ(X_new.slice_2(j).row(p), ((*this).PARS).col(j));
    }
  }

  const Tables::table<U> cols_xy = this->get_cols_xy(X_new, Y_new);
  ((*this).fit_results).add(cols_xy, "#plot#xiexiytheyth" + label);
}

#endif
} // namespace nD
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
