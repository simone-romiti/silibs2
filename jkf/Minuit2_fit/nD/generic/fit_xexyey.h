// fit_xexyey.h
/*
This file declares routines allowing to fit a generic std::function
using the Minuit library, with jkf data in the format xyey (i.e. no error on the
x)
*/

#include "./base_fit.h"

namespace Silibs2 {
namespace jkf {
namespace Minuit2_fit {
namespace nD {
#ifndef Silibs2_jkf_Minuit2_fit_nD_fit_xexyey_h
#define Silibs2_jkf_Minuit2_fit_nD_fit_xexyey_h

//! 1-dimensional fit
template <class U = double>
class fit_xexyey : public Silibs2::jkf::Minuit2_fit::nD::base_fit<U> {

protected:
  /*! vector of jkf matrices, one for each point, e.g.
  X = {x_1, x_2, ..., x_npts} means that x1 contains
  N_jkf columns made as:
  (x_1)_1, ..., (x_1)_nvar
  */
  // std::vector<Silibs2::jkf::matrix<U>> X;
  Silibs2::jkf::array3d<U> X;

  /*jkf for the Y */
  Silibs2::jkf::matrix<U> Y;

  /*!
  Residue function for a single jackknife
  row index    = index of variable
  column index =  index of point
  */
  std::function<U(const Tables::table<U> &, const Tables::table<U> &,
                  const std::vector<U> &, const std::vector<U> &,
                  const std::vector<U> &)>
      ch2_red_fun_single_jkf =
          [](const Tables::table<U> &x, const Tables::table<U> &ex,
             const std::vector<U> &y, const std::vector<U> &ey,
             const std::vector<U> &p) { return 0.0; };

public:
  fit_xexyey();
  ~fit_xexyey();

  // /*! setting the jkf of the data */
  // void set_data(const std::vector<Silibs2::jkf::matrix<U>> &X_jkf,
  //               const Silibs2::jkf::matrix<U> &Y_jkf);

  /*! setting the jkf of the data */
  void set_data(const Silibs2::jkf::array3d<U> &X_jkf_3d,
                const Silibs2::jkf::matrix<U> &Y_jkf);

  int get_n_var() const; // number of variables
  int get_n_pts() const; /*!< number of points */
  int get_n_dof() const; /*!< number of degrees of freedom */

  int get_N_jkf() const; /*!< number of jackknife */

  // set n_dof = n_pts-n_pars unless n_dof has been set manually
  void set_n_dof_auto();

  /*!
  Sets the ansatz to fit and automatically deduce the
  reduced chi squared function
  */
  virtual void set_ansatz(
      const std::function<U(const std::vector<U> &, const std::vector<U> &)>
          &ansatz);

  /*! Setting the residue function for a single jkf */
  void set_ch2_red_fun_single_jkf(
      const std::function<U(const Tables::table<U> &, const Tables::table<U> &,
                            const std::vector<U> &, const std::vector<U> &,
                            const std::vector<U> &)> &res);

  /*
  returns the table of x points for the jackknife index 'j'
  row -> variable
  column -> point
  */
  Tables::table<U> get_x(const int &k) const;

  /*
  returns the table of ex points
  row -> variable
  column -> point
  */
  Tables::table<U> get_ex() const;

  /*
  table in the xiexiyey format, i.e.
  row index -> point
  column index -> variable(followed by its error),
  but the last 2 columns are "y ey"
  */
  Tables::table<U> get_cols_xy(const Silibs2::jkf::array3d<U> &X_vec,
                               const Silibs2::jkf::matrix<U> &Y_jkf) const;

  Tables::table<U>
  get_cols_xyyth(const Silibs2::jkf::array3d<U> &X_vec,
                 const Silibs2::jkf::matrix<U> &Y_jkf,
                 const Silibs2::jkf::matrix<U> &Y_th_jkf) const;

  void fit();           /*!< do the fit */
  void store_results(); // store the results of the fit
  void add_dense_th(const std::string &info, const int &idx,
                    const std::vector<U> &x_fixed, const int &N = 100);

  /*
  Adds to fit_results the values of Y_th
  calculated for the i-th variable fixed to x0 */
  void add_Yth_xi_fixed(const int &i, const U &x0, const std::string &label);
};

#endif
} // namespace nD
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2