// fit.h
/*! All fitting libraries */

// 1D fits
#include "./oneD/constant/constant.hpp"
#include "./oneD/generic/generic.hpp"
#include "./oneD/polynomial/polynomial.hpp"

// nD fits
// #include "./nD/constant/constant.hpp" -> TO BE IMPLEMENTED
#include "./nD/generic/generic.hpp"
// #include "./nD/polynomial/polynomial.hpp" -> TO BE IMPLEMENTED
