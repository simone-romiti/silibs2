// nD_paraboloid.cpp

#include "Silibs2.h"

using namespace Silibs2;

int main(int argc, char *argv[]) {

  std::random_device rd;  //  seed for the random number engine
  std::mt19937 gen(rd()); // Standard mersenne_twister_engine seeded with rd()
  std::uniform_real_distribution<> dis(0.0, 6.0);
  std::uniform_real_distribution<> dis_jkf(0.0, 0.05);

  const int n_var = 2;
  const int n_pts = 10;
  const int N_jkf = 10;

  std::vector<jkf::matrix<double>> x(n_pts);
  std::vector<jkf::matrix<double>> ex(n_pts);
  jkf::matrix<double> y(n_pts, N_jkf);
  jkf::matrix<double> ey(n_pts, N_jkf);
  for (int j = 0; j < n_pts; ++j) {
    x[j].resize(n_var, N_jkf);
    ex[j].resize(n_var, N_jkf);
    for (int k = 0; k < N_jkf; ++k) {
      const double ex_rand = dis_jkf(gen);
      const double ey_rand = dis_jkf(gen);
      for (int i = 0; i < n_var; ++i) {
        x[j](i, k) = dis(gen) + ex_rand;
      }
      y(j, k) = std::pow(x[j](0, k), 2) + std::pow(x[j](1, k), 2) + ey_rand;
    }
  }

  Silibs2::jkf::Minuit2_fit::nD::fit_xexyey<double> minu;
  minu.set_pars({"a", "b"}, {0.5, 1.4});
  minu.set_data(x, y);
  std::function<double(const std::vector<double> &,
                       const std::vector<double> &)>
      ansatz = [](const std::vector<double> &z, const std::vector<double> &p) {
        return p[0] * std::pow(z[0], 2) + p[1] * std::pow(z[1], 2);
      };

  minu.set_ansatz(ansatz);
  minu.fit();

  return 0;
}