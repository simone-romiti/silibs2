# Silibs::jkf::Minuit2_fit

In this folder are defined the classes needed to do the fit of jackknife data using Minuit2.

The class *base_fit* it the one from which the others inherit. The classes implemented do far are:

1. **fit_xyey**
2. **fit_xexyey**
