// deps.h
// dependencies all in one file

/*! base class for 1D ansatz fitting */
#include "../../../Minuit2_fit/fit.hpp"
#include "../../matrix.hpp"
#include "../../results.hpp"
