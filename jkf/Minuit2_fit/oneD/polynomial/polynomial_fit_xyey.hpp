// fit_Minuit_v2.hpp
/*! This file defines what is declared in fit_Minuit2.h */

#include "../generic/fit_xyey.hpp"
#include "./polynomial_fit_xyey.h"

namespace Silibs2 {
namespace jkf {

namespace Minuit2_fit {
#ifndef Silibs2_jkf_Minuit2_fit_polynomial_fit_xyey_hpp
#define Silibs2_jkf_Minuit2_fit_polynomial_fit_xyey_hpp

template <class U>
polynomial_fit_xyey<U>::polynomial_fit_xyey() {} // default contructor

template <class U>
polynomial_fit_xyey<U>::~polynomial_fit_xyey() {} // default destructor

template <class U> void polynomial_fit_xyey<U>::set_n_deg(const int &n) {
  if (n < 0) {
    std::cerr
        << "Error. The degree of a polynomial must be not-negative. Aborting."
        << "\n";
    abort();
  }

  const std::function<U(const U &, const std::vector<U> &)> ansatz =
      [](const U &x0, const std::vector<U> &an) -> U {
    return Silibs2::Functions::polynomial(x0, an);
  };

  this->check_data_init();
  this->set_ansatz(ansatz);
} // set_n_deg()

template <class U>
void polynomial_fit_xyey<U>::set_pars_auto(
    const std::vector<std::string> &names, const std::vector<double> &guesses) {
  const int n = names.size();     /*!< degree of the polynomial */
  this->set_pars(names, guesses); /*!< setting the parameters */
  this->set_n_deg(n);
} // set_pars_auto()

template <class U>
void polynomial_fit_xyey<U>::set_pars_auto(
    const std::vector<std::string> &names) {
  const int n = names.size();           /*!< degree of the polynomial */
  const std::vector<U> guesses(n, 1.0); /*!< boundless guesses*/
  this->set_pars_auto(names, guesses);  /*!< setting the parameters */
} // set_pars_auto()

#endif
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
