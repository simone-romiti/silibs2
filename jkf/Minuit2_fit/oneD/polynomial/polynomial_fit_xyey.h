// polynomial_fit_xyey.h
/*! Specialization of fit_xyey to a polynomial ansatz */

#include "../generic/fit_xyey.h"

namespace Silibs2 {
namespace jkf {
namespace Minuit2_fit {
#ifndef Silibs2_jkf_Minuit2_fit_polynomial_fit_xyey_h
#define Silibs2_jkf_Minuit2_fit_polynomial_fit_xyey_h

//! 1-dimensional fit
template <class U = double>
class polynomial_fit_xyey : public Silibs2::jkf::Minuit2_fit::fit_xyey<U> {

public:
  polynomial_fit_xyey();
  ~polynomial_fit_xyey();

  // set the degrgee of the polynomial, in the sense that the qansatz is
  // automatically deduced
  void set_n_deg(const int &n);

  /*! you can specify only the degree of the polynomial */
  /*! the degree of the polynomial is set equal to names.size() */
  void set_pars_auto(const std::vector<std::string> &names,
                     const std::vector<double> &guesses);
  void set_pars_auto(const std::vector<std::string> &names);
};

#endif
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
