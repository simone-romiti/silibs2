// fit_Minuit_v2.hpp
/*! This file defines what is declared in fit_Minuit2.h */

#include "./base_fit.hpp"
#include "./fit_xexyey.h"

namespace Silibs2 {
namespace jkf {

namespace Minuit2_fit {
#ifndef Silibs2_jkf_Minuit2_fit_fit_xexyey_hpp
#define Silibs2_jkf_Minuit2_fit_fit_xexyey_hpp

template <class U> fit_xexyey<U>::fit_xexyey() {}  // end default contructor
template <class U> fit_xexyey<U>::~fit_xexyey() {} // end default contructor

template <class U>
void fit_xexyey<U>::set_data(const Silibs2::jkf::matrix<U> &X_jkf,
                             const Silibs2::jkf::matrix<U> &Y_jkf) {
  (*this).X = X_jkf;        /*!< jackknifes for the x values */
  (*this).Y = Y_jkf;        /*!< jackknifes for the y values */
  (*this).data_init = true; /*!< data have been initialized */
} // set_data()

template <class U> int fit_xexyey<U>::get_n_pts() {
  this->check_data_init();
  return ((*this).Y).rows();
} // get_n_pts()

template <class U> int fit_xexyey<U>::get_N_jkf() {
  this->check_data_init();
  return ((*this).Y).cols();
} // get_N_jkf()

template <class U> void fit_xexyey<U>::set_n_dof_auto() {
  this->check_data_init(); /*!< data initialization */
  this->check_pars_init(); /*!< parameters initialization */

  if (!(*this).n_dof_maually_set) { // if n_dof_maually_set = false ...
    const int n_pts = X.rows() + Y.rows();
    const int n_pars = this->get_n_pars();
    (*this).n_dof = Silibs2::Minuit2_fit::get_dof(n_pts, n_pars);
  }
}

template <class U> void fit_xexyey<U>::set_n_sigma_Bayes(const double &n) {
  (*this).n_sigma_Bayes = n;
} // set_n_sigma_Bayes()

template <class U>
void fit_xexyey<U>::set_ansatz(
    const std::function<U(const U &, const std::vector<U> &)> &ansatz) {

  (*this).ANSATZ = ansatz; /*!< setting the ansatz */
  this->set_n_dof_auto();  // check also data and parameters

  /*! automatic deduction of the CH2_RED_FUN function */
  auto ch2_fun = [&](const std::vector<U> &x, const std::vector<U> &ex,
                     const std::vector<U> &y, const std::vector<U> &ey,
                     const std::vector<U> &p) {
    return Silibs2::Minuit2_fit::ch2_red_ansatz_xexyey_value(
        (*this).ANSATZ, x, ex, y, ey, p, (*this).n_dof);
  };
  this->set_ch2_red_fun_single_jkf(ch2_fun);
} // end set_ansatz()

template <class U>
void fit_xexyey<U>::set_ch2_red_fun_single_jkf(
    const std::function<U(const std::vector<U> &x, const std::vector<U> &ex,
                          const std::vector<U> &y, const std::vector<U> &ey,
                          const std::vector<U> &p)> &res) {
  (*this).ch2_red_fun_single_jkf = res;
  (*this).residue_init = true;
} // set_ch2_red_single_jkf()

template <class U> void fit_xexyey<U>::fit() {
  const int n_pars = this->get_n_pars(); /*!< number of parameters */
  const int n_pts = this->get_n_pts();   /*!< number of points */
  const int N_jkf = this->get_N_jkf();   /*!< number of jackknifes */
  this->set_n_dof_auto();

  ((*this).PARS).resize(n_pars, N_jkf); /*! jkf of fit parameters */
  ((*this).CH2_RED).resize(1, N_jkf);   /*!< jkf of reduced chi squadred */

  const std::vector<U> ex = X.errs();  /*!< errors on the x */
  const std::vector<U> ey = Y.errs();  /*!< errors on the y */
  ((*this).Y_th).resize(n_pts, N_jkf); /*!< jkf of the theoretical values */

  /*! loop over jackknifes */
  for (int k = 0; k < N_jkf; k++) {
    if ((*this).verbose) {
      std::cout << "Jacknife number " << k << "\n";
    }
    const std::vector<U> x = ((*this).X).col(k); /*!< k-th jackknife */
    const std::vector<U> y = ((*this).Y).col(k); /*!< k-th jackknife */

    // /*! residue function of the k-th jackknife */

    Silibs2::Minuit2_fit::fit_xexyey<U> mini; /*!< fit class */
    mini.set_verbose((*this).verbose);        // same verbosity for each jkf

    mini.set_pars((*this).NAMES, (*this).GUESS, (*this).BOUND_L,
                  (*this).BOUND_R); /*!< specifying the parameters */

    mini.set_data(x, ex, y, ey); /*!< setting the data */

    mini.set_n_dof_manually((*this).n_dof);

    if ((*this).n_sigma_Bayes != 0) {
      mini.set_n_sigma_Bayes((*this).n_sigma_Bayes);
    }

    mini.set_ansatz((*this).ANSATZ); /*!< setting the ansatz */

    mini.fit(); /*!< doing the fit */
    if (!mini.fit_did_converge()) {
      ((*this).jkf_not_conv).push_back(k);
    }

    /*! setting the k-th jkf of: */
    ((*this).PARS).set_col(mini.get_PARS(), k);     /*!< parameters */
    ((*this).CH2_RED)(0, k) = mini.get_CH2_RED();   /*!< reduced ch2 */
    ((*this).Y_th).set_col(mini.get_vec_y_th(), k); /*!< theoretical y */
  }

  /*! The fit has been done */

  (*this).fit_done = true;
} // fit()

template <class U>
void fit_xexyey<U>::store_results(const bool &dense_th, const int &N) {
  this->check_fit_done();              // check if the fit was done
  const int N_jkf = this->get_N_jkf(); // number of jackknifes

  /*! ---------------------------------------------------------- */
  /*! Storing the results of the fit in the object 'fit_results' */
  /*! ---------------------------------------------------------- */

  // reference to the variable (it's just a shotcut)
  jkf::results<U> &fr = (*this).fit_results;

  fr.set_title("Results of the xexyey fit"); /*!< Title */
  this->store_conv_info();

  /*! Parameters with their names */
  fr.add_jkf_rows((*this).PARS, (*this).NAMES);
  fr.add_jkf((*this).CH2_RED, "CH2_RED"); /*!< chi^2_red*/
  fr.add(X, "#jkf#data_x");               /*!< adding jkf of y values */
  fr.add(Y, "#jkf#data_y");               /*!< adding jkf of y values */

  /*! data in the "x ex y ey y_th ey_th" format */
  jkf::matrix<U> x_ex_y_ey_yth_eyth =
      jkf::build_x_ex_y_ey_yth_eyth(X, Y, (*this).Y_th);
  fr.add(x_ex_y_ey_yth_eyth, {"#x_ex_y_ey_yth_eyth"});

  if (dense_th) {
    /* plotting a dense list of theoretical points */

    // finding the minimum and maximum value of the 'x'
    U a = Functions::min_value(X.avrs());
    U b = Functions::max_value(X.avrs());

    if ((*this).extr_x) { // dense points include the extrapolation point
      const U x0 = (*this).X_extr_imp.avrs()[0];
      a = Functions::min_value<U>({a, x0});
      b = Functions::max_value<U>({b, x0});
    }
    if ((*this).extr_y) { // dense points include the extrapolated x
      const U x0 = (*this).X_extr.avrs()[0];
      a = Functions::min_value<U>({a, x0});
      b = Functions::max_value<U>({b, x0});
    }

    this->add_dense_th(N_jkf, a, b, dense_th, N);
  }

} // store_results()

#endif
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
