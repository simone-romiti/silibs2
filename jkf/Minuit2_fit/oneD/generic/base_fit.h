// base_fit.h
/*
This file declares routines allowing to fit a generic std::function
using the Minuit library, with data in the format xyey (i.e. no error on the x)
*/

#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <numeric>
#include <string>

#include "../deps.h"

namespace Silibs2 {
namespace jkf {
namespace Minuit2_fit {
#ifndef Silibs2_jkf_Minuit2_fit_base_fit_h
#define Silibs2_jkf_Minuit2_fit_base_fit_h

//! 1-dimensional fit
template <class U = double>
class base_fit : public Silibs2::Minuit2_fit::base<U> {
protected:
  /*! control variables */
  bool data_init = false;    /*!< true if data have been set */
  bool residue_init = false; /*!< true if the residue function has been set */
  bool fit_done = false;     /*!< true when the fit has been done */
  bool verbose = false;      // verbosity of the output

  /*! Ansatz to be fitted (if unspecified it is the null function)*/
  std::function<U(const U &, const std::vector<U> &)> ANSATZ =
      [](const U &, const std::vector<U> &) { return 0; };

  /*! jkf of Best-fit values for the parameters */
  Silibs2::jkf::matrix<U> PARS;

  /*!
  jkf of residue (reduced chi squared) i.e. the value resulting from the
  minimization.
  */
  Silibs2::jkf::matrix<U> CH2_RED;

  /*!
  Theoretical values obtained from the ansatz
  with the best fit values for the parameters
  */
  Silibs2::jkf::matrix<U> Y_th;

  /*!
  Theoretical value of the extrapolation to a given point,
  for instance the value of y_th at the physical point of the variable x
  */
  Silibs2::jkf::matrix<U> X_extr;     /*!< x0 such that y_th(x0) = y_extrap */
  Silibs2::jkf::matrix<U> Y_extr_imp; /*!< imposed value of y to find X_extr*/

  Silibs2::jkf::matrix<U> X_extr_imp; /*!< imposed value of x to find Y_extr */
  Silibs2::jkf::matrix<U> Y_extr;     /*!< y_th(x_extrap)*/
  bool extr_x = false; /*!< true if 'y' has been extrapolated wrt 'x' */
  bool extr_y = false; /*!< true if 'x' has been extrapolated wrt 'y' */

  std::vector<int> jkf_not_conv;
  Silibs2::jkf::results<U> fit_results; /*!< results of the fit in one file */
  std::string results_subtit = "";      // subtitle of the fit results

public:
  base_fit();
  ~base_fit();

  void set_verbose(const bool &v); // (*this).verbose = v
  void check_data_init();          /*!< Checks if data were passed */
  void check_residue_init();       /*!< Checks if residue_init is true */

  /*!
  Checks if all the conditions necessary
  to do the fit have been fullfilled
  */
  void check_fit_init();
  void check_fit_done(); /*!< checks if the fit was done */

  Silibs2::jkf::matrix<U> get_PARS();    /*!< jkf of best fit parameters */
  Silibs2::jkf::matrix<U> get_CH2_RED(); /*!< jkf of best fit ch2_red */

  /*!< set X_extr (suitable when the ansatz is analytically invertible) */
  void set_X_extr(const jkf::matrix<U> &x_extr_jkf, const U &y_extr);
  void find_X_extr(const U &y_extr, const U &a, const U &b, const U &dx,
                   const U &dy);     /*!< find x extrapolated to y_extr */
  jkf::matrix<U> get_X_extr() const; /*!< return the x extrapolated */

  void find_Y_extr(const std::vector<U> &
                       x_extr); /*!< find y extrapolated to the jkf of x_extr */
  void find_Y_extr(const U &x_extr); /*!< find y extrapolated to x_extr */
  jkf::matrix<U> get_Y_extr() const; /*!< return the y extrapolated */

  void give_details(const std::string &details); /*!< setting the subtitle of
                                                    the fit_results */

  /* add a dense list of theoretical points to the fit_results */
  void add_dense_th(const int &N_jkf, const U &a, const U &b,
                    const bool &dense_th, const int &N = 100);

  /*!
  prints out the results of the fit
  nd = number of digits for the smart printing of CH2_RED
  */
  void print_fit_results(const std::string &dest, const std::string &sep = "\t",
                         const int &prec = 16, const int &nd = 2);

  Silibs2::jkf::matrix<U> get_Y_th(); /*!< returns Y_th */

  //! adds info about the convergence to the fit results
  void store_conv_info();
};

#endif
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
