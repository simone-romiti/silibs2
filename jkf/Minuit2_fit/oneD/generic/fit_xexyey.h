// fit_xexyey.h
/*
This file declares routines allowing to fit a generic std::function
using the Minuit library, with jkf data in the format xyey (i.e. no error on the
x)
*/

#include "./base_fit.h"

namespace Silibs2 {
namespace jkf {

namespace Minuit2_fit {
#ifndef Silibs2_jkf_Minuit2_fit_fit_xexyey_h
#define Silibs2_jkf_Minuit2_fit_fit_xexyey_h

//! 1-dimensional fit
template <class U = double>
class fit_xexyey : public Silibs2::jkf::Minuit2_fit::base_fit<U> {

protected:
  /*! x,y,ey */
  Silibs2::jkf::matrix<U> X;
  double n_sigma_Bayes = 0;

  Silibs2::jkf::matrix<U> Y;

  /*! Residue function for a single jackknife */
  std::function<U(const std::vector<U> &, const std::vector<U> &,
                  const std::vector<U> &, const std::vector<U> &,
                  const std::vector<U> &)>
      ch2_red_fun_single_jkf =
          [](const std::vector<U> &x, const std::vector<U> &ex,
             const std::vector<U> &y, const std::vector<U> &ey,
             const std::vector<U> &p) { return 0.0; };

public:
  fit_xexyey();
  ~fit_xexyey();

  /*! setting the jkf of the data */
  void set_data(const Silibs2::jkf::matrix<U> &X_jkf,
                const Silibs2::jkf::matrix<U> &Y_jkf);

  int get_n_pts(); /*!< number of points */
  int get_N_jkf(); /*!< number of jackknife */

  // set n_dof = n_pts-n_pars unless n_dof has been set manually
  void set_n_dof_auto();

  // number of sigma 1for varying the x_B
  // (pseudo-Bayesian parameters for the xi)
  void set_n_sigma_Bayes(const double &n);

  /*!
  Sets the ansatz to fit and automatically deduce the
  reduced chi squared function
  */
  virtual void
  set_ansatz(const std::function<U(const U &, const std::vector<U> &)> &ansatz);

  /*! Setting the residue function for a single jkf */
  void set_ch2_red_fun_single_jkf(
      const std::function<U(const std::vector<U> &, const std::vector<U> &,
                            const std::vector<U> &, const std::vector<U> &,
                            const std::vector<U> &)> &res);

  void fit(); /*!< do the fit */

  /*
  Storing the results of the fit in '(*this).fit_results'
  - PARS
  - CH2_RED
  - X, Y
  - x_ex_y_ey_yth_eyth
  - dense_th = true -> 'N' theoretical densely spaced points between min and max
    (if an extrapolation is done, also that point is considered)
  */
  void store_results(const bool &dense_th = true, const int &N = 100);
};

#endif
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
