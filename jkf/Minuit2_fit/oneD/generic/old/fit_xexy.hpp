// fit_xexy.hpp
/*! This file defines what is declared in fit_xexy.h */

#include "./fit_xexy.h"

namespace Silibs2 {
namespace jkf {
namespace Minuit2_fit {
#ifndef Silibs2_jkf_Minuit2_fit_fit_xexy_hpp
#define Silibs2_jkf_Minuit2_fit_fit_xexy_hpp

template <class U> fit_xexy<U>::fit_xexy() {}  /*!< default contructor */
template <class U> fit_xexy<U>::~fit_xexy() {} /*!< default destructor */

template <class U>
void fit_xexy<U>::set_data(const Silibs2::jkf::matrix<U> &X_jkf,
                           const std::vector<U> &y_vals) {
  (*this).X = X_jkf;        /*!< jackknifes for the x values */
  (*this).vec_y = y_vals;   /*!< y values */
  (*this).data_init = true; /*!< data have been initialized */
} // set_data()

template <class U> int fit_xexy<U>::get_n_pts() {
  this->check_data_init();
  return ((*this).X).rows();
} // get_n_pts()

template <class U> int fit_xexy<U>::get_N_jkf() {
  this->check_data_init();
  return ((*this).X).cols();
} // get_N_jkf()

template <class U>
void fit_xexy<U>::set_ansatz(
    const std::function<U(const U &, const std::vector<U> &)> &ansatz) {

  (*this).ANSATZ = ansatz; /*!< setting the ansatz */

  /*! automatic deduction of the CH2_RED_FUN function */
  auto ch2_fun = [&](const std::vector<U> &x, const std::vector<U> &ex,
                     const std::vector<U> &y, const std::vector<U> &p) {
    return Silibs2::Minuit2_fit::ch2_red_ansatz_xexy_value((*this).ANSATZ, x,
                                                           ex, y, p);
  };
  this->set_ch2_red_fun_single_jkf(ch2_fun);
} // end set_ansatz()

template <class U>
void fit_xexy<U>::set_ch2_red_fun_single_jkf(
    const std::function<U(const std::vector<U> &x, const std::vector<U> &ex,
                          const std::vector<U> &y, const std::vector<U> &p)>
        &res) {
  (*this).ch2_red_fun_single_jkf = res;
  (*this).residue_init = true;
} // set_ch2_red_single_jkf()

template <class U> void fit_xexy<U>::fit() {
  const int n_pars = this->get_n_pars(); /*!< number of parameters */
  const int n_pts = this->get_n_pts();   /*!< number of points */
  const int N_jkf = this->get_N_jkf();   /*!< number of jackknifes */

  ((*this).PARS).resize(n_pars, N_jkf); /*! jkf of fit parameters */
  ((*this).CH2_RED).resize(1, N_jkf);   /*!< jkf of reduced chi squadred */

  const std::vector<U> ex = X.errs();  /*!< errors on the x */
  ((*this).Y_th).resize(n_pts, N_jkf); /*!< jkf of the theoretical values */

  /*! loop over jackknifes */
  for (int k = 0; k < N_jkf; k++) {
    std::vector<U> x = ((*this).X).col(k); /*!< k-th jackknife */

    Silibs2::Minuit2_fit::fit_xexy<U> mini; /*!< fit class */
    mini.set_data(x, ex, (*this).vec_y);    /*!< setting the data */
    mini.set_ansatz((*this).ANSATZ);        /*!< setting the ansatz */

    mini.set_pars((*this).NAMES, (*this).GUESS, (*this).BOUND_L,
                  (*this).BOUND_R); /*!< specifying the parameters */
    mini.fit();                     /*!< doing the fit */

    /*! setting the k-th jkf of: */

    ((*this).PARS).set_col(mini.get_PARS(), k);     /*!< parameters */
    ((*this).CH2_RED)(0, k) = mini.get_CH2_RED();   /*!< reduced ch2 */
    ((*this).Y_th).set_col(mini.get_vec_y_th(), k); /*!< theoretical y */
  }

  (*this).fit_done = true;

  /*! ---------------------------------------------------------- */
  /*! Storing the results of the fit in the object 'fit_results' */
  /*! ---------------------------------------------------------- */

  ((*this).fit_results).set_title("Results of the xexy fit"); /*!< Title */

  /*! Parameters with their names */
  ((*this).fit_results).add_jkf_rows((*this).PARS, (*this).NAMES);
  ((*this).fit_results).add_jkf((*this).CH2_RED, "CH2_RED"); /*!< chi^2_red*/
  ((*this).fit_results).add(X, "#jkf#data_x"); /*!< adding jkf of y values */
  jkf::matrix<U> Y = jkf::trivial_jkf({(*this).vec_y}, this->get_N_jkf());
  ((*this).fit_results).add(Y.ae(), "#jkf#y_ae"); /*!< adding y values */

  /*! data in the "x y ey y_th ey_th" format */
  jkf::matrix<U> x_ex_y_ey_yth_eyth =
      jkf::build_x_ex_y_ey_yth_eyth(X, Y, (*this).Y_th);
  std::string cols_names = "#x_ex_y_ey_yth_eyth";
  ((*this).fit_results).add(x_ex_y_ey_yth_eyth, cols_names);

  /*! The fit has been done */
  (*this).fit_done = true;
} // fit()

#endif
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
