// fit_xexy.h
/*
This file declares routines allowing to fit a generic std::function
using the Minuit library, with jkf data in the format xyey (i.e. no error on the
x)
*/

#include "./base_fit.hpp"

namespace Silibs2 {
namespace jkf {

namespace Minuit2_fit {
#ifndef Silibs2_jkf_Minuit2_fit_fit_xexy_h
#define Silibs2_jkf_Minuit2_fit_fit_xexy_h

//! 1-dimensional fit
template <class U = double>
class fit_xexy : public Silibs2::jkf::Minuit2_fit::base_fit<U> {

protected:
  Silibs2::jkf::matrix<U> X; /*!< jkf of the 'x' */
  std::vector<U> vec_y;      /*!< std::vector of 'y' */

  /*! Residue function for a single jackknife */
  std::function<U(const std::vector<U> &, const std::vector<U> &,
                  const std::vector<U> &, const std::vector<U> &)>
      ch2_red_fun_single_jkf =
          [](const std::vector<U> &x, const std::vector<U> &ex,
             const std::vector<U> &y, const std::vector<U> &p) { return 0.0; };

public:
  fit_xexy();
  ~fit_xexy();

  /*! setting the data */
  void set_data(const Silibs2::jkf::matrix<U> &X_jkf,
                const std::vector<U> &y_vals);

  int get_n_pts(); /*!< number of points */
  int get_N_jkf(); /*!< number of jackknifes */

  /*!
  Sets the ansatz to fit and automatically deduce the
  reduced chi squared function
  */
  void
  set_ansatz(const std::function<U(const U &, const std::vector<U> &)> &ansatz);

  /*! Setting the residue function for a single jkf */
  void set_ch2_red_fun_single_jkf(
      const std::function<U(const std::vector<U> &, const std::vector<U> &,
                            const std::vector<U> &, const std::vector<U> &)>
          &res);

  void fit(); /*!< do the fit */
};

#endif
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
