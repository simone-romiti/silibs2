// fit_xyey.hpp
/*! This file defines what is declared in fit_Minuit2.h */

#include "./base_fit.hpp"
#include "./fit_xyey.h"

namespace Silibs2 {
namespace jkf {

namespace Minuit2_fit {
#ifndef Silibs2_jkf_Minuit2_fit_fit_xyey_hpp
#define Silibs2_jkf_Minuit2_fit_fit_xyey_hpp

template <class U> fit_xyey<U>::fit_xyey() {}  // default contructor
template <class U> fit_xyey<U>::~fit_xyey() {} // default destructor

template <class U>
void fit_xyey<U>::set_data(const std::vector<U> &x_i,
                           const Silibs2::jkf::matrix<U> &Y_jkf) {
  (*this).x = x_i;
  (*this).Y = Y_jkf;
  (*this).data_init = true;
} // set_data()

template <class U> int fit_xyey<U>::get_n_pts() {
  this->check_data_init();
  return ((*this).Y).rows();
}

template <class U> int fit_xyey<U>::get_N_jkf() {
  this->check_data_init();
  return ((*this).Y).cols();
}

template <class U> void fit_xyey<U>::set_n_dof_auto() {
  this->check_data_init(); /*!< data initialization */
  this->check_pars_init(); /*!< parameters initialization */

  if (!(*this).n_dof_maually_set) { // if n_dof_maually_set = false ...
    const int n_pts = Y.rows(), n_pars = this->get_n_pars();
    (*this).n_dof = Silibs2::Minuit2_fit::get_dof(n_pts, n_pars);
  }
}

template <class U>
void fit_xyey<U>::set_ansatz(
    const std::function<U(const U &, const std::vector<U> &)> &ansatz) {
  (*this).ANSATZ = ansatz; /*!< setting the ansatz */
  this->set_n_dof_auto();  // check also data and parameters

  /*! automatic deduction of the CH2_RED_FUN function */
  std::function<U(const std::vector<U> &, const std::vector<U> &,
                  const std::vector<U> &)>
      ch2_red_fun = [&](const std::vector<U> &y, const std::vector<U> &ey,
                        const std::vector<U> &p) -> U {
    return Silibs2::Minuit2_fit::ch2_red_ansatz_xyey_value(
        (*this).ANSATZ, (*this).x, y, ey, p, (*this).n_dof);
  };

  this->set_ch2_red_fun_single_jkf(ch2_red_fun);
} // set_ansatz()

template <class U>
void fit_xyey<U>::set_ch2_red_fun_single_jkf(
    const std::function<U(const std::vector<U> &y, const std::vector<U> &ey,
                          const std::vector<U> &p)> &res) {
  (*this).ch2_red_fun_single_jkf = res;
  (*this).residue_init = true;
} // set_ch2_red_single_jkf()

template <class U> void fit_xyey<U>::fit() {

  const int n_pars = this->get_n_pars(); /*!< number of parameters */
  const int n_pts = this->get_n_pts();   /*!< number of points */
  const int N_jkf = this->get_N_jkf();   /*!< number of jackknifes */

  this->set_n_dof_auto();

  ((*this).PARS).resize(n_pars, N_jkf); /*! jkf of fit parameters */
  ((*this).CH2_RED).resize(1, N_jkf);   /*!< jkf of reduced chi squadred */

  const std::vector<U> ey = Y.errs();  /*!< errors on the y */
  ((*this).Y_th).resize(n_pts, N_jkf); /*!< jkf of the theoratical values */

  /*! loop over jackknifes */
  (*this).jkf_not_conv.resize(0);
  for (int k = 0; k < N_jkf; k++) {
    if ((*this).verbose) {
      std::cout << "Jacknife number " << k << "\n";
    }
    std::vector<U> y = ((*this).Y).col(k); /*!< k-th jackknife */

    Silibs2::Minuit2_fit::fit_xyey<U> mini; /*!< fit class */
    mini.set_verbose((*this).verbose);      // same verbosity for each jkf

    mini.set_pars((*this).NAMES, (*this).GUESS, (*this).BOUND_L,
                  (*this).BOUND_R); /*!< specifying the parameters */

    mini.set_data((*this).x, y, ey); /*!< setting the data */

    mini.set_n_dof_manually((*this).n_dof);

    mini.set_ansatz((*this).ANSATZ); /*!< setting the ansatz */

    mini.fit(); /*!< doing the fit */
    if (!mini.fit_did_converge()) {
      ((*this).jkf_not_conv).push_back(k);
    }

    ((*this).PARS).set_col(mini.get_PARS(), k);     // best fit parameters
    ((*this).CH2_RED)(0, k) = mini.get_CH2_RED();   // reduced chi square
    ((*this).Y_th).set_col(mini.get_vec_y_th(), k); // theoretical y
  }

  /*! The fit has been done */
  (*this).fit_done = true;
} // fit()

template <class U>
void fit_xyey<U>::store_results(const bool &dense_th, const int &N) {
  this->check_fit_done();              // check if the fit was done
  const int N_jkf = this->get_N_jkf(); // number of jackknifes

  /*! ---------------------------------------------------------- */
  /*! Storing the results of the fit in the object 'fit_results' */
  /*! ---------------------------------------------------------- */

  // reference to the variable (it's just a shotcut)
  jkf::results<U> &fr = (*this).fit_results;
  this->store_conv_info();

  fr.set_title("Results of the xyey fit"); /*!< Title */
  fr.set_subtitle((*this).results_subtit); /*!< Subtitle */

  /*! Parameters with their names */
  fr.add_jkf_rows((*this).PARS, (*this).NAMES);
  fr.add_jkf((*this).CH2_RED, "CH2_RED"); /*!< chi^2_red*/

  /*!< adding jkf of x values */
  fr.add(jkf::trivial_jkf(x, N_jkf), "#jkf#data_x");
  fr.add(Y, "#jkf#data_y"); /*!< adding jkf of y values */

  /*! data in the "x y ey y_th ey_th" format */
  jkf::matrix<U> x_y_ey_yth_eyth =
      jkf::build_x_y_ey_yth_eyth(x, Y, (*this).Y_th);
  fr.add(x_y_ey_yth_eyth, {"#x_y_ey_yth_eyth"});
  if (dense_th) { /* plotting a dense list of theoretical points */
    // finding the minimum and maximum value of the 'x'
    U a = Functions::min_value(x);
    U b = Functions::max_value(x);

    if ((*this).extr_x) { // dense points include the extrapolation point
      const U x0 = (*this).X_extr_imp.avrs()[0];
      a = Functions::min_value<U>({a, x0});
      b = Functions::max_value<U>({b, x0});
    }
    if ((*this).extr_y) { // dense points include the extrapolated x
      const U x0 = (*this).X_extr.avrs()[0];
      a = Functions::min_value<U>({a, x0});
      b = Functions::max_value<U>({b, x0});
    }

    this->add_dense_th(N_jkf, a, b, dense_th, N);
  }
} // store_results()

#endif
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
