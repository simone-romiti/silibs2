// fit_Minuit_v2.hpp
/*
This file defines what is declared in fit_Minuit2.h
*/

#include "../deps.hpp"
#include "./base_fit.h"

namespace Silibs2 {
namespace jkf {

namespace Minuit2_fit {

#ifndef Silibs2_jkf_Minuit2_fit_base_fit_hpp
#define Silibs2_jkf_Minuit2_fit_base_fit_hpp

template <class U> base_fit<U>::base_fit() {}  // end default contructor
template <class U> base_fit<U>::~base_fit() {} // end default contructor

template <class U> void base_fit<U>::set_verbose(const bool &v) {
  (*this).verbose = v;
}

template <class U> void base_fit<U>::check_data_init() {
  if (!(*this).data_init) {
    std::cerr << "Error: you haven't passed the data yet.\n"
              << "(Recall for instance that the ansatz must be passed after "
                 "the data, so "
                 "that the redidue function can be deduced)\n"
              << "aborting." << '\n';
    abort();
  }
}

template <class U> void base_fit<U>::check_residue_init() {
  if (!residue_init) {
    std::cerr << "Error: residue function wasn't specified. Check if you "
                 "passed it correctly or, if you gave the ansatz, if you "
                 "provided also the data. Aborting."
              << '\n';
    abort();
  }
} // check_residue_init()

template <class U> void base_fit<U>::check_fit_init() {
  this->check_residue_init();
  this->check_pars_init();
} // check_fit_init()

template <class U> void base_fit<U>::check_fit_done() {
  if (!fit_done) {
    std::cerr << "Error: you haven't done the fit yet. Aborting." << '\n';
    abort();
  }
} // check_fit_done()

template <class U> Silibs2::jkf::matrix<U> base_fit<U>::get_PARS() {
  check_fit_done();
  return PARS;
}
template <class U> Silibs2::jkf::matrix<U> base_fit<U>::get_CH2_RED() {
  this->check_fit_done();
  return (*this).CH2_RED;
} // get_CH2_RED()

template <class U>
void base_fit<U>::set_X_extr(const jkf::matrix<U> &x_extr_jkf,
                             const U &y_extr) {
  this->check_fit_done();
  X_extr = x_extr_jkf;
  Y_extr_imp = jkf::trivial_jkf({y_extr}, PARS.cols());
  (*this).extr_y = true;
}

template <class U>
void base_fit<U>::find_X_extr(const U &y_extr, const U &a, const U &b,
                              const U &dx, const U &dy) {
  this->check_fit_done();
  X_extr = jkf::find_root((*this).ANSATZ, (*this).PARS, y_extr, a, b, dx, dy);
  Y_extr_imp = jkf::trivial_jkf({y_extr}, PARS.cols());
  (*this).extr_y = true;
} // find_X_extr()

template <class U> jkf::matrix<U> base_fit<U>::get_X_extr() const {
  if ((*this).extr_y) {
    return X_extr;
  } else {
    std::cerr << "Error. Trying to access X_extr, but it hasn't still been "
                 "found. Aborting.\n";
    abort();
  }
} // get_X_extr()

template <class U> void base_fit<U>::find_Y_extr(const std::vector<U> &x_extr) {
  this->check_fit_done();
  const int nj = ((*this).PARS).cols(); /*!< number of jkf */
  ((*this).Y_extr).resize(1, nj);
  for (int j = 0; j < nj; ++j) {
    ((*this).Y_extr)(0, j) = (*this).ANSATZ(x_extr[j], (*this).PARS.col(j));
  }
  X_extr_imp = jkf::matrix_from_rows<U>({x_extr});
  (*this).extr_x = true;
} // find_Y_extr()

template <class U> void base_fit<U>::find_Y_extr(const U &x_extr) {
  this->find_Y_extr(jkf::trivial_jkf<U>({x_extr}, PARS.cols()).row(0));
} // find_Y_extr()

template <class U> jkf::matrix<U> base_fit<U>::get_Y_extr() const {
  if ((*this).extr_x) {
    return Y_extr;
  } else {
    std::cerr << "Error. Trying to access Y_extr, but it hasn't still been "
                 "found. Aborting.\n";
    abort();
  }
} // get_X_extr()

template <class U> void base_fit<U>::give_details(const std::string &details) {
  (*this).results_subtit = details + (*this).results_subtit;
  ((*this).fit_results).set_subtitle((*this).results_subtit);
} // give_details()

template <class U>
void base_fit<U>::add_dense_th(const int &N_jkf, const U &a, const U &b,
                               const bool &dense_th, const int &N) {
  jkf::results<U> &fr = (*this).fit_results;

  /* plotting a dense list of theoretical points */
  std::vector<U> x_dense(N + 1);
  jkf::matrix<U> Y_th_dense(N + 1, N_jkf); // jkf of the 'N' points

  // filling the matrix
  for (int i = 0; i <= N; ++i) {
    const U xi = a + i * ((b - a) / (N + 0.0));
    x_dense[i] = xi;
    for (int k = 0; k < N_jkf; ++k) {
      Y_th_dense(i, k) = (*this).ANSATZ(xi, ((*this).PARS).col(k));
    }
  }

  /*! data in the "x y_th ey_th" format */
  jkf::matrix<U> dense_x_yth_eyth = jkf::build_xyey(x_dense, Y_th_dense);
  fr.add(dense_x_yth_eyth, {"#dense#x_yth_eyth"});
}

template <class U>
void base_fit<U>::print_fit_results(const std::string &dest,
                                    const std::string &sep, const int &prec,
                                    const int &nd) {
  jkf::results<U> &fr = (*this).fit_results;

  if ((*this).fit_done) {
    Silibs2::Functions::touch(dest); /*!< touch output file */

    if (extr_x) { /*!< prints the extrapolation on'x' if done */
      fr.add_jkf(X_extr_imp, "X_extr(fixed)");
      fr.add_jkf(Y_extr, "Y_extr");
    }
    if (extr_y) { /*!< prints the extrapolation 'y' if done */
      fr.add_jkf(X_extr, "X_extr");
      fr.add_jkf(Y_extr_imp, "Y_extr(fixed)");
    }

    fr.add_str(CH2_RED.scientific_digits(nd), "#CH2_RED(smart_print)");

    fr.print_summary(dest, sep, prec);
  } else {
    std::cerr << "Error. Cannot print the results of the fit before the "
                 "fit has been done. Aborting."
              << "\n";
    abort();
  }
} // print_fit_results()

template <class U> Silibs2::jkf::matrix<U> base_fit<U>::get_Y_th() {
  return (*this).Y_th;
} // get_Y_th()

template <class U> void base_fit<U>::store_conv_info() {
  const int jnc = ((*this).jkf_not_conv).size(); // jkf which didn't converge

  if (jnc != 0) {
    (*this).results_subtit +=
        "\n\n[WARNING: The fit did not converge for the following jackknife "
        "indices:\n";
    for (int m = 0; m < jnc - 1; ++m) {
      (*this).results_subtit += std::to_string(m) + ", ";
    }
    (*this).results_subtit += std::to_string(jnc - 1) + "]";
  } else {
    (*this).results_subtit += "\n\n[The fit converged for all jkf indices]";
  }

  /* later one can add further details to the fit results without
  removing the error message of non-convergece */
  this->give_details("");
} // store_conv_info()

#endif
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
