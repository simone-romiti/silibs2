// constant_fit_xyey.h
/*! Specialization of fit_xyey to a polynomial ansatz */

#include "../polynomial/polynomial.h"

namespace Silibs2 {
namespace jkf {
namespace Minuit2_fit {
#ifndef Silibs2_jkf_Minuit2_fit_constant_fit_xyey_h
#define Silibs2_jkf_Minuit2_fit_constant_fit_xyey_h

template <class U = double>
class constant_fit_xyey : public Silibs2::jkf::Minuit2_fit::fit_xyey<U> {

public:
  constant_fit_xyey();
  ~constant_fit_xyey() {}

  void set_par(const std::string &name, const U &guess = 1.0);
};

#endif
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
