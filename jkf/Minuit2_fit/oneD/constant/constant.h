// routines for constant fits

#include "./constant_fit_xexyey.h"
#include "./constant_fit_xyey.h"

namespace Silibs2 {
namespace jkf {
namespace Minuit2_fit {
#ifndef Silibs2_jkf_Minuit2_fit_constant_h
#define Silibs2_jkf_Minuit2_fit_constant_h

/* ------------------------------------------------- */
/* intercept of the constant fit using the guess 'g' */
/* ------------------------------------------------- */

template <class U = double>
jkf::matrix<U> const_fit_xyey_value(const std::vector<U> &x,
                                    const jkf::matrix<U> &Y, const U &g);

template <class U = double>
jkf::matrix<U> const_fit_xexyey_value(const jkf::matrix<U> &X,
                                      const jkf::matrix<U> &Y, const U &g);

#endif
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
