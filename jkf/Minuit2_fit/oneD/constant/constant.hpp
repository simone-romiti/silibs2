// routines for constant fits

#include "./constant_fit_xexyey.hpp"
#include "./constant_fit_xyey.hpp"
#include "constant.h"

namespace Silibs2 {
namespace jkf {
namespace Minuit2_fit {
#ifndef Silibs2_jkf_Minuit2_fit_constant_hpp
#define Silibs2_jkf_Minuit2_fit_constant_hpp

template <class U>
jkf::matrix<U> const_fit_xyey_value(const std::vector<U> &x,
                                    const jkf::matrix<U> &Y, const U &g) {
  constant_fit_xyey<U> minu;
  minu.set_par("a0", g);
  minu.set_data(x, Y);
  minu.fit();
  return minu.get_PARS();
}

template <class U>
jkf::matrix<U> const_fit_xexyey_value(const jkf::matrix<U> &X,
                                      const jkf::matrix<U> &Y, const U &g) {
  constant_fit_xexyey<U> minu;
  minu.set_data(X, Y);
  minu.set_par("a0", g);
  minu.fit();
  return minu.get_PARS();
}

#endif
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
