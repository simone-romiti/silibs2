// constant_fit_xexyey.hpp
/*! This file defines what is declared in fit_Minuit2.h */

#include "../polynomial/polynomial.hpp"
#include "./constant_fit_xexyey.h"

namespace Silibs2 {
namespace jkf {
namespace Minuit2_fit {
#ifndef Silibs2_jkf_Minuit2_fit_constant_fit_xexyey_hpp
#define Silibs2_jkf_Minuit2_fit_constant_fit_xexyey_hpp

template <class U>
constant_fit_xexyey<U>::constant_fit_xexyey() {} // default contructor

template <class U>
void constant_fit_xexyey<U>::set_par(const std::string &name, const U &guess) {
  this->check_data_init();
  this->set_pars({name}, {guess});
  const std::function<U(const U &, const std::vector<U> &)> ansatz =
      [](const U &x0, const std::vector<U> &an) { return an[0]; };
  this->set_ansatz(ansatz);
} // set_pars()

#endif
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
