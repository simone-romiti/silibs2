// constant_fit_xexyey.h
/*! Specialization of fit_xexyey to a polynomial ansatz */

#include "../polynomial/polynomial.h"

namespace Silibs2 {
namespace jkf {
namespace Minuit2_fit {
#ifndef Silibs2_jkf_Minuit2_fit_constant_fit_xexyey_h
#define Silibs2_jkf_Minuit2_fit_constant_fit_xexyey_h

template <class U = double>
class constant_fit_xexyey : public polynomial_fit_xexyey<U> {

public:
  constant_fit_xexyey();
  ~constant_fit_xexyey() {}

  void set_par(const std::string &name, const U &guess = 1.0);
};

#endif
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
