// deps.h
// dependencies all in one file

/*! base class for 1D ansatz fitting */
#include "../../../Minuit2_fit/fit.h"
#include "../../matrix.h"
#include "../../results.h"
