// deps.h
// dependencies all in one file

/*! base class for 1D ansatz fitting */
#include "../../../Minuit2_fit/fit.hpp"
#include "../../array3d.hpp"
#include "../../matrix.hpp"
