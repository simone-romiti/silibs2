// fit_Minuit_v2.hpp
/*
This file defines what is declared in fit_Minuit2.h
*/

#include "./base_fit.h"

namespace Silibs2 {
namespace jkf {
namespace Minuit2_fit {
namespace nD {
#ifndef Silibs2_jkf_Minuit2_fit_nD_base_fit_hpp
#define Silibs2_jkf_Minuit2_fit_nD_base_fit_hpp

template <class U> base_fit<U>::base_fit() {}  // end default contructor
template <class U> base_fit<U>::~base_fit() {} // end default contructor

template <class U> void base_fit<U>::check_data_init() const {
  if (!(*this).data_init) {
    std::cerr << "Error: you haven't passed the data yet.\n"
              << "(Recall for instance that the ansatz must be passed after "
                 "the data, so "
                 "that the redidue function can be deduced)\n"
              << "aborting." << '\n';
    abort();
  }
}

template <class U> void base_fit<U>::check_residue_init() const {
  if (!residue_init) {
    std::cerr << "Error: residue function wasn't specified. Check if you "
                 "passed it correctly or, if you gave the ansatz, if you "
                 "provided also the data. Aborting."
              << '\n';
    abort();
  }
} // check_residue_init()

template <class U> void base_fit<U>::check_fit_init() {
  this->check_residue_init();
  this->check_pars_init();
} // check_fit_init()

template <class U> void base_fit<U>::check_fit_done() {
  if (!fit_done) {
    std::cerr << "Error: you haven't done the fit yet. Aborting." << '\n';
    abort();
  }
} // check_fit_done()

template <class U> Silibs2::jkf::matrix<U> base_fit<U>::get_PARS() {
  check_fit_done();
  return PARS;
}
template <class U> Silibs2::jkf::matrix<U> base_fit<U>::get_CH2_RED() {
  this->check_fit_done();
  return (*this).CH2_RED;
} // get_CH2_RED()

template <class U> void base_fit<U>::find_Y_extr(const jkf::matrix<U> &x_extr) {
  this->check_fit_done();
  const int nj = ((*this).PARS).cols(); /*!< number of jkf */
  ((*this).Y_extr).resize(1, nj);
  for (int j = 0; j < nj; ++j) {
    ((*this).Y_extr)(0, j) = (*this).ANSATZ(x_extr.col(j), (*this).PARS.col(j));
  }
  X_extr_imp = jkf::matrix_from_rows<U>({x_extr});
  (*this).extr_x = true;
} // find_Y_extr()

template <class U> void base_fit<U>::find_Y_extr(const std::vector<U> &x_extr) {
  this->find_Y_extr(jkf::trivial_jkf<U>(x_extr, PARS.cols()).row(0));
} // find_Y_extr()

template <class U> jkf::matrix<U> base_fit<U>::get_Y_extr() const {
  if ((*this).extr_x) {
    return Y_extr;
  } else {
    std::cerr << "Error. Trying to access Y_extr, but it hasn't still been "
                 "found. Aborting.\n";
    abort();
  }
} // get_X_extr()

template <class U> void base_fit<U>::give_details(const std::string &details) {
  (*this).results_subtit = details + (*this).results_subtit;
  ((*this).fit_results).set_subtitle((*this).results_subtit);
} // give_details()

template <class U>
void base_fit<U>::print_fit_results(const std::string &dest,
                                    const std::string &sep, const int &prec) {
  if ((*this).fit_done) {
    Silibs2::Functions::touch(dest); /*!< touch output file */

    if (extr_x) { /*!< prints the extrapolation on'x' if done */
      ((*this).fit_results).add_jkf(X_extr_imp, "X_extr(fixed)");
      ((*this).fit_results).add_jkf(Y_extr, "Y_extr");
    }

    ((*this).fit_results).print_summary(dest, sep, prec);
  } else {
    std::cerr << "Error. Cannot print the results of the fit before the "
                 "fit has been done. Aborting."
              << "\n";
    abort();
  }
} // print_fit_results()

template <class U> Silibs2::jkf::matrix<U> base_fit<U>::get_Y_th() {
  return (*this).Y_th;
} // get_Y_th()

#endif
} // namespace nD
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
