// base_fit.h
/*
This file declares routines allowing to fit a generic std::function
which represents a function from R^n --> R^m
using the Minuit library
*/

#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <numeric>
#include <string>

#include "../deps.h"

namespace Silibs2 {
namespace jkf {
namespace Minuit2_fit {
namespace nmD {
#ifndef Silibs2_jkf_Minuit2_fit_nmD_base_fit_h
#define Silibs2_jkf_Minuit2_fit_nmD_base_fit_h

//! 1-dimensional fit
template <class U = double>
class base_fit : public Silibs2::Minuit2_fit::base<U> {
protected:
  int Nm = 0; // number of independent variables

  /*! control variables */
  bool data_init = false;    /*!< true if data have been set */
  bool residue_init = false; /*!< true if the residue function has been set */
  bool fit_done = false;     /*!< true when the fit has been done */

  /*! Ansatz to be fitted (if unspecified it is the null function)*/
  std::function<std::vector<U>(const std::vector<U> &, const std::vector<U> &)>
      ANSATZ;

  /*! jkf of Best-fit values for the parameters */
  Silibs2::jkf::matrix<U> PARS;

  /*!
  jkf of residue (reduced chi squared) i.e. the value resulting from the
  minimization.
  */
  Silibs2::jkf::matrix<U> CH2_RED;

  /*!
  Theoretical values obtained from the ansatz
  with the best fit values for the parameters
  */
  Silibs2::jkf::matrix<U> Y_th;

  Silibs2::jkf::matrix<U> X_extr_imp; /*!< imposed value of x to find Y_extr */
  Silibs2::jkf::matrix<U> Y_extr;     /*!< y_th(X_extr_imp.col(j)), \forall j*/

  bool extr_x = false; /*!< true if 'y' has been extrapolated wrt 'x' */
  bool extr_y = false; /*!< true if 'x' has been extrapolated wrt 'y' */

  Silibs2::jkf::results<U> fit_results; /*!< results of the fit in one file */
  std::string results_subtit = "";      // subtitle of the fit results

public:
  base_fit();
  ~base_fit();

  void check_data_init() const;    /*!< Checks if data were passed */
  void check_residue_init() const; /*!< Checks if residue_init is true */

  /*!
  Checks if all the conditions necessary
  to do the fit have been fullfilled
  */
  void check_fit_init();
  void check_fit_done(); /*!< checks if the fit was done */

  Silibs2::jkf::matrix<U> get_PARS();    /*!< jkf of best fit parameters */
  Silibs2::jkf::matrix<U> get_CH2_RED(); /*!< jkf of best fit ch2_red */

  /*!< find y extrapolated to the jkf of x_extr */
  void find_Y_extr(const jkf::matrix<U> &x_extr);
  /*!< find y extrapolated to x_extr */
  void find_Y_extr(const std::vector<U> &x_extr);
  jkf::matrix<U> get_Y_extr() const; /*!< return the y extrapolated */

  /*!< setting the subtitle of the fit_results */
  void give_details(const std::string &details);

  /*! prints out the results of the fit */
  void print_fit_results(const std::string &dest, const std::string &sep = "\t",
                         const int &prec = 16);

  Silibs2::jkf::matrix<U> get_Y_th(); /*!< returns Y_th */
};

#endif
} // namespace nmD
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2