// fit_Minuit_v2.hpp
/*! This file defines what is declared in fit_Minuit2.h */

#include "./fit_xexyey.h"

namespace Silibs2 {
namespace jkf {
namespace Minuit2_fit {
namespace nD {
#ifndef Silibs2_jkf_Minuit2_fit_nD_fit_xexyey_hpp
#define Silibs2_jkf_Minuit2_fit_nD_fit_xexyey_hpp

template <class U> fit_xexyey<U>::fit_xexyey() {}  // end default contructor
template <class U> fit_xexyey<U>::~fit_xexyey() {} // end default contructor

// template <class U>
// void fit_xexyey<U>::set_data(const std::vector<jkf::matrix<U>> &X_jkf,
//                              const jkf::matrix<U> &Y_jkf) {
//   (*this).X = X_jkf;        /*!< jackknifes for the x values */
//   (*this).Y = Y_jkf;        /*!< jackknifes for the y values */
//   (*this).data_init = true; /*!< data have been initialized */
// } // set_data()

template <class U>
void fit_xexyey<U>::set_data(const jkf::array3d<U> &X_jkf,
                             const jkf::matrix<U> &Y_jkf) {
  (*this).X = X_jkf;        /*!< jackknifes for the x values */
  (*this).Y = Y_jkf;        /*!< jackknifes for the y values */
  (*this).data_init = true; /*!< data have been initialized */
} // set_data()

template <class U> int fit_xexyey<U>::get_n_var() const {
  return X[0].rows(); // number of independent variables
}

template <class U> int fit_xexyey<U>::get_n_pts() const {
  this->check_data_init();
  return ((*this).Y).rows();
} // get_n_pts()

template <class U> int fit_xexyey<U>::get_N_jkf() const {
  this->check_data_init();
  return ((*this).Y).cols();
} // get_N_jkf()

template <class U>
void fit_xexyey<U>::set_ansatz(
    const std::function<U(const std::vector<U> &, const std::vector<U> &)>
        &ansatz) {
  (*this).ANSATZ = ansatz; /*!< setting the ansatz */

  /*! automatic deduction of the CH2_RED_FUN function */
  auto ch2_fun = [&](const Tables::table<U> &x, const Tables::table<U> &ex,
                     const std::vector<U> &y, const std::vector<U> &ey,
                     const std::vector<U> &p) {
    return Silibs2::Minuit2_fit::nD::ch2_red_ansatz_xexyey_value(
        (*this).ANSATZ, x, ex, y, ey, p);
  };
  this->set_ch2_red_fun_single_jkf(ch2_fun);
} // end set_ansatz()

template <class U>
void fit_xexyey<U>::set_ch2_red_fun_single_jkf(
    const std::function<U(const Tables::table<U> &x, const Tables::table<U> &ex,
                          const std::vector<U> &y, const std::vector<U> &ey,
                          const std::vector<U> &p)> &res) {
  (*this).ch2_red_fun_single_jkf = res;
  (*this).residue_init = true;
} // set_ch2_red_single_jkf()

template <class U> Tables::table<U> fit_xexyey<U>::get_x(const int &k) const {
  const int n_var = this->get_n_var();
  const int n_pts = this->get_n_pts();
  Tables::table<U> x(n_var, n_pts);
  for (int i = 0; i < n_var; ++i) {
    for (int j = 0; j < n_pts; ++j) {
      x(i, j) = X(j, i, k); // j-th point, i-th variable, k-th jkf
    }
  }
  return x;
}

template <class U> Tables::table<U> fit_xexyey<U>::get_ex() const {
  const int n_var = this->get_n_var();
  const int n_pts = this->get_n_pts();
  Tables::table<U> ex(n_var, n_pts); // erros on the xi for each point
  for (int j = 0; j < n_pts; ++j) {
    ex.set_col(j, X[j].errs());
  }
  return ex;
}

template <class U>
Tables::table<U> fit_xexyey<U>::get_cols_xy(const jkf::array3d<U> &X_vec,
                                            const jkf::matrix<U> &Y_jkf) const {
  const int n_pts = X_vec.size();
  const int n_var = X_vec[0].rows();
  Tables::table<U> cols_xy(n_pts, 2 * (n_var + 1));
  const jkf::matrix<U> ae_Y = Y_jkf.ae(); // avr+err for each y point
  for (int j = 0; j < n_pts; ++j) {
    // average and error for each x point
    const jkf::matrix<U> ae_Xj = X_vec[j].ae();
    for (int i = 0; i < n_var; ++i) {
      cols_xy(j, 2 * i) = ae_Xj(i, 0);     // average
      cols_xy(j, 2 * i + 1) = ae_Xj(i, 1); // error
    }
    cols_xy(j, 2 * n_var) = ae_Y(j, 0);     // average
    cols_xy(j, 2 * n_var + 1) = ae_Y(j, 1); // error
  }
  return cols_xy;
}

template <class U>
Tables::table<U>
fit_xexyey<U>::get_cols_xyyth(const jkf::array3d<U> &X_vec,
                              const jkf::matrix<U> &Y_jkf,
                              const jkf::matrix<U> &Y_th_jkf) const {
  const std::array<int, 3> shp = X_vec.shape();
  const int n_pts = shp[0];
  const int n_var = shp[1];
  Tables::table<U> cols_xyyth(n_pts, 2 * (n_var + 2));
  const jkf::matrix<U> ae_Y = Y_jkf.ae();      // avr+err for each y point
  const jkf::matrix<U> ae_Yth = Y_th_jkf.ae(); // avr+err for each yth point
  for (int j = 0; j < n_pts; ++j) {
    // average and error for each x point
    const jkf::matrix<U> ae_Xj = X_vec[j].ae();
    for (int i = 0; i < n_var; ++i) {
      cols_xyyth(j, 2 * i) = ae_Xj(i, 0);     // average
      cols_xyyth(j, 2 * i + 1) = ae_Xj(i, 1); // error
    }
    cols_xyyth(j, 2 * n_var) = ae_Y(j, 0);       // average
    cols_xyyth(j, 2 * n_var + 1) = ae_Y(j, 1);   // error
    cols_xyyth(j, 2 * n_var + 2) = ae_Yth(j, 0); // average
    cols_xyyth(j, 2 * n_var + 3) = ae_Yth(j, 1); // error
  }
  return cols_xyyth;
}

template <class U>
Tables::table<U> fit_xexyey<U>::get_cols_xy_theory(const int &N) const {
  const int n_var = this->get_n_var();
  const int N_jkf = this->get_N_jkf();

  jkf::array3d<U> X_dense(N, n_var, N_jkf);
  jkf::matrix<U> Y_dense(N, N_jkf);

  for (int k = 0; k < N_jkf; ++k) {
    const jkf::matrix<U> x_pts = this->get_x(k); // variables \times points
    std::vector<U> a(n_var), delta(n_var);
    std::vector<U> x_dense;
    for (int j = 0; j < N; ++j) {
      for (int i = 0; i < n_var; ++i) {
        const U a_i = Functions::min_value(x_pts.row(i));
        const U b_i = Functions::max_value(x_pts.row(i));
        a[i] = a_i;
        delta[i] = (b_i - a_i) / (N + 0.0);
        const U x_ij = a[i] + j * delta[i];
        x_dense.push_back(x_ij);
        X_dense(j, i, k) = x_ij;
      }
      Y_dense(j, k) = (*this).ANSATZ(x_dense, (*this).PARS.col(k));
    }
  }
  return this->get_cols_xy(X_dense, Y_dense);
}

template <class U>
void fit_xexyey<U>::print_conv_info(const std::vector<int> &jkf_not_conv) {
  const int jnc = jkf_not_conv.size(); // jkf which didn't converge
  if (jnc != 0) {
    (*this).results_subtit +=
        "\n\n[WARNING: The fit did not converge for the following jackknife "
        "indices:\n";
    for (int m = 0; m < jnc - 1; ++m) {
      (*this).results_subtit += std::to_string(m) + ", ";
    }
    (*this).results_subtit += std::to_string(jnc - 1) + "]";
  } else {
    (*this).results_subtit += "\n\n[The fit converged for all jkf indices]";
  }
  /* later one can add further details to the fit results without
  removing the error message of non-convergece */
  this->give_details("");
} // print_conv_info()

template <class U> void fit_xexyey<U>::fit() {
  const int n_pars = this->get_n_pars(); /*!< number of parameters */
  const int n_pts = this->get_n_pts();   /*!< number of points */
  const int N_jkf = this->get_N_jkf();   /*!< number of jackknifes */

  ((*this).PARS).resize(n_pars, N_jkf); /*! jkf of fit parameters */
  ((*this).CH2_RED).resize(1, N_jkf);   /*!< jkf of reduced chi squadred */

  Tables::table<U> ex = this->get_ex();
  const std::vector<U> ey = Y.errs();  /*!< errors on the y */
  ((*this).Y_th).resize(n_pts, N_jkf); /*!< jkf of the theoretical values */

  // jkf indices for which the fit did not converge
  std::vector<int> jkf_not_conv(0);
  for (int k = 0; k < N_jkf; k++) { /*! loop over jackknifes */
    std::clog << "Jackknife number : " << k << "\n";

    const Tables::table<U> x = this->get_x(k);
    const std::vector<U> y = Y.col(k); /*!< k-th jackknife */

    Silibs2::Minuit2_fit::nD::fit_xexyey<U> mini; /*!< fit class */
    mini.set_data(x, ex, y, ey);                  /*!< setting the data */
    mini.set_ansatz((*this).ANSATZ);              /*!< setting the ansatz */
    mini.set_pars((*this).NAMES, (*this).GUESS, (*this).BOUND_L,
                  (*this).BOUND_R); /*!< specifying the parameters */
    mini.fit();                     /*!< doing the fit */
    if (!mini.fit_did_converge()) {
      jkf_not_conv.push_back(k);
    }

    /*! setting the k-th jkf of: */

    ((*this).PARS).set_col(mini.get_PARS(), k);     /*!< parameters */
    ((*this).CH2_RED)(0, k) = mini.get_CH2_RED();   /*!< reduced ch2 */
    ((*this).Y_th).set_col(mini.get_vec_y_th(), k); /*!< theoretical y */
  }

  (*this).fit_done = true;

  /*! ---------------------------------------------------------- */
  /*! Storing the results of the fit in the object 'fit_results' */
  /*! ---------------------------------------------------------- */

  ((*this).fit_results).set_title("Results of the xexyey fit"); /*!< Title */
  this->print_conv_info(jkf_not_conv);

  /*! Parameters with their names */
  ((*this).fit_results).add_jkf_rows((*this).PARS, (*this).NAMES);
  ((*this).fit_results).add_jkf((*this).CH2_RED, "CH2_RED"); /*!< chi^2_red*/
  for (int j = 0; j < n_pts; ++j) {
    /*! jkf of the n_var-tuples for each point */
    ((*this).fit_results).add(X[j], "#jkf#data_x_" + std::to_string(j + 1));
  }
  ((*this).fit_results).add(Y, "#jkf#data_y"); /*!< adding jkf of y values */

  /*
  columns with the individual variables for each point and the yth
  are added to the fit results
  */
  const Tables::table<U> cols_xy = this->get_cols_xyyth(X, Y, (*this).Y_th);
  ((*this).fit_results).add(cols_xy, "#plot#xiexiyeyytheyth");

  // /* Dense theoretical points are added */
  // const Tables::table<U> cols_xy_theory = this->get_cols_xy_theory(100);
  // ((*this).fit_results).add(cols_xy_theory, "#plot_th#xiexiytheyth");

  /*! The fit has been done */
  (*this).fit_done = true;
} // fit()

#endif
} // namespace nD
} // namespace Minuit2_fit
} // namespace jkf
} // namespace Silibs2
