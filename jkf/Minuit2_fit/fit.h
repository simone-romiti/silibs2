// fit.h
/*! All fitting libraries */

// 1D fits
#include "./oneD/constant/constant.h"
#include "./oneD/generic/generic.h"
#include "./oneD/polynomial/polynomial.h"

// nD fits
// #include "./nD/constant/constant.h" -> TO BE IMPLEMENTED
#include "./nD/generic/generic.h"
// #include "./nD/polynomial/polynomial.h" -> TO BE IMPLEMENTED
