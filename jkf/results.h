// results.h
/*!
This file declares a class for printing and retrieving
a bunch of jkf matrices in a single file.
The main scope of it is to reduce the number of output files,
so that is it easier to read the results of a fit for instance.
*/

#include <string>
#include <vector>

#include "./matrix.h"

namespace Silibs2 {
namespace jkf {
#ifndef Silibs2_jkf_results_h
#define Silibs2_jkf_results_h

template <class U = double> class results {
private:
  std::vector<jkf::matrix<U>> M; /*!< list of jkf::matrix */
  std::vector<std::string> H;    /*!< list of headers */

  /*!< list of Tables::table<std::string> */
  std::vector<Tables::table<std::string>> M_str;
  std::vector<std::string> H_str;

  std::string TITLE = "";    /*!< Title of the results */
  std::string SUBTITLE = ""; /*!< Subtitle of the results */

public:
  results();
  ~results();

  void set_title(const std::string &t);    /*!< Setting the title */
  void set_subtitle(const std::string &s); /*!< Setting the subtitle */

  /*! Adds a matrix with a header. ae() isn't calculated and printed out */
  void add(const Tables::table<U> &m, const std::string &h);

  /*! Adds a Tables::table with a header 'h' */
  void add_str(const Tables::table<std::string> &m, const std::string &h);

  /*! Adds a matrix with a header. ae() is calculated and printed out */
  void add_jkf(const jkf::matrix<U> &m, const std::string &h);

  /*!
  Adds a series of 1xN_jkf matrices
  corresponding to the rows of the matrix and headers in 'h'
   */
  void add_jkf_rows(const jkf::matrix<U> &m, const std::vector<std::string> &h);

  /*!
  Prints the matrices {m1,...,mN} with headers {h1,...,hN}
  in the format;

  #jkf#h1
  m1
  #ae#h1
  m1.ae()
  ...
  #jkf#hN
  #ae#hN
  mN.ae()
  */
  void print_summary(const std::string &dest, const std::string &sep = "\t",
                     const int &prec = 16) const;
};

#endif
} // namespace jkf
} // namespace Silibs2
