// functions.hpp

#include "../Functions/functions.h"
#include "../Minuit2_fit/fit.h"
#include "./functions.h"
#include "./matrix.h"
#include "./results.hpp"

namespace Silibs2 {
namespace jkf {
#ifndef Silibs2_jkf_functions_hpp
#define Silibs2_jkf_functions_hpp

#endif
} // namespace jkf
} // namespace Silibs2
