// matrix.hpp
/*! Definitions for matrix.h */

#include "../Functions/functions.hpp"
#include "../Tables/table.hpp"
#include "./matrix.h"

namespace Silibs2 {
namespace jkf {
#ifndef Silibs2_jkf_matrix_hpp
#define Silibs2_jkf_matrix_hpp
using namespace Functions;

template <class U> matrix<U>::matrix() {}
template <class U> matrix<U>::~matrix() {}

template <class U> matrix<U>::matrix(const Tables::table<U> &t) {
  (*this).LIST_OF_ROWS = t.list_of_rows(); //!< same elements
  (*this).ROWS = t.rows();                 //!< same rows
  (*this).COLS = t.cols();                 //!< same columns
}
template <class U> matrix<U>::matrix(const matrix<U> &m) { (*this) = m; }

template <class U> matrix<U>::matrix(const int &ro, const int &co) {
  Tables::table<U> t(ro, co);
  (*this) = t;
}

template <class U>
matrix<U>::matrix(const int &ro, const int &co, const U &val) {
  Tables::table<U> t(ro, co, val);
  (*this) = t;
}

template <class U>
matrix<U>::matrix(const std::string &path, const std::string &head,
                  const std::string &sep, const int &ign_lines) {
  (*this) = Tables::read_table_sep<U>(path, head, ign_lines, sep);
}

template <class U>
matrix<U>::matrix(const int &ro, const int &co,
                  const std::function<U(const int &, const int &)> &fun) {
  Tables::table<U> t(ro, co, fun);
  (*this) = t;
}

template <class U> std::vector<U> matrix<U>::avrs() const {
  int T_ext = this->rows();
  std::vector<U> a(0);
  for (int t = 0; t < T_ext; t++) {
    a.push_back(average(this->row(t)));
  }
  return a;
} // avrs()

template <class U> std::vector<U> matrix<U>::errs() const {
  int T_ext = this->rows();
  std::vector<U> e(0);
  for (int t = 0; t < T_ext; t++) {
    e.push_back(error_jkf(this->row(t)));
  }
  return e;
} // errs()

template <class U> matrix<U> matrix<U>::ae() const {
  int T_ext = this->rows();

  matrix<U> res(T_ext, 2);
  for (int t = 0; t < T_ext; t++) {
    res(t, 0) = average(this->row(t));
    res(t, 1) = error_jkf(this->row(t));
  }

  return res;
} // end ae()

template <class U>
void matrix<U>::print_jkf_ae(const std::string &path, const std::string &sep,
                             const int &prec, const std::string &h,
                             const int &nd) const {
  std::string h_jkf = "#jkf", h_ae = "#ae";
  if (h != "") {
    h_jkf += "#" + h;
    h_ae += "#" + h;
  }
  Tables::print_tables<U>(path, {(*this), this->ae()}, {h_jkf, h_ae}, sep,
                          prec);

  // printing average and error in scientific notation
  const matrix<U> ae_jkf = this->ae();
  const matrix<std::string> ae_smart_print = this->scientific_digits(nd);
  const std::string ae_smart_print_str = ae_smart_print.to_string();
  const std::string s0 = "#avr_err(smart_print)\n" + ae_smart_print_str + "\n";
  // appending to the file
  Functions::append_string(s0, path);
}

template <class U>
matrix<std::string> matrix<U>::scientific_digits(const int &nd) const {
  using namespace Functions::smart_printing;
  const int nr = this->rows();
  const Tables::table<U> ae_matr = this->ae();
  Tables::table<std::string> t(nr, 1);
  for (int i = 0; i < nr; ++i) {
    t(i, 0) = smart_print(ae_matr(i, 0), {ae_matr(i, 1)}, nd);
  }
  return t;
} // smart_print_ae()

template <class U>
void matrix<U>::smart_print_ae(const std::string &path, const int &nd) const {
  Tables::table<std::string> t = this->scientific_digits(nd);
  t.print(path);
} // smart_print_ae()

template <class U> matrix<U> matrix<U>::operator*(const U &lambda) const {
  matrix<U> matr((*this).ROWS, (*this).COLS);
  auto lp = [&](const int &i, const int &j) {
    matr(i, j) = (lambda) * ((*this)(i, j));
  };
  this->loop_elements(lp);
  return matr;
} // operator *()

template <class U> void matrix<U>::operator*=(const U &lambda) {
  *this = (*this) * lambda;
} // operator -=()

template <class U> matrix<U> matrix<U>::operator/(const U &lambda) const {
  matrix<U> matr((*this).ROWS, (*this).COLS);
  auto lp = [&](const int &i, const int &j) {
    matr(i, j) = ((*this)(i, j)) / (lambda);
  };
  this->loop_elements(lp);
  return matr;
} // operator / ()

template <class U> void matrix<U>::operator/=(const U &lambda) {
  (*this) = (*this) / lambda;
} // operator -= ()

template <class U> void matrix<U>::operator+=(const matrix<U> &m) {
  (*this) = (*this) + m;
} // operator +=()

template <class U> void matrix<U>::operator-=(const matrix<U> &m) {
  (*this) = ((*this) - m);
} // operator -=()

/* --------- */
/* functions */
/* --------- */

/*!  -------------------- */
/*!  READING and PRINTING */
/*!  -------------------- */

template <class U>
void print_matrices(const std::string &path, const std::vector<matrix<U>> &M,
                    const std::vector<std::string> &header,
                    const std::string &sep, const int &prec) {
  const int n = (int)M.size();
  std::vector<Tables::table<U>> T(n);
  for (int i = 0; i < n; ++i) {
    T[i] = M[i];
  }
  Tables::print_tables(path, T, header, sep, prec);
} // print_matrices()

template <class U>
matrix<U> read_matrix_sep(const std::string &path, const std::string &head,
                          const int &ign_lines, const std::string &sep) {
  return Tables::read_table_sep<U>(path, head, ign_lines, sep);
} // read_matrix_sep()

template <class U>
matrix<U> read_matrix(const std::string &path, const std::string &head,
                      const int &ign_lines) {
  return Tables::read_table<U>(path, head, ign_lines);
} // read_matrix()

template <class U>
matrix<U> read_jkf(const std::string &path, const std::string &h) {
  std::string H = "#jkf";
  if (h != "") {
    H += ("#" + h);
  }
  return read_matrix<U>(path, H);
} // read_jkf()

template <class U> matrix<U> read_avr_err(const std::string &path) {
  return read_matrix<U>(path, "#avr_err");
} // read_avr_err()

template <class U>
std::vector<U> read_row(const int &n, const std::string &path,
                        const std::string &head, const int &ign_lines) {
  return read_matrix<U>(path, head, ign_lines).row(n);
}

template <class U>
std::vector<U> read_col(const int &n, const std::string &path,
                        const std::string &head, const int &ign_lines) {
  return read_matrix<U>(path, head, ign_lines).col(n);
}

/*!  ---------------------------- */
/*!  OPERATIONS on the components */
/*!  ---------------------------- */

template <class U, class V> matrix<U> cast_matrix(const matrix<V> &matr) {
  return Tables::cast_table<U>(matr);
} // cast_matrix()

template <class M_type1, class M_type2>
M_type1 convert_matrix(const M_type2 &matr) {
  return Tables::convert_table<M_type1, M_type2>(matr);
} // convert_matrix()

/*!  ------------------------------ */
/*!  BUILDING from rows and columns */
/*!  ------------------------------ */

template <class U>
matrix<U> matrix_from_rows(const std::vector<std::vector<U>> &rows) {
  return Tables::table_from_rows(rows);
} // matrix_from_rows()

template <class U>
matrix<U> matrix_from_cols(const std::vector<std::vector<U>> &cols) {
  return Tables::table_from_cols(cols);
} // matrix_from_cols()

template <class U>
jkf::matrix<U> build_xyey(const std::vector<U> &x, const jkf::matrix<U> &Y) {
  return Tables::table_from_cols<U>({x, Y.avrs(), Y.errs()});
} // build_xyey()

template <class U>
jkf::matrix<U> build_x_y_ey_yth_eyth(const std::vector<U> &x,
                                     const jkf::matrix<U> &Y,
                                     const jkf::matrix<U> &Y_th) {
  return Tables::table_from_cols<U>(
      {x, Y.avrs(), Y.errs(), Y_th.avrs(), Y_th.errs()});
} // build_x_y_ey_yth_eyth()

template <class U>
jkf::matrix<U> build_xexyey(const jkf::matrix<U> &X, const jkf::matrix<U> &Y) {
  return Tables::table_from_cols<U>({X.avrs(), X.errs(), Y.avrs(), Y.errs()});
} // build_xexyey()

template <class U>
jkf::matrix<U> build_x_ex_y_ey_yth_eyth(const jkf::matrix<U> &X,
                                        const jkf::matrix<U> &Y,
                                        const jkf::matrix<U> &Y_th) {
  return Tables::table_from_cols<U>(
      {X.avrs(), X.errs(), Y.avrs(), Y.errs(), Y_th.avrs(), Y_th.errs()});
} // build_x_ex_y_ey_yth_eyth()

template <class U>
jkf::matrix<U> build_xiyey(const Tables::table<U> &Xi,
                           const jkf::matrix<U> &Y) {
  const int n_vars = Xi.cols();
  Tables::table<U> T = Xi;
  T.insert_col(Y.avrs(), n_vars);
  T.insert_col(Y.errs(), n_vars + 1);
  return T;
}

/*!  -------------- */
/*!  Linear Algebra */
/*!  -------------- */

template <class U>
matrix<U> lin_comb(const matrix<U> &m1, const matrix<U> &m2, const U &a,
                   const U &b) {
  assert(m1.rows() == m2.rows() && m1.cols() == m2.cols() &&
         "Invalid linear combination.");

  int R = m1.rows(), C = m1.cols();
  matrix<U> matr(R, C);
  matr.loop_elements([&](const int &i, const int &j) {
    matr(i, j) = a * m1(i, j) + b * m2(i, j);
  });
  return matr;
} // lin_comb()

template <class U>
std::pair<matrix<U>, matrix<U>>
lin_comb(const std::pair<matrix<U>, matrix<U>> &m1,
         const std::pair<matrix<U>, matrix<U>> &m2, const U &a, const U &b) {
  return std::make_pair(lin_comb(m1.first, m2.first, a, b),
                        lin_comb(m1.second, m2.second, a, b));
} // lin_comb();

template <class U>
matrix<U> operator+(const matrix<U> &m1, const matrix<U> &m2) {
  return lin_comb(m1, m2, +1.0, +1.0);
}

template <class U>
matrix<U> operator-(const matrix<U> &m1, const matrix<U> &m2) {
  return lin_comb(m1, m2, +1.0, -1.0);
}

template <class U> matrix<U> operator*(const U &lambda, const matrix<U> &m) {
  return (m * lambda);
} // operator * ()

template <class U>
matrix<U> tensor_prod(const std::vector<U> &v, const std::vector<U> &w) {
  const int n = v.size();
  const int m = w.size();
  matrix<U> M(n, m);
  M.loop_elements([&](const int &i, const int &j) { M(i, j) = v[i] * w[j]; });
  return M;
} // tensor_prod()

/*! ------------------------- */
/*! Component-wise operations */
/*! ------------------------- */

template <class U>
jkf::matrix<U> operation_cwise(const jkf::matrix<U> &A,
                               const std::function<U(const U &)> &f) {
  const int R = A.rows();
  const int C = A.cols();

  jkf::matrix<U> M(R, C);
  M.loop_elements([&](const int &i, const int &j) { M(i, j) = f(A(i, j)); });
  return M;
} // operation_cwise()

template <class U1, class U2>
jkf::matrix<U1> pow_cwise(const jkf::matrix<U1> &A, const U2 &a) {
  std::function<U1(const U1 &)> f = [&](const U1 &x) { return pow(x, a); };
  return operation_cwise(A, f);
} // pow_cwise()

template <class U> jkf::matrix<U> square_cwise(const jkf::matrix<U> &A) {
  return pow_cwise(A, 2.0);
} // pow_cwise()

template <class U> jkf::matrix<U> sqrt_cwise(const jkf::matrix<U> &A) {
  std::function<U(const U &)> f = [&](const U &x) { return sqrt(x); };
  return operation_cwise(A, f);
} // sqrt_cwise()

template <class U>
jkf::matrix<U>
operation_cwise(const jkf::matrix<U> &A, const jkf::matrix<U> &B,
                const std::function<U(const U &, const U &)> &f) {
  assert((A.shape() == B.shape()) && "Cannot compare the two matrices.");
  const int R = A.rows();
  const int C = A.cols();

  jkf::matrix<U> M(R, C);
  M.loop_elements(
      [&](const int &i, const int &j) { M(i, j) = f(A(i, j), B(i, j)); });
  return M;
} // operation_cwise()

template <class U>
jkf::matrix<U> ratio_cwise(const jkf::matrix<U> &A, const jkf::matrix<U> &B,
                           const bool &ign_boundary) {
  assert(A.rows() == B.rows() && A.cols() == B.cols() &&
         "Cannot divide the two matrices componet-wise");

  const int T_ext = A.rows();
  const int N_jkf = A.cols();

  jkf::matrix<U> M(T_ext, N_jkf);

  for (int t = 0; t < T_ext; t++) {
    for (int j = 0; j < N_jkf; j++) {
      if (B(t, j) == 0 && (t == T_ext - 1 || t == 0)) {
        /*
        first and/or last point may be ill-defined
        (e.g forward derivative)
        */
        M(t, j) = 0.0;
      } else {
        M(t, j) = A(t, j) / B(t, j);
      }
    }
  }
  return M;
} // ratio_cwise()

template <class U>
jkf::matrix<U> operator/(const jkf::matrix<U> &A, const jkf::matrix<U> &B) {
  return ratio_cwise(A, B, true);
} // ratio_cwise()

template <class U>
jkf::matrix<U> mult_cwise(const jkf::matrix<U> &A, const jkf::matrix<U> &B) {
  std::function<U(const U &, const U &)> f = [&](const U &a, const U &b) {
    return a * b;
  };
  return operation_cwise(A, B, f);
} // multiplication_cwise()

template <class U>
jkf::matrix<U> operator*(const jkf::matrix<U> &m1, const jkf::matrix<U> &m2) {
  return mult_cwise(m1, m2);
} // operator *()

template <class U>
jkf::matrix<U> average_matrices(const std::vector<jkf::matrix<U>> &vec_M) {
  int n = vec_M.size();
  /* get number of rows and columns */
  int R = vec_M[0].rows(), C = vec_M[0].cols();
  jkf::matrix<U> M(R, C, 0.0);

  if (vec_M[0].size() == 0) {
    std::cerr << "Error. Cannot average null-size matrices.\n";
    std::cerr << "Abort.\n";
    abort();
  }

  for (int i = 0; i < n; ++i) {
    M += vec_M[i] / (n + 0.0);
  }
  return M;
} // average_matrices()

/* ---- */
/* Math */
/* ---- */

/*! functions::find_root() for each jkf of y0 */
template <class U>
jkf::matrix<U> find_root(std::function<U(const U &, const std::vector<U> &)> f,
                         const jkf::matrix<U> &PARS, const U &y0, const U &a,
                         const U &b, const U &dx, const U &dy) {
  const int N_jkf = PARS.cols(); /*!< number of jkf */
  jkf::matrix<U> R(1, N_jkf);
  for (int i = 0; i < N_jkf; ++i) {
    const std::function<U(const U &)> f_i = [&](const U &x) {
      return f(x, PARS.col(i));
    };
    R(0, i) = Functions::find_root(f_i, y0, a, b, dx, dy);
  }
  return R;
} // find root()

/*! ------- */
/*! Lattice */
/*! ------- */

template <class U>
jkf::matrix<U> Gaussian_jkf(const int &N_jkf, const std::vector<U> &A,
                            const std::vector<U> &eA, const int &seed) {

  assert(N_jkf > 1 && " Cannot generate jackknifes from mean and error.");
  std::random_device
      rd; // Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd()); // Standard mersenne_twister_engine seeded with rd()
  gen.seed(seed);

  int T = A.size();
  assert(T == (int)eA.size() &&
         " Error. The number of means must equal the number of errors.");
  jkf::matrix<U> A_jkf(T, N_jkf);
  for (int t = 0; t < T; ++t) {
    /* The error is reproduced by a gaussian with sigma =
       err_Z*(sqrt(N_jkf)/(N_jkf-1)) */

    std::normal_distribution<U> dis{
        A[t], eA[t] * (sqrt(N_jkf + 0.0) / (N_jkf - 1.0))};

    std::vector<U> vec_A_t(N_jkf);
    for (int k = 0; k < N_jkf; ++k) {
      vec_A_t[k] = dis(gen);
    }
    A_jkf.set_row(vec_A_t, t);
  }

  return A_jkf;
} // Gaussian_jkf()

template <class U>
jkf::matrix<U> jkf_matrix_from_gauge_skip(
    const std::string &path, const std::string &diagram, const int &N_gauge,
    const int &step, const int &N_start, const int &nchars, const int &N_jkf,
    const int &ReIm, const int &ign_lines, const std::string &head) {

  std::vector<std::string> full_paths(
      0); /*!< list of full path of configurations */

  int n_conf = 0;  /*!< number of found configurations */
  int n_dirs = 0;  /*!< directories opened */
  int n_fails = 0; /*!< number of not-foiund directories */

  while (n_dirs < N_gauge) {
    std::string name = boost::lexical_cast<std::string>(N_start + n_conf);
    while ((int)name.size() < nchars) {
      name = boost::lexical_cast<std::string>("0") + name;
    }

    std::string single_full_path = path + name + "/" + diagram;
    std::ifstream IN(single_full_path);
    if (!IN.fail()) {
      full_paths.push_back(single_full_path);
      ++n_dirs;
      n_fails--;
    } else {
      ++n_fails;
    }
    IN.close();

    n_conf += step;
    if (n_fails == N_gauge) {
      std::cerr << "Too much failures: check the input parameters of this "
                   "function.\n";
      std::cerr << "The program stopped at :\n" << single_full_path << "\n";
      assert(false);
    }
  }

  int n_rows = read_matrix<U>(full_paths[0], head, ign_lines).rows();
  jkf::matrix<U> gauge_corr(n_rows, N_gauge);
  for (int g = 0; g < N_gauge; ++g) {
    jkf::matrix<U> corr = read_matrix<U>(full_paths[g], head, ign_lines);
    gauge_corr.set_col(corr.col(ReIm), g);
  }

  return gauge_to_jkf(gauge_corr, N_jkf);
} // jkf_matrix_from_gauge_skip()

template <class U>
std::pair<jkf::matrix<U>, jkf::matrix<U>> jkf_matrix_from_gauge_skip_ReIm(
    const std::string &path, const std::string &diagram, const int &N_gauge,
    const int &step, const int &N_start, const int &nchars, const int &N_jkf,
    const int &ign_lines, const std::string &head) {
  jkf::matrix<U> m_Re = jkf_matrix_from_gauge_skip<U>(
      path, diagram, N_gauge, step, N_start, nchars, N_jkf, 0, ign_lines, head);
  jkf::matrix<U> m_Im = jkf_matrix_from_gauge_skip<U>(
      path, diagram, N_gauge, step, N_start, nchars, N_jkf, 1, ign_lines, head);
  return std::make_pair(m_Re, m_Im);
} // jkf_matrix_from_gauge_skip_ReIm()

template <class U>
jkf::matrix<U> gauge_to_jkf(const jkf::matrix<U> &m_data, const int &N_jkf,
                            const bool &lie_col) {
  jkf::matrix<U> m0 = m_data;
  if (lie_col == false) {
    m0.transposeInPlace();
  }

  int n = m0.rows();
  jkf::matrix<U> m_jkf(n, N_jkf);

  for (int t = 0; t < n; t++) {
    std::vector<U> jkf_sample = make_jkf(m0.row(t), N_jkf);
    m_jkf.set_row(jkf_sample, t);
  }

  if (lie_col == false) {
    m_jkf.transposeInPlace();
  }
  return m_jkf;
} // gauge_to_jkf()

template <class U>
jkf::matrix<U> time_symm_cols(const jkf::matrix<U> &m, const int &p) {
  int T = m.rows();
  int C = m.cols();
  int T_symm =
      T / 2 + T % 2; /* temporal extension of the symmetrized correlator */

  jkf::matrix<U> m_symm(T_symm, C);
  std::vector<U> v_row;
  for (int c = 0; c < C; ++c) {
    v_row = m.col(c);
    for (int t = 0; t < T_symm; t++) {
      m_symm(t, c) = (v_row[t] + p * v_row[T - t - 1]) / 2.0;
    }
  }

  return m_symm;
} // time_symm_cols()

template <class U>
jkf::matrix<U> trivial_jkf(const std::vector<U> &v, const int &N_jkf) {
  const int T = v.size();
  jkf::matrix<U> m(T, N_jkf);
  for (int j = 0; j < N_jkf; ++j) {
    m.set_col(v, j);
  }
  return m;
} // trivial jkf()

template <class U>
jkf::matrix<U> constant_jkf(const std::vector<U> &v, const int &T_ext) {
  const int N_jkf = v.size();
  jkf::matrix<U> m(T_ext, N_jkf);
  for (int t = 0; t < T_ext; ++t) {
    m.set_row(v, t);
  }
  return m;
} // constant jkf()

template <class U>
jkf::matrix<U> trivial_constant_jkf(const U &x, const int &T_ext,
                                    const int &N_jkf) {
  const std::vector<U> v(N_jkf, x); /*!< vector of (trivial) jkf */
  return constant_jkf(v, T_ext);
} // trivial_constant jkf()

template <class U>
jkf::matrix<U> backward_der(const unsigned int &n, const jkf::matrix<U> &M) {
  int R = M.rows(), C = M.cols();
  jkf::matrix<U> D(R, C);
  for (int i = 0; i < C; ++i) {
    D.set_col(i, Functions::backward_der(n, M.col(i)));
  }
  return D;
} // backward_der()

template <class U>
jkf::matrix<U> forward_der(const unsigned int &n, const jkf::matrix<U> &M) {
  int R = M.rows(), C = M.cols();
  jkf::matrix<U> D(R, C);
  for (int i = 0; i < C; ++i) {
    D.set_col(i, Functions::forward_der(n, M.col(i)));
  }
  return D;
} // forward_der()

template <class U>
jkf::matrix<U> symm_der(const unsigned int &n, const jkf::matrix<U> &M) {
  int R = M.rows(), C = M.cols();
  jkf::matrix<U> D(R, C);
  for (int i = 0; i < C; ++i) {
    D.set_col(i, Functions::symm_der(n, M.col(i)));
  }
  return D;
} // symm_der()

template <class U>
jkf::matrix<U> even_der_2n(const unsigned int &n, const jkf::matrix<U> &M) {
  int T_ext = M.rows(), N_jkf = M.cols();
  jkf::matrix<U> D(T_ext, N_jkf);
  for (int j = 0; j < N_jkf; ++j) {
    D.set_col(j, Functions::even_der_2n(n, M.col(j)));
  }
  return D;
} // even_der_2n()

/* ------- */
/* Physics */
/* ------- */

template <class U> jkf::matrix<U> GeV_to_MeV(const jkf::matrix<U> &M) {
  const std::function<U(const U &)> f = [](const U &x) {
    return Functions::GeV_to_MeV(x);
  };
  return operation_cwise(M, f);
}

template <class U> jkf::matrix<U> GeV_inv_to_MeV_inv(const jkf::matrix<U> &M) {
  const std::function<U(const U &)> f = [](const U &x) {
    return Functions::GeV_inv_to_MeV_inv(x);
  };
  return operation_cwise(M, f);
}

template <class U> jkf::matrix<U> MeV_to_GeV(const jkf::matrix<U> &M) {
  const std::function<U(const U &)> f = [](const U &x) {
    return Functions::MeV_to_GeV(x);
  };
  return operation_cwise(M, f);
}

template <class U> jkf::matrix<U> MeV_inv_to_GeV_inv(const jkf::matrix<U> &M) {
  const std::function<U(const U &)> f = [](const U &x) {
    return Functions::MeV_inv_to_GeV_inv(x);
  };
  return operation_cwise(M, f);
}

template <class U> jkf::matrix<U> fm_to_GeV_inv(const jkf::matrix<U> &M) {
  const std::function<U(const U &)> f = [](const U &x) {
    return Functions::fm_to_GeV_inv(x);
  };
  return operation_cwise(M, f);
}

template <class U> jkf::matrix<U> GeV_inv_to_fm(const jkf::matrix<U> &M) {
  const std::function<U(const U &)> f = [](const U &x) {
    return Functions::GeV_inv_to_fm(x);
  };
  return operation_cwise(M, f);
}

template <class U> jkf::matrix<U> fm_to_MeV_inv(const jkf::matrix<U> &M) {
  const std::function<U(const U &)> f = [](const U &x) {
    return Functions::fm_to_MeV_inv(x);
  };
  return operation_cwise(M, f);
}

template <class U> jkf::matrix<U> MeV_inv_to_fm(const jkf::matrix<U> &M) {
  const std::function<U(const U &)> f = [](const U &x) {
    return Functions::MeV_inv_to_fm(x);
  };
  return operation_cwise(M, f);
}

#endif
} // namespace jkf
} // namespace Silibs2
