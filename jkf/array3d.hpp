// array3d.hpp
/*! Definitions for array3d.h */

#include "./array3d.h"

namespace Silibs2 {
namespace jkf {
#ifndef Silibs2_jkf_array3d_hpp
#define Silibs2_jkf_array3d_hpp

template <class U> array3d<U>::array3d() {}
template <class U> array3d<U>::~array3d() {}

template <class U>
array3d<U>::array3d(const int &a, const int &b, const int &c, const U &val) {
  this->resize(a, b, c);
}

template <class U>
void array3d<U>::resize(const int &a, const int &b, const int &c,
                        const U &val) {
  D1 = a;
  D2 = b;
  D3 = c;
  ARR.resize(D1);
  for (int i = 0; i < D1; ++i) {
    ARR[i].resize(D2, D3);
  }
}

template <class U> std::array<int, 3> array3d<U>::shape() const {
  return {D1, D2, D3};
}

template <class U>
void array3d<U>::check_access(const int &i, const int &j, const int &k) const {
  assert(0 <= i && i < D1 && "Invalid 1st index");
  assert(0 <= j && j < D2 && "Invalid 2nd index");
  assert(0 <= k && k < D3 && "Invalid 3rd index");
}

template <class U> jkf::matrix<U> &array3d<U>::operator[](const int &i) {
  return ARR[i];
}

template <class U>
const jkf::matrix<U> &array3d<U>::operator[](const int &i) const {
  return ARR[i];
}

template <class U>
U &array3d<U>::operator()(const int &i, const int &j, const int &k) {
  return ARR[i](j, k); // called as ARR(i,j,k)
}

template <class U>
const U &array3d<U>::operator()(const int &i, const int &j,
                                const int &k) const {
  return ARR[i](j, k); // called as ARR(i,j,k)
}

template <class U> Tables::table<U> array3d<U>::avrs() const {
  Tables::table<U> t(D1, D2);
  for (int i = 0; i < D1; ++i) {
    const std::vector<U> avrs_i = ARR[i].avrs();
    for (int j = 0; j < D2; ++j) {
      t(i, j) = avrs_i[j];
    }
  }
  return t;
} // avrs()

template <class U> Tables::table<U> array3d<U>::errs() const {
  Tables::table<U> t(D1, D2);
  for (int i = 0; i < D1; ++i) {
    const std::vector<U> errs_i = ARR[i].errs();
    for (int j = 0; j < D2; ++j) {
      t(i, j) = errs_i[j];
    }
  }
  return t;
} // errs()

template <class U>
void array3d<U>::print_avrs(const std::string &file, const std::string &sep,
                            const int &prec, const std::string &header) const {
  (this->avrs()).print(file, sep, prec, header);
}

template <class U> jkf::matrix<U> array3d<U>::slice_0(const int &i0) {
  jkf::matrix<U> M(D2, D3);
  for (int j = 0; j < D2; ++j) {
    for (int k = 0; k < D3; ++k) {
      M(j, k) = (*this)(i0, j, k);
    }
  }
  return M;
} // slice_0()

template <class U> jkf::matrix<U> array3d<U>::slice_1(const int &j0) {
  jkf::matrix<U> M(D1, D3);
  for (int i = 0; i < D1; ++i) {
    for (int k = 0; k < D3; ++k) {
      M(i, k) = (*this)(i, j0, k);
    }
  }
  return M;
} // slice_1()

template <class U> jkf::matrix<U> array3d<U>::slice_2(const int &k0) {
  jkf::matrix<U> M(D1, D2);
  for (int i = 0; i < D1; ++i) {
    for (int j = 0; j < D2; ++j) {
      M(i, j) = (*this)(i, j, k0);
    }
  }
  return M;
} // slice_2()

#endif
} // namespace jkf
} // namespace Silibs2
