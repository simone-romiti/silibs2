// matrix.h
/*! This header declares the class jkf::matrix, used for Linear Algebra */

#include "../Tables/table.h"

namespace Silibs2 {
namespace jkf {
#ifndef Silibs2_jkf_matrix_h
#define Silibs2_jkf_matrix_h

template <class U = double> class matrix : public Tables::table<U> {

public:
  matrix();
  ~matrix();

  /*! Copy contructor */
  matrix<U>(const Tables::table<U> &t);
  /*! Copy contructor */
  matrix<U>(const matrix<U> &matrix);

  /* Create a jkf::matrix of size ro*co with all elements equal to val */
  matrix<U>(const int &ro, const int &co);
  matrix<U>(const int &ro, const int &co, const U &val);

  /* Create a jkf::matrix reading its elements from the file in 'path' */
  matrix<U>(const std::string &path, const std::string &head = "#jkf",
            const std::string &sep = "\t", const int &ign_lines = 0);

  /* Create a jkf::matrix from a law f(i,j) */
  matrix<U>(const int &ro, const int &co,
            const std::function<U(const int &, const int &)> &fun);

  /*! jackknife averages */
  std::vector<U> avrs() const;

  /*! jackknife errors */
  std::vector<U> errs() const;

  /*! average and error jackknife */
  matrix<U> ae() const;

  /*!
  Printing the jkf::matrix
  with it averages and errors
  and a header 'h' (if not empty)
  */
  void print_jkf_ae(const std::string &path, const std::string &sep = "\t",
                    const int &prec = 16, const std::string &h = "",
                    const int &nd = 2) const;

  /*!
  average and error using the smart printer.
  Example:
  1.234(3) means 1,234 +/- 0.003
  nd is the number of significant digits to keep inside the parentheses
  */
  matrix<std::string> scientific_digits(const int &nd = 2) const;

  /*! Printing average and error using scientific_digits */
  void smart_print_ae(const std::string &path, const int &nd = 2) const;

  /*! Multiplication by a scalar from the right */
  matrix<U> operator*(const U &lambda) const;
  void operator*=(const U &lambda);
  /*! Division by a scalar */
  matrix<U> operator/(const U &lambda) const;
  void operator/=(const U &lambda);
  /*! Addition and subtraction*/
  void operator+=(const matrix<U> &m);
  void operator-=(const matrix<U> &m);
};

/* --------- */
/* functions */
/* --------- */

/*!  -------------------- */
/*! READING and PRINTING */
/*!  -------------------- */

template <class U = double>
void print_matrices(const std::string &path,
                    const std::vector<jkf::matrix<U>> &M,
                    const std::vector<std::string> &header = "",
                    const std::string &sep = "\t", const int &prec = 16);

/*!
Extract from the file in 'path' the matrix of values found after the line
equal to the header 'head' (head="" --> the file is read from the beginning ).
It stops at the first empty line of when EOF is reached
ign_lines = number of lines ignored after the line of the 'head'
sep = separation between columns
*/
template <class U = double>
jkf::matrix<U>
read_matrix_sep(const std::string &path, const std::string &head = "#jkf",
                const int &ign_lines = 0, const std::string &sep = "\t");

/*!
like read_matrix_sep(),
but ignoring multiple occurencies of tabs and spaces
*/
template <class U = double>
jkf::matrix<U> read_matrix(const std::string &path,
                           const std::string &head = "",
                           const int &ign_lines = 0);

/*! read matrix with header #jkf or #jkf#h if 'h' is not empty */
template <class U = double>
jkf::matrix<U> read_jkf(const std::string &path, const std::string &h = "");

/*! read matrix with header #avr_err */
template <class U = double>
jkf::matrix<U> read_avr_err(const std::string &path);

/*! uses read_matrix to extract the 'n'-th row*/
template <class U = double>
std::vector<U> read_row(const int &n, const std::string &path,
                        const std::string &head = "#jkf",
                        const int &ign_lines = 0);

/*! uses read_matrix to extract the 'n'-th column */
template <class U = double>
std::vector<U> read_col(const int &n, const std::string &path,
                        const std::string &head = "#jkf",
                        const int &ign_lines = 0);

/*!  ---------------------------- */
/*!  OPERATIONS on the components */
/*!  ---------------------------- */

template <class U, class V>
jkf::matrix<U> cast_matrix(const jkf::matrix<V> &matr);
template <class M_type1, class M_type2>
M_type1 convert_matrix(const M_type2 &matr);

/*!  ------------------------------ */
/*!  BUILDING from rows and columns */
/*!  ------------------------------ */

template <class U = double>
jkf::matrix<U> matrix_from_rows(const std::vector<std::vector<U>> &rows);
template <class U = double>
jkf::matrix<U> matrix_from_cols(const std::vector<std::vector<U>> &cols);

/*! Print the content of a vector x and a jkf::matrix Y in the format xyey */
template <class U = double>
jkf::matrix<U> build_xyey(const std::vector<U> &x, const jkf::matrix<U> &Y);

/*!
Print the content of a vector x and a jkf::matrix Y
in the format x_y_ey_yth_eyth
*/
template <class U = double>
jkf::matrix<U> build_x_y_ey_yth_eyth(const std::vector<U> &x,
                                     const jkf::matrix<U> &Y,
                                     const jkf::matrix<U> &Y_th);

/*! Print the content of 2 matrices X,Y in the format xexyey */
template <class U = double>
jkf::matrix<U> build_xexyey(const jkf::matrix<U> &X, const jkf::matrix<U> &Y);

/*!
Print the content of 3 matrices X,Y,Y_th
in the format build_x_ex_y_ey_yth_eyth
*/
template <class U = double>
jkf::matrix<U> build_x_ex_y_ey_yth_eyth(const jkf::matrix<U> &X,
                                        const jkf::matrix<U> &Y,
                                        const jkf::matrix<U> &Y_th);

/*!
returns the columns:
x1 x2 ... xN y ey
*/
template <class U = double>
jkf::matrix<U> build_xiyey(const Tables::table<U> &Xi, const jkf::matrix<U> &Y);

/*!  ------- */
/*!  Algebra */
/*!  ------- */

/*! linear combination a*m1 + b*m2 */
template <class U = double>
jkf::matrix<U> lin_comb(const jkf::matrix<U> &m1, const jkf::matrix<U> &m2,
                        const U &a, const U &b);

/*! pair of linear combinations: a*m1<i> + b*m2<i> for i=0,1 */
template <class U = double>
std::pair<jkf::matrix<U>, jkf::matrix<U>>
lin_comb(const std::pair<jkf::matrix<U>, jkf::matrix<U>> &m1,
         const std::pair<jkf::matrix<U>, jkf::matrix<U>> &m2, const U &a,
         const U &b);

template <class U = double>
jkf::matrix<U> operator+(const jkf::matrix<U> &m1, const jkf::matrix<U> &m2);
template <class U = double>
jkf::matrix<U> operator-(const jkf::matrix<U> &m1, const jkf::matrix<U> &m2);

/*! multiplication by a scalar from the left */
template <class U = double>
jkf::matrix<U> operator*(const U &lambda, const jkf::matrix<U> &m);

/*! tensor product of 2 std::vectors : M_ij = v_i * w_j */
template <class U = double>
jkf::matrix<U> tensor_prod(const std::vector<U> &v, const std::vector<U> &w);

/*! ------------------------- */
/*! Component-wise operations */
/*! ------------------------- */

/*! Returns a matrix M whose elements are : M_ij = f(A_ij) */
template <class U = double>
jkf::matrix<U> operation_cwise(const jkf::matrix<U> &A,
                               const std::function<U(const U &)> &f);

/*! Returns a matrix M whose elements are : M_ij = pow(A_ij, a) */
template <class U1, class U2>
jkf::matrix<U1> pow_cwise(const jkf::matrix<U1> &A, const U2 &a);

/*! Returns a matrix M whose elements are : M_ij = pow(A_ij, 2) */
template <class U = double>
jkf::matrix<U> square_cwise(const jkf::matrix<U> &A);

/*! Returns a matrix M whose elements are : M_ij = sqrt(A_ij) */
template <class U = double> jkf::matrix<U> sqrt_cwise(const jkf::matrix<U> &A);

/*! Returns a matrix M whose elements are : M_ij = f(A_ij, B_ij) */
template <class U = double>
jkf::matrix<U> operation_cwise(const jkf::matrix<U> &A, const jkf::matrix<U> &B,
                               const std::function<U(const U &, const U &)> &f);

/*! Returns a matrix M whose elements are : M_ij = A_ij * B_ij */
template <class U = double>
jkf::matrix<U> mult_cwise(const jkf::matrix<U> &A, const jkf::matrix<U> &B);

/*! alias for mult_cwise */
template <class U = double>
jkf::matrix<U> operator*(const jkf::matrix<U> &m1, const jkf::matrix<U> &m2);

/*
Returns a matrix M whose elements are : M_ij = A_ij/B_ij
ign_boundary=true --> if (note, if) at t=0 and/or t=T_ext the denominator is 0,
the result is set to zero
*/
template <class U = double>
jkf::matrix<U> ratio_cwise(const jkf::matrix<U> &A, const jkf::matrix<U> &B,
                           const bool &ign_boundary = true);

/*! Returns ratio_cwise(A,B,true); */
template <class U = double>
jkf::matrix<U> operator/(const jkf::matrix<U> &A, const jkf::matrix<U> &B);

/*! Returns a matrix whose elements are the averages of the elements of the
 * matrices in the std::vector */
template <class U = double>
jkf::matrix<U> average_matrices(const std::vector<jkf::matrix<U>> &vec_M);

/* ---- */
/* Math */
/* ---- */

/*!
Silibs2::functions::find_root() for each jkf of y0
In this case, the std::function takes a jkf::matrix of parameters.
*/
template <class U>
jkf::matrix<U> find_root(std::function<U(const U &, const std::vector<U> &)> f,
                         const jkf::matrix<U> &PARS, const U &y0, const U &a,
                         const U &b, const U &dx, const U &dy);

/*! ------- */
/*! Lattice */
/*! ------- */

/*!
generates Gaussian jackknifes from averages 'A' and errors 'eA'
the defauls seed is a randomly chosen number
*/
template <class U = double>
jkf::matrix<U> Gaussian_jkf(const int &N_jkf, const std::vector<U> &A,
                            const std::vector<U> &eA, const int &seed = 92387);

/*
returns the matrix of jackknifes [T][N_jkf]
ReIm = 0 --> takes the real part (1st column of file)
ReIm = 1 --> takes the imaginary part (2nd column of file)
If it doesn't find the directory of a configuration, it skips it and pass to the
next, but if the skips equal N_gauge, then it aborts.
*/
template <class U = double>
jkf::matrix<U> jkf_matrix_from_gauge_skip(const std::string &path,
                                          const std::string &diagram,
                                          const int &N_gauge, const int &step,
                                          const int &N_start, const int &nchars,
                                          const int &N_jkf, const int &ReIm = 0,
                                          const int &ign_lines = 0,
                                          const std::string &head = "");

/*! Real and imaginary part read with jkf_from_gauge_skip */
template <class U = double>
std::pair<jkf::matrix<U>, jkf::matrix<U>> jkf_matrix_from_gauge_skip_ReIm(
    const std::string &path, const std::string &diagram, const int &N_gauge,
    const int &step, const int &N_start, const int &nchars, const int &N_jkf,
    const int &ign_lines = 0, const std::string &head = "");

/*! returns the matrix of jackknifes from the one of gauge configurations
row = true --> gauge confs and jackknifes lie on the row indexes
respectively of the starting and returned matrix.
lie_col = true --> they lie on the column indexes
*/
template <class U = double>
jkf::matrix<U> gauge_to_jkf(const jkf::matrix<U> &m_data, const int &N_jkf,
                            const bool &lie_col = true);

/*! For each column, impose time symmetry on the row values */
template <class U = double>
jkf::matrix<U> time_symm_cols(const jkf::matrix<U> &m, const int &p);

// /*! From the matrix of jackknifes of some quantity builds a matrix of average
//  * and error for each time */
// template <class U = double> jkf::matrix<U> avr_err(const jkf::matrix<U> &m);

/*!
trivial jackknifes, i.e. 0 error :
this functions returns a matrix with N_jkf columns all equal to 'v'
*/
template <class U = double>
jkf::matrix<U> trivial_jkf(const std::vector<U> &v, const int &N_jkf);

/*!
T_ext (constant) jackknifes,
i.e. the same for vector 'v' of jkf for each row
*/
template <class U = double>
jkf::matrix<U> constant_jkf(const std::vector<U> &v, const int &T_ext);

/*! trivial and constant jkf from a single value */
template <class U = double>
jkf::matrix<U> trivial_constant_jkf(const U &x, const int &T_ext,
                                    const int &N_jkf);

/*! backward derivative */
template <class U = double>
jkf::matrix<U> backward_der(const unsigned int &n, const jkf::matrix<U> &M);

/*! forward derivative */
template <class U = double>
jkf::matrix<U> forward_der(const unsigned int &n, const jkf::matrix<U> &M);

/*! symmetric derivative */
template <class U = double>
jkf::matrix<U> symm_der(const unsigned int &n, const jkf::matrix<U> &M);

/*! Derivative of degree 2*n */
template <class U = double>
jkf::matrix<U> even_der_2n(const unsigned int &n, const jkf::matrix<U> &M);

/* ------- */
/* Physics */
/* ------- */
template <class U = double> jkf::matrix<U> GeV_to_MeV(const jkf::matrix<U> &M);

template <class U = double>
jkf::matrix<U> GeV_inv_to_MeV_inv(const jkf::matrix<U> &M);

template <class U = double> jkf::matrix<U> MeV_to_GeV(const jkf::matrix<U> &M);

template <class U = double>
jkf::matrix<U> MeV_inv_to_GeV_inv(const jkf::matrix<U> &M);

template <class U = double>
jkf::matrix<U> fm_to_GeV_inv(const jkf::matrix<U> &M);

template <class U = double>
jkf::matrix<U> GeV_inv_to_fm(const jkf::matrix<U> &M);

template <class U = double>
jkf::matrix<U> fm_to_MeV_inv(const jkf::matrix<U> &M);

template <class U = double>
jkf::matrix<U> MeV_inv_to_fm(const jkf::matrix<U> &M);

#endif
} // namespace jkf
} // namespace Silibs2
