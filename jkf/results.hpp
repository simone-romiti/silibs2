// results.hpp
/*! Definitions of results.h */

#include <string>
#include <vector>

#include "./results.h"

namespace Silibs2 {
namespace jkf {
#ifndef Silibs2_jkf_results_hpp
#define Silibs2_jkf_results_hpp

template <class U> results<U>::results() {
  M.resize(0);
  H.resize(0);
}

template <class U> results<U>::~results() {}

template <class U> void results<U>::set_title(const std::string &t) {
  TITLE = t;
} // set_title()

template <class U> void results<U>::set_subtitle(const std::string &s) {
  SUBTITLE = s;
} // set_title()

template <class U>
void results<U>::add(const Tables::table<U> &m, const std::string &h) {
  M.push_back(m);
  H.push_back(h);
} // add()

template <class U>
void results<U>::add_str(const Tables::table<std::string> &m,
                         const std::string &h) {
  M_str.push_back(m);
  H_str.push_back(h);
} // add()

template <class U>
void results<U>::add_jkf(const jkf::matrix<U> &m, const std::string &h) {
  M.push_back(m);
  M.push_back(m.ae());
  H.push_back("#jkf#" + h);
  H.push_back("#ae#" + h);
} // add_jkf()

template <class U>
void results<U>::add_jkf_rows(const jkf::matrix<U> &m,
                              const std::vector<std::string> &h) {

  int nR = m.rows();
  if (nR != (int)h.size()) {
    std::cerr << "Error:\n";
    std::cerr << "Number of headers : " << h.size() << "\n";
    std::cerr << "Rows of the matrix : " << nR << "\n";
    std::cerr << "Aborting\n";
    abort();
  }

  int N_jkf = m.cols(); /*!< number of jackknifes */
  for (int i = 0; i < nR; ++i) {
    jkf::matrix<U> m_i = m.block(i, 0, 1, N_jkf); /*!< i.th row */
    M.push_back(m_i);                             /*!< jkf */
    M.push_back(m_i.ae());                        /*!< ae() */
    H.push_back("#jkf#" + h[i]);
    H.push_back("#ae#" + h[i]);
    /* code */
  }
} // add_jkf_rows()

template <class U>
void results<U>::print_summary(const std::string &dest, const std::string &sep,
                               const int &prec) const {
  /*! clean up the content of the output file */
  Functions::clear_content(dest);
  Functions::touch(dest);

  std::string header = "";
  if (TITLE != "") { /*!< adding title if specified */
    header = "*** " + TITLE + " ***\n" + header;
  }
  std::string subheader = "\n";
  /*! adding subtitle if specified */
  if (SUBTITLE != "") {
    subheader = "----------\n" + SUBTITLE + "\n----------\n" + subheader;
  }
  Functions::append_string(header, dest);    /*!< printing title */
  Functions::append_string(subheader, dest); /*!< printing subtitle */

  int n = M.size(); /*!< number of matrices to be printed out */
  for (int i = 0; i < n; ++i) {
    M[i].append(dest, sep, H[i], prec);
    Functions::append_string("\n", dest);
  }

  int n_str = M_str.size(); /*!< number of matrices to be printed out */
  for (int i = 0; i < n_str; ++i) {
    M_str[i].append(dest, sep, H_str[i], prec);
    Functions::append_string("\n", dest);
  }
} // print_summary()

#endif
} // namespace jkf
} // namespace Silibs2
