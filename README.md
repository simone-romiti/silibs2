# Silibs2

This repository contains a set of C++ and Python libraries.
The Python ones are placed under the **Python/** subfolder.

## Structure of the repository

* cpp_scripts/
  Scripts for compiling and linking using this library

* Functions/
  User-defined functions of various kinds

* jkf/
  jackknifes

* LA/
  Linear Algebra

* Lattice/
  Analysis of lattice correlators

* Minuit2_fit/
  Fitting with Minuit2

* Python/
 
 Python modules (mostly plotting utilities)
  
* RC/
  Tables with rows and columns names
  
* scripts/
  Run python scripts from c++
 

* Tables/
  Generic table object (base class for RC::table, LA::matrix, jkf::matrix)

* test/
  random tests



## Main differences with respect to Silibs

1. **Upgrade to Minuit2**. The older version of this repository used the first version of the Minuit minimization library (the one written in Fortran). I used it installing the whole ROOT framework.
This version is thought to be used with a Minuit2 standalone installation.

2. **Better handling of jackknifes routines**. Here in Silibs2 jackknife methods are defined in separated classes and namespaces, with a simpler approach to their input and output.

## Notes

* Study the getdist-gui package -> gui for generating plots with matplotlib.
[https://getdist.readthedocs.io/en/latest/intro.html](https://getdist.readthedocs.io/en/latest/intro.html)

* Check out the Numba package (open source jit compiler): accelerating python compiling frequently used functions. 
  [http://numba.pydata.org/](http://numba.pydata.org/)