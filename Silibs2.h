// Silibs2.h
/*! Include this header if you want to include all Silibs2 libraries */

#ifndef Silibs2_hpp
#define Silibs2_hpp

#include "./Functions/functions.h" /*!< generic tables */

#include "./Minuit2_fit/fit.h" /*!< Fit with Minuit2 */

#include "./Tables/table.h" /*!< generic tables */

#include "./RC/table.h" /*!< generic tables */

#include "./LA/matrix.h" /*!< matrices for linear algebra */
#include "./jkf/jkf.h"   /*!< matrices for jacknifes */

#include "./Lattice/lattice.h" /*!< lattice correlators */

// #include "./scripts/python.h" /*!< python scripts */

namespace Silibs2 {}
namespace SILIBS2 = Silibs2;
namespace silibs2 = Silibs2;
#endif
